
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DeleteSuperGroupRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DeleteSuperGroupRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SuperGroupAccess">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SuperGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SuperGroupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeleteSuperGroupRequest", propOrder = {
    "superGroupAccess"
})
public class DeleteSuperGroupRequest {

    @XmlElement(name = "SuperGroupAccess", required = true)
    protected DeleteSuperGroupRequest.SuperGroupAccess superGroupAccess;

    /**
     * Obtiene el valor de la propiedad superGroupAccess.
     * 
     * @return
     *     possible object is
     *     {@link DeleteSuperGroupRequest.SuperGroupAccess }
     *     
     */
    public DeleteSuperGroupRequest.SuperGroupAccess getSuperGroupAccess() {
        return superGroupAccess;
    }

    /**
     * Define el valor de la propiedad superGroupAccess.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteSuperGroupRequest.SuperGroupAccess }
     *     
     */
    public void setSuperGroupAccess(DeleteSuperGroupRequest.SuperGroupAccess value) {
        this.superGroupAccess = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SuperGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SuperGroupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "superGroupKey",
        "superGroupCode"
    })
    public static class SuperGroupAccess {

        @XmlElement(name = "SuperGroupKey")
        protected String superGroupKey;
        @XmlElement(name = "SuperGroupCode")
        protected String superGroupCode;

        /**
         * Obtiene el valor de la propiedad superGroupKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuperGroupKey() {
            return superGroupKey;
        }

        /**
         * Define el valor de la propiedad superGroupKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuperGroupKey(String value) {
            this.superGroupKey = value;
        }

        /**
         * Obtiene el valor de la propiedad superGroupCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuperGroupCode() {
            return superGroupCode;
        }

        /**
         * Define el valor de la propiedad superGroupCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuperGroupCode(String value) {
            this.superGroupCode = value;
        }

    }

}
