
package com.huawei.bme.cbsinterface.bccommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CustInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CustInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CustNodeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CustClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CustCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ParentCustKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CustBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustBasicInfo" minOccurs="0" form="qualified"/>
 *         &lt;element name="NoticeSuppress" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustInfo", propOrder = {
    "custType",
    "custNodeType",
    "custClass",
    "custCode",
    "parentCustKey",
    "custBasicInfo",
    "noticeSuppress"
})
public class CustInfo {

    @XmlElement(name = "CustType")
    protected String custType;
    @XmlElement(name = "CustNodeType")
    protected String custNodeType;
    @XmlElement(name = "CustClass")
    protected String custClass;
    @XmlElement(name = "CustCode")
    protected String custCode;
    @XmlElement(name = "ParentCustKey")
    protected String parentCustKey;
    @XmlElement(name = "CustBasicInfo")
    protected CustBasicInfo custBasicInfo;
    @XmlElement(name = "NoticeSuppress")
    protected List<CustInfo.NoticeSuppress> noticeSuppress;

    /**
     * Obtiene el valor de la propiedad custType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustType() {
        return custType;
    }

    /**
     * Define el valor de la propiedad custType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustType(String value) {
        this.custType = value;
    }

    /**
     * Obtiene el valor de la propiedad custNodeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustNodeType() {
        return custNodeType;
    }

    /**
     * Define el valor de la propiedad custNodeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustNodeType(String value) {
        this.custNodeType = value;
    }

    /**
     * Obtiene el valor de la propiedad custClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustClass() {
        return custClass;
    }

    /**
     * Define el valor de la propiedad custClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustClass(String value) {
        this.custClass = value;
    }

    /**
     * Obtiene el valor de la propiedad custCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustCode() {
        return custCode;
    }

    /**
     * Define el valor de la propiedad custCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustCode(String value) {
        this.custCode = value;
    }

    /**
     * Obtiene el valor de la propiedad parentCustKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentCustKey() {
        return parentCustKey;
    }

    /**
     * Define el valor de la propiedad parentCustKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentCustKey(String value) {
        this.parentCustKey = value;
    }

    /**
     * Obtiene el valor de la propiedad custBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link CustBasicInfo }
     *     
     */
    public CustBasicInfo getCustBasicInfo() {
        return custBasicInfo;
    }

    /**
     * Define el valor de la propiedad custBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link CustBasicInfo }
     *     
     */
    public void setCustBasicInfo(CustBasicInfo value) {
        this.custBasicInfo = value;
    }

    /**
     * Gets the value of the noticeSuppress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the noticeSuppress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNoticeSuppress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustInfo.NoticeSuppress }
     * 
     * 
     */
    public List<CustInfo.NoticeSuppress> getNoticeSuppress() {
        if (noticeSuppress == null) {
            noticeSuppress = new ArrayList<CustInfo.NoticeSuppress>();
        }
        return this.noticeSuppress;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "channelType",
        "noticeType",
        "subNoticeType"
    })
    public static class NoticeSuppress {

        @XmlElement(name = "ChannelType", required = true)
        protected String channelType;
        @XmlElement(name = "NoticeType", required = true)
        protected String noticeType;
        @XmlElement(name = "SubNoticeType")
        protected String subNoticeType;

        /**
         * Obtiene el valor de la propiedad channelType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChannelType() {
            return channelType;
        }

        /**
         * Define el valor de la propiedad channelType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChannelType(String value) {
            this.channelType = value;
        }

        /**
         * Obtiene el valor de la propiedad noticeType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNoticeType() {
            return noticeType;
        }

        /**
         * Define el valor de la propiedad noticeType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNoticeType(String value) {
            this.noticeType = value;
        }

        /**
         * Obtiene el valor de la propiedad subNoticeType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubNoticeType() {
            return subNoticeType;
        }

        /**
         * Define el valor de la propiedad subNoticeType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubNoticeType(String value) {
            this.subNoticeType = value;
        }

    }

}
