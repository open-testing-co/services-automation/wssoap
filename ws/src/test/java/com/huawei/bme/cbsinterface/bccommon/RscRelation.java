
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RscRelation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RscRelation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RelationObjType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="RelationDestIdentify" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0" form="qualified"/>
 *                   &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0" form="qualified"/>
 *         &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ShareLimit" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RscRelation", propOrder = {
    "relationObjType",
    "relationDestIdentify",
    "offeringKey",
    "shareRule",
    "shareLimit",
    "startTime",
    "endTime"
})
public class RscRelation {

    @XmlElement(name = "RelationObjType", required = true)
    protected String relationObjType;
    @XmlElement(name = "RelationDestIdentify", required = true)
    protected RscRelation.RelationDestIdentify relationDestIdentify;
    @XmlElement(name = "OfferingKey")
    protected OfferingKey offeringKey;
    @XmlElement(name = "ShareRule")
    protected String shareRule;
    @XmlElement(name = "ShareLimit")
    protected RscRelation.ShareLimit shareLimit;
    @XmlElement(name = "StartTime")
    protected String startTime;
    @XmlElement(name = "EndTime")
    protected String endTime;

    /**
     * Obtiene el valor de la propiedad relationObjType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationObjType() {
        return relationObjType;
    }

    /**
     * Define el valor de la propiedad relationObjType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationObjType(String value) {
        this.relationObjType = value;
    }

    /**
     * Obtiene el valor de la propiedad relationDestIdentify.
     * 
     * @return
     *     possible object is
     *     {@link RscRelation.RelationDestIdentify }
     *     
     */
    public RscRelation.RelationDestIdentify getRelationDestIdentify() {
        return relationDestIdentify;
    }

    /**
     * Define el valor de la propiedad relationDestIdentify.
     * 
     * @param value
     *     allowed object is
     *     {@link RscRelation.RelationDestIdentify }
     *     
     */
    public void setRelationDestIdentify(RscRelation.RelationDestIdentify value) {
        this.relationDestIdentify = value;
    }

    /**
     * Obtiene el valor de la propiedad offeringKey.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getOfferingKey() {
        return offeringKey;
    }

    /**
     * Define el valor de la propiedad offeringKey.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setOfferingKey(OfferingKey value) {
        this.offeringKey = value;
    }

    /**
     * Obtiene el valor de la propiedad shareRule.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShareRule() {
        return shareRule;
    }

    /**
     * Define el valor de la propiedad shareRule.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShareRule(String value) {
        this.shareRule = value;
    }

    /**
     * Obtiene el valor de la propiedad shareLimit.
     * 
     * @return
     *     possible object is
     *     {@link RscRelation.ShareLimit }
     *     
     */
    public RscRelation.ShareLimit getShareLimit() {
        return shareLimit;
    }

    /**
     * Define el valor de la propiedad shareLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link RscRelation.ShareLimit }
     *     
     */
    public void setShareLimit(RscRelation.ShareLimit value) {
        this.shareLimit = value;
    }

    /**
     * Obtiene el valor de la propiedad startTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * Define el valor de la propiedad startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartTime(String value) {
        this.startTime = value;
    }

    /**
     * Obtiene el valor de la propiedad endTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * Define el valor de la propiedad endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndTime(String value) {
        this.endTime = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0" form="qualified"/>
     *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subIdentify",
        "subGroupAccessCode"
    })
    public static class RelationDestIdentify {

        @XmlElement(name = "SubIdentify")
        protected SubAccessCode subIdentify;
        @XmlElement(name = "SubGroupAccessCode")
        protected SubGroupAccessCode subGroupAccessCode;

        /**
         * Obtiene el valor de la propiedad subIdentify.
         * 
         * @return
         *     possible object is
         *     {@link SubAccessCode }
         *     
         */
        public SubAccessCode getSubIdentify() {
            return subIdentify;
        }

        /**
         * Define el valor de la propiedad subIdentify.
         * 
         * @param value
         *     allowed object is
         *     {@link SubAccessCode }
         *     
         */
        public void setSubIdentify(SubAccessCode value) {
            this.subIdentify = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public SubGroupAccessCode getSubGroupAccessCode() {
            return subGroupAccessCode;
        }

        /**
         * Define el valor de la propiedad subGroupAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public void setSubGroupAccessCode(SubGroupAccessCode value) {
            this.subGroupAccessCode = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitCycleType",
        "limitValue",
        "measureUnit"
    })
    public static class ShareLimit {

        @XmlElement(name = "LimitCycleType", required = true)
        protected String limitCycleType;
        @XmlElement(name = "LimitValue")
        protected long limitValue;
        @XmlElement(name = "MeasureUnit", required = true)
        protected BigInteger measureUnit;

        /**
         * Obtiene el valor de la propiedad limitCycleType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitCycleType() {
            return limitCycleType;
        }

        /**
         * Define el valor de la propiedad limitCycleType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitCycleType(String value) {
            this.limitCycleType = value;
        }

        /**
         * Obtiene el valor de la propiedad limitValue.
         * 
         */
        public long getLimitValue() {
            return limitValue;
        }

        /**
         * Define el valor de la propiedad limitValue.
         * 
         */
        public void setLimitValue(long value) {
            this.limitValue = value;
        }

        /**
         * Obtiene el valor de la propiedad measureUnit.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMeasureUnit() {
            return measureUnit;
        }

        /**
         * Define el valor de la propiedad measureUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMeasureUnit(BigInteger value) {
            this.measureUnit = value;
        }

    }

}
