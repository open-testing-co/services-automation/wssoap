
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryExpireSubToMicroRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryExpireSubToMicroRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TimePeriod">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PagingInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryExpireSubToMicroRequest", propOrder = {

})
public class QueryExpireSubToMicroRequest {

    @XmlElement(name = "TimePeriod", required = true)
    protected QueryExpireSubToMicroRequest.TimePeriod timePeriod;
    @XmlElement(name = "PagingInfo", required = true)
    protected QueryExpireSubToMicroRequest.PagingInfo pagingInfo;

    /**
     * Obtiene el valor de la propiedad timePeriod.
     * 
     * @return
     *     possible object is
     *     {@link QueryExpireSubToMicroRequest.TimePeriod }
     *     
     */
    public QueryExpireSubToMicroRequest.TimePeriod getTimePeriod() {
        return timePeriod;
    }

    /**
     * Define el valor de la propiedad timePeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryExpireSubToMicroRequest.TimePeriod }
     *     
     */
    public void setTimePeriod(QueryExpireSubToMicroRequest.TimePeriod value) {
        this.timePeriod = value;
    }

    /**
     * Obtiene el valor de la propiedad pagingInfo.
     * 
     * @return
     *     possible object is
     *     {@link QueryExpireSubToMicroRequest.PagingInfo }
     *     
     */
    public QueryExpireSubToMicroRequest.PagingInfo getPagingInfo() {
        return pagingInfo;
    }

    /**
     * Define el valor de la propiedad pagingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryExpireSubToMicroRequest.PagingInfo }
     *     
     */
    public void setPagingInfo(QueryExpireSubToMicroRequest.PagingInfo value) {
        this.pagingInfo = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class PagingInfo {

        @XmlElement(name = "TotalRowNum", required = true)
        protected BigInteger totalRowNum;
        @XmlElement(name = "BeginRowNum", required = true)
        protected BigInteger beginRowNum;
        @XmlElement(name = "FetchRowNum", required = true)
        protected BigInteger fetchRowNum;

        /**
         * Obtiene el valor de la propiedad totalRowNum.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotalRowNum() {
            return totalRowNum;
        }

        /**
         * Define el valor de la propiedad totalRowNum.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotalRowNum(BigInteger value) {
            this.totalRowNum = value;
        }

        /**
         * Obtiene el valor de la propiedad beginRowNum.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getBeginRowNum() {
            return beginRowNum;
        }

        /**
         * Define el valor de la propiedad beginRowNum.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setBeginRowNum(BigInteger value) {
            this.beginRowNum = value;
        }

        /**
         * Obtiene el valor de la propiedad fetchRowNum.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getFetchRowNum() {
            return fetchRowNum;
        }

        /**
         * Define el valor de la propiedad fetchRowNum.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setFetchRowNum(BigInteger value) {
            this.fetchRowNum = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class TimePeriod {

        @XmlElement(name = "StartTime", required = true)
        protected String startTime;
        @XmlElement(name = "EndTime", required = true)
        protected String endTime;

        /**
         * Obtiene el valor de la propiedad startTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStartTime() {
            return startTime;
        }

        /**
         * Define el valor de la propiedad startTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStartTime(String value) {
            this.startTime = value;
        }

        /**
         * Obtiene el valor de la propiedad endTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndTime() {
            return endTime;
        }

        /**
         * Define el valor de la propiedad endTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndTime(String value) {
            this.endTime = value;
        }

    }

}
