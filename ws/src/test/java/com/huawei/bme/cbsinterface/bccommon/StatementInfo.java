
package com.huawei.bme.cbsinterface.bccommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para StatementInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="StatementInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContactInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Contact" minOccurs="0" form="qualified"/>
 *         &lt;element name="SmtMedium" type="{http://www.huawei.com/bme/cbsinterface/bccommon}StatementMedium" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *         &lt;element name="SmtLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatementInfo", propOrder = {
    "contactInfo",
    "smtMedium",
    "smtLang"
})
public class StatementInfo {

    @XmlElement(name = "ContactInfo")
    protected Contact contactInfo;
    @XmlElement(name = "SmtMedium")
    protected List<StatementMedium> smtMedium;
    @XmlElement(name = "SmtLang")
    protected String smtLang;

    /**
     * Obtiene el valor de la propiedad contactInfo.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContactInfo() {
        return contactInfo;
    }

    /**
     * Define el valor de la propiedad contactInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContactInfo(Contact value) {
        this.contactInfo = value;
    }

    /**
     * Gets the value of the smtMedium property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the smtMedium property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSmtMedium().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatementMedium }
     * 
     * 
     */
    public List<StatementMedium> getSmtMedium() {
        if (smtMedium == null) {
            smtMedium = new ArrayList<StatementMedium>();
        }
        return this.smtMedium;
    }

    /**
     * Obtiene el valor de la propiedad smtLang.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmtLang() {
        return smtLang;
    }

    /**
     * Define el valor de la propiedad smtLang.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmtLang(String value) {
        this.smtLang = value;
    }

}
