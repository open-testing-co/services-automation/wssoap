
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;
import com.huawei.bme.cbsinterface.bccommon.InstProperty;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;


/**
 * <p>Clase Java para FeeQuotationRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FeeQuotationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChargeObj">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
 *                   &lt;element name="AcctAccessCode" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
 *                           &lt;sequence>
 *                             &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
 *                   &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ChargeElement">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FeeAmount" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ChargeAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="OfferingFee" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="ParentOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *                                       &lt;element name="OInstProperty" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
 *                                               &lt;sequence>
 *                                                 &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                 &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/extension>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="ProductInst" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="ProductID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                                 &lt;element name="PInstProperty" maxOccurs="unbounded" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                           &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/extension>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="OperationFee" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="OperationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="OperationProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeeQuotationRequest", propOrder = {
    "chargeObj",
    "chargeElement"
})
public class FeeQuotationRequest {

    @XmlElement(name = "ChargeObj", required = true)
    protected FeeQuotationRequest.ChargeObj chargeObj;
    @XmlElement(name = "ChargeElement", required = true)
    protected FeeQuotationRequest.ChargeElement chargeElement;

    /**
     * Obtiene el valor de la propiedad chargeObj.
     * 
     * @return
     *     possible object is
     *     {@link FeeQuotationRequest.ChargeObj }
     *     
     */
    public FeeQuotationRequest.ChargeObj getChargeObj() {
        return chargeObj;
    }

    /**
     * Define el valor de la propiedad chargeObj.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeQuotationRequest.ChargeObj }
     *     
     */
    public void setChargeObj(FeeQuotationRequest.ChargeObj value) {
        this.chargeObj = value;
    }

    /**
     * Obtiene el valor de la propiedad chargeElement.
     * 
     * @return
     *     possible object is
     *     {@link FeeQuotationRequest.ChargeElement }
     *     
     */
    public FeeQuotationRequest.ChargeElement getChargeElement() {
        return chargeElement;
    }

    /**
     * Define el valor de la propiedad chargeElement.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeQuotationRequest.ChargeElement }
     *     
     */
    public void setChargeElement(FeeQuotationRequest.ChargeElement value) {
        this.chargeElement = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FeeAmount" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ChargeAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="OfferingFee" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                             &lt;element name="ParentOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
     *                             &lt;element name="OInstProperty" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
     *                                     &lt;sequence>
     *                                       &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                       &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/extension>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="ProductInst" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="ProductID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                                       &lt;element name="PInstProperty" maxOccurs="unbounded" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
     *                                               &lt;sequence>
     *                                                 &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                                 &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                             &lt;/extension>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="OperationFee" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="OperationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OperationProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "feeAmount",
        "offeringFee",
        "operationFee"
    })
    public static class ChargeElement {

        @XmlElement(name = "FeeAmount")
        protected List<FeeQuotationRequest.ChargeElement.FeeAmount> feeAmount;
        @XmlElement(name = "OfferingFee")
        protected FeeQuotationRequest.ChargeElement.OfferingFee offeringFee;
        @XmlElement(name = "OperationFee")
        protected FeeQuotationRequest.ChargeElement.OperationFee operationFee;

        /**
         * Gets the value of the feeAmount property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the feeAmount property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFeeAmount().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FeeQuotationRequest.ChargeElement.FeeAmount }
         * 
         * 
         */
        public List<FeeQuotationRequest.ChargeElement.FeeAmount> getFeeAmount() {
            if (feeAmount == null) {
                feeAmount = new ArrayList<FeeQuotationRequest.ChargeElement.FeeAmount>();
            }
            return this.feeAmount;
        }

        /**
         * Obtiene el valor de la propiedad offeringFee.
         * 
         * @return
         *     possible object is
         *     {@link FeeQuotationRequest.ChargeElement.OfferingFee }
         *     
         */
        public FeeQuotationRequest.ChargeElement.OfferingFee getOfferingFee() {
            return offeringFee;
        }

        /**
         * Define el valor de la propiedad offeringFee.
         * 
         * @param value
         *     allowed object is
         *     {@link FeeQuotationRequest.ChargeElement.OfferingFee }
         *     
         */
        public void setOfferingFee(FeeQuotationRequest.ChargeElement.OfferingFee value) {
            this.offeringFee = value;
        }

        /**
         * Obtiene el valor de la propiedad operationFee.
         * 
         * @return
         *     possible object is
         *     {@link FeeQuotationRequest.ChargeElement.OperationFee }
         *     
         */
        public FeeQuotationRequest.ChargeElement.OperationFee getOperationFee() {
            return operationFee;
        }

        /**
         * Define el valor de la propiedad operationFee.
         * 
         * @param value
         *     allowed object is
         *     {@link FeeQuotationRequest.ChargeElement.OperationFee }
         *     
         */
        public void setOperationFee(FeeQuotationRequest.ChargeElement.OperationFee value) {
            this.operationFee = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ChargeAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "chargeCode",
            "chargeAmt",
            "currencyID"
        })
        public static class FeeAmount {

            @XmlElement(name = "ChargeCode", required = true)
            protected String chargeCode;
            @XmlElement(name = "ChargeAmt")
            protected long chargeAmt;
            @XmlElement(name = "CurrencyID", required = true)
            protected BigInteger currencyID;

            /**
             * Obtiene el valor de la propiedad chargeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChargeCode() {
                return chargeCode;
            }

            /**
             * Define el valor de la propiedad chargeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChargeCode(String value) {
                this.chargeCode = value;
            }

            /**
             * Obtiene el valor de la propiedad chargeAmt.
             * 
             */
            public long getChargeAmt() {
                return chargeAmt;
            }

            /**
             * Define el valor de la propiedad chargeAmt.
             * 
             */
            public void setChargeAmt(long value) {
                this.chargeAmt = value;
            }

            /**
             * Obtiene el valor de la propiedad currencyID.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCurrencyID() {
                return currencyID;
            }

            /**
             * Define el valor de la propiedad currencyID.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCurrencyID(BigInteger value) {
                this.currencyID = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                   &lt;element name="ParentOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
         *                   &lt;element name="OInstProperty" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
         *                           &lt;sequence>
         *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/extension>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="ProductInst" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="ProductID" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                             &lt;element name="PInstProperty" maxOccurs="unbounded" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
         *                                     &lt;sequence>
         *                                       &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                       &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                   &lt;/extension>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "addOffering",
            "delOffering"
        })
        public static class OfferingFee {

            @XmlElement(name = "AddOffering")
            protected List<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering> addOffering;
            @XmlElement(name = "DelOffering")
            protected List<FeeQuotationRequest.ChargeElement.OfferingFee.DelOffering> delOffering;

            /**
             * Gets the value of the addOffering property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the addOffering property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddOffering().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering }
             * 
             * 
             */
            public List<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering> getAddOffering() {
                if (addOffering == null) {
                    addOffering = new ArrayList<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering>();
                }
                return this.addOffering;
            }

            /**
             * Gets the value of the delOffering property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the delOffering property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDelOffering().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link FeeQuotationRequest.ChargeElement.OfferingFee.DelOffering }
             * 
             * 
             */
            public List<FeeQuotationRequest.ChargeElement.OfferingFee.DelOffering> getDelOffering() {
                if (delOffering == null) {
                    delOffering = new ArrayList<FeeQuotationRequest.ChargeElement.OfferingFee.DelOffering>();
                }
                return this.delOffering;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *         &lt;element name="ParentOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
             *         &lt;element name="OInstProperty" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
             *                 &lt;sequence>
             *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/extension>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="ProductInst" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="ProductID" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *                   &lt;element name="PInstProperty" maxOccurs="unbounded" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
             *                           &lt;sequence>
             *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                           &lt;/sequence>
             *                         &lt;/extension>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offeringID",
                "parentOfferingKey",
                "oInstProperty",
                "productInst",
                "trialStartTime",
                "trialEndTime"
            })
            public static class AddOffering {

                @XmlElement(name = "OfferingID", required = true, nillable = true)
                protected BigInteger offeringID;
                @XmlElement(name = "ParentOfferingKey")
                protected OfferingKey parentOfferingKey;
                @XmlElement(name = "OInstProperty")
                protected List<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.OInstProperty> oInstProperty;
                @XmlElement(name = "ProductInst")
                protected List<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst> productInst;
                @XmlElement(name = "TrialStartTime")
                protected String trialStartTime;
                @XmlElement(name = "TrialEndTime")
                protected String trialEndTime;

                /**
                 * Obtiene el valor de la propiedad offeringID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getOfferingID() {
                    return offeringID;
                }

                /**
                 * Define el valor de la propiedad offeringID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setOfferingID(BigInteger value) {
                    this.offeringID = value;
                }

                /**
                 * Obtiene el valor de la propiedad parentOfferingKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getParentOfferingKey() {
                    return parentOfferingKey;
                }

                /**
                 * Define el valor de la propiedad parentOfferingKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setParentOfferingKey(OfferingKey value) {
                    this.parentOfferingKey = value;
                }

                /**
                 * Gets the value of the oInstProperty property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the oInstProperty property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getOInstProperty().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.OInstProperty }
                 * 
                 * 
                 */
                public List<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.OInstProperty> getOInstProperty() {
                    if (oInstProperty == null) {
                        oInstProperty = new ArrayList<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.OInstProperty>();
                    }
                    return this.oInstProperty;
                }

                /**
                 * Gets the value of the productInst property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the productInst property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getProductInst().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst }
                 * 
                 * 
                 */
                public List<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst> getProductInst() {
                    if (productInst == null) {
                        productInst = new ArrayList<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst>();
                    }
                    return this.productInst;
                }

                /**
                 * Obtiene el valor de la propiedad trialStartTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTrialStartTime() {
                    return trialStartTime;
                }

                /**
                 * Define el valor de la propiedad trialStartTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTrialStartTime(String value) {
                    this.trialStartTime = value;
                }

                /**
                 * Obtiene el valor de la propiedad trialEndTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTrialEndTime() {
                    return trialEndTime;
                }

                /**
                 * Define el valor de la propiedad trialEndTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTrialEndTime(String value) {
                    this.trialEndTime = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
                 *       &lt;sequence>
                 *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/extension>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "effectiveTime",
                    "expirationTime"
                })
                public static class OInstProperty
                    extends InstProperty
                {

                    @XmlElement(name = "EffectiveTime")
                    protected String effectiveTime;
                    @XmlElement(name = "ExpirationTime")
                    protected String expirationTime;

                    /**
                     * Obtiene el valor de la propiedad effectiveTime.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEffectiveTime() {
                        return effectiveTime;
                    }

                    /**
                     * Define el valor de la propiedad effectiveTime.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEffectiveTime(String value) {
                        this.effectiveTime = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad expirationTime.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getExpirationTime() {
                        return expirationTime;
                    }

                    /**
                     * Define el valor de la propiedad expirationTime.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setExpirationTime(String value) {
                        this.expirationTime = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="ProductID" type="{http://www.w3.org/2001/XMLSchema}long"/>
                 *         &lt;element name="PInstProperty" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
                 *                 &lt;sequence>
                 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *               &lt;/extension>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "productID",
                    "pInstProperty"
                })
                public static class ProductInst {

                    @XmlElement(name = "ProductID")
                    protected long productID;
                    @XmlElement(name = "PInstProperty")
                    protected List<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst.PInstProperty> pInstProperty;

                    /**
                     * Obtiene el valor de la propiedad productID.
                     * 
                     */
                    public long getProductID() {
                        return productID;
                    }

                    /**
                     * Define el valor de la propiedad productID.
                     * 
                     */
                    public void setProductID(long value) {
                        this.productID = value;
                    }

                    /**
                     * Gets the value of the pInstProperty property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the pInstProperty property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getPInstProperty().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst.PInstProperty }
                     * 
                     * 
                     */
                    public List<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst.PInstProperty> getPInstProperty() {
                        if (pInstProperty == null) {
                            pInstProperty = new ArrayList<FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst.PInstProperty>();
                        }
                        return this.pInstProperty;
                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
                     *       &lt;sequence>
                     *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *       &lt;/sequence>
                     *     &lt;/extension>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "effectiveTime",
                        "expirationTime"
                    })
                    public static class PInstProperty
                        extends InstProperty
                    {

                        @XmlElement(name = "EffectiveTime")
                        protected String effectiveTime;
                        @XmlElement(name = "ExpirationTime")
                        protected String expirationTime;

                        /**
                         * Obtiene el valor de la propiedad effectiveTime.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getEffectiveTime() {
                            return effectiveTime;
                        }

                        /**
                         * Define el valor de la propiedad effectiveTime.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEffectiveTime(String value) {
                            this.effectiveTime = value;
                        }

                        /**
                         * Obtiene el valor de la propiedad expirationTime.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getExpirationTime() {
                            return expirationTime;
                        }

                        /**
                         * Define el valor de la propiedad expirationTime.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setExpirationTime(String value) {
                            this.expirationTime = value;
                        }

                    }

                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offeringKey"
            })
            public static class DelOffering {

                @XmlElement(name = "OfferingKey", required = true, nillable = true)
                protected OfferingKey offeringKey;

                /**
                 * Obtiene el valor de la propiedad offeringKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getOfferingKey() {
                    return offeringKey;
                }

                /**
                 * Define el valor de la propiedad offeringKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setOfferingKey(OfferingKey value) {
                    this.offeringKey = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="OperationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OperationProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "operationCode",
            "operationProperty"
        })
        public static class OperationFee {

            @XmlElement(name = "OperationCode", required = true)
            protected String operationCode;
            @XmlElement(name = "OperationProperty")
            protected List<SimpleProperty> operationProperty;

            /**
             * Obtiene el valor de la propiedad operationCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOperationCode() {
                return operationCode;
            }

            /**
             * Define el valor de la propiedad operationCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOperationCode(String value) {
                this.operationCode = value;
            }

            /**
             * Gets the value of the operationProperty property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the operationProperty property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOperationProperty().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link SimpleProperty }
             * 
             * 
             */
            public List<SimpleProperty> getOperationProperty() {
                if (operationProperty == null) {
                    operationProperty = new ArrayList<SimpleProperty>();
                }
                return this.operationProperty;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
     *         &lt;element name="AcctAccessCode" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
     *                 &lt;sequence>
     *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
     *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subAccessCode",
        "acctAccessCode",
        "custAccessCode",
        "subGroupAccessCode"
    })
    public static class ChargeObj {

        @XmlElement(name = "SubAccessCode")
        protected SubAccessCode subAccessCode;
        @XmlElement(name = "AcctAccessCode")
        protected FeeQuotationRequest.ChargeObj.AcctAccessCode acctAccessCode;
        @XmlElement(name = "CustAccessCode")
        protected CustAccessCode custAccessCode;
        @XmlElement(name = "SubGroupAccessCode")
        protected SubGroupAccessCode subGroupAccessCode;

        /**
         * Obtiene el valor de la propiedad subAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubAccessCode }
         *     
         */
        public SubAccessCode getSubAccessCode() {
            return subAccessCode;
        }

        /**
         * Define el valor de la propiedad subAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubAccessCode }
         *     
         */
        public void setSubAccessCode(SubAccessCode value) {
            this.subAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad acctAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link FeeQuotationRequest.ChargeObj.AcctAccessCode }
         *     
         */
        public FeeQuotationRequest.ChargeObj.AcctAccessCode getAcctAccessCode() {
            return acctAccessCode;
        }

        /**
         * Define el valor de la propiedad acctAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link FeeQuotationRequest.ChargeObj.AcctAccessCode }
         *     
         */
        public void setAcctAccessCode(FeeQuotationRequest.ChargeObj.AcctAccessCode value) {
            this.acctAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad custAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link CustAccessCode }
         *     
         */
        public CustAccessCode getCustAccessCode() {
            return custAccessCode;
        }

        /**
         * Define el valor de la propiedad custAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link CustAccessCode }
         *     
         */
        public void setCustAccessCode(CustAccessCode value) {
            this.custAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public SubGroupAccessCode getSubGroupAccessCode() {
            return subGroupAccessCode;
        }

        /**
         * Define el valor de la propiedad subGroupAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public void setSubGroupAccessCode(SubGroupAccessCode value) {
            this.subGroupAccessCode = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
         *       &lt;sequence>
         *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "payType"
        })
        public static class AcctAccessCode
            extends com.huawei.bme.cbsinterface.bccommon.AcctAccessCode
        {

            @XmlElement(name = "PayType")
            protected String payType;

            /**
             * Obtiene el valor de la propiedad payType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayType() {
                return payType;
            }

            /**
             * Define el valor de la propiedad payType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayType(String value) {
                this.payType = value;
            }

        }

    }

}
