
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CustBasicInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CustBasicInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustSegment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DFTPwd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DFTWrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DFTIVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DFTBillCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DFTCurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *         &lt;element name="CustLevel" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="CustLoyalty" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DunningFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CustProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustBasicInfo", propOrder = {
    "custSegment",
    "dftPwd",
    "dftWrittenLang",
    "dftivrLang",
    "dftBillCycleType",
    "dftCurrencyID",
    "custLevel",
    "custLoyalty",
    "dunningFlag",
    "custProperty"
})
public class CustBasicInfo {

    @XmlElement(name = "CustSegment")
    protected String custSegment;
    @XmlElement(name = "DFTPwd")
    protected String dftPwd;
    @XmlElement(name = "DFTWrittenLang")
    protected String dftWrittenLang;
    @XmlElement(name = "DFTIVRLang")
    protected String dftivrLang;
    @XmlElement(name = "DFTBillCycleType")
    protected String dftBillCycleType;
    @XmlElement(name = "DFTCurrencyID")
    protected BigInteger dftCurrencyID;
    @XmlElement(name = "CustLevel", required = true)
    protected String custLevel;
    @XmlElement(name = "CustLoyalty")
    protected String custLoyalty;
    @XmlElement(name = "DunningFlag")
    protected String dunningFlag;
    @XmlElement(name = "CustProperty")
    protected List<SimpleProperty> custProperty;

    /**
     * Obtiene el valor de la propiedad custSegment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustSegment() {
        return custSegment;
    }

    /**
     * Define el valor de la propiedad custSegment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustSegment(String value) {
        this.custSegment = value;
    }

    /**
     * Obtiene el valor de la propiedad dftPwd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDFTPwd() {
        return dftPwd;
    }

    /**
     * Define el valor de la propiedad dftPwd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDFTPwd(String value) {
        this.dftPwd = value;
    }

    /**
     * Obtiene el valor de la propiedad dftWrittenLang.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDFTWrittenLang() {
        return dftWrittenLang;
    }

    /**
     * Define el valor de la propiedad dftWrittenLang.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDFTWrittenLang(String value) {
        this.dftWrittenLang = value;
    }

    /**
     * Obtiene el valor de la propiedad dftivrLang.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDFTIVRLang() {
        return dftivrLang;
    }

    /**
     * Define el valor de la propiedad dftivrLang.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDFTIVRLang(String value) {
        this.dftivrLang = value;
    }

    /**
     * Obtiene el valor de la propiedad dftBillCycleType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDFTBillCycleType() {
        return dftBillCycleType;
    }

    /**
     * Define el valor de la propiedad dftBillCycleType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDFTBillCycleType(String value) {
        this.dftBillCycleType = value;
    }

    /**
     * Obtiene el valor de la propiedad dftCurrencyID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDFTCurrencyID() {
        return dftCurrencyID;
    }

    /**
     * Define el valor de la propiedad dftCurrencyID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDFTCurrencyID(BigInteger value) {
        this.dftCurrencyID = value;
    }

    /**
     * Obtiene el valor de la propiedad custLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLevel() {
        return custLevel;
    }

    /**
     * Define el valor de la propiedad custLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLevel(String value) {
        this.custLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad custLoyalty.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLoyalty() {
        return custLoyalty;
    }

    /**
     * Define el valor de la propiedad custLoyalty.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLoyalty(String value) {
        this.custLoyalty = value;
    }

    /**
     * Obtiene el valor de la propiedad dunningFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDunningFlag() {
        return dunningFlag;
    }

    /**
     * Define el valor de la propiedad dunningFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDunningFlag(String value) {
        this.dunningFlag = value;
    }

    /**
     * Gets the value of the custProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the custProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getCustProperty() {
        if (custProperty == null) {
            custProperty = new ArrayList<SimpleProperty>();
        }
        return this.custProperty;
    }

}
