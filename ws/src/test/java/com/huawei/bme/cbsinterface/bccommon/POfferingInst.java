
package com.huawei.bme.cbsinterface.bccommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para POfferingInst complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="POfferingInst">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" form="qualified"/>
 *         &lt;element name="BundledFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OfferingClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ParentOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0" form="qualified"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ProductInst" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ProductInst" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *         &lt;element name="OfferingInstProperty" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
 *                 &lt;sequence>
 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "POfferingInst", propOrder = {
    "offeringKey",
    "bundledFlag",
    "offeringClass",
    "parentOfferingKey",
    "status",
    "trialStartTime",
    "trialEndTime",
    "productInst",
    "offeringInstProperty"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.CreateSubscriberRequest.PrimaryOffering.class
})
public class POfferingInst {

    @XmlElement(name = "OfferingKey", required = true, nillable = true)
    protected OfferingKey offeringKey;
    @XmlElement(name = "BundledFlag")
    protected String bundledFlag;
    @XmlElement(name = "OfferingClass")
    protected String offeringClass;
    @XmlElement(name = "ParentOfferingKey")
    protected OfferingKey parentOfferingKey;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "TrialStartTime")
    protected String trialStartTime;
    @XmlElement(name = "TrialEndTime")
    protected String trialEndTime;
    @XmlElement(name = "ProductInst")
    protected List<ProductInst> productInst;
    @XmlElement(name = "OfferingInstProperty")
    protected List<POfferingInst.OfferingInstProperty> offeringInstProperty;

    /**
     * Obtiene el valor de la propiedad offeringKey.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getOfferingKey() {
        return offeringKey;
    }

    /**
     * Define el valor de la propiedad offeringKey.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setOfferingKey(OfferingKey value) {
        this.offeringKey = value;
    }

    /**
     * Obtiene el valor de la propiedad bundledFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBundledFlag() {
        return bundledFlag;
    }

    /**
     * Define el valor de la propiedad bundledFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBundledFlag(String value) {
        this.bundledFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad offeringClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferingClass() {
        return offeringClass;
    }

    /**
     * Define el valor de la propiedad offeringClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferingClass(String value) {
        this.offeringClass = value;
    }

    /**
     * Obtiene el valor de la propiedad parentOfferingKey.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getParentOfferingKey() {
        return parentOfferingKey;
    }

    /**
     * Define el valor de la propiedad parentOfferingKey.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setParentOfferingKey(OfferingKey value) {
        this.parentOfferingKey = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad trialStartTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrialStartTime() {
        return trialStartTime;
    }

    /**
     * Define el valor de la propiedad trialStartTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrialStartTime(String value) {
        this.trialStartTime = value;
    }

    /**
     * Obtiene el valor de la propiedad trialEndTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrialEndTime() {
        return trialEndTime;
    }

    /**
     * Define el valor de la propiedad trialEndTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrialEndTime(String value) {
        this.trialEndTime = value;
    }

    /**
     * Gets the value of the productInst property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productInst property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductInst().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductInst }
     * 
     * 
     */
    public List<ProductInst> getProductInst() {
        if (productInst == null) {
            productInst = new ArrayList<ProductInst>();
        }
        return this.productInst;
    }

    /**
     * Gets the value of the offeringInstProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offeringInstProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOfferingInstProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link POfferingInst.OfferingInstProperty }
     * 
     * 
     */
    public List<POfferingInst.OfferingInstProperty> getOfferingInstProperty() {
        if (offeringInstProperty == null) {
            offeringInstProperty = new ArrayList<POfferingInst.OfferingInstProperty>();
        }
        return this.offeringInstProperty;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
     *       &lt;sequence>
     *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "effectiveTime",
        "expirationTime"
    })
    public static class OfferingInstProperty
        extends InstProperty
    {

        @XmlElement(name = "EffectiveTime")
        protected String effectiveTime;
        @XmlElement(name = "ExpirationTime")
        protected String expirationTime;

        /**
         * Obtiene el valor de la propiedad effectiveTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Define el valor de la propiedad effectiveTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffectiveTime(String value) {
            this.effectiveTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationTime() {
            return expirationTime;
        }

        /**
         * Define el valor de la propiedad expirationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationTime(String value) {
            this.expirationTime = value;
        }

    }

}
