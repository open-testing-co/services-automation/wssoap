
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QuerySubLifeCycleResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QuerySubLifeCycleResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrentStatusIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LifeCycleStatus" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RBlacklistStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FraudTimes" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubLifeCycleResult", propOrder = {
    "currentStatusIndex",
    "lifeCycleStatus",
    "rBlacklistStatus",
    "fraudTimes"
})
public class QuerySubLifeCycleResult {

    @XmlElement(name = "CurrentStatusIndex")
    protected String currentStatusIndex;
    @XmlElement(name = "LifeCycleStatus")
    protected List<QuerySubLifeCycleResult.LifeCycleStatus> lifeCycleStatus;
    @XmlElement(name = "RBlacklistStatus", required = true)
    protected String rBlacklistStatus;
    @XmlElement(name = "FraudTimes", required = true)
    protected BigInteger fraudTimes;

    /**
     * Obtiene el valor de la propiedad currentStatusIndex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentStatusIndex() {
        return currentStatusIndex;
    }

    /**
     * Define el valor de la propiedad currentStatusIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentStatusIndex(String value) {
        this.currentStatusIndex = value;
    }

    /**
     * Gets the value of the lifeCycleStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lifeCycleStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLifeCycleStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuerySubLifeCycleResult.LifeCycleStatus }
     * 
     * 
     */
    public List<QuerySubLifeCycleResult.LifeCycleStatus> getLifeCycleStatus() {
        if (lifeCycleStatus == null) {
            lifeCycleStatus = new ArrayList<QuerySubLifeCycleResult.LifeCycleStatus>();
        }
        return this.lifeCycleStatus;
    }

    /**
     * Obtiene el valor de la propiedad rBlacklistStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRBlacklistStatus() {
        return rBlacklistStatus;
    }

    /**
     * Define el valor de la propiedad rBlacklistStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRBlacklistStatus(String value) {
        this.rBlacklistStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad fraudTimes.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFraudTimes() {
        return fraudTimes;
    }

    /**
     * Define el valor de la propiedad fraudTimes.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFraudTimes(BigInteger value) {
        this.fraudTimes = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="StatusName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="StatusExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="StatusIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "statusName",
        "statusExpireTime",
        "statusIndex"
    })
    public static class LifeCycleStatus {

        @XmlElement(name = "StatusName", required = true)
        protected String statusName;
        @XmlElement(name = "StatusExpireTime", required = true)
        protected String statusExpireTime;
        @XmlElement(name = "StatusIndex", required = true)
        protected String statusIndex;

        /**
         * Obtiene el valor de la propiedad statusName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatusName() {
            return statusName;
        }

        /**
         * Define el valor de la propiedad statusName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatusName(String value) {
            this.statusName = value;
        }

        /**
         * Obtiene el valor de la propiedad statusExpireTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatusExpireTime() {
            return statusExpireTime;
        }

        /**
         * Define el valor de la propiedad statusExpireTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatusExpireTime(String value) {
            this.statusExpireTime = value;
        }

        /**
         * Obtiene el valor de la propiedad statusIndex.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatusIndex() {
            return statusIndex;
        }

        /**
         * Define el valor de la propiedad statusIndex.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatusIndex(String value) {
            this.statusIndex = value;
        }

    }

}
