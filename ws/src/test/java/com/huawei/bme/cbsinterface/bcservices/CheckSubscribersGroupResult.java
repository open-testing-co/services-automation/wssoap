
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CheckSubscribersGroupResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CheckSubscribersGroupResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupRelation" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UserGroup" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GroupID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ActiveFlg" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckSubscribersGroupResult", propOrder = {
    "groupRelation",
    "userGroup"
})
public class CheckSubscribersGroupResult {

    @XmlElement(name = "GroupRelation")
    protected int groupRelation;
    @XmlElement(name = "UserGroup")
    protected List<CheckSubscribersGroupResult.UserGroup> userGroup;

    /**
     * Obtiene el valor de la propiedad groupRelation.
     * 
     */
    public int getGroupRelation() {
        return groupRelation;
    }

    /**
     * Define el valor de la propiedad groupRelation.
     * 
     */
    public void setGroupRelation(int value) {
        this.groupRelation = value;
    }

    /**
     * Gets the value of the userGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CheckSubscribersGroupResult.UserGroup }
     * 
     * 
     */
    public List<CheckSubscribersGroupResult.UserGroup> getUserGroup() {
        if (userGroup == null) {
            userGroup = new ArrayList<CheckSubscribersGroupResult.UserGroup>();
        }
        return this.userGroup;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GroupID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="GroupName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ActiveFlg" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "groupID",
        "groupName",
        "activeFlg"
    })
    public static class UserGroup {

        @XmlElement(name = "GroupID", required = true)
        protected String groupID;
        @XmlElement(name = "GroupName", required = true)
        protected String groupName;
        @XmlElement(name = "ActiveFlg")
        protected Integer activeFlg;

        /**
         * Obtiene el valor de la propiedad groupID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroupID() {
            return groupID;
        }

        /**
         * Define el valor de la propiedad groupID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroupID(String value) {
            this.groupID = value;
        }

        /**
         * Obtiene el valor de la propiedad groupName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroupName() {
            return groupName;
        }

        /**
         * Define el valor de la propiedad groupName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroupName(String value) {
            this.groupName = value;
        }

        /**
         * Obtiene el valor de la propiedad activeFlg.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getActiveFlg() {
            return activeFlg;
        }

        /**
         * Define el valor de la propiedad activeFlg.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setActiveFlg(Integer value) {
            this.activeFlg = value;
        }

    }

}
