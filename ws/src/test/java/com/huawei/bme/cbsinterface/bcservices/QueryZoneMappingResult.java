
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryZoneMappingResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryZoneMappingResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="zoneDetail" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="zonetID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="zoneType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="zoneName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryZoneMappingResult", propOrder = {
    "zoneDetail"
})
public class QueryZoneMappingResult {

    @XmlElement(required = true)
    protected List<QueryZoneMappingResult.ZoneDetail> zoneDetail;

    /**
     * Gets the value of the zoneDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zoneDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZoneDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryZoneMappingResult.ZoneDetail }
     * 
     * 
     */
    public List<QueryZoneMappingResult.ZoneDetail> getZoneDetail() {
        if (zoneDetail == null) {
            zoneDetail = new ArrayList<QueryZoneMappingResult.ZoneDetail>();
        }
        return this.zoneDetail;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="zonetID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="zoneType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="zoneName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "zonetID",
        "zoneType",
        "zoneName"
    })
    public static class ZoneDetail {

        protected long zonetID;
        @XmlElement(required = true)
        protected String zoneType;
        @XmlElement(required = true)
        protected String zoneName;

        /**
         * Obtiene el valor de la propiedad zonetID.
         * 
         */
        public long getZonetID() {
            return zonetID;
        }

        /**
         * Define el valor de la propiedad zonetID.
         * 
         */
        public void setZonetID(long value) {
            this.zonetID = value;
        }

        /**
         * Obtiene el valor de la propiedad zoneType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZoneType() {
            return zoneType;
        }

        /**
         * Define el valor de la propiedad zoneType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZoneType(String value) {
            this.zoneType = value;
        }

        /**
         * Obtiene el valor de la propiedad zoneName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZoneName() {
            return zoneName;
        }

        /**
         * Define el valor de la propiedad zoneName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZoneName(String value) {
            this.zoneName = value;
        }

    }

}
