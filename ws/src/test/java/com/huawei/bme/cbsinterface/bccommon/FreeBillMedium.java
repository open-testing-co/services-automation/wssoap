
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FreeBillMedium complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FreeBillMedium">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BMCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="BMType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FreeBillMedium", propOrder = {
    "bmCode",
    "bmType"
})
public class FreeBillMedium {

    @XmlElement(name = "BMCode", required = true)
    protected String bmCode;
    @XmlElement(name = "BMType", required = true)
    protected String bmType;

    /**
     * Obtiene el valor de la propiedad bmCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBMCode() {
        return bmCode;
    }

    /**
     * Define el valor de la propiedad bmCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBMCode(String value) {
        this.bmCode = value;
    }

    /**
     * Obtiene el valor de la propiedad bmType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBMType() {
        return bmType;
    }

    /**
     * Define el valor de la propiedad bmType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBMType(String value) {
        this.bmType = value;
    }

}
