
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;


/**
 * <p>Clase Java para ChangeCustNoticeSuppressRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeCustNoticeSuppressRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode"/>
 *         &lt;element name="SuppressSetting">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AddSuppressSet" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="DelSuppressSet" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeCustNoticeSuppressRequest", propOrder = {
    "custAccessCode",
    "suppressSetting"
})
public class ChangeCustNoticeSuppressRequest {

    @XmlElement(name = "CustAccessCode", required = true)
    protected CustAccessCode custAccessCode;
    @XmlElement(name = "SuppressSetting", required = true)
    protected ChangeCustNoticeSuppressRequest.SuppressSetting suppressSetting;

    /**
     * Obtiene el valor de la propiedad custAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link CustAccessCode }
     *     
     */
    public CustAccessCode getCustAccessCode() {
        return custAccessCode;
    }

    /**
     * Define el valor de la propiedad custAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link CustAccessCode }
     *     
     */
    public void setCustAccessCode(CustAccessCode value) {
        this.custAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad suppressSetting.
     * 
     * @return
     *     possible object is
     *     {@link ChangeCustNoticeSuppressRequest.SuppressSetting }
     *     
     */
    public ChangeCustNoticeSuppressRequest.SuppressSetting getSuppressSetting() {
        return suppressSetting;
    }

    /**
     * Define el valor de la propiedad suppressSetting.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeCustNoticeSuppressRequest.SuppressSetting }
     *     
     */
    public void setSuppressSetting(ChangeCustNoticeSuppressRequest.SuppressSetting value) {
        this.suppressSetting = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AddSuppressSet" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DelSuppressSet" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "addSuppressSet",
        "delSuppressSet"
    })
    public static class SuppressSetting {

        @XmlElement(name = "AddSuppressSet")
        protected List<ChangeCustNoticeSuppressRequest.SuppressSetting.AddSuppressSet> addSuppressSet;
        @XmlElement(name = "DelSuppressSet")
        protected List<ChangeCustNoticeSuppressRequest.SuppressSetting.DelSuppressSet> delSuppressSet;

        /**
         * Gets the value of the addSuppressSet property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addSuppressSet property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddSuppressSet().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeCustNoticeSuppressRequest.SuppressSetting.AddSuppressSet }
         * 
         * 
         */
        public List<ChangeCustNoticeSuppressRequest.SuppressSetting.AddSuppressSet> getAddSuppressSet() {
            if (addSuppressSet == null) {
                addSuppressSet = new ArrayList<ChangeCustNoticeSuppressRequest.SuppressSetting.AddSuppressSet>();
            }
            return this.addSuppressSet;
        }

        /**
         * Gets the value of the delSuppressSet property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the delSuppressSet property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDelSuppressSet().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeCustNoticeSuppressRequest.SuppressSetting.DelSuppressSet }
         * 
         * 
         */
        public List<ChangeCustNoticeSuppressRequest.SuppressSetting.DelSuppressSet> getDelSuppressSet() {
            if (delSuppressSet == null) {
                delSuppressSet = new ArrayList<ChangeCustNoticeSuppressRequest.SuppressSetting.DelSuppressSet>();
            }
            return this.delSuppressSet;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "channelType",
            "noticeType",
            "subNoticeType"
        })
        public static class AddSuppressSet {

            @XmlElement(name = "ChannelType", required = true)
            protected String channelType;
            @XmlElement(name = "NoticeType", required = true)
            protected String noticeType;
            @XmlElement(name = "SubNoticeType")
            protected String subNoticeType;

            /**
             * Obtiene el valor de la propiedad channelType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelType() {
                return channelType;
            }

            /**
             * Define el valor de la propiedad channelType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelType(String value) {
                this.channelType = value;
            }

            /**
             * Obtiene el valor de la propiedad noticeType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNoticeType() {
                return noticeType;
            }

            /**
             * Define el valor de la propiedad noticeType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNoticeType(String value) {
                this.noticeType = value;
            }

            /**
             * Obtiene el valor de la propiedad subNoticeType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubNoticeType() {
                return subNoticeType;
            }

            /**
             * Define el valor de la propiedad subNoticeType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubNoticeType(String value) {
                this.subNoticeType = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ChannelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="NoticeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="SubNoticeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "channelType",
            "noticeType",
            "subNoticeType"
        })
        public static class DelSuppressSet {

            @XmlElement(name = "ChannelType", required = true)
            protected String channelType;
            @XmlElement(name = "NoticeType", required = true)
            protected String noticeType;
            @XmlElement(name = "SubNoticeType")
            protected String subNoticeType;

            /**
             * Obtiene el valor de la propiedad channelType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChannelType() {
                return channelType;
            }

            /**
             * Define el valor de la propiedad channelType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChannelType(String value) {
                this.channelType = value;
            }

            /**
             * Obtiene el valor de la propiedad noticeType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNoticeType() {
                return noticeType;
            }

            /**
             * Define el valor de la propiedad noticeType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNoticeType(String value) {
                this.noticeType = value;
            }

            /**
             * Obtiene el valor de la propiedad subNoticeType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubNoticeType() {
                return subNoticeType;
            }

            /**
             * Define el valor de la propiedad subNoticeType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubNoticeType(String value) {
                this.subNoticeType = value;
            }

        }

    }

}
