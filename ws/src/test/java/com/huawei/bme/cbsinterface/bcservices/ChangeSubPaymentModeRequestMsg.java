
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader" form="unqualified"/>
 *         &lt;element name="ChangeSubPaymentModeRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeSubPaymentModeRequest" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "changeSubPaymentModeRequest"
})
@XmlRootElement(name = "ChangeSubPaymentModeRequestMsg")
public class ChangeSubPaymentModeRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "ChangeSubPaymentModeRequest", namespace = "", required = true)
    protected ChangeSubPaymentModeRequest changeSubPaymentModeRequest;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad changeSubPaymentModeRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubPaymentModeRequest }
     *     
     */
    public ChangeSubPaymentModeRequest getChangeSubPaymentModeRequest() {
        return changeSubPaymentModeRequest;
    }

    /**
     * Define el valor de la propiedad changeSubPaymentModeRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubPaymentModeRequest }
     *     
     */
    public void setChangeSubPaymentModeRequest(ChangeSubPaymentModeRequest value) {
        this.changeSubPaymentModeRequest = value;
    }

}
