
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader" form="unqualified"/>
 *         &lt;element name="QueryDataPackageUsageResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryDataPackageUsageResult" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "queryDataPackageUsageResult"
})
@XmlRootElement(name = "QueryDataPackageUsageResultMsg")
public class QueryDataPackageUsageResultMsg {

    @XmlElement(name = "ResultHeader", namespace = "", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "QueryDataPackageUsageResult", namespace = "", required = true)
    protected QueryDataPackageUsageResult queryDataPackageUsageResult;

    /**
     * Obtiene el valor de la propiedad resultHeader.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Define el valor de la propiedad resultHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad queryDataPackageUsageResult.
     * 
     * @return
     *     possible object is
     *     {@link QueryDataPackageUsageResult }
     *     
     */
    public QueryDataPackageUsageResult getQueryDataPackageUsageResult() {
        return queryDataPackageUsageResult;
    }

    /**
     * Define el valor de la propiedad queryDataPackageUsageResult.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryDataPackageUsageResult }
     *     
     */
    public void setQueryDataPackageUsageResult(QueryDataPackageUsageResult value) {
        this.queryDataPackageUsageResult = value;
    }

}
