
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QuerySubInfoExToCubeResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QuerySubInfoExToCubeResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Subscriber">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ServiceNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BalanceAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="PrimaryOfferName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PrimaryOfferKey" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="LifeCycleStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ManagementSatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BlacklistStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AdditionalProperty" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PaymentLimitUsage" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubInfoExToCubeResult", propOrder = {
    "subscriber"
})
public class QuerySubInfoExToCubeResult {

    @XmlElement(name = "Subscriber", required = true)
    protected QuerySubInfoExToCubeResult.Subscriber subscriber;

    /**
     * Obtiene el valor de la propiedad subscriber.
     * 
     * @return
     *     possible object is
     *     {@link QuerySubInfoExToCubeResult.Subscriber }
     *     
     */
    public QuerySubInfoExToCubeResult.Subscriber getSubscriber() {
        return subscriber;
    }

    /**
     * Define el valor de la propiedad subscriber.
     * 
     * @param value
     *     allowed object is
     *     {@link QuerySubInfoExToCubeResult.Subscriber }
     *     
     */
    public void setSubscriber(QuerySubInfoExToCubeResult.Subscriber value) {
        this.subscriber = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ServiceNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BalanceAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="PrimaryOfferName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PrimaryOfferKey" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="LifeCycleStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ManagementSatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BlacklistStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AdditionalProperty" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PaymentLimitUsage" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceNum",
        "paymentType",
        "balanceAmount",
        "primaryOfferName",
        "primaryOfferKey",
        "lifeCycleStatus",
        "managementSatus",
        "blacklistStatus",
        "additionalProperty",
        "paymentLimitUsage"
    })
    public static class Subscriber {

        @XmlElement(name = "ServiceNum", required = true)
        protected String serviceNum;
        @XmlElement(name = "PaymentType", required = true, nillable = true)
        protected String paymentType;
        @XmlElement(name = "BalanceAmount")
        protected long balanceAmount;
        @XmlElement(name = "PrimaryOfferName", required = true)
        protected String primaryOfferName;
        @XmlElement(name = "PrimaryOfferKey", required = true)
        protected BigInteger primaryOfferKey;
        @XmlElement(name = "LifeCycleStatus")
        protected String lifeCycleStatus;
        @XmlElement(name = "ManagementSatus")
        protected String managementSatus;
        @XmlElement(name = "BlacklistStatus")
        protected String blacklistStatus;
        @XmlElement(name = "AdditionalProperty")
        protected List<QuerySubInfoExToCubeResult.Subscriber.AdditionalProperty> additionalProperty;
        @XmlElement(name = "PaymentLimitUsage")
        protected QuerySubInfoExToCubeResult.Subscriber.PaymentLimitUsage paymentLimitUsage;

        /**
         * Obtiene el valor de la propiedad serviceNum.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceNum() {
            return serviceNum;
        }

        /**
         * Define el valor de la propiedad serviceNum.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceNum(String value) {
            this.serviceNum = value;
        }

        /**
         * Obtiene el valor de la propiedad paymentType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaymentType() {
            return paymentType;
        }

        /**
         * Define el valor de la propiedad paymentType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaymentType(String value) {
            this.paymentType = value;
        }

        /**
         * Obtiene el valor de la propiedad balanceAmount.
         * 
         */
        public long getBalanceAmount() {
            return balanceAmount;
        }

        /**
         * Define el valor de la propiedad balanceAmount.
         * 
         */
        public void setBalanceAmount(long value) {
            this.balanceAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryOfferName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryOfferName() {
            return primaryOfferName;
        }

        /**
         * Define el valor de la propiedad primaryOfferName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryOfferName(String value) {
            this.primaryOfferName = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryOfferKey.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getPrimaryOfferKey() {
            return primaryOfferKey;
        }

        /**
         * Define el valor de la propiedad primaryOfferKey.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setPrimaryOfferKey(BigInteger value) {
            this.primaryOfferKey = value;
        }

        /**
         * Obtiene el valor de la propiedad lifeCycleStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLifeCycleStatus() {
            return lifeCycleStatus;
        }

        /**
         * Define el valor de la propiedad lifeCycleStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLifeCycleStatus(String value) {
            this.lifeCycleStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad managementSatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getManagementSatus() {
            return managementSatus;
        }

        /**
         * Define el valor de la propiedad managementSatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setManagementSatus(String value) {
            this.managementSatus = value;
        }

        /**
         * Obtiene el valor de la propiedad blacklistStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBlacklistStatus() {
            return blacklistStatus;
        }

        /**
         * Define el valor de la propiedad blacklistStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBlacklistStatus(String value) {
            this.blacklistStatus = value;
        }

        /**
         * Gets the value of the additionalProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdditionalProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QuerySubInfoExToCubeResult.Subscriber.AdditionalProperty }
         * 
         * 
         */
        public List<QuerySubInfoExToCubeResult.Subscriber.AdditionalProperty> getAdditionalProperty() {
            if (additionalProperty == null) {
                additionalProperty = new ArrayList<QuerySubInfoExToCubeResult.Subscriber.AdditionalProperty>();
            }
            return this.additionalProperty;
        }

        /**
         * Obtiene el valor de la propiedad paymentLimitUsage.
         * 
         * @return
         *     possible object is
         *     {@link QuerySubInfoExToCubeResult.Subscriber.PaymentLimitUsage }
         *     
         */
        public QuerySubInfoExToCubeResult.Subscriber.PaymentLimitUsage getPaymentLimitUsage() {
            return paymentLimitUsage;
        }

        /**
         * Define el valor de la propiedad paymentLimitUsage.
         * 
         * @param value
         *     allowed object is
         *     {@link QuerySubInfoExToCubeResult.Subscriber.PaymentLimitUsage }
         *     
         */
        public void setPaymentLimitUsage(QuerySubInfoExToCubeResult.Subscriber.PaymentLimitUsage value) {
            this.paymentLimitUsage = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "code",
            "value"
        })
        public static class AdditionalProperty {

            @XmlElement(name = "Code", required = true)
            protected String code;
            @XmlElement(name = "Value", required = true)
            protected String value;

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "amount",
            "usedAmount",
            "limitType"
        })
        public static class PaymentLimitUsage {

            @XmlElement(name = "Amount")
            protected long amount;
            @XmlElement(name = "UsedAmount")
            protected long usedAmount;
            @XmlElement(name = "LimitType", required = true)
            protected String limitType;

            /**
             * Obtiene el valor de la propiedad amount.
             * 
             */
            public long getAmount() {
                return amount;
            }

            /**
             * Define el valor de la propiedad amount.
             * 
             */
            public void setAmount(long value) {
                this.amount = value;
            }

            /**
             * Obtiene el valor de la propiedad usedAmount.
             * 
             */
            public long getUsedAmount() {
                return usedAmount;
            }

            /**
             * Define el valor de la propiedad usedAmount.
             * 
             */
            public void setUsedAmount(long value) {
                this.usedAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad limitType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLimitType() {
                return limitType;
            }

            /**
             * Define el valor de la propiedad limitType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLimitType(String value) {
                this.limitType = value;
            }

        }

    }

}
