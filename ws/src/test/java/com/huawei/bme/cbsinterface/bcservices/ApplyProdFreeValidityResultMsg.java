
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader" form="unqualified"/>
 *         &lt;element name="ApplyProdFreeValidityResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ApplyProdFreeValidityResult" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "applyProdFreeValidityResult"
})
@XmlRootElement(name = "ApplyProdFreeValidityResultMsg")
public class ApplyProdFreeValidityResultMsg {

    @XmlElement(name = "ResultHeader", namespace = "", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "ApplyProdFreeValidityResult", namespace = "", required = true)
    protected ApplyProdFreeValidityResult applyProdFreeValidityResult;

    /**
     * Obtiene el valor de la propiedad resultHeader.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Define el valor de la propiedad resultHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad applyProdFreeValidityResult.
     * 
     * @return
     *     possible object is
     *     {@link ApplyProdFreeValidityResult }
     *     
     */
    public ApplyProdFreeValidityResult getApplyProdFreeValidityResult() {
        return applyProdFreeValidityResult;
    }

    /**
     * Define el valor de la propiedad applyProdFreeValidityResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplyProdFreeValidityResult }
     *     
     */
    public void setApplyProdFreeValidityResult(ApplyProdFreeValidityResult value) {
        this.applyProdFreeValidityResult = value;
    }

}
