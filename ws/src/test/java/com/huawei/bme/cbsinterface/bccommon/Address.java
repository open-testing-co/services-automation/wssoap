
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Address complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Address">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddressKey" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="Address1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Address12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="PostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address", propOrder = {
    "addressKey",
    "address1",
    "address2",
    "address3",
    "address4",
    "address5",
    "address6",
    "address7",
    "address8",
    "address9",
    "address10",
    "address11",
    "address12",
    "postCode"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.ChangeSubPaymentModeRequest.PaymentModeChange.AddressInfo.class,
    com.huawei.bme.cbsinterface.bcservices.SupplementProfileRequest.AddressInfo.class
})
public class Address {

    @XmlElement(name = "AddressKey", required = true)
    protected String addressKey;
    @XmlElement(name = "Address1")
    protected String address1;
    @XmlElement(name = "Address2")
    protected String address2;
    @XmlElement(name = "Address3")
    protected String address3;
    @XmlElement(name = "Address4")
    protected String address4;
    @XmlElement(name = "Address5")
    protected String address5;
    @XmlElement(name = "Address6")
    protected String address6;
    @XmlElement(name = "Address7")
    protected String address7;
    @XmlElement(name = "Address8")
    protected String address8;
    @XmlElement(name = "Address9")
    protected String address9;
    @XmlElement(name = "Address10")
    protected String address10;
    @XmlElement(name = "Address11")
    protected String address11;
    @XmlElement(name = "Address12")
    protected String address12;
    @XmlElement(name = "PostCode")
    protected String postCode;

    /**
     * Obtiene el valor de la propiedad addressKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressKey() {
        return addressKey;
    }

    /**
     * Define el valor de la propiedad addressKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressKey(String value) {
        this.addressKey = value;
    }

    /**
     * Obtiene el valor de la propiedad address1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Define el valor de la propiedad address1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Obtiene el valor de la propiedad address2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Define el valor de la propiedad address2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress2(String value) {
        this.address2 = value;
    }

    /**
     * Obtiene el valor de la propiedad address3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * Define el valor de la propiedad address3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress3(String value) {
        this.address3 = value;
    }

    /**
     * Obtiene el valor de la propiedad address4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress4() {
        return address4;
    }

    /**
     * Define el valor de la propiedad address4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress4(String value) {
        this.address4 = value;
    }

    /**
     * Obtiene el valor de la propiedad address5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress5() {
        return address5;
    }

    /**
     * Define el valor de la propiedad address5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress5(String value) {
        this.address5 = value;
    }

    /**
     * Obtiene el valor de la propiedad address6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress6() {
        return address6;
    }

    /**
     * Define el valor de la propiedad address6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress6(String value) {
        this.address6 = value;
    }

    /**
     * Obtiene el valor de la propiedad address7.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress7() {
        return address7;
    }

    /**
     * Define el valor de la propiedad address7.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress7(String value) {
        this.address7 = value;
    }

    /**
     * Obtiene el valor de la propiedad address8.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress8() {
        return address8;
    }

    /**
     * Define el valor de la propiedad address8.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress8(String value) {
        this.address8 = value;
    }

    /**
     * Obtiene el valor de la propiedad address9.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress9() {
        return address9;
    }

    /**
     * Define el valor de la propiedad address9.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress9(String value) {
        this.address9 = value;
    }

    /**
     * Obtiene el valor de la propiedad address10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress10() {
        return address10;
    }

    /**
     * Define el valor de la propiedad address10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress10(String value) {
        this.address10 = value;
    }

    /**
     * Obtiene el valor de la propiedad address11.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress11() {
        return address11;
    }

    /**
     * Define el valor de la propiedad address11.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress11(String value) {
        this.address11 = value;
    }

    /**
     * Obtiene el valor de la propiedad address12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress12() {
        return address12;
    }

    /**
     * Define el valor de la propiedad address12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress12(String value) {
        this.address12 = value;
    }

    /**
     * Obtiene el valor de la propiedad postCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Define el valor de la propiedad postCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostCode(String value) {
        this.postCode = value;
    }

}
