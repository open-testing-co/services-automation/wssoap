
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.PayRelExtRule;


/**
 * <p>Clase Java para QueryPaymentLimitUsageResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryPaymentLimitUsageResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LimitUsageList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
 *                   &lt;element name="LimitInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="UsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="MeasureID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryPaymentLimitUsageResult", propOrder = {
    "limitUsageList"
})
public class QueryPaymentLimitUsageResult {

    @XmlElement(name = "LimitUsageList")
    protected List<QueryPaymentLimitUsageResult.LimitUsageList> limitUsageList;

    /**
     * Gets the value of the limitUsageList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the limitUsageList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLimitUsageList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryPaymentLimitUsageResult.LimitUsageList }
     * 
     * 
     */
    public List<QueryPaymentLimitUsageResult.LimitUsageList> getLimitUsageList() {
        if (limitUsageList == null) {
            limitUsageList = new ArrayList<QueryPaymentLimitUsageResult.LimitUsageList>();
        }
        return this.limitUsageList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
     *         &lt;element name="LimitInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="UsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="MeasureID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "payRelationKey",
        "priority",
        "payRelExtRule",
        "limitInstID",
        "amount",
        "usageAmount",
        "limitType",
        "currencyID",
        "measureID"
    })
    public static class LimitUsageList {

        @XmlElement(name = "PayRelationKey", required = true)
        protected String payRelationKey;
        @XmlElement(name = "Priority", required = true)
        protected BigInteger priority;
        @XmlElement(name = "PayRelExtRule")
        protected PayRelExtRule payRelExtRule;
        @XmlElement(name = "LimitInstID")
        protected long limitInstID;
        @XmlElement(name = "Amount")
        protected long amount;
        @XmlElement(name = "UsageAmount")
        protected long usageAmount;
        @XmlElement(name = "LimitType", required = true)
        protected String limitType;
        @XmlElement(name = "CurrencyID")
        protected BigInteger currencyID;
        @XmlElement(name = "MeasureID")
        protected BigInteger measureID;

        /**
         * Obtiene el valor de la propiedad payRelationKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayRelationKey() {
            return payRelationKey;
        }

        /**
         * Define el valor de la propiedad payRelationKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayRelationKey(String value) {
            this.payRelationKey = value;
        }

        /**
         * Obtiene el valor de la propiedad priority.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getPriority() {
            return priority;
        }

        /**
         * Define el valor de la propiedad priority.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setPriority(BigInteger value) {
            this.priority = value;
        }

        /**
         * Obtiene el valor de la propiedad payRelExtRule.
         * 
         * @return
         *     possible object is
         *     {@link PayRelExtRule }
         *     
         */
        public PayRelExtRule getPayRelExtRule() {
            return payRelExtRule;
        }

        /**
         * Define el valor de la propiedad payRelExtRule.
         * 
         * @param value
         *     allowed object is
         *     {@link PayRelExtRule }
         *     
         */
        public void setPayRelExtRule(PayRelExtRule value) {
            this.payRelExtRule = value;
        }

        /**
         * Obtiene el valor de la propiedad limitInstID.
         * 
         */
        public long getLimitInstID() {
            return limitInstID;
        }

        /**
         * Define el valor de la propiedad limitInstID.
         * 
         */
        public void setLimitInstID(long value) {
            this.limitInstID = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         */
        public long getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         */
        public void setAmount(long value) {
            this.amount = value;
        }

        /**
         * Obtiene el valor de la propiedad usageAmount.
         * 
         */
        public long getUsageAmount() {
            return usageAmount;
        }

        /**
         * Define el valor de la propiedad usageAmount.
         * 
         */
        public void setUsageAmount(long value) {
            this.usageAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad limitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitType() {
            return limitType;
        }

        /**
         * Define el valor de la propiedad limitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitType(String value) {
            this.limitType = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Obtiene el valor de la propiedad measureID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMeasureID() {
            return measureID;
        }

        /**
         * Define el valor de la propiedad measureID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMeasureID(BigInteger value) {
            this.measureID = value;
        }

    }

}
