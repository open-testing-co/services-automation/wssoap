
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.EffectMode;


/**
 * <p>Clase Java para ChangeAcctCreditLimitRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeAcctCreditLimitRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctAccessCode">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
 *                 &lt;sequence>
 *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AccountCredit" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CommonCreditLimit" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="NewLimitAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TmpCreditLimit" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="OpType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="NewTmpLimitAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
 *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddAccountCredit" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="LimitAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DelAccountCredit" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeAcctCreditLimitRequest", propOrder = {
    "acctAccessCode",
    "accountCredit",
    "addAccountCredit",
    "delAccountCredit"
})
public class ChangeAcctCreditLimitRequest {

    @XmlElement(name = "AcctAccessCode", required = true)
    protected ChangeAcctCreditLimitRequest.AcctAccessCode acctAccessCode;
    @XmlElement(name = "AccountCredit")
    protected List<ChangeAcctCreditLimitRequest.AccountCredit> accountCredit;
    @XmlElement(name = "AddAccountCredit")
    protected ChangeAcctCreditLimitRequest.AddAccountCredit addAccountCredit;
    @XmlElement(name = "DelAccountCredit")
    protected ChangeAcctCreditLimitRequest.DelAccountCredit delAccountCredit;

    /**
     * Obtiene el valor de la propiedad acctAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctCreditLimitRequest.AcctAccessCode }
     *     
     */
    public ChangeAcctCreditLimitRequest.AcctAccessCode getAcctAccessCode() {
        return acctAccessCode;
    }

    /**
     * Define el valor de la propiedad acctAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctCreditLimitRequest.AcctAccessCode }
     *     
     */
    public void setAcctAccessCode(ChangeAcctCreditLimitRequest.AcctAccessCode value) {
        this.acctAccessCode = value;
    }

    /**
     * Gets the value of the accountCredit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountCredit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountCredit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeAcctCreditLimitRequest.AccountCredit }
     * 
     * 
     */
    public List<ChangeAcctCreditLimitRequest.AccountCredit> getAccountCredit() {
        if (accountCredit == null) {
            accountCredit = new ArrayList<ChangeAcctCreditLimitRequest.AccountCredit>();
        }
        return this.accountCredit;
    }

    /**
     * Obtiene el valor de la propiedad addAccountCredit.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctCreditLimitRequest.AddAccountCredit }
     *     
     */
    public ChangeAcctCreditLimitRequest.AddAccountCredit getAddAccountCredit() {
        return addAccountCredit;
    }

    /**
     * Define el valor de la propiedad addAccountCredit.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctCreditLimitRequest.AddAccountCredit }
     *     
     */
    public void setAddAccountCredit(ChangeAcctCreditLimitRequest.AddAccountCredit value) {
        this.addAccountCredit = value;
    }

    /**
     * Obtiene el valor de la propiedad delAccountCredit.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctCreditLimitRequest.DelAccountCredit }
     *     
     */
    public ChangeAcctCreditLimitRequest.DelAccountCredit getDelAccountCredit() {
        return delAccountCredit;
    }

    /**
     * Define el valor de la propiedad delAccountCredit.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctCreditLimitRequest.DelAccountCredit }
     *     
     */
    public void setDelAccountCredit(ChangeAcctCreditLimitRequest.DelAccountCredit value) {
        this.delAccountCredit = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CommonCreditLimit" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="NewLimitAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TmpCreditLimit" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="OpType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="NewTmpLimitAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
     *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "creditLimitType",
        "commonCreditLimit",
        "tmpCreditLimit"
    })
    public static class AccountCredit {

        @XmlElement(name = "CreditLimitType")
        protected String creditLimitType;
        @XmlElement(name = "CommonCreditLimit")
        protected ChangeAcctCreditLimitRequest.AccountCredit.CommonCreditLimit commonCreditLimit;
        @XmlElement(name = "TmpCreditLimit")
        protected ChangeAcctCreditLimitRequest.AccountCredit.TmpCreditLimit tmpCreditLimit;

        /**
         * Obtiene el valor de la propiedad creditLimitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreditLimitType() {
            return creditLimitType;
        }

        /**
         * Define el valor de la propiedad creditLimitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreditLimitType(String value) {
            this.creditLimitType = value;
        }

        /**
         * Obtiene el valor de la propiedad commonCreditLimit.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctCreditLimitRequest.AccountCredit.CommonCreditLimit }
         *     
         */
        public ChangeAcctCreditLimitRequest.AccountCredit.CommonCreditLimit getCommonCreditLimit() {
            return commonCreditLimit;
        }

        /**
         * Define el valor de la propiedad commonCreditLimit.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctCreditLimitRequest.AccountCredit.CommonCreditLimit }
         *     
         */
        public void setCommonCreditLimit(ChangeAcctCreditLimitRequest.AccountCredit.CommonCreditLimit value) {
            this.commonCreditLimit = value;
        }

        /**
         * Obtiene el valor de la propiedad tmpCreditLimit.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctCreditLimitRequest.AccountCredit.TmpCreditLimit }
         *     
         */
        public ChangeAcctCreditLimitRequest.AccountCredit.TmpCreditLimit getTmpCreditLimit() {
            return tmpCreditLimit;
        }

        /**
         * Define el valor de la propiedad tmpCreditLimit.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctCreditLimitRequest.AccountCredit.TmpCreditLimit }
         *     
         */
        public void setTmpCreditLimit(ChangeAcctCreditLimitRequest.AccountCredit.TmpCreditLimit value) {
            this.tmpCreditLimit = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="NewLimitAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "newLimitAmount",
            "effectiveTime"
        })
        public static class CommonCreditLimit {

            @XmlElement(name = "NewLimitAmount")
            protected long newLimitAmount;
            @XmlElement(name = "EffectiveTime", required = true)
            protected EffectMode effectiveTime;

            /**
             * Obtiene el valor de la propiedad newLimitAmount.
             * 
             */
            public long getNewLimitAmount() {
                return newLimitAmount;
            }

            /**
             * Define el valor de la propiedad newLimitAmount.
             * 
             */
            public void setNewLimitAmount(long value) {
                this.newLimitAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link EffectMode }
             *     
             */
            public EffectMode getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link EffectMode }
             *     
             */
            public void setEffectiveTime(EffectMode value) {
                this.effectiveTime = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="OpType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="NewTmpLimitAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
         *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "opType",
            "creditInstID",
            "newTmpLimitAmount",
            "effectiveTime",
            "expirationTime"
        })
        public static class TmpCreditLimit {

            @XmlElement(name = "OpType")
            protected String opType;
            @XmlElement(name = "CreditInstID")
            protected Long creditInstID;
            @XmlElement(name = "NewTmpLimitAmount")
            protected Long newTmpLimitAmount;
            @XmlElement(name = "EffectiveTime")
            protected EffectMode effectiveTime;
            @XmlElement(name = "ExpirationTime")
            protected String expirationTime;

            /**
             * Obtiene el valor de la propiedad opType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOpType() {
                return opType;
            }

            /**
             * Define el valor de la propiedad opType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOpType(String value) {
                this.opType = value;
            }

            /**
             * Obtiene el valor de la propiedad creditInstID.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getCreditInstID() {
                return creditInstID;
            }

            /**
             * Define el valor de la propiedad creditInstID.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setCreditInstID(Long value) {
                this.creditInstID = value;
            }

            /**
             * Obtiene el valor de la propiedad newTmpLimitAmount.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getNewTmpLimitAmount() {
                return newTmpLimitAmount;
            }

            /**
             * Define el valor de la propiedad newTmpLimitAmount.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setNewTmpLimitAmount(Long value) {
                this.newTmpLimitAmount = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link EffectMode }
             *     
             */
            public EffectMode getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link EffectMode }
             *     
             */
            public void setEffectiveTime(EffectMode value) {
                this.effectiveTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationTime(String value) {
                this.expirationTime = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
     *       &lt;sequence>
     *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "payType"
    })
    public static class AcctAccessCode
        extends com.huawei.bme.cbsinterface.bccommon.AcctAccessCode
    {

        @XmlElement(name = "PayType")
        protected String payType;

        /**
         * Obtiene el valor de la propiedad payType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayType() {
            return payType;
        }

        /**
         * Define el valor de la propiedad payType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayType(String value) {
            this.payType = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="LimitAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "creditLimitType",
        "limitAmount",
        "effectiveTime"
    })
    public static class AddAccountCredit {

        @XmlElement(name = "CreditLimitType", required = true)
        protected String creditLimitType;
        @XmlElement(name = "LimitAmount")
        protected long limitAmount;
        @XmlElement(name = "EffectiveTime")
        protected EffectMode effectiveTime;

        /**
         * Obtiene el valor de la propiedad creditLimitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreditLimitType() {
            return creditLimitType;
        }

        /**
         * Define el valor de la propiedad creditLimitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreditLimitType(String value) {
            this.creditLimitType = value;
        }

        /**
         * Obtiene el valor de la propiedad limitAmount.
         * 
         */
        public long getLimitAmount() {
            return limitAmount;
        }

        /**
         * Define el valor de la propiedad limitAmount.
         * 
         */
        public void setLimitAmount(long value) {
            this.limitAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveTime.
         * 
         * @return
         *     possible object is
         *     {@link EffectMode }
         *     
         */
        public EffectMode getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Define el valor de la propiedad effectiveTime.
         * 
         * @param value
         *     allowed object is
         *     {@link EffectMode }
         *     
         */
        public void setEffectiveTime(EffectMode value) {
            this.effectiveTime = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "creditLimitType",
        "expirationTime"
    })
    public static class DelAccountCredit {

        @XmlElement(name = "CreditLimitType", required = true)
        protected String creditLimitType;
        @XmlElement(name = "ExpirationTime")
        protected String expirationTime;

        /**
         * Obtiene el valor de la propiedad creditLimitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreditLimitType() {
            return creditLimitType;
        }

        /**
         * Define el valor de la propiedad creditLimitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreditLimitType(String value) {
            this.creditLimitType = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationTime() {
            return expirationTime;
        }

        /**
         * Define el valor de la propiedad expirationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationTime(String value) {
            this.expirationTime = value;
        }

    }

}
