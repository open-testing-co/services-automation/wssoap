
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CleanUpCustDataRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CleanUpCustDataRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegisterCustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CleanUpCustDataRequest", propOrder = {
    "registerCustKey"
})
public class CleanUpCustDataRequest {

    @XmlElement(name = "RegisterCustKey", required = true)
    protected String registerCustKey;

    /**
     * Obtiene el valor de la propiedad registerCustKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterCustKey() {
        return registerCustKey;
    }

    /**
     * Define el valor de la propiedad registerCustKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterCustKey(String value) {
        this.registerCustKey = value;
    }

}
