
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.ActiveMode;
import com.huawei.bme.cbsinterface.bccommon.Address;
import com.huawei.bme.cbsinterface.bccommon.CustInfo;
import com.huawei.bme.cbsinterface.bccommon.EffectMode;
import com.huawei.bme.cbsinterface.bccommon.IndividualInfo;
import com.huawei.bme.cbsinterface.bccommon.OfferingInst;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;
import com.huawei.bme.cbsinterface.bccommon.OrgInfo;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;


/**
 * <p>Clase Java para ChangeAcctOwnershipRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeAcctOwnershipRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OldOwnership">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Account">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PrepaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PostpaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NewOwnership">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RegisterCustomer">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
 *                             &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                             &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="OpType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="UserCustomer" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
 *                             &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                             &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Account" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PrepaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PostpaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OldAutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="NewAutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Subscriber" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="SubPayRelation" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ShiftPayRelation" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PrimaryOffering" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="SupplementaryOffering" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ShiftOffering" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OldOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                                       &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
 *                                     &lt;sequence>
 *                                       &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
 *                                       &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/extension>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="ControlProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeAcctOwnershipRequest", propOrder = {
    "oldOwnership",
    "newOwnership"
})
public class ChangeAcctOwnershipRequest {

    @XmlElement(name = "OldOwnership", required = true)
    protected ChangeAcctOwnershipRequest.OldOwnership oldOwnership;
    @XmlElement(name = "NewOwnership", required = true)
    protected ChangeAcctOwnershipRequest.NewOwnership newOwnership;

    /**
     * Obtiene el valor de la propiedad oldOwnership.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctOwnershipRequest.OldOwnership }
     *     
     */
    public ChangeAcctOwnershipRequest.OldOwnership getOldOwnership() {
        return oldOwnership;
    }

    /**
     * Define el valor de la propiedad oldOwnership.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctOwnershipRequest.OldOwnership }
     *     
     */
    public void setOldOwnership(ChangeAcctOwnershipRequest.OldOwnership value) {
        this.oldOwnership = value;
    }

    /**
     * Obtiene el valor de la propiedad newOwnership.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctOwnershipRequest.NewOwnership }
     *     
     */
    public ChangeAcctOwnershipRequest.NewOwnership getNewOwnership() {
        return newOwnership;
    }

    /**
     * Define el valor de la propiedad newOwnership.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctOwnershipRequest.NewOwnership }
     *     
     */
    public void setNewOwnership(ChangeAcctOwnershipRequest.NewOwnership value) {
        this.newOwnership = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RegisterCustomer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
     *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="OpType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="UserCustomer" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
     *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Account" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PrepaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PostpaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OldAutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="NewAutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Subscriber" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="SubPayRelation" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ShiftPayRelation" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PrimaryOffering" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="SupplementaryOffering" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ShiftOffering" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OldOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *                             &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
     *                           &lt;sequence>
     *                             &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
     *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/extension>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="ControlProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "registerCustomer",
        "userCustomer",
        "account",
        "subscriber",
        "primaryOffering",
        "supplementaryOffering",
        "addressInfo",
        "controlProperty"
    })
    public static class NewOwnership {

        @XmlElement(name = "RegisterCustomer", required = true)
        protected ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer registerCustomer;
        @XmlElement(name = "UserCustomer")
        protected ChangeAcctOwnershipRequest.NewOwnership.UserCustomer userCustomer;
        @XmlElement(name = "Account")
        protected ChangeAcctOwnershipRequest.NewOwnership.Account account;
        @XmlElement(name = "Subscriber")
        protected ChangeAcctOwnershipRequest.NewOwnership.Subscriber subscriber;
        @XmlElement(name = "PrimaryOffering")
        protected ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering primaryOffering;
        @XmlElement(name = "SupplementaryOffering")
        protected ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering supplementaryOffering;
        @XmlElement(name = "AddressInfo")
        protected List<Address> addressInfo;
        @XmlElement(name = "ControlProperty")
        protected List<SimpleProperty> controlProperty;

        /**
         * Obtiene el valor de la propiedad registerCustomer.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer }
         *     
         */
        public ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer getRegisterCustomer() {
            return registerCustomer;
        }

        /**
         * Define el valor de la propiedad registerCustomer.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer }
         *     
         */
        public void setRegisterCustomer(ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer value) {
            this.registerCustomer = value;
        }

        /**
         * Obtiene el valor de la propiedad userCustomer.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.UserCustomer }
         *     
         */
        public ChangeAcctOwnershipRequest.NewOwnership.UserCustomer getUserCustomer() {
            return userCustomer;
        }

        /**
         * Define el valor de la propiedad userCustomer.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.UserCustomer }
         *     
         */
        public void setUserCustomer(ChangeAcctOwnershipRequest.NewOwnership.UserCustomer value) {
            this.userCustomer = value;
        }

        /**
         * Obtiene el valor de la propiedad account.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.Account }
         *     
         */
        public ChangeAcctOwnershipRequest.NewOwnership.Account getAccount() {
            return account;
        }

        /**
         * Define el valor de la propiedad account.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.Account }
         *     
         */
        public void setAccount(ChangeAcctOwnershipRequest.NewOwnership.Account value) {
            this.account = value;
        }

        /**
         * Obtiene el valor de la propiedad subscriber.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.Subscriber }
         *     
         */
        public ChangeAcctOwnershipRequest.NewOwnership.Subscriber getSubscriber() {
            return subscriber;
        }

        /**
         * Define el valor de la propiedad subscriber.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.Subscriber }
         *     
         */
        public void setSubscriber(ChangeAcctOwnershipRequest.NewOwnership.Subscriber value) {
            this.subscriber = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryOffering.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering }
         *     
         */
        public ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering getPrimaryOffering() {
            return primaryOffering;
        }

        /**
         * Define el valor de la propiedad primaryOffering.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering }
         *     
         */
        public void setPrimaryOffering(ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering value) {
            this.primaryOffering = value;
        }

        /**
         * Obtiene el valor de la propiedad supplementaryOffering.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering }
         *     
         */
        public ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering getSupplementaryOffering() {
            return supplementaryOffering;
        }

        /**
         * Define el valor de la propiedad supplementaryOffering.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering }
         *     
         */
        public void setSupplementaryOffering(ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering value) {
            this.supplementaryOffering = value;
        }

        /**
         * Gets the value of the addressInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addressInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddressInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Address }
         * 
         * 
         */
        public List<Address> getAddressInfo() {
            if (addressInfo == null) {
                addressInfo = new ArrayList<Address>();
            }
            return this.addressInfo;
        }

        /**
         * Gets the value of the controlProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the controlProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getControlProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getControlProperty() {
            if (controlProperty == null) {
                controlProperty = new ArrayList<SimpleProperty>();
            }
            return this.controlProperty;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PrepaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PostpaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OldAutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="NewAutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "prepaidAcctKey",
            "postpaidAcctKey",
            "autoPayChannel"
        })
        public static class Account {

            @XmlElement(name = "PrepaidAcctKey")
            protected String prepaidAcctKey;
            @XmlElement(name = "PostpaidAcctKey")
            protected String postpaidAcctKey;
            @XmlElement(name = "AutoPayChannel")
            protected List<ChangeAcctOwnershipRequest.NewOwnership.Account.AutoPayChannel> autoPayChannel;

            /**
             * Obtiene el valor de la propiedad prepaidAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrepaidAcctKey() {
                return prepaidAcctKey;
            }

            /**
             * Define el valor de la propiedad prepaidAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrepaidAcctKey(String value) {
                this.prepaidAcctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad postpaidAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostpaidAcctKey() {
                return postpaidAcctKey;
            }

            /**
             * Define el valor de la propiedad postpaidAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostpaidAcctKey(String value) {
                this.postpaidAcctKey = value;
            }

            /**
             * Gets the value of the autoPayChannel property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the autoPayChannel property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAutoPayChannel().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeAcctOwnershipRequest.NewOwnership.Account.AutoPayChannel }
             * 
             * 
             */
            public List<ChangeAcctOwnershipRequest.NewOwnership.Account.AutoPayChannel> getAutoPayChannel() {
                if (autoPayChannel == null) {
                    autoPayChannel = new ArrayList<ChangeAcctOwnershipRequest.NewOwnership.Account.AutoPayChannel>();
                }
                return this.autoPayChannel;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OldAutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="NewAutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "oldAutoPayChannelKey",
                "newAutoPayChannelKey"
            })
            public static class AutoPayChannel {

                @XmlElement(name = "OldAutoPayChannelKey", required = true)
                protected String oldAutoPayChannelKey;
                @XmlElement(name = "NewAutoPayChannelKey", required = true)
                protected String newAutoPayChannelKey;

                /**
                 * Obtiene el valor de la propiedad oldAutoPayChannelKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOldAutoPayChannelKey() {
                    return oldAutoPayChannelKey;
                }

                /**
                 * Define el valor de la propiedad oldAutoPayChannelKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOldAutoPayChannelKey(String value) {
                    this.oldAutoPayChannelKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad newAutoPayChannelKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNewAutoPayChannelKey() {
                    return newAutoPayChannelKey;
                }

                /**
                 * Define el valor de la propiedad newAutoPayChannelKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNewAutoPayChannelKey(String value) {
                    this.newAutoPayChannelKey = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "newOfferingKey"
        })
        public static class PrimaryOffering {

            @XmlElement(name = "NewOfferingKey")
            protected OfferingKey newOfferingKey;

            /**
             * Obtiene el valor de la propiedad newOfferingKey.
             * 
             * @return
             *     possible object is
             *     {@link OfferingKey }
             *     
             */
            public OfferingKey getNewOfferingKey() {
                return newOfferingKey;
            }

            /**
             * Define el valor de la propiedad newOfferingKey.
             * 
             * @param value
             *     allowed object is
             *     {@link OfferingKey }
             *     
             */
            public void setNewOfferingKey(OfferingKey value) {
                this.newOfferingKey = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
         *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
         *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
         *       &lt;/sequence>
         *       &lt;attribute name="OpType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "custKey",
            "custInfo",
            "individualInfo",
            "orgInfo"
        })
        public static class RegisterCustomer {

            @XmlElement(name = "CustKey", required = true)
            protected String custKey;
            @XmlElement(name = "CustInfo")
            protected CustInfo custInfo;
            @XmlElement(name = "IndividualInfo")
            protected IndividualInfo individualInfo;
            @XmlElement(name = "OrgInfo")
            protected OrgInfo orgInfo;
            @XmlAttribute(name = "OpType", required = true)
            protected String opType;

            /**
             * Obtiene el valor de la propiedad custKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustKey() {
                return custKey;
            }

            /**
             * Define el valor de la propiedad custKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustKey(String value) {
                this.custKey = value;
            }

            /**
             * Obtiene el valor de la propiedad custInfo.
             * 
             * @return
             *     possible object is
             *     {@link CustInfo }
             *     
             */
            public CustInfo getCustInfo() {
                return custInfo;
            }

            /**
             * Define el valor de la propiedad custInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link CustInfo }
             *     
             */
            public void setCustInfo(CustInfo value) {
                this.custInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad individualInfo.
             * 
             * @return
             *     possible object is
             *     {@link IndividualInfo }
             *     
             */
            public IndividualInfo getIndividualInfo() {
                return individualInfo;
            }

            /**
             * Define el valor de la propiedad individualInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link IndividualInfo }
             *     
             */
            public void setIndividualInfo(IndividualInfo value) {
                this.individualInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad orgInfo.
             * 
             * @return
             *     possible object is
             *     {@link OrgInfo }
             *     
             */
            public OrgInfo getOrgInfo() {
                return orgInfo;
            }

            /**
             * Define el valor de la propiedad orgInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OrgInfo }
             *     
             */
            public void setOrgInfo(OrgInfo value) {
                this.orgInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad opType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOpType() {
                return opType;
            }

            /**
             * Define el valor de la propiedad opType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOpType(String value) {
                this.opType = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="SubPayRelation" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ShiftPayRelation" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "subscriberKey",
            "userCustomerKey",
            "writtenLang",
            "ivrLang",
            "subPassword",
            "subPayRelation"
        })
        public static class Subscriber {

            @XmlElement(name = "SubscriberKey")
            protected String subscriberKey;
            @XmlElement(name = "UserCustomerKey")
            protected String userCustomerKey;
            @XmlElement(name = "WrittenLang")
            protected String writtenLang;
            @XmlElement(name = "IVRLang")
            protected String ivrLang;
            @XmlElement(name = "SubPassword")
            protected String subPassword;
            @XmlElement(name = "SubPayRelation")
            protected ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation subPayRelation;

            /**
             * Obtiene el valor de la propiedad subscriberKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubscriberKey() {
                return subscriberKey;
            }

            /**
             * Define el valor de la propiedad subscriberKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubscriberKey(String value) {
                this.subscriberKey = value;
            }

            /**
             * Obtiene el valor de la propiedad userCustomerKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUserCustomerKey() {
                return userCustomerKey;
            }

            /**
             * Define el valor de la propiedad userCustomerKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUserCustomerKey(String value) {
                this.userCustomerKey = value;
            }

            /**
             * Obtiene el valor de la propiedad writtenLang.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getWrittenLang() {
                return writtenLang;
            }

            /**
             * Define el valor de la propiedad writtenLang.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setWrittenLang(String value) {
                this.writtenLang = value;
            }

            /**
             * Obtiene el valor de la propiedad ivrLang.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIVRLang() {
                return ivrLang;
            }

            /**
             * Define el valor de la propiedad ivrLang.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIVRLang(String value) {
                this.ivrLang = value;
            }

            /**
             * Obtiene el valor de la propiedad subPassword.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubPassword() {
                return subPassword;
            }

            /**
             * Define el valor de la propiedad subPassword.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubPassword(String value) {
                this.subPassword = value;
            }

            /**
             * Obtiene el valor de la propiedad subPayRelation.
             * 
             * @return
             *     possible object is
             *     {@link ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation }
             *     
             */
            public ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation getSubPayRelation() {
                return subPayRelation;
            }

            /**
             * Define el valor de la propiedad subPayRelation.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation }
             *     
             */
            public void setSubPayRelation(ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation value) {
                this.subPayRelation = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ShiftPayRelation" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "shiftPayRelation"
            })
            public static class SubPayRelation {

                @XmlElement(name = "ShiftPayRelation")
                protected List<ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation.ShiftPayRelation> shiftPayRelation;

                /**
                 * Gets the value of the shiftPayRelation property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the shiftPayRelation property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getShiftPayRelation().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation.ShiftPayRelation }
                 * 
                 * 
                 */
                public List<ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation.ShiftPayRelation> getShiftPayRelation() {
                    if (shiftPayRelation == null) {
                        shiftPayRelation = new ArrayList<ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation.ShiftPayRelation>();
                    }
                    return this.shiftPayRelation;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "oldPayRelationKey",
                    "newPayRelationKey"
                })
                public static class ShiftPayRelation {

                    @XmlElement(name = "OldPayRelationKey", required = true)
                    protected String oldPayRelationKey;
                    @XmlElement(name = "NewPayRelationKey", required = true)
                    protected String newPayRelationKey;

                    /**
                     * Obtiene el valor de la propiedad oldPayRelationKey.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOldPayRelationKey() {
                        return oldPayRelationKey;
                    }

                    /**
                     * Define el valor de la propiedad oldPayRelationKey.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOldPayRelationKey(String value) {
                        this.oldPayRelationKey = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad newPayRelationKey.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNewPayRelationKey() {
                        return newPayRelationKey;
                    }

                    /**
                     * Define el valor de la propiedad newPayRelationKey.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNewPayRelationKey(String value) {
                        this.newPayRelationKey = value;
                    }

                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ShiftOffering" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OldOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
         *                   &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
         *                 &lt;sequence>
         *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
         *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/extension>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "shiftOffering",
            "delOffering",
            "addOffering"
        })
        public static class SupplementaryOffering {

            @XmlElement(name = "ShiftOffering")
            protected List<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering> shiftOffering;
            @XmlElement(name = "DelOffering")
            protected List<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering> delOffering;
            @XmlElement(name = "AddOffering")
            protected List<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering> addOffering;

            /**
             * Gets the value of the shiftOffering property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the shiftOffering property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getShiftOffering().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering }
             * 
             * 
             */
            public List<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering> getShiftOffering() {
                if (shiftOffering == null) {
                    shiftOffering = new ArrayList<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering>();
                }
                return this.shiftOffering;
            }

            /**
             * Gets the value of the delOffering property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the delOffering property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDelOffering().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering }
             * 
             * 
             */
            public List<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering> getDelOffering() {
                if (delOffering == null) {
                    delOffering = new ArrayList<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering>();
                }
                return this.delOffering;
            }

            /**
             * Gets the value of the addOffering property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the addOffering property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddOffering().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering }
             * 
             * 
             */
            public List<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering> getAddOffering() {
                if (addOffering == null) {
                    addOffering = new ArrayList<ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering>();
                }
                return this.addOffering;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
             *       &lt;sequence>
             *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
             *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/extension>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "effectiveTime",
                "expirationTime",
                "activationTime"
            })
            public static class AddOffering
                extends OfferingInst
            {

                @XmlElement(name = "EffectiveTime", required = true)
                protected EffectMode effectiveTime;
                @XmlElement(name = "ExpirationTime", required = true)
                protected String expirationTime;
                @XmlElement(name = "ActivationTime")
                protected ActiveMode activationTime;

                /**
                 * Obtiene el valor de la propiedad effectiveTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link EffectMode }
                 *     
                 */
                public EffectMode getEffectiveTime() {
                    return effectiveTime;
                }

                /**
                 * Define el valor de la propiedad effectiveTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link EffectMode }
                 *     
                 */
                public void setEffectiveTime(EffectMode value) {
                    this.effectiveTime = value;
                }

                /**
                 * Obtiene el valor de la propiedad expirationTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExpirationTime() {
                    return expirationTime;
                }

                /**
                 * Define el valor de la propiedad expirationTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExpirationTime(String value) {
                    this.expirationTime = value;
                }

                /**
                 * Obtiene el valor de la propiedad activationTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActiveMode }
                 *     
                 */
                public ActiveMode getActivationTime() {
                    return activationTime;
                }

                /**
                 * Define el valor de la propiedad activationTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActiveMode }
                 *     
                 */
                public void setActivationTime(ActiveMode value) {
                    this.activationTime = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offeringKey"
            })
            public static class DelOffering {

                @XmlElement(name = "OfferingKey", required = true)
                protected OfferingKey offeringKey;

                /**
                 * Obtiene el valor de la propiedad offeringKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getOfferingKey() {
                    return offeringKey;
                }

                /**
                 * Define el valor de la propiedad offeringKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setOfferingKey(OfferingKey value) {
                    this.offeringKey = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OldOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
             *         &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "oldOfferingKey",
                "newOfferingKey"
            })
            public static class ShiftOffering {

                @XmlElement(name = "OldOfferingKey", required = true, nillable = true)
                protected OfferingKey oldOfferingKey;
                @XmlElement(name = "NewOfferingKey", required = true, nillable = true)
                protected OfferingKey newOfferingKey;

                /**
                 * Obtiene el valor de la propiedad oldOfferingKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getOldOfferingKey() {
                    return oldOfferingKey;
                }

                /**
                 * Define el valor de la propiedad oldOfferingKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setOldOfferingKey(OfferingKey value) {
                    this.oldOfferingKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad newOfferingKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getNewOfferingKey() {
                    return newOfferingKey;
                }

                /**
                 * Define el valor de la propiedad newOfferingKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setNewOfferingKey(OfferingKey value) {
                    this.newOfferingKey = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
         *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
         *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "custKey",
            "custInfo",
            "individualInfo",
            "orgInfo"
        })
        public static class UserCustomer {

            @XmlElement(name = "CustKey", required = true)
            protected String custKey;
            @XmlElement(name = "CustInfo")
            protected CustInfo custInfo;
            @XmlElement(name = "IndividualInfo")
            protected IndividualInfo individualInfo;
            @XmlElement(name = "OrgInfo")
            protected OrgInfo orgInfo;

            /**
             * Obtiene el valor de la propiedad custKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustKey() {
                return custKey;
            }

            /**
             * Define el valor de la propiedad custKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustKey(String value) {
                this.custKey = value;
            }

            /**
             * Obtiene el valor de la propiedad custInfo.
             * 
             * @return
             *     possible object is
             *     {@link CustInfo }
             *     
             */
            public CustInfo getCustInfo() {
                return custInfo;
            }

            /**
             * Define el valor de la propiedad custInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link CustInfo }
             *     
             */
            public void setCustInfo(CustInfo value) {
                this.custInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad individualInfo.
             * 
             * @return
             *     possible object is
             *     {@link IndividualInfo }
             *     
             */
            public IndividualInfo getIndividualInfo() {
                return individualInfo;
            }

            /**
             * Define el valor de la propiedad individualInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link IndividualInfo }
             *     
             */
            public void setIndividualInfo(IndividualInfo value) {
                this.individualInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad orgInfo.
             * 
             * @return
             *     possible object is
             *     {@link OrgInfo }
             *     
             */
            public OrgInfo getOrgInfo() {
                return orgInfo;
            }

            /**
             * Define el valor de la propiedad orgInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OrgInfo }
             *     
             */
            public void setOrgInfo(OrgInfo value) {
                this.orgInfo = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Account">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PrepaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PostpaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "account",
        "subscriberKey"
    })
    public static class OldOwnership {

        @XmlElement(name = "Account", required = true)
        protected ChangeAcctOwnershipRequest.OldOwnership.Account account;
        @XmlElement(name = "SubscriberKey", required = true)
        protected String subscriberKey;

        /**
         * Obtiene el valor de la propiedad account.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctOwnershipRequest.OldOwnership.Account }
         *     
         */
        public ChangeAcctOwnershipRequest.OldOwnership.Account getAccount() {
            return account;
        }

        /**
         * Define el valor de la propiedad account.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctOwnershipRequest.OldOwnership.Account }
         *     
         */
        public void setAccount(ChangeAcctOwnershipRequest.OldOwnership.Account value) {
            this.account = value;
        }

        /**
         * Obtiene el valor de la propiedad subscriberKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubscriberKey() {
            return subscriberKey;
        }

        /**
         * Define el valor de la propiedad subscriberKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubscriberKey(String value) {
            this.subscriberKey = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PrepaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PostpaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "prepaidAcctKey",
            "postpaidAcctKey"
        })
        public static class Account {

            @XmlElement(name = "PrepaidAcctKey")
            protected String prepaidAcctKey;
            @XmlElement(name = "PostpaidAcctKey")
            protected String postpaidAcctKey;

            /**
             * Obtiene el valor de la propiedad prepaidAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrepaidAcctKey() {
                return prepaidAcctKey;
            }

            /**
             * Define el valor de la propiedad prepaidAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrepaidAcctKey(String value) {
                this.prepaidAcctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad postpaidAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostpaidAcctKey() {
                return postpaidAcctKey;
            }

            /**
             * Define el valor de la propiedad postpaidAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostpaidAcctKey(String value) {
                this.postpaidAcctKey = value;
            }

        }

    }

}
