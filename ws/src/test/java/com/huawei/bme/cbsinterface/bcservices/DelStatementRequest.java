
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;


/**
 * <p>Clase Java para DelStatementRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DelStatementRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegisterCust" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode"/>
 *         &lt;element name="SmtKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DelStatementRequest", propOrder = {
    "registerCust",
    "smtKey"
})
public class DelStatementRequest {

    @XmlElement(name = "RegisterCust", required = true)
    protected CustAccessCode registerCust;
    @XmlElement(name = "SmtKey", required = true)
    protected String smtKey;

    /**
     * Obtiene el valor de la propiedad registerCust.
     * 
     * @return
     *     possible object is
     *     {@link CustAccessCode }
     *     
     */
    public CustAccessCode getRegisterCust() {
        return registerCust;
    }

    /**
     * Define el valor de la propiedad registerCust.
     * 
     * @param value
     *     allowed object is
     *     {@link CustAccessCode }
     *     
     */
    public void setRegisterCust(CustAccessCode value) {
        this.registerCust = value;
    }

    /**
     * Obtiene el valor de la propiedad smtKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmtKey() {
        return smtKey;
    }

    /**
     * Define el valor de la propiedad smtKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmtKey(String value) {
        this.smtKey = value;
    }

}
