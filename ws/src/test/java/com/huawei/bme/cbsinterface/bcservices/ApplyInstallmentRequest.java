
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;


/**
 * <p>Clase Java para ApplyInstallmentRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ApplyInstallmentRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplyObj">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
 *                   &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
 *                   &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="TotalCycle" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="CycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CycleLength" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="CycleRefDate" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InatallmentPlan">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PlanType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FirstCycleAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="FinalCycleAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplyInstallmentRequest", propOrder = {
    "applyObj",
    "contractID",
    "totalAmount",
    "currencyID",
    "totalCycle",
    "cycleType",
    "cycleLength",
    "cycleRefDate",
    "chargeCode",
    "inatallmentPlan",
    "additionalProperty"
})
public class ApplyInstallmentRequest {

    @XmlElement(name = "ApplyObj", required = true)
    protected ApplyInstallmentRequest.ApplyObj applyObj;
    @XmlElement(name = "ContractID", required = true)
    protected String contractID;
    @XmlElement(name = "TotalAmount")
    protected long totalAmount;
    @XmlElement(name = "CurrencyID", required = true)
    protected BigInteger currencyID;
    @XmlElement(name = "TotalCycle", required = true)
    protected BigInteger totalCycle;
    @XmlElement(name = "CycleType", required = true)
    protected String cycleType;
    @XmlElement(name = "CycleLength", required = true)
    protected BigInteger cycleLength;
    @XmlElement(name = "CycleRefDate")
    protected BigInteger cycleRefDate;
    @XmlElement(name = "ChargeCode")
    protected String chargeCode;
    @XmlElement(name = "InatallmentPlan", required = true)
    protected ApplyInstallmentRequest.InatallmentPlan inatallmentPlan;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;

    /**
     * Obtiene el valor de la propiedad applyObj.
     * 
     * @return
     *     possible object is
     *     {@link ApplyInstallmentRequest.ApplyObj }
     *     
     */
    public ApplyInstallmentRequest.ApplyObj getApplyObj() {
        return applyObj;
    }

    /**
     * Define el valor de la propiedad applyObj.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplyInstallmentRequest.ApplyObj }
     *     
     */
    public void setApplyObj(ApplyInstallmentRequest.ApplyObj value) {
        this.applyObj = value;
    }

    /**
     * Obtiene el valor de la propiedad contractID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractID() {
        return contractID;
    }

    /**
     * Define el valor de la propiedad contractID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractID(String value) {
        this.contractID = value;
    }

    /**
     * Obtiene el valor de la propiedad totalAmount.
     * 
     */
    public long getTotalAmount() {
        return totalAmount;
    }

    /**
     * Define el valor de la propiedad totalAmount.
     * 
     */
    public void setTotalAmount(long value) {
        this.totalAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrencyID() {
        return currencyID;
    }

    /**
     * Define el valor de la propiedad currencyID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrencyID(BigInteger value) {
        this.currencyID = value;
    }

    /**
     * Obtiene el valor de la propiedad totalCycle.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalCycle() {
        return totalCycle;
    }

    /**
     * Define el valor de la propiedad totalCycle.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalCycle(BigInteger value) {
        this.totalCycle = value;
    }

    /**
     * Obtiene el valor de la propiedad cycleType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCycleType() {
        return cycleType;
    }

    /**
     * Define el valor de la propiedad cycleType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCycleType(String value) {
        this.cycleType = value;
    }

    /**
     * Obtiene el valor de la propiedad cycleLength.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCycleLength() {
        return cycleLength;
    }

    /**
     * Define el valor de la propiedad cycleLength.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCycleLength(BigInteger value) {
        this.cycleLength = value;
    }

    /**
     * Obtiene el valor de la propiedad cycleRefDate.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCycleRefDate() {
        return cycleRefDate;
    }

    /**
     * Define el valor de la propiedad cycleRefDate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCycleRefDate(BigInteger value) {
        this.cycleRefDate = value;
    }

    /**
     * Obtiene el valor de la propiedad chargeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * Define el valor de la propiedad chargeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeCode(String value) {
        this.chargeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad inatallmentPlan.
     * 
     * @return
     *     possible object is
     *     {@link ApplyInstallmentRequest.InatallmentPlan }
     *     
     */
    public ApplyInstallmentRequest.InatallmentPlan getInatallmentPlan() {
        return inatallmentPlan;
    }

    /**
     * Define el valor de la propiedad inatallmentPlan.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplyInstallmentRequest.InatallmentPlan }
     *     
     */
    public void setInatallmentPlan(ApplyInstallmentRequest.InatallmentPlan value) {
        this.inatallmentPlan = value;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
     *         &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
     *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subAccessCode",
        "custAccessCode",
        "subGroupAccessCode"
    })
    public static class ApplyObj {

        @XmlElement(name = "SubAccessCode")
        protected SubAccessCode subAccessCode;
        @XmlElement(name = "CustAccessCode")
        protected CustAccessCode custAccessCode;
        @XmlElement(name = "SubGroupAccessCode")
        protected SubGroupAccessCode subGroupAccessCode;

        /**
         * Obtiene el valor de la propiedad subAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubAccessCode }
         *     
         */
        public SubAccessCode getSubAccessCode() {
            return subAccessCode;
        }

        /**
         * Define el valor de la propiedad subAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubAccessCode }
         *     
         */
        public void setSubAccessCode(SubAccessCode value) {
            this.subAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad custAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link CustAccessCode }
         *     
         */
        public CustAccessCode getCustAccessCode() {
            return custAccessCode;
        }

        /**
         * Define el valor de la propiedad custAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link CustAccessCode }
         *     
         */
        public void setCustAccessCode(CustAccessCode value) {
            this.custAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public SubGroupAccessCode getSubGroupAccessCode() {
            return subGroupAccessCode;
        }

        /**
         * Define el valor de la propiedad subGroupAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public void setSubGroupAccessCode(SubGroupAccessCode value) {
            this.subGroupAccessCode = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PlanType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FirstCycleAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="FinalCycleAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "planType",
        "firstCycleAmount",
        "finalCycleAmount"
    })
    public static class InatallmentPlan {

        @XmlElement(name = "PlanType", required = true)
        protected String planType;
        @XmlElement(name = "FirstCycleAmount")
        protected Long firstCycleAmount;
        @XmlElement(name = "FinalCycleAmount")
        protected Long finalCycleAmount;

        /**
         * Obtiene el valor de la propiedad planType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlanType() {
            return planType;
        }

        /**
         * Define el valor de la propiedad planType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlanType(String value) {
            this.planType = value;
        }

        /**
         * Obtiene el valor de la propiedad firstCycleAmount.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getFirstCycleAmount() {
            return firstCycleAmount;
        }

        /**
         * Define el valor de la propiedad firstCycleAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setFirstCycleAmount(Long value) {
            this.firstCycleAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad finalCycleAmount.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getFinalCycleAmount() {
            return finalCycleAmount;
        }

        /**
         * Define el valor de la propiedad finalCycleAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setFinalCycleAmount(Long value) {
            this.finalCycleAmount = value;
        }

    }

}
