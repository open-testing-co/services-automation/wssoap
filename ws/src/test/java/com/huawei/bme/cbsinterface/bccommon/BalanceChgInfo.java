
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BalanceChgInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BalanceChgInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/>
 *         &lt;element name="BalanceTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OldBalanceAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *         &lt;element name="NewBalanceAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BalanceChgInfo", propOrder = {
    "balanceType",
    "balanceID",
    "balanceTypeName",
    "oldBalanceAmt",
    "newBalanceAmt",
    "currencyID"
})
public class BalanceChgInfo {

    @XmlElement(name = "BalanceType", required = true)
    protected String balanceType;
    @XmlElement(name = "BalanceID")
    protected Long balanceID;
    @XmlElement(name = "BalanceTypeName")
    protected String balanceTypeName;
    @XmlElement(name = "OldBalanceAmt")
    protected long oldBalanceAmt;
    @XmlElement(name = "NewBalanceAmt")
    protected long newBalanceAmt;
    @XmlElement(name = "CurrencyID", required = true)
    protected BigInteger currencyID;

    /**
     * Obtiene el valor de la propiedad balanceType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceType() {
        return balanceType;
    }

    /**
     * Define el valor de la propiedad balanceType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceType(String value) {
        this.balanceType = value;
    }

    /**
     * Obtiene el valor de la propiedad balanceID.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBalanceID() {
        return balanceID;
    }

    /**
     * Define el valor de la propiedad balanceID.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBalanceID(Long value) {
        this.balanceID = value;
    }

    /**
     * Obtiene el valor de la propiedad balanceTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceTypeName() {
        return balanceTypeName;
    }

    /**
     * Define el valor de la propiedad balanceTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceTypeName(String value) {
        this.balanceTypeName = value;
    }

    /**
     * Obtiene el valor de la propiedad oldBalanceAmt.
     * 
     */
    public long getOldBalanceAmt() {
        return oldBalanceAmt;
    }

    /**
     * Define el valor de la propiedad oldBalanceAmt.
     * 
     */
    public void setOldBalanceAmt(long value) {
        this.oldBalanceAmt = value;
    }

    /**
     * Obtiene el valor de la propiedad newBalanceAmt.
     * 
     */
    public long getNewBalanceAmt() {
        return newBalanceAmt;
    }

    /**
     * Define el valor de la propiedad newBalanceAmt.
     * 
     */
    public void setNewBalanceAmt(long value) {
        this.newBalanceAmt = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrencyID() {
        return currencyID;
    }

    /**
     * Define el valor de la propiedad currencyID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrencyID(BigInteger value) {
        this.currencyID = value;
    }

}
