
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.PayRelExtRule;


/**
 * <p>Clase Java para QueryPaymentRelationResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryPaymentRelationResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentRelationList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PayRelation" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="DefaultPayFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PayObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PayObjKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PayObjCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
 *                             &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OriginType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="OriginKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PaymentLimit" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PaymentLimitInfo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
 *                                     &lt;sequence>
 *                                     &lt;/sequence>
 *                                   &lt;/extension>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryPaymentRelationResult", propOrder = {
    "paymentRelationList"
})
public class QueryPaymentRelationResult {

    @XmlElement(name = "PaymentRelationList")
    protected QueryPaymentRelationResult.PaymentRelationList paymentRelationList;

    /**
     * Obtiene el valor de la propiedad paymentRelationList.
     * 
     * @return
     *     possible object is
     *     {@link QueryPaymentRelationResult.PaymentRelationList }
     *     
     */
    public QueryPaymentRelationResult.PaymentRelationList getPaymentRelationList() {
        return paymentRelationList;
    }

    /**
     * Define el valor de la propiedad paymentRelationList.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryPaymentRelationResult.PaymentRelationList }
     *     
     */
    public void setPaymentRelationList(QueryPaymentRelationResult.PaymentRelationList value) {
        this.paymentRelationList = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PayRelation" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DefaultPayFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PayObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PayObjKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PayObjCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
     *                   &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OriginType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OriginKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PaymentLimit" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PaymentLimitInfo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
     *                           &lt;sequence>
     *                           &lt;/sequence>
     *                         &lt;/extension>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "payRelation",
        "paymentLimit"
    })
    public static class PaymentRelationList {

        @XmlElement(name = "PayRelation", required = true)
        protected List<QueryPaymentRelationResult.PaymentRelationList.PayRelation> payRelation;
        @XmlElement(name = "PaymentLimit")
        protected List<QueryPaymentRelationResult.PaymentRelationList.PaymentLimit> paymentLimit;

        /**
         * Gets the value of the payRelation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the payRelation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPayRelation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryPaymentRelationResult.PaymentRelationList.PayRelation }
         * 
         * 
         */
        public List<QueryPaymentRelationResult.PaymentRelationList.PayRelation> getPayRelation() {
            if (payRelation == null) {
                payRelation = new ArrayList<QueryPaymentRelationResult.PaymentRelationList.PayRelation>();
            }
            return this.payRelation;
        }

        /**
         * Gets the value of the paymentLimit property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the paymentLimit property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPaymentLimit().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryPaymentRelationResult.PaymentRelationList.PaymentLimit }
         * 
         * 
         */
        public List<QueryPaymentRelationResult.PaymentRelationList.PaymentLimit> getPaymentLimit() {
            if (paymentLimit == null) {
                paymentLimit = new ArrayList<QueryPaymentRelationResult.PaymentRelationList.PaymentLimit>();
            }
            return this.paymentLimit;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DefaultPayFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PayObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PayObjKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PayObjCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
         *         &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OriginType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OriginKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "payRelationKey",
            "defaultPayFlag",
            "acctKey",
            "payObjType",
            "payObjKey",
            "payObjCode",
            "priority",
            "payRelExtRule",
            "onlyPayRelFlag",
            "paymentLimitKey",
            "originType",
            "originKey",
            "effectiveTime",
            "expirationTime"
        })
        public static class PayRelation {

            @XmlElement(name = "PayRelationKey", required = true)
            protected String payRelationKey;
            @XmlElement(name = "DefaultPayFlag", required = true)
            protected String defaultPayFlag;
            @XmlElement(name = "AcctKey", required = true)
            protected String acctKey;
            @XmlElement(name = "PayObjType", required = true)
            protected String payObjType;
            @XmlElement(name = "PayObjKey", required = true)
            protected String payObjKey;
            @XmlElement(name = "PayObjCode", required = true)
            protected String payObjCode;
            @XmlElement(name = "Priority", required = true)
            protected BigInteger priority;
            @XmlElement(name = "PayRelExtRule")
            protected PayRelExtRule payRelExtRule;
            @XmlElement(name = "OnlyPayRelFlag")
            protected String onlyPayRelFlag;
            @XmlElement(name = "PaymentLimitKey")
            protected String paymentLimitKey;
            @XmlElement(name = "OriginType")
            protected String originType;
            @XmlElement(name = "OriginKey")
            protected String originKey;
            @XmlElement(name = "EffectiveTime", required = true)
            protected String effectiveTime;
            @XmlElement(name = "ExpirationTime", required = true)
            protected String expirationTime;

            /**
             * Obtiene el valor de la propiedad payRelationKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayRelationKey() {
                return payRelationKey;
            }

            /**
             * Define el valor de la propiedad payRelationKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayRelationKey(String value) {
                this.payRelationKey = value;
            }

            /**
             * Obtiene el valor de la propiedad defaultPayFlag.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDefaultPayFlag() {
                return defaultPayFlag;
            }

            /**
             * Define el valor de la propiedad defaultPayFlag.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDefaultPayFlag(String value) {
                this.defaultPayFlag = value;
            }

            /**
             * Obtiene el valor de la propiedad acctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctKey() {
                return acctKey;
            }

            /**
             * Define el valor de la propiedad acctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctKey(String value) {
                this.acctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad payObjType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayObjType() {
                return payObjType;
            }

            /**
             * Define el valor de la propiedad payObjType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayObjType(String value) {
                this.payObjType = value;
            }

            /**
             * Obtiene el valor de la propiedad payObjKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayObjKey() {
                return payObjKey;
            }

            /**
             * Define el valor de la propiedad payObjKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayObjKey(String value) {
                this.payObjKey = value;
            }

            /**
             * Obtiene el valor de la propiedad payObjCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayObjCode() {
                return payObjCode;
            }

            /**
             * Define el valor de la propiedad payObjCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayObjCode(String value) {
                this.payObjCode = value;
            }

            /**
             * Obtiene el valor de la propiedad priority.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPriority() {
                return priority;
            }

            /**
             * Define el valor de la propiedad priority.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPriority(BigInteger value) {
                this.priority = value;
            }

            /**
             * Obtiene el valor de la propiedad payRelExtRule.
             * 
             * @return
             *     possible object is
             *     {@link PayRelExtRule }
             *     
             */
            public PayRelExtRule getPayRelExtRule() {
                return payRelExtRule;
            }

            /**
             * Define el valor de la propiedad payRelExtRule.
             * 
             * @param value
             *     allowed object is
             *     {@link PayRelExtRule }
             *     
             */
            public void setPayRelExtRule(PayRelExtRule value) {
                this.payRelExtRule = value;
            }

            /**
             * Obtiene el valor de la propiedad onlyPayRelFlag.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOnlyPayRelFlag() {
                return onlyPayRelFlag;
            }

            /**
             * Define el valor de la propiedad onlyPayRelFlag.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOnlyPayRelFlag(String value) {
                this.onlyPayRelFlag = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentLimitKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPaymentLimitKey() {
                return paymentLimitKey;
            }

            /**
             * Define el valor de la propiedad paymentLimitKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPaymentLimitKey(String value) {
                this.paymentLimitKey = value;
            }

            /**
             * Obtiene el valor de la propiedad originType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOriginType() {
                return originType;
            }

            /**
             * Define el valor de la propiedad originType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOriginType(String value) {
                this.originType = value;
            }

            /**
             * Obtiene el valor de la propiedad originKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOriginKey() {
                return originKey;
            }

            /**
             * Define el valor de la propiedad originKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOriginKey(String value) {
                this.originKey = value;
            }

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationTime(String value) {
                this.expirationTime = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PaymentLimitInfo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
         *                 &lt;sequence>
         *                 &lt;/sequence>
         *               &lt;/extension>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "paymentLimitKey",
            "paymentLimitInfo"
        })
        public static class PaymentLimit {

            @XmlElement(name = "PaymentLimitKey", required = true)
            protected String paymentLimitKey;
            @XmlElement(name = "PaymentLimitInfo", required = true)
            protected QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo paymentLimitInfo;

            /**
             * Obtiene el valor de la propiedad paymentLimitKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPaymentLimitKey() {
                return paymentLimitKey;
            }

            /**
             * Define el valor de la propiedad paymentLimitKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPaymentLimitKey(String value) {
                this.paymentLimitKey = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentLimitInfo.
             * 
             * @return
             *     possible object is
             *     {@link QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo }
             *     
             */
            public QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo getPaymentLimitInfo() {
                return paymentLimitInfo;
            }

            /**
             * Define el valor de la propiedad paymentLimitInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo }
             *     
             */
            public void setPaymentLimitInfo(QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo value) {
                this.paymentLimitInfo = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
             *       &lt;sequence>
             *       &lt;/sequence>
             *     &lt;/extension>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class PaymentLimitInfo
                extends com.huawei.bme.cbsinterface.bccommon.PaymentLimit
            {


            }

        }

    }

}
