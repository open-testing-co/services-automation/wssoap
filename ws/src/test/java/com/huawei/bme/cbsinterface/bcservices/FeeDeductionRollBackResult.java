
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.BalanceChgInfo;
import com.huawei.bme.cbsinterface.bccommon.FreeUnitChgInfo;


/**
 * <p>Clase Java para FeeDeductionRollBackResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FeeDeductionRollBackResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DeductSerialNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BalanceRollBack" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BalanceChgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}BalanceChgInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FreeUnitRollBack" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OwnerType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OwnerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FreeUnitChgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}FreeUnitChgInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CreditLimitChangeList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="OriginLimitAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="PaidAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrentAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="AccmBeginDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AccmEndDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeeDeductionRollBackResult", propOrder = {
    "deductSerialNo",
    "balanceRollBack",
    "freeUnitRollBack",
    "creditLimitChangeList"
})
public class FeeDeductionRollBackResult {

    @XmlElement(name = "DeductSerialNo", required = true)
    protected String deductSerialNo;
    @XmlElement(name = "BalanceRollBack")
    protected List<FeeDeductionRollBackResult.BalanceRollBack> balanceRollBack;
    @XmlElement(name = "FreeUnitRollBack")
    protected List<FeeDeductionRollBackResult.FreeUnitRollBack> freeUnitRollBack;
    @XmlElement(name = "CreditLimitChangeList")
    protected List<FeeDeductionRollBackResult.CreditLimitChangeList> creditLimitChangeList;

    /**
     * Obtiene el valor de la propiedad deductSerialNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeductSerialNo() {
        return deductSerialNo;
    }

    /**
     * Define el valor de la propiedad deductSerialNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeductSerialNo(String value) {
        this.deductSerialNo = value;
    }

    /**
     * Gets the value of the balanceRollBack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the balanceRollBack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBalanceRollBack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeeDeductionRollBackResult.BalanceRollBack }
     * 
     * 
     */
    public List<FeeDeductionRollBackResult.BalanceRollBack> getBalanceRollBack() {
        if (balanceRollBack == null) {
            balanceRollBack = new ArrayList<FeeDeductionRollBackResult.BalanceRollBack>();
        }
        return this.balanceRollBack;
    }

    /**
     * Gets the value of the freeUnitRollBack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the freeUnitRollBack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreeUnitRollBack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeeDeductionRollBackResult.FreeUnitRollBack }
     * 
     * 
     */
    public List<FeeDeductionRollBackResult.FreeUnitRollBack> getFreeUnitRollBack() {
        if (freeUnitRollBack == null) {
            freeUnitRollBack = new ArrayList<FeeDeductionRollBackResult.FreeUnitRollBack>();
        }
        return this.freeUnitRollBack;
    }

    /**
     * Gets the value of the creditLimitChangeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditLimitChangeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditLimitChangeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeeDeductionRollBackResult.CreditLimitChangeList }
     * 
     * 
     */
    public List<FeeDeductionRollBackResult.CreditLimitChangeList> getCreditLimitChangeList() {
        if (creditLimitChangeList == null) {
            creditLimitChangeList = new ArrayList<FeeDeductionRollBackResult.CreditLimitChangeList>();
        }
        return this.creditLimitChangeList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BalanceChgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}BalanceChgInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "balanceChgInfo"
    })
    public static class BalanceRollBack {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "BalanceChgInfo", required = true, nillable = true)
        protected List<BalanceChgInfo> balanceChgInfo;

        /**
         * Obtiene el valor de la propiedad acctKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Define el valor de la propiedad acctKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Gets the value of the balanceChgInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceChgInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceChgInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BalanceChgInfo }
         * 
         * 
         */
        public List<BalanceChgInfo> getBalanceChgInfo() {
            if (balanceChgInfo == null) {
                balanceChgInfo = new ArrayList<BalanceChgInfo>();
            }
            return this.balanceChgInfo;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="OriginLimitAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="PaidAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrentAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="AccmBeginDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AccmEndDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "creditLimitType",
        "creditInstID",
        "originLimitAmt",
        "paidAmt",
        "currentAmt",
        "currencyID",
        "accmBeginDate",
        "accmEndDate"
    })
    public static class CreditLimitChangeList {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "CreditLimitType", required = true)
        protected String creditLimitType;
        @XmlElement(name = "CreditInstID")
        protected long creditInstID;
        @XmlElement(name = "OriginLimitAmt")
        protected long originLimitAmt;
        @XmlElement(name = "PaidAmt")
        protected long paidAmt;
        @XmlElement(name = "CurrentAmt")
        protected long currentAmt;
        @XmlElement(name = "CurrencyID", required = true)
        protected BigInteger currencyID;
        @XmlElement(name = "AccmBeginDate", required = true)
        protected String accmBeginDate;
        @XmlElement(name = "AccmEndDate", required = true)
        protected String accmEndDate;

        /**
         * Obtiene el valor de la propiedad acctKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Define el valor de la propiedad acctKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Obtiene el valor de la propiedad creditLimitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreditLimitType() {
            return creditLimitType;
        }

        /**
         * Define el valor de la propiedad creditLimitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreditLimitType(String value) {
            this.creditLimitType = value;
        }

        /**
         * Obtiene el valor de la propiedad creditInstID.
         * 
         */
        public long getCreditInstID() {
            return creditInstID;
        }

        /**
         * Define el valor de la propiedad creditInstID.
         * 
         */
        public void setCreditInstID(long value) {
            this.creditInstID = value;
        }

        /**
         * Obtiene el valor de la propiedad originLimitAmt.
         * 
         */
        public long getOriginLimitAmt() {
            return originLimitAmt;
        }

        /**
         * Define el valor de la propiedad originLimitAmt.
         * 
         */
        public void setOriginLimitAmt(long value) {
            this.originLimitAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad paidAmt.
         * 
         */
        public long getPaidAmt() {
            return paidAmt;
        }

        /**
         * Define el valor de la propiedad paidAmt.
         * 
         */
        public void setPaidAmt(long value) {
            this.paidAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad currentAmt.
         * 
         */
        public long getCurrentAmt() {
            return currentAmt;
        }

        /**
         * Define el valor de la propiedad currentAmt.
         * 
         */
        public void setCurrentAmt(long value) {
            this.currentAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Obtiene el valor de la propiedad accmBeginDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccmBeginDate() {
            return accmBeginDate;
        }

        /**
         * Define el valor de la propiedad accmBeginDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccmBeginDate(String value) {
            this.accmBeginDate = value;
        }

        /**
         * Obtiene el valor de la propiedad accmEndDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccmEndDate() {
            return accmEndDate;
        }

        /**
         * Define el valor de la propiedad accmEndDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccmEndDate(String value) {
            this.accmEndDate = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OwnerType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OwnerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FreeUnitChgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}FreeUnitChgInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ownerType",
        "ownerKey",
        "freeUnitChgInfo"
    })
    public static class FreeUnitRollBack {

        @XmlElement(name = "OwnerType", required = true)
        protected String ownerType;
        @XmlElement(name = "OwnerKey", required = true)
        protected String ownerKey;
        @XmlElement(name = "FreeUnitChgInfo", required = true)
        protected List<FreeUnitChgInfo> freeUnitChgInfo;

        /**
         * Obtiene el valor de la propiedad ownerType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOwnerType() {
            return ownerType;
        }

        /**
         * Define el valor de la propiedad ownerType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOwnerType(String value) {
            this.ownerType = value;
        }

        /**
         * Obtiene el valor de la propiedad ownerKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOwnerKey() {
            return ownerKey;
        }

        /**
         * Define el valor de la propiedad ownerKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOwnerKey(String value) {
            this.ownerKey = value;
        }

        /**
         * Gets the value of the freeUnitChgInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the freeUnitChgInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFreeUnitChgInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FreeUnitChgInfo }
         * 
         * 
         */
        public List<FreeUnitChgInfo> getFreeUnitChgInfo() {
            if (freeUnitChgInfo == null) {
                freeUnitChgInfo = new ArrayList<FreeUnitChgInfo>();
            }
            return this.freeUnitChgInfo;
        }

    }

}
