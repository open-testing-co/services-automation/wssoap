
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;


/**
 * <p>Clase Java para ManCrossGroupRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ManCrossGroupRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubGroupAccessCodeA" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode"/>
 *         &lt;element name="SubGroupAccessCodeB" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode"/>
 *         &lt;element name="OperationType" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManCrossGroupRequest", propOrder = {
    "subGroupAccessCodeA",
    "subGroupAccessCodeB",
    "operationType"
})
public class ManCrossGroupRequest {

    @XmlElement(name = "SubGroupAccessCodeA", required = true)
    protected SubGroupAccessCode subGroupAccessCodeA;
    @XmlElement(name = "SubGroupAccessCodeB", required = true)
    protected SubGroupAccessCode subGroupAccessCodeB;
    @XmlElement(name = "OperationType", required = true)
    protected BigInteger operationType;

    /**
     * Obtiene el valor de la propiedad subGroupAccessCodeA.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public SubGroupAccessCode getSubGroupAccessCodeA() {
        return subGroupAccessCodeA;
    }

    /**
     * Define el valor de la propiedad subGroupAccessCodeA.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public void setSubGroupAccessCodeA(SubGroupAccessCode value) {
        this.subGroupAccessCodeA = value;
    }

    /**
     * Obtiene el valor de la propiedad subGroupAccessCodeB.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public SubGroupAccessCode getSubGroupAccessCodeB() {
        return subGroupAccessCodeB;
    }

    /**
     * Define el valor de la propiedad subGroupAccessCodeB.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public void setSubGroupAccessCodeB(SubGroupAccessCode value) {
        this.subGroupAccessCodeB = value;
    }

    /**
     * Obtiene el valor de la propiedad operationType.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOperationType() {
        return operationType;
    }

    /**
     * Define el valor de la propiedad operationType.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOperationType(BigInteger value) {
        this.operationType = value;
    }

}
