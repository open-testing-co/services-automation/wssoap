
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;


/**
 * <p>Clase Java para ChangeConsumptionLimitRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeConsumptionLimitRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LimitObj">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
 *                   &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddLimit" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice maxOccurs="unbounded" minOccurs="0">
 *                     &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                     &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="LimitParam">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="UnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="MesureType" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                     &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                     &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="MesureID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;/choice>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DelLimit" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ModifyLimit" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OldAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="NewAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="LimitParam" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AddLimitParam" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="DelLimitParam" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="ModifyLimitParam" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="OldParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                       &lt;element name="NewParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeConsumptionLimitRequest", propOrder = {
    "limitObj",
    "addLimit",
    "delLimit",
    "modifyLimit"
})
public class ChangeConsumptionLimitRequest {

    @XmlElement(name = "LimitObj", required = true)
    protected ChangeConsumptionLimitRequest.LimitObj limitObj;
    @XmlElement(name = "AddLimit")
    protected List<ChangeConsumptionLimitRequest.AddLimit> addLimit;
    @XmlElement(name = "DelLimit")
    protected List<ChangeConsumptionLimitRequest.DelLimit> delLimit;
    @XmlElement(name = "ModifyLimit")
    protected List<ChangeConsumptionLimitRequest.ModifyLimit> modifyLimit;

    /**
     * Obtiene el valor de la propiedad limitObj.
     * 
     * @return
     *     possible object is
     *     {@link ChangeConsumptionLimitRequest.LimitObj }
     *     
     */
    public ChangeConsumptionLimitRequest.LimitObj getLimitObj() {
        return limitObj;
    }

    /**
     * Define el valor de la propiedad limitObj.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeConsumptionLimitRequest.LimitObj }
     *     
     */
    public void setLimitObj(ChangeConsumptionLimitRequest.LimitObj value) {
        this.limitObj = value;
    }

    /**
     * Gets the value of the addLimit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addLimit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddLimit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeConsumptionLimitRequest.AddLimit }
     * 
     * 
     */
    public List<ChangeConsumptionLimitRequest.AddLimit> getAddLimit() {
        if (addLimit == null) {
            addLimit = new ArrayList<ChangeConsumptionLimitRequest.AddLimit>();
        }
        return this.addLimit;
    }

    /**
     * Gets the value of the delLimit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the delLimit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDelLimit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeConsumptionLimitRequest.DelLimit }
     * 
     * 
     */
    public List<ChangeConsumptionLimitRequest.DelLimit> getDelLimit() {
        if (delLimit == null) {
            delLimit = new ArrayList<ChangeConsumptionLimitRequest.DelLimit>();
        }
        return this.delLimit;
    }

    /**
     * Gets the value of the modifyLimit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modifyLimit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModifyLimit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeConsumptionLimitRequest.ModifyLimit }
     * 
     * 
     */
    public List<ChangeConsumptionLimitRequest.ModifyLimit> getModifyLimit() {
        if (modifyLimit == null) {
            modifyLimit = new ArrayList<ChangeConsumptionLimitRequest.ModifyLimit>();
        }
        return this.modifyLimit;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice maxOccurs="unbounded" minOccurs="0">
     *           &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *           &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;element name="LimitParam">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                     &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="UnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;element name="MesureType" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *           &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *           &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;element name="MesureID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;/choice>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "effectiveTimeOrLimitValueOrExpirationTime"
    })
    public static class AddLimit {

        @XmlElementRefs({
            @XmlElementRef(name = "LimitValue", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "MesureType", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LimitType", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "EffectiveTime", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "UnitType", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CurrencyID", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "MesureID", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LimitParam", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "ExpirationTime", namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", type = JAXBElement.class, required = false)
        })
        protected List<JAXBElement<?>> effectiveTimeOrLimitValueOrExpirationTime;

        /**
         * Gets the value of the effectiveTimeOrLimitValueOrExpirationTime property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the effectiveTimeOrLimitValueOrExpirationTime property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEffectiveTimeOrLimitValueOrExpirationTime().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link Long }{@code >}
         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
         * {@link JAXBElement }{@code <}{@link ChangeConsumptionLimitRequest.AddLimit.LimitParam }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * 
         * 
         */
        public List<JAXBElement<?>> getEffectiveTimeOrLimitValueOrExpirationTime() {
            if (effectiveTimeOrLimitValueOrExpirationTime == null) {
                effectiveTimeOrLimitValueOrExpirationTime = new ArrayList<JAXBElement<?>>();
            }
            return this.effectiveTimeOrLimitValueOrExpirationTime;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "paramCode",
            "paramValue"
        })
        public static class LimitParam {

            @XmlElement(name = "ParamCode", required = true)
            protected String paramCode;
            @XmlElement(name = "ParamValue")
            protected long paramValue;

            /**
             * Obtiene el valor de la propiedad paramCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParamCode() {
                return paramCode;
            }

            /**
             * Define el valor de la propiedad paramCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParamCode(String value) {
                this.paramCode = value;
            }

            /**
             * Obtiene el valor de la propiedad paramValue.
             * 
             */
            public long getParamValue() {
                return paramValue;
            }

            /**
             * Define el valor de la propiedad paramValue.
             * 
             */
            public void setParamValue(long value) {
                this.paramValue = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitType"
    })
    public static class DelLimit {

        @XmlElement(name = "LimitType", required = true)
        protected String limitType;

        /**
         * Obtiene el valor de la propiedad limitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitType() {
            return limitType;
        }

        /**
         * Define el valor de la propiedad limitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitType(String value) {
            this.limitType = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
     *         &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subAccessCode",
        "custAccessCode"
    })
    public static class LimitObj {

        @XmlElement(name = "SubAccessCode")
        protected SubAccessCode subAccessCode;
        @XmlElement(name = "CustAccessCode")
        protected CustAccessCode custAccessCode;

        /**
         * Obtiene el valor de la propiedad subAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubAccessCode }
         *     
         */
        public SubAccessCode getSubAccessCode() {
            return subAccessCode;
        }

        /**
         * Define el valor de la propiedad subAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubAccessCode }
         *     
         */
        public void setSubAccessCode(SubAccessCode value) {
            this.subAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad custAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link CustAccessCode }
         *     
         */
        public CustAccessCode getCustAccessCode() {
            return custAccessCode;
        }

        /**
         * Define el valor de la propiedad custAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link CustAccessCode }
         *     
         */
        public void setCustAccessCode(CustAccessCode value) {
            this.custAccessCode = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OldAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="NewAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="LimitParam" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AddLimitParam" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="DelLimitParam" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="ModifyLimitParam" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="OldParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                             &lt;element name="NewParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitType",
        "oldAmount",
        "newAmount",
        "limitParam"
    })
    public static class ModifyLimit {

        @XmlElement(name = "LimitType", required = true)
        protected String limitType;
        @XmlElement(name = "OldAmount")
        protected Long oldAmount;
        @XmlElement(name = "NewAmount")
        protected Long newAmount;
        @XmlElement(name = "LimitParam")
        protected ChangeConsumptionLimitRequest.ModifyLimit.LimitParam limitParam;

        /**
         * Obtiene el valor de la propiedad limitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitType() {
            return limitType;
        }

        /**
         * Define el valor de la propiedad limitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitType(String value) {
            this.limitType = value;
        }

        /**
         * Obtiene el valor de la propiedad oldAmount.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getOldAmount() {
            return oldAmount;
        }

        /**
         * Define el valor de la propiedad oldAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setOldAmount(Long value) {
            this.oldAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad newAmount.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getNewAmount() {
            return newAmount;
        }

        /**
         * Define el valor de la propiedad newAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setNewAmount(Long value) {
            this.newAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad limitParam.
         * 
         * @return
         *     possible object is
         *     {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam }
         *     
         */
        public ChangeConsumptionLimitRequest.ModifyLimit.LimitParam getLimitParam() {
            return limitParam;
        }

        /**
         * Define el valor de la propiedad limitParam.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam }
         *     
         */
        public void setLimitParam(ChangeConsumptionLimitRequest.ModifyLimit.LimitParam value) {
            this.limitParam = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AddLimitParam" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="DelLimitParam" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="ModifyLimitParam" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="OldParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                   &lt;element name="NewParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "addLimitParam",
            "delLimitParam",
            "modifyLimitParam"
        })
        public static class LimitParam {

            @XmlElement(name = "AddLimitParam")
            protected List<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.AddLimitParam> addLimitParam;
            @XmlElement(name = "DelLimitParam")
            protected List<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.DelLimitParam> delLimitParam;
            @XmlElement(name = "ModifyLimitParam")
            protected List<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.ModifyLimitParam> modifyLimitParam;

            /**
             * Gets the value of the addLimitParam property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the addLimitParam property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddLimitParam().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.AddLimitParam }
             * 
             * 
             */
            public List<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.AddLimitParam> getAddLimitParam() {
                if (addLimitParam == null) {
                    addLimitParam = new ArrayList<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.AddLimitParam>();
                }
                return this.addLimitParam;
            }

            /**
             * Gets the value of the delLimitParam property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the delLimitParam property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDelLimitParam().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.DelLimitParam }
             * 
             * 
             */
            public List<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.DelLimitParam> getDelLimitParam() {
                if (delLimitParam == null) {
                    delLimitParam = new ArrayList<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.DelLimitParam>();
                }
                return this.delLimitParam;
            }

            /**
             * Gets the value of the modifyLimitParam property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the modifyLimitParam property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getModifyLimitParam().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.ModifyLimitParam }
             * 
             * 
             */
            public List<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.ModifyLimitParam> getModifyLimitParam() {
                if (modifyLimitParam == null) {
                    modifyLimitParam = new ArrayList<ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.ModifyLimitParam>();
                }
                return this.modifyLimitParam;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "paramCode",
                "paramValue"
            })
            public static class AddLimitParam {

                @XmlElement(name = "ParamCode", required = true)
                protected String paramCode;
                @XmlElement(name = "ParamValue")
                protected long paramValue;

                /**
                 * Obtiene el valor de la propiedad paramCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParamCode() {
                    return paramCode;
                }

                /**
                 * Define el valor de la propiedad paramCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParamCode(String value) {
                    this.paramCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad paramValue.
                 * 
                 */
                public long getParamValue() {
                    return paramValue;
                }

                /**
                 * Define el valor de la propiedad paramValue.
                 * 
                 */
                public void setParamValue(long value) {
                    this.paramValue = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "paramCode"
            })
            public static class DelLimitParam {

                @XmlElement(name = "ParamCode", required = true)
                protected String paramCode;

                /**
                 * Obtiene el valor de la propiedad paramCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParamCode() {
                    return paramCode;
                }

                /**
                 * Define el valor de la propiedad paramCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParamCode(String value) {
                    this.paramCode = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="OldParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *         &lt;element name="NewParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "paramCode",
                "oldParamValue",
                "newParamValue"
            })
            public static class ModifyLimitParam {

                @XmlElement(name = "ParamCode", required = true)
                protected String paramCode;
                @XmlElement(name = "OldParamValue")
                protected long oldParamValue;
                @XmlElement(name = "NewParamValue")
                protected long newParamValue;

                /**
                 * Obtiene el valor de la propiedad paramCode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getParamCode() {
                    return paramCode;
                }

                /**
                 * Define el valor de la propiedad paramCode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setParamCode(String value) {
                    this.paramCode = value;
                }

                /**
                 * Obtiene el valor de la propiedad oldParamValue.
                 * 
                 */
                public long getOldParamValue() {
                    return oldParamValue;
                }

                /**
                 * Define el valor de la propiedad oldParamValue.
                 * 
                 */
                public void setOldParamValue(long value) {
                    this.oldParamValue = value;
                }

                /**
                 * Obtiene el valor de la propiedad newParamValue.
                 * 
                 */
                public long getNewParamValue() {
                    return newParamValue;
                }

                /**
                 * Define el valor de la propiedad newParamValue.
                 * 
                 */
                public void setNewParamValue(long value) {
                    this.newParamValue = value;
                }

            }

        }

    }

}
