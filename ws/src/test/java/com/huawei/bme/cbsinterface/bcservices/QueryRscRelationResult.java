
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;


/**
 * <p>Clase Java para QueryRscRelationResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryRscRelationResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RscRelation" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PayObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PayObjCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RelationDestIdentify" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RelationDestCust" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CustomerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustomerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *                   &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ShareLimit" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="UsageMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="NotifyRule" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ActionRule" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="NotifyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Quota" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryRscRelationResult", propOrder = {
    "rscRelation"
})
public class QueryRscRelationResult {

    @XmlElement(name = "RscRelation")
    protected List<QueryRscRelationResult.RscRelation> rscRelation;

    /**
     * Gets the value of the rscRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rscRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRscRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryRscRelationResult.RscRelation }
     * 
     * 
     */
    public List<QueryRscRelationResult.RscRelation> getRscRelation() {
        if (rscRelation == null) {
            rscRelation = new ArrayList<QueryRscRelationResult.RscRelation>();
        }
        return this.rscRelation;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PayObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PayObjCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RelationDestIdentify" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RelationDestCust" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CustomerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustomerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
     *         &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ShareLimit" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="UsageMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="NotifyRule" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ActionRule" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="NotifyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Quota" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "payRelationKey",
        "payObjType",
        "payObjCode",
        "relationDestIdentify",
        "relationDestCust",
        "offeringKey",
        "shareRule",
        "shareLimit",
        "priority",
        "startTime",
        "endTime",
        "usageAmount",
        "usageMeasureUnit",
        "notifyRule",
        "freeUnitType"
    })
    public static class RscRelation {

        @XmlElement(name = "PayRelationKey", required = true)
        protected String payRelationKey;
        @XmlElement(name = "PayObjType", required = true)
        protected String payObjType;
        @XmlElement(name = "PayObjCode", required = true)
        protected String payObjCode;
        @XmlElement(name = "RelationDestIdentify")
        protected QueryRscRelationResult.RscRelation.RelationDestIdentify relationDestIdentify;
        @XmlElement(name = "RelationDestCust")
        protected QueryRscRelationResult.RscRelation.RelationDestCust relationDestCust;
        @XmlElement(name = "OfferingKey")
        protected OfferingKey offeringKey;
        @XmlElement(name = "ShareRule")
        protected String shareRule;
        @XmlElement(name = "ShareLimit")
        protected QueryRscRelationResult.RscRelation.ShareLimit shareLimit;
        @XmlElement(name = "Priority", required = true)
        protected BigInteger priority;
        @XmlElement(name = "StartTime", required = true)
        protected String startTime;
        @XmlElement(name = "EndTime", required = true)
        protected String endTime;
        @XmlElement(name = "UsageAmount")
        protected long usageAmount;
        @XmlElement(name = "UsageMeasureUnit")
        protected BigInteger usageMeasureUnit;
        @XmlElement(name = "NotifyRule")
        protected List<QueryRscRelationResult.RscRelation.NotifyRule> notifyRule;
        @XmlElement(name = "FreeUnitType")
        protected String freeUnitType;

        /**
         * Obtiene el valor de la propiedad payRelationKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayRelationKey() {
            return payRelationKey;
        }

        /**
         * Define el valor de la propiedad payRelationKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayRelationKey(String value) {
            this.payRelationKey = value;
        }

        /**
         * Obtiene el valor de la propiedad payObjType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayObjType() {
            return payObjType;
        }

        /**
         * Define el valor de la propiedad payObjType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayObjType(String value) {
            this.payObjType = value;
        }

        /**
         * Obtiene el valor de la propiedad payObjCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayObjCode() {
            return payObjCode;
        }

        /**
         * Define el valor de la propiedad payObjCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayObjCode(String value) {
            this.payObjCode = value;
        }

        /**
         * Obtiene el valor de la propiedad relationDestIdentify.
         * 
         * @return
         *     possible object is
         *     {@link QueryRscRelationResult.RscRelation.RelationDestIdentify }
         *     
         */
        public QueryRscRelationResult.RscRelation.RelationDestIdentify getRelationDestIdentify() {
            return relationDestIdentify;
        }

        /**
         * Define el valor de la propiedad relationDestIdentify.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryRscRelationResult.RscRelation.RelationDestIdentify }
         *     
         */
        public void setRelationDestIdentify(QueryRscRelationResult.RscRelation.RelationDestIdentify value) {
            this.relationDestIdentify = value;
        }

        /**
         * Obtiene el valor de la propiedad relationDestCust.
         * 
         * @return
         *     possible object is
         *     {@link QueryRscRelationResult.RscRelation.RelationDestCust }
         *     
         */
        public QueryRscRelationResult.RscRelation.RelationDestCust getRelationDestCust() {
            return relationDestCust;
        }

        /**
         * Define el valor de la propiedad relationDestCust.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryRscRelationResult.RscRelation.RelationDestCust }
         *     
         */
        public void setRelationDestCust(QueryRscRelationResult.RscRelation.RelationDestCust value) {
            this.relationDestCust = value;
        }

        /**
         * Obtiene el valor de la propiedad offeringKey.
         * 
         * @return
         *     possible object is
         *     {@link OfferingKey }
         *     
         */
        public OfferingKey getOfferingKey() {
            return offeringKey;
        }

        /**
         * Define el valor de la propiedad offeringKey.
         * 
         * @param value
         *     allowed object is
         *     {@link OfferingKey }
         *     
         */
        public void setOfferingKey(OfferingKey value) {
            this.offeringKey = value;
        }

        /**
         * Obtiene el valor de la propiedad shareRule.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShareRule() {
            return shareRule;
        }

        /**
         * Define el valor de la propiedad shareRule.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShareRule(String value) {
            this.shareRule = value;
        }

        /**
         * Obtiene el valor de la propiedad shareLimit.
         * 
         * @return
         *     possible object is
         *     {@link QueryRscRelationResult.RscRelation.ShareLimit }
         *     
         */
        public QueryRscRelationResult.RscRelation.ShareLimit getShareLimit() {
            return shareLimit;
        }

        /**
         * Define el valor de la propiedad shareLimit.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryRscRelationResult.RscRelation.ShareLimit }
         *     
         */
        public void setShareLimit(QueryRscRelationResult.RscRelation.ShareLimit value) {
            this.shareLimit = value;
        }

        /**
         * Obtiene el valor de la propiedad priority.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getPriority() {
            return priority;
        }

        /**
         * Define el valor de la propiedad priority.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setPriority(BigInteger value) {
            this.priority = value;
        }

        /**
         * Obtiene el valor de la propiedad startTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStartTime() {
            return startTime;
        }

        /**
         * Define el valor de la propiedad startTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStartTime(String value) {
            this.startTime = value;
        }

        /**
         * Obtiene el valor de la propiedad endTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndTime() {
            return endTime;
        }

        /**
         * Define el valor de la propiedad endTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndTime(String value) {
            this.endTime = value;
        }

        /**
         * Obtiene el valor de la propiedad usageAmount.
         * 
         */
        public long getUsageAmount() {
            return usageAmount;
        }

        /**
         * Define el valor de la propiedad usageAmount.
         * 
         */
        public void setUsageAmount(long value) {
            this.usageAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad usageMeasureUnit.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getUsageMeasureUnit() {
            return usageMeasureUnit;
        }

        /**
         * Define el valor de la propiedad usageMeasureUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setUsageMeasureUnit(BigInteger value) {
            this.usageMeasureUnit = value;
        }

        /**
         * Gets the value of the notifyRule property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the notifyRule property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getNotifyRule().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryRscRelationResult.RscRelation.NotifyRule }
         * 
         * 
         */
        public List<QueryRscRelationResult.RscRelation.NotifyRule> getNotifyRule() {
            if (notifyRule == null) {
                notifyRule = new ArrayList<QueryRscRelationResult.RscRelation.NotifyRule>();
            }
            return this.notifyRule;
        }

        /**
         * Obtiene el valor de la propiedad freeUnitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFreeUnitType() {
            return freeUnitType;
        }

        /**
         * Define el valor de la propiedad freeUnitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFreeUnitType(String value) {
            this.freeUnitType = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ActionRule" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="NotifyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Quota" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "actionRule",
            "notifyType",
            "startTime",
            "endTime",
            "quota"
        })
        public static class NotifyRule {

            @XmlElement(name = "ActionRule", required = true)
            protected BigInteger actionRule;
            @XmlElement(name = "NotifyType", required = true)
            protected String notifyType;
            @XmlElement(name = "StartTime", required = true)
            protected String startTime;
            @XmlElement(name = "EndTime", required = true)
            protected String endTime;
            @XmlElement(name = "Quota")
            protected long quota;

            /**
             * Obtiene el valor de la propiedad actionRule.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getActionRule() {
                return actionRule;
            }

            /**
             * Define el valor de la propiedad actionRule.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setActionRule(BigInteger value) {
                this.actionRule = value;
            }

            /**
             * Obtiene el valor de la propiedad notifyType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNotifyType() {
                return notifyType;
            }

            /**
             * Define el valor de la propiedad notifyType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNotifyType(String value) {
                this.notifyType = value;
            }

            /**
             * Obtiene el valor de la propiedad startTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStartTime() {
                return startTime;
            }

            /**
             * Define el valor de la propiedad startTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStartTime(String value) {
                this.startTime = value;
            }

            /**
             * Obtiene el valor de la propiedad endTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEndTime() {
                return endTime;
            }

            /**
             * Define el valor de la propiedad endTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEndTime(String value) {
                this.endTime = value;
            }

            /**
             * Obtiene el valor de la propiedad quota.
             * 
             */
            public long getQuota() {
                return quota;
            }

            /**
             * Define el valor de la propiedad quota.
             * 
             */
            public void setQuota(long value) {
                this.quota = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CustomerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustomerCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "customerKey",
            "customerCode"
        })
        public static class RelationDestCust {

            @XmlElement(name = "CustomerKey", required = true)
            protected String customerKey;
            @XmlElement(name = "CustomerCode", required = true)
            protected String customerCode;

            /**
             * Obtiene el valor de la propiedad customerKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerKey() {
                return customerKey;
            }

            /**
             * Define el valor de la propiedad customerKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerKey(String value) {
                this.customerKey = value;
            }

            /**
             * Obtiene el valor de la propiedad customerCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerCode() {
                return customerCode;
            }

            /**
             * Define el valor de la propiedad customerCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerCode(String value) {
                this.customerCode = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "primaryIdentity"
        })
        public static class RelationDestIdentify {

            @XmlElement(name = "PrimaryIdentity", required = true)
            protected String primaryIdentity;

            /**
             * Obtiene el valor de la propiedad primaryIdentity.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimaryIdentity() {
                return primaryIdentity;
            }

            /**
             * Define el valor de la propiedad primaryIdentity.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimaryIdentity(String value) {
                this.primaryIdentity = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "limitCycleType",
            "limitValue",
            "measureUnit"
        })
        public static class ShareLimit {

            @XmlElement(name = "LimitCycleType", required = true)
            protected String limitCycleType;
            @XmlElement(name = "LimitValue")
            protected long limitValue;
            @XmlElement(name = "MeasureUnit", required = true)
            protected BigInteger measureUnit;

            /**
             * Obtiene el valor de la propiedad limitCycleType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLimitCycleType() {
                return limitCycleType;
            }

            /**
             * Define el valor de la propiedad limitCycleType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLimitCycleType(String value) {
                this.limitCycleType = value;
            }

            /**
             * Obtiene el valor de la propiedad limitValue.
             * 
             */
            public long getLimitValue() {
                return limitValue;
            }

            /**
             * Define el valor de la propiedad limitValue.
             * 
             */
            public void setLimitValue(long value) {
                this.limitValue = value;
            }

            /**
             * Obtiene el valor de la propiedad measureUnit.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMeasureUnit() {
                return measureUnit;
            }

            /**
             * Define el valor de la propiedad measureUnit.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMeasureUnit(BigInteger value) {
                this.measureUnit = value;
            }

        }

    }

}
