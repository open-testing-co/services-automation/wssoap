
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SalesInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SalesInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SalesChannelID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SalesID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalesInfo", propOrder = {
    "salesChannelID",
    "salesID"
})
public class SalesInfo {

    @XmlElement(name = "SalesChannelID")
    protected String salesChannelID;
    @XmlElement(name = "SalesID")
    protected String salesID;

    /**
     * Obtiene el valor de la propiedad salesChannelID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesChannelID() {
        return salesChannelID;
    }

    /**
     * Define el valor de la propiedad salesChannelID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesChannelID(String value) {
        this.salesChannelID = value;
    }

    /**
     * Obtiene el valor de la propiedad salesID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalesID() {
        return salesID;
    }

    /**
     * Define el valor de la propiedad salesID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalesID(String value) {
        this.salesID = value;
    }

}
