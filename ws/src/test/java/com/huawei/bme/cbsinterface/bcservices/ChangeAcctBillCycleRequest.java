
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.AcctAccessCode;
import com.huawei.bme.cbsinterface.bccommon.EffectMode;


/**
 * <p>Clase Java para ChangeAcctBillCycleRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeAcctBillCycleRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Account">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RootAccount">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="SubAccount" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OldBillCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewBillCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeAcctBillCycleRequest", propOrder = {
    "account",
    "oldBillCycleType",
    "newBillCycleType",
    "effectiveTime"
})
public class ChangeAcctBillCycleRequest {

    @XmlElement(name = "Account", required = true)
    protected ChangeAcctBillCycleRequest.Account account;
    @XmlElement(name = "OldBillCycleType")
    protected String oldBillCycleType;
    @XmlElement(name = "NewBillCycleType", required = true)
    protected String newBillCycleType;
    @XmlElement(name = "EffectiveTime", required = true)
    protected EffectMode effectiveTime;

    /**
     * Obtiene el valor de la propiedad account.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctBillCycleRequest.Account }
     *     
     */
    public ChangeAcctBillCycleRequest.Account getAccount() {
        return account;
    }

    /**
     * Define el valor de la propiedad account.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctBillCycleRequest.Account }
     *     
     */
    public void setAccount(ChangeAcctBillCycleRequest.Account value) {
        this.account = value;
    }

    /**
     * Obtiene el valor de la propiedad oldBillCycleType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldBillCycleType() {
        return oldBillCycleType;
    }

    /**
     * Define el valor de la propiedad oldBillCycleType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldBillCycleType(String value) {
        this.oldBillCycleType = value;
    }

    /**
     * Obtiene el valor de la propiedad newBillCycleType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewBillCycleType() {
        return newBillCycleType;
    }

    /**
     * Define el valor de la propiedad newBillCycleType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewBillCycleType(String value) {
        this.newBillCycleType = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveTime.
     * 
     * @return
     *     possible object is
     *     {@link EffectMode }
     *     
     */
    public EffectMode getEffectiveTime() {
        return effectiveTime;
    }

    /**
     * Define el valor de la propiedad effectiveTime.
     * 
     * @param value
     *     allowed object is
     *     {@link EffectMode }
     *     
     */
    public void setEffectiveTime(EffectMode value) {
        this.effectiveTime = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RootAccount">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="SubAccount" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rootAccount",
        "subAccount"
    })
    public static class Account {

        @XmlElement(name = "RootAccount", required = true)
        protected ChangeAcctBillCycleRequest.Account.RootAccount rootAccount;
        @XmlElement(name = "SubAccount")
        protected List<ChangeAcctBillCycleRequest.Account.SubAccount> subAccount;

        /**
         * Obtiene el valor de la propiedad rootAccount.
         * 
         * @return
         *     possible object is
         *     {@link ChangeAcctBillCycleRequest.Account.RootAccount }
         *     
         */
        public ChangeAcctBillCycleRequest.Account.RootAccount getRootAccount() {
            return rootAccount;
        }

        /**
         * Define el valor de la propiedad rootAccount.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeAcctBillCycleRequest.Account.RootAccount }
         *     
         */
        public void setRootAccount(ChangeAcctBillCycleRequest.Account.RootAccount value) {
            this.rootAccount = value;
        }

        /**
         * Gets the value of the subAccount property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the subAccount property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSubAccount().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeAcctBillCycleRequest.Account.SubAccount }
         * 
         * 
         */
        public List<ChangeAcctBillCycleRequest.Account.SubAccount> getSubAccount() {
            if (subAccount == null) {
                subAccount = new ArrayList<ChangeAcctBillCycleRequest.Account.SubAccount>();
            }
            return this.subAccount;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
         *       &lt;sequence>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class RootAccount
            extends AcctAccessCode
        {


        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
         *       &lt;sequence>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class SubAccount
            extends AcctAccessCode
        {


        }

    }

}
