
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.AccountBasicInfo;
import com.huawei.bme.cbsinterface.bccommon.Address;
import com.huawei.bme.cbsinterface.bccommon.CustInfo;
import com.huawei.bme.cbsinterface.bccommon.IndividualInfo;
import com.huawei.bme.cbsinterface.bccommon.OrgInfo;
import com.huawei.bme.cbsinterface.bccommon.POfferingInst;


/**
 * <p>Clase Java para QueryCustomerInfoResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryCustomerInfoResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Customer" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
 *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                   &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
 *                   &lt;element name="OfferingInst" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
 *                           &lt;sequence>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Subscriber" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubscriberInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}Subscriber">
 *                           &lt;sequence>
 *                             &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="UserCustomer" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
 *                                       &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                                       &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PrimaryOffering" type="{http://www.huawei.com/bme/cbsinterface/bccommon}POfferingInst"/>
 *                   &lt;element name="SupplementaryOffering" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
 *                           &lt;sequence>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SubGroup" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubGroupKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubGroupInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroup">
 *                           &lt;sequence>
 *                             &lt;element name="GroupType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="UserCustomer" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
 *                                       &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                                       &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
 *                             &lt;element name="RGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PrimaryOffering" type="{http://www.huawei.com/bme/cbsinterface/bccommon}POfferingInst"/>
 *                   &lt;element name="SupplementaryOffering" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
 *                           &lt;sequence>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Account" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AcctInfo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AcctCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="UserCustomer" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
 *                                       &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                                       &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="ParentAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="RootAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AcctBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountBasicInfo"/>
 *                             &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AcctClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="AcctPayMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="AutoPayChannelInfo">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
 *                                               &lt;sequence>
 *                                               &lt;/sequence>
 *                                             &lt;/extension>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;element name="BillCycleOpenDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="BillCycleEndDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="OfferingInst" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
 *                           &lt;sequence>
 *                             &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryCustomerInfoResult", propOrder = {
    "customer",
    "subscriber",
    "subGroup",
    "account"
})
public class QueryCustomerInfoResult {

    @XmlElement(name = "Customer")
    protected QueryCustomerInfoResult.Customer customer;
    @XmlElement(name = "Subscriber")
    protected QueryCustomerInfoResult.Subscriber subscriber;
    @XmlElement(name = "SubGroup")
    protected QueryCustomerInfoResult.SubGroup subGroup;
    @XmlElement(name = "Account")
    protected List<QueryCustomerInfoResult.Account> account;

    /**
     * Obtiene el valor de la propiedad customer.
     * 
     * @return
     *     possible object is
     *     {@link QueryCustomerInfoResult.Customer }
     *     
     */
    public QueryCustomerInfoResult.Customer getCustomer() {
        return customer;
    }

    /**
     * Define el valor de la propiedad customer.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryCustomerInfoResult.Customer }
     *     
     */
    public void setCustomer(QueryCustomerInfoResult.Customer value) {
        this.customer = value;
    }

    /**
     * Obtiene el valor de la propiedad subscriber.
     * 
     * @return
     *     possible object is
     *     {@link QueryCustomerInfoResult.Subscriber }
     *     
     */
    public QueryCustomerInfoResult.Subscriber getSubscriber() {
        return subscriber;
    }

    /**
     * Define el valor de la propiedad subscriber.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryCustomerInfoResult.Subscriber }
     *     
     */
    public void setSubscriber(QueryCustomerInfoResult.Subscriber value) {
        this.subscriber = value;
    }

    /**
     * Obtiene el valor de la propiedad subGroup.
     * 
     * @return
     *     possible object is
     *     {@link QueryCustomerInfoResult.SubGroup }
     *     
     */
    public QueryCustomerInfoResult.SubGroup getSubGroup() {
        return subGroup;
    }

    /**
     * Define el valor de la propiedad subGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryCustomerInfoResult.SubGroup }
     *     
     */
    public void setSubGroup(QueryCustomerInfoResult.SubGroup value) {
        this.subGroup = value;
    }

    /**
     * Gets the value of the account property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the account property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryCustomerInfoResult.Account }
     * 
     * 
     */
    public List<QueryCustomerInfoResult.Account> getAccount() {
        if (account == null) {
            account = new ArrayList<QueryCustomerInfoResult.Account>();
        }
        return this.account;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AcctInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AcctCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="UserCustomer" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
     *                             &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *                             &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="ParentAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="RootAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AcctBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountBasicInfo"/>
     *                   &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AcctClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                   &lt;element name="AcctPayMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="AutoPayChannelInfo">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
     *                                     &lt;sequence>
     *                                     &lt;/sequence>
     *                                   &lt;/extension>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" maxOccurs="unbounded" minOccurs="0"/>
     *                   &lt;element name="BillCycleOpenDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BillCycleEndDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="OfferingInst" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
     *                 &lt;sequence>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "acctInfo",
        "offeringInst"
    })
    public static class Account {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "AcctInfo", required = true)
        protected QueryCustomerInfoResult.Account.AcctInfo acctInfo;
        @XmlElement(name = "OfferingInst")
        protected List<QueryCustomerInfoResult.Account.OfferingInst> offeringInst;

        /**
         * Obtiene el valor de la propiedad acctKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Define el valor de la propiedad acctKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Obtiene el valor de la propiedad acctInfo.
         * 
         * @return
         *     possible object is
         *     {@link QueryCustomerInfoResult.Account.AcctInfo }
         *     
         */
        public QueryCustomerInfoResult.Account.AcctInfo getAcctInfo() {
            return acctInfo;
        }

        /**
         * Define el valor de la propiedad acctInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryCustomerInfoResult.Account.AcctInfo }
         *     
         */
        public void setAcctInfo(QueryCustomerInfoResult.Account.AcctInfo value) {
            this.acctInfo = value;
        }

        /**
         * Gets the value of the offeringInst property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the offeringInst property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOfferingInst().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryCustomerInfoResult.Account.OfferingInst }
         * 
         * 
         */
        public List<QueryCustomerInfoResult.Account.OfferingInst> getOfferingInst() {
            if (offeringInst == null) {
                offeringInst = new ArrayList<QueryCustomerInfoResult.Account.OfferingInst>();
            }
            return this.offeringInst;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AcctCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="UserCustomer" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
         *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
         *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="ParentAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="RootAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AcctBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountBasicInfo"/>
         *         &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AcctType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AcctClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *         &lt;element name="AcctPayMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="AutoPayChannelInfo">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
         *                           &lt;sequence>
         *                           &lt;/sequence>
         *                         &lt;/extension>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" maxOccurs="unbounded" minOccurs="0"/>
         *         &lt;element name="BillCycleOpenDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BillCycleEndDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "acctCode",
            "userCustomerKey",
            "userCustomer",
            "parentAcctKey",
            "rootAcctKey",
            "acctBasicInfo",
            "billCycleType",
            "acctType",
            "paymentType",
            "acctClass",
            "currencyID",
            "acctPayMethod",
            "autoPayChannel",
            "addressInfo",
            "billCycleOpenDate",
            "billCycleEndDate"
        })
        public static class AcctInfo {

            @XmlElement(name = "AcctCode", required = true)
            protected String acctCode;
            @XmlElement(name = "UserCustomerKey")
            protected String userCustomerKey;
            @XmlElement(name = "UserCustomer")
            protected QueryCustomerInfoResult.Account.AcctInfo.UserCustomer userCustomer;
            @XmlElement(name = "ParentAcctKey")
            protected String parentAcctKey;
            @XmlElement(name = "RootAcctKey")
            protected String rootAcctKey;
            @XmlElement(name = "AcctBasicInfo", required = true)
            protected AccountBasicInfo acctBasicInfo;
            @XmlElement(name = "BillCycleType", required = true)
            protected String billCycleType;
            @XmlElement(name = "AcctType", required = true)
            protected String acctType;
            @XmlElement(name = "PaymentType", required = true)
            protected String paymentType;
            @XmlElement(name = "AcctClass", required = true)
            protected String acctClass;
            @XmlElement(name = "CurrencyID", required = true)
            protected BigInteger currencyID;
            @XmlElement(name = "AcctPayMethod", required = true)
            protected String acctPayMethod;
            @XmlElement(name = "AutoPayChannel")
            protected List<QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel> autoPayChannel;
            @XmlElement(name = "AddressInfo")
            protected List<Address> addressInfo;
            @XmlElement(name = "BillCycleOpenDate")
            protected String billCycleOpenDate;
            @XmlElement(name = "BillCycleEndDate")
            protected String billCycleEndDate;

            /**
             * Obtiene el valor de la propiedad acctCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctCode() {
                return acctCode;
            }

            /**
             * Define el valor de la propiedad acctCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctCode(String value) {
                this.acctCode = value;
            }

            /**
             * Obtiene el valor de la propiedad userCustomerKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUserCustomerKey() {
                return userCustomerKey;
            }

            /**
             * Define el valor de la propiedad userCustomerKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUserCustomerKey(String value) {
                this.userCustomerKey = value;
            }

            /**
             * Obtiene el valor de la propiedad userCustomer.
             * 
             * @return
             *     possible object is
             *     {@link QueryCustomerInfoResult.Account.AcctInfo.UserCustomer }
             *     
             */
            public QueryCustomerInfoResult.Account.AcctInfo.UserCustomer getUserCustomer() {
                return userCustomer;
            }

            /**
             * Define el valor de la propiedad userCustomer.
             * 
             * @param value
             *     allowed object is
             *     {@link QueryCustomerInfoResult.Account.AcctInfo.UserCustomer }
             *     
             */
            public void setUserCustomer(QueryCustomerInfoResult.Account.AcctInfo.UserCustomer value) {
                this.userCustomer = value;
            }

            /**
             * Obtiene el valor de la propiedad parentAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParentAcctKey() {
                return parentAcctKey;
            }

            /**
             * Define el valor de la propiedad parentAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParentAcctKey(String value) {
                this.parentAcctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad rootAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRootAcctKey() {
                return rootAcctKey;
            }

            /**
             * Define el valor de la propiedad rootAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRootAcctKey(String value) {
                this.rootAcctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad acctBasicInfo.
             * 
             * @return
             *     possible object is
             *     {@link AccountBasicInfo }
             *     
             */
            public AccountBasicInfo getAcctBasicInfo() {
                return acctBasicInfo;
            }

            /**
             * Define el valor de la propiedad acctBasicInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link AccountBasicInfo }
             *     
             */
            public void setAcctBasicInfo(AccountBasicInfo value) {
                this.acctBasicInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad billCycleType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleType() {
                return billCycleType;
            }

            /**
             * Define el valor de la propiedad billCycleType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleType(String value) {
                this.billCycleType = value;
            }

            /**
             * Obtiene el valor de la propiedad acctType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctType() {
                return acctType;
            }

            /**
             * Define el valor de la propiedad acctType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctType(String value) {
                this.acctType = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPaymentType() {
                return paymentType;
            }

            /**
             * Define el valor de la propiedad paymentType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPaymentType(String value) {
                this.paymentType = value;
            }

            /**
             * Obtiene el valor de la propiedad acctClass.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctClass() {
                return acctClass;
            }

            /**
             * Define el valor de la propiedad acctClass.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctClass(String value) {
                this.acctClass = value;
            }

            /**
             * Obtiene el valor de la propiedad currencyID.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getCurrencyID() {
                return currencyID;
            }

            /**
             * Define el valor de la propiedad currencyID.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setCurrencyID(BigInteger value) {
                this.currencyID = value;
            }

            /**
             * Obtiene el valor de la propiedad acctPayMethod.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctPayMethod() {
                return acctPayMethod;
            }

            /**
             * Define el valor de la propiedad acctPayMethod.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctPayMethod(String value) {
                this.acctPayMethod = value;
            }

            /**
             * Gets the value of the autoPayChannel property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the autoPayChannel property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAutoPayChannel().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel }
             * 
             * 
             */
            public List<QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel> getAutoPayChannel() {
                if (autoPayChannel == null) {
                    autoPayChannel = new ArrayList<QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel>();
                }
                return this.autoPayChannel;
            }

            /**
             * Gets the value of the addressInfo property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the addressInfo property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddressInfo().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Address }
             * 
             * 
             */
            public List<Address> getAddressInfo() {
                if (addressInfo == null) {
                    addressInfo = new ArrayList<Address>();
                }
                return this.addressInfo;
            }

            /**
             * Obtiene el valor de la propiedad billCycleOpenDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleOpenDate() {
                return billCycleOpenDate;
            }

            /**
             * Define el valor de la propiedad billCycleOpenDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleOpenDate(String value) {
                this.billCycleOpenDate = value;
            }

            /**
             * Obtiene el valor de la propiedad billCycleEndDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBillCycleEndDate() {
                return billCycleEndDate;
            }

            /**
             * Define el valor de la propiedad billCycleEndDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBillCycleEndDate(String value) {
                this.billCycleEndDate = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="AutoPayChannelInfo">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
             *                 &lt;sequence>
             *                 &lt;/sequence>
             *               &lt;/extension>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "autoPayChannelKey",
                "autoPayChannelInfo"
            })
            public static class AutoPayChannel {

                @XmlElement(name = "AutoPayChannelKey", required = true)
                protected String autoPayChannelKey;
                @XmlElement(name = "AutoPayChannelInfo", required = true)
                protected QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo autoPayChannelInfo;

                /**
                 * Obtiene el valor de la propiedad autoPayChannelKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAutoPayChannelKey() {
                    return autoPayChannelKey;
                }

                /**
                 * Define el valor de la propiedad autoPayChannelKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAutoPayChannelKey(String value) {
                    this.autoPayChannelKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad autoPayChannelInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo }
                 *     
                 */
                public QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo getAutoPayChannelInfo() {
                    return autoPayChannelInfo;
                }

                /**
                 * Define el valor de la propiedad autoPayChannelInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo }
                 *     
                 */
                public void setAutoPayChannelInfo(QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo value) {
                    this.autoPayChannelInfo = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
                 *       &lt;sequence>
                 *       &lt;/sequence>
                 *     &lt;/extension>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class AutoPayChannelInfo
                    extends com.huawei.bme.cbsinterface.bccommon.AutoPayChannelInfo
                {


                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
             *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
             *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "custKey",
                "custInfo",
                "individualInfo",
                "orgInfo"
            })
            public static class UserCustomer {

                @XmlElement(name = "CustKey", required = true)
                protected String custKey;
                @XmlElement(name = "CustInfo", required = true)
                protected CustInfo custInfo;
                @XmlElement(name = "IndividualInfo")
                protected IndividualInfo individualInfo;
                @XmlElement(name = "OrgInfo")
                protected OrgInfo orgInfo;

                /**
                 * Obtiene el valor de la propiedad custKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustKey() {
                    return custKey;
                }

                /**
                 * Define el valor de la propiedad custKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustKey(String value) {
                    this.custKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad custInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CustInfo }
                 *     
                 */
                public CustInfo getCustInfo() {
                    return custInfo;
                }

                /**
                 * Define el valor de la propiedad custInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CustInfo }
                 *     
                 */
                public void setCustInfo(CustInfo value) {
                    this.custInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad individualInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IndividualInfo }
                 *     
                 */
                public IndividualInfo getIndividualInfo() {
                    return individualInfo;
                }

                /**
                 * Define el valor de la propiedad individualInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IndividualInfo }
                 *     
                 */
                public void setIndividualInfo(IndividualInfo value) {
                    this.individualInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad orgInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrgInfo }
                 *     
                 */
                public OrgInfo getOrgInfo() {
                    return orgInfo;
                }

                /**
                 * Define el valor de la propiedad orgInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrgInfo }
                 *     
                 */
                public void setOrgInfo(OrgInfo value) {
                    this.orgInfo = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
         *       &lt;sequence>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "effectiveTime",
            "expirationTime",
            "activationMode",
            "activationTime",
            "activeTimeLimit"
        })
        public static class OfferingInst
            extends com.huawei.bme.cbsinterface.bccommon.OfferingInst
        {

            @XmlElement(name = "EffectiveTime", required = true)
            protected String effectiveTime;
            @XmlElement(name = "ExpirationTime", required = true)
            protected String expirationTime;
            @XmlElement(name = "ActivationMode", required = true)
            protected String activationMode;
            @XmlElement(name = "ActivationTime")
            protected String activationTime;
            @XmlElement(name = "ActiveTimeLimit", required = true)
            protected String activeTimeLimit;

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationTime(String value) {
                this.expirationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad activationMode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationMode() {
                return activationMode;
            }

            /**
             * Define el valor de la propiedad activationMode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationMode(String value) {
                this.activationMode = value;
            }

            /**
             * Obtiene el valor de la propiedad activationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationTime() {
                return activationTime;
            }

            /**
             * Define el valor de la propiedad activationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationTime(String value) {
                this.activationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad activeTimeLimit.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActiveTimeLimit() {
                return activeTimeLimit;
            }

            /**
             * Define el valor de la propiedad activeTimeLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActiveTimeLimit(String value) {
                this.activeTimeLimit = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
     *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
     *         &lt;element name="OfferingInst" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
     *                 &lt;sequence>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custKey",
        "custInfo",
        "individualInfo",
        "orgInfo",
        "addressInfo",
        "offeringInst"
    })
    public static class Customer {

        @XmlElement(name = "CustKey", required = true)
        protected String custKey;
        @XmlElement(name = "CustInfo", required = true)
        protected CustInfo custInfo;
        @XmlElement(name = "IndividualInfo")
        protected IndividualInfo individualInfo;
        @XmlElement(name = "OrgInfo")
        protected OrgInfo orgInfo;
        @XmlElement(name = "AddressInfo")
        protected Address addressInfo;
        @XmlElement(name = "OfferingInst")
        protected List<QueryCustomerInfoResult.Customer.OfferingInst> offeringInst;

        /**
         * Obtiene el valor de la propiedad custKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustKey() {
            return custKey;
        }

        /**
         * Define el valor de la propiedad custKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustKey(String value) {
            this.custKey = value;
        }

        /**
         * Obtiene el valor de la propiedad custInfo.
         * 
         * @return
         *     possible object is
         *     {@link CustInfo }
         *     
         */
        public CustInfo getCustInfo() {
            return custInfo;
        }

        /**
         * Define el valor de la propiedad custInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInfo }
         *     
         */
        public void setCustInfo(CustInfo value) {
            this.custInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad individualInfo.
         * 
         * @return
         *     possible object is
         *     {@link IndividualInfo }
         *     
         */
        public IndividualInfo getIndividualInfo() {
            return individualInfo;
        }

        /**
         * Define el valor de la propiedad individualInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link IndividualInfo }
         *     
         */
        public void setIndividualInfo(IndividualInfo value) {
            this.individualInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad orgInfo.
         * 
         * @return
         *     possible object is
         *     {@link OrgInfo }
         *     
         */
        public OrgInfo getOrgInfo() {
            return orgInfo;
        }

        /**
         * Define el valor de la propiedad orgInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgInfo }
         *     
         */
        public void setOrgInfo(OrgInfo value) {
            this.orgInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad addressInfo.
         * 
         * @return
         *     possible object is
         *     {@link Address }
         *     
         */
        public Address getAddressInfo() {
            return addressInfo;
        }

        /**
         * Define el valor de la propiedad addressInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link Address }
         *     
         */
        public void setAddressInfo(Address value) {
            this.addressInfo = value;
        }

        /**
         * Gets the value of the offeringInst property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the offeringInst property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOfferingInst().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryCustomerInfoResult.Customer.OfferingInst }
         * 
         * 
         */
        public List<QueryCustomerInfoResult.Customer.OfferingInst> getOfferingInst() {
            if (offeringInst == null) {
                offeringInst = new ArrayList<QueryCustomerInfoResult.Customer.OfferingInst>();
            }
            return this.offeringInst;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
         *       &lt;sequence>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "effectiveTime",
            "expirationTime",
            "activationMode",
            "activationTime",
            "activeTimeLimit"
        })
        public static class OfferingInst
            extends com.huawei.bme.cbsinterface.bccommon.OfferingInst
        {

            @XmlElement(name = "EffectiveTime", required = true)
            protected String effectiveTime;
            @XmlElement(name = "ExpirationTime", required = true)
            protected String expirationTime;
            @XmlElement(name = "ActivationMode", required = true)
            protected String activationMode;
            @XmlElement(name = "ActivationTime")
            protected String activationTime;
            @XmlElement(name = "ActiveTimeLimit")
            protected String activeTimeLimit;

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationTime(String value) {
                this.expirationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad activationMode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationMode() {
                return activationMode;
            }

            /**
             * Define el valor de la propiedad activationMode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationMode(String value) {
                this.activationMode = value;
            }

            /**
             * Obtiene el valor de la propiedad activationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationTime() {
                return activationTime;
            }

            /**
             * Define el valor de la propiedad activationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationTime(String value) {
                this.activationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad activeTimeLimit.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActiveTimeLimit() {
                return activeTimeLimit;
            }

            /**
             * Define el valor de la propiedad activeTimeLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActiveTimeLimit(String value) {
                this.activeTimeLimit = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubGroupKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubGroupInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroup">
     *                 &lt;sequence>
     *                   &lt;element name="GroupType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="UserCustomer" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
     *                             &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *                             &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
     *                   &lt;element name="RGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PrimaryOffering" type="{http://www.huawei.com/bme/cbsinterface/bccommon}POfferingInst"/>
     *         &lt;element name="SupplementaryOffering" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
     *                 &lt;sequence>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subGroupKey",
        "subGroupInfo",
        "primaryOffering",
        "supplementaryOffering"
    })
    public static class SubGroup {

        @XmlElement(name = "SubGroupKey", required = true)
        protected String subGroupKey;
        @XmlElement(name = "SubGroupInfo", required = true)
        protected QueryCustomerInfoResult.SubGroup.SubGroupInfo subGroupInfo;
        @XmlElement(name = "PrimaryOffering", required = true)
        protected POfferingInst primaryOffering;
        @XmlElement(name = "SupplementaryOffering")
        protected List<QueryCustomerInfoResult.SubGroup.SupplementaryOffering> supplementaryOffering;

        /**
         * Obtiene el valor de la propiedad subGroupKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubGroupKey() {
            return subGroupKey;
        }

        /**
         * Define el valor de la propiedad subGroupKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubGroupKey(String value) {
            this.subGroupKey = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupInfo.
         * 
         * @return
         *     possible object is
         *     {@link QueryCustomerInfoResult.SubGroup.SubGroupInfo }
         *     
         */
        public QueryCustomerInfoResult.SubGroup.SubGroupInfo getSubGroupInfo() {
            return subGroupInfo;
        }

        /**
         * Define el valor de la propiedad subGroupInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryCustomerInfoResult.SubGroup.SubGroupInfo }
         *     
         */
        public void setSubGroupInfo(QueryCustomerInfoResult.SubGroup.SubGroupInfo value) {
            this.subGroupInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryOffering.
         * 
         * @return
         *     possible object is
         *     {@link POfferingInst }
         *     
         */
        public POfferingInst getPrimaryOffering() {
            return primaryOffering;
        }

        /**
         * Define el valor de la propiedad primaryOffering.
         * 
         * @param value
         *     allowed object is
         *     {@link POfferingInst }
         *     
         */
        public void setPrimaryOffering(POfferingInst value) {
            this.primaryOffering = value;
        }

        /**
         * Gets the value of the supplementaryOffering property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the supplementaryOffering property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSupplementaryOffering().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryCustomerInfoResult.SubGroup.SupplementaryOffering }
         * 
         * 
         */
        public List<QueryCustomerInfoResult.SubGroup.SupplementaryOffering> getSupplementaryOffering() {
            if (supplementaryOffering == null) {
                supplementaryOffering = new ArrayList<QueryCustomerInfoResult.SubGroup.SupplementaryOffering>();
            }
            return this.supplementaryOffering;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroup">
         *       &lt;sequence>
         *         &lt;element name="GroupType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="UserCustomer" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
         *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
         *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
         *         &lt;element name="RGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "groupType",
            "status",
            "statusDetail",
            "userCustomer",
            "addressInfo",
            "rGroupKey"
        })
        public static class SubGroupInfo
            extends com.huawei.bme.cbsinterface.bccommon.SubGroup
        {

            @XmlElement(name = "GroupType", required = true)
            protected String groupType;
            @XmlElement(name = "Status", required = true)
            protected String status;
            @XmlElement(name = "StatusDetail", required = true)
            protected String statusDetail;
            @XmlElement(name = "UserCustomer")
            protected QueryCustomerInfoResult.SubGroup.SubGroupInfo.UserCustomer userCustomer;
            @XmlElement(name = "AddressInfo")
            protected Address addressInfo;
            @XmlElement(name = "RGroupKey")
            protected String rGroupKey;

            /**
             * Obtiene el valor de la propiedad groupType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroupType() {
                return groupType;
            }

            /**
             * Define el valor de la propiedad groupType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroupType(String value) {
                this.groupType = value;
            }

            /**
             * Obtiene el valor de la propiedad status.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * Define el valor de la propiedad status.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

            /**
             * Obtiene el valor de la propiedad statusDetail.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusDetail() {
                return statusDetail;
            }

            /**
             * Define el valor de la propiedad statusDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusDetail(String value) {
                this.statusDetail = value;
            }

            /**
             * Obtiene el valor de la propiedad userCustomer.
             * 
             * @return
             *     possible object is
             *     {@link QueryCustomerInfoResult.SubGroup.SubGroupInfo.UserCustomer }
             *     
             */
            public QueryCustomerInfoResult.SubGroup.SubGroupInfo.UserCustomer getUserCustomer() {
                return userCustomer;
            }

            /**
             * Define el valor de la propiedad userCustomer.
             * 
             * @param value
             *     allowed object is
             *     {@link QueryCustomerInfoResult.SubGroup.SubGroupInfo.UserCustomer }
             *     
             */
            public void setUserCustomer(QueryCustomerInfoResult.SubGroup.SubGroupInfo.UserCustomer value) {
                this.userCustomer = value;
            }

            /**
             * Obtiene el valor de la propiedad addressInfo.
             * 
             * @return
             *     possible object is
             *     {@link Address }
             *     
             */
            public Address getAddressInfo() {
                return addressInfo;
            }

            /**
             * Define el valor de la propiedad addressInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link Address }
             *     
             */
            public void setAddressInfo(Address value) {
                this.addressInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad rGroupKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRGroupKey() {
                return rGroupKey;
            }

            /**
             * Define el valor de la propiedad rGroupKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRGroupKey(String value) {
                this.rGroupKey = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
             *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
             *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "custKey",
                "custInfo",
                "individualInfo",
                "orgInfo"
            })
            public static class UserCustomer {

                @XmlElement(name = "CustKey", required = true)
                protected String custKey;
                @XmlElement(name = "CustInfo", required = true)
                protected CustInfo custInfo;
                @XmlElement(name = "IndividualInfo")
                protected IndividualInfo individualInfo;
                @XmlElement(name = "OrgInfo")
                protected OrgInfo orgInfo;

                /**
                 * Obtiene el valor de la propiedad custKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustKey() {
                    return custKey;
                }

                /**
                 * Define el valor de la propiedad custKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustKey(String value) {
                    this.custKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad custInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CustInfo }
                 *     
                 */
                public CustInfo getCustInfo() {
                    return custInfo;
                }

                /**
                 * Define el valor de la propiedad custInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CustInfo }
                 *     
                 */
                public void setCustInfo(CustInfo value) {
                    this.custInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad individualInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IndividualInfo }
                 *     
                 */
                public IndividualInfo getIndividualInfo() {
                    return individualInfo;
                }

                /**
                 * Define el valor de la propiedad individualInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IndividualInfo }
                 *     
                 */
                public void setIndividualInfo(IndividualInfo value) {
                    this.individualInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad orgInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrgInfo }
                 *     
                 */
                public OrgInfo getOrgInfo() {
                    return orgInfo;
                }

                /**
                 * Define el valor de la propiedad orgInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrgInfo }
                 *     
                 */
                public void setOrgInfo(OrgInfo value) {
                    this.orgInfo = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
         *       &lt;sequence>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "effectiveTime",
            "expirationTime",
            "activationMode",
            "activationTime",
            "activeTimeLimit"
        })
        public static class SupplementaryOffering
            extends com.huawei.bme.cbsinterface.bccommon.OfferingInst
        {

            @XmlElement(name = "EffectiveTime", required = true)
            protected String effectiveTime;
            @XmlElement(name = "ExpirationTime", required = true)
            protected String expirationTime;
            @XmlElement(name = "ActivationMode", required = true)
            protected String activationMode;
            @XmlElement(name = "ActivationTime")
            protected String activationTime;
            @XmlElement(name = "ActiveTimeLimit", required = true)
            protected String activeTimeLimit;

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationTime(String value) {
                this.expirationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad activationMode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationMode() {
                return activationMode;
            }

            /**
             * Define el valor de la propiedad activationMode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationMode(String value) {
                this.activationMode = value;
            }

            /**
             * Obtiene el valor de la propiedad activationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationTime() {
                return activationTime;
            }

            /**
             * Define el valor de la propiedad activationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationTime(String value) {
                this.activationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad activeTimeLimit.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActiveTimeLimit() {
                return activeTimeLimit;
            }

            /**
             * Define el valor de la propiedad activeTimeLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActiveTimeLimit(String value) {
                this.activeTimeLimit = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubscriberInfo">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}Subscriber">
     *                 &lt;sequence>
     *                   &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="UserCustomer" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
     *                             &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *                             &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PaymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PrimaryOffering" type="{http://www.huawei.com/bme/cbsinterface/bccommon}POfferingInst"/>
     *         &lt;element name="SupplementaryOffering" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
     *                 &lt;sequence>
     *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subscriberKey",
        "subscriberInfo",
        "paymentMode",
        "primaryOffering",
        "supplementaryOffering"
    })
    public static class Subscriber {

        @XmlElement(name = "SubscriberKey", required = true)
        protected String subscriberKey;
        @XmlElement(name = "SubscriberInfo", required = true)
        protected QueryCustomerInfoResult.Subscriber.SubscriberInfo subscriberInfo;
        @XmlElement(name = "PaymentMode", required = true, nillable = true)
        protected String paymentMode;
        @XmlElement(name = "PrimaryOffering", required = true)
        protected POfferingInst primaryOffering;
        @XmlElement(name = "SupplementaryOffering")
        protected List<QueryCustomerInfoResult.Subscriber.SupplementaryOffering> supplementaryOffering;

        /**
         * Obtiene el valor de la propiedad subscriberKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubscriberKey() {
            return subscriberKey;
        }

        /**
         * Define el valor de la propiedad subscriberKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubscriberKey(String value) {
            this.subscriberKey = value;
        }

        /**
         * Obtiene el valor de la propiedad subscriberInfo.
         * 
         * @return
         *     possible object is
         *     {@link QueryCustomerInfoResult.Subscriber.SubscriberInfo }
         *     
         */
        public QueryCustomerInfoResult.Subscriber.SubscriberInfo getSubscriberInfo() {
            return subscriberInfo;
        }

        /**
         * Define el valor de la propiedad subscriberInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryCustomerInfoResult.Subscriber.SubscriberInfo }
         *     
         */
        public void setSubscriberInfo(QueryCustomerInfoResult.Subscriber.SubscriberInfo value) {
            this.subscriberInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad paymentMode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPaymentMode() {
            return paymentMode;
        }

        /**
         * Define el valor de la propiedad paymentMode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPaymentMode(String value) {
            this.paymentMode = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryOffering.
         * 
         * @return
         *     possible object is
         *     {@link POfferingInst }
         *     
         */
        public POfferingInst getPrimaryOffering() {
            return primaryOffering;
        }

        /**
         * Define el valor de la propiedad primaryOffering.
         * 
         * @param value
         *     allowed object is
         *     {@link POfferingInst }
         *     
         */
        public void setPrimaryOffering(POfferingInst value) {
            this.primaryOffering = value;
        }

        /**
         * Gets the value of the supplementaryOffering property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the supplementaryOffering property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSupplementaryOffering().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryCustomerInfoResult.Subscriber.SupplementaryOffering }
         * 
         * 
         */
        public List<QueryCustomerInfoResult.Subscriber.SupplementaryOffering> getSupplementaryOffering() {
            if (supplementaryOffering == null) {
                supplementaryOffering = new ArrayList<QueryCustomerInfoResult.Subscriber.SupplementaryOffering>();
            }
            return this.supplementaryOffering;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}Subscriber">
         *       &lt;sequence>
         *         &lt;element name="StatusDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="UserCustomer" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
         *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
         *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "statusDetail",
            "activationTime",
            "userCustomer",
            "addressInfo"
        })
        public static class SubscriberInfo
            extends com.huawei.bme.cbsinterface.bccommon.Subscriber
        {

            @XmlElement(name = "StatusDetail", required = true)
            protected String statusDetail;
            @XmlElement(name = "ActivationTime")
            protected String activationTime;
            @XmlElement(name = "UserCustomer")
            protected QueryCustomerInfoResult.Subscriber.SubscriberInfo.UserCustomer userCustomer;
            @XmlElement(name = "AddressInfo")
            protected Address addressInfo;

            /**
             * Obtiene el valor de la propiedad statusDetail.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatusDetail() {
                return statusDetail;
            }

            /**
             * Define el valor de la propiedad statusDetail.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatusDetail(String value) {
                this.statusDetail = value;
            }

            /**
             * Obtiene el valor de la propiedad activationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationTime() {
                return activationTime;
            }

            /**
             * Define el valor de la propiedad activationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationTime(String value) {
                this.activationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad userCustomer.
             * 
             * @return
             *     possible object is
             *     {@link QueryCustomerInfoResult.Subscriber.SubscriberInfo.UserCustomer }
             *     
             */
            public QueryCustomerInfoResult.Subscriber.SubscriberInfo.UserCustomer getUserCustomer() {
                return userCustomer;
            }

            /**
             * Define el valor de la propiedad userCustomer.
             * 
             * @param value
             *     allowed object is
             *     {@link QueryCustomerInfoResult.Subscriber.SubscriberInfo.UserCustomer }
             *     
             */
            public void setUserCustomer(QueryCustomerInfoResult.Subscriber.SubscriberInfo.UserCustomer value) {
                this.userCustomer = value;
            }

            /**
             * Obtiene el valor de la propiedad addressInfo.
             * 
             * @return
             *     possible object is
             *     {@link Address }
             *     
             */
            public Address getAddressInfo() {
                return addressInfo;
            }

            /**
             * Define el valor de la propiedad addressInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link Address }
             *     
             */
            public void setAddressInfo(Address value) {
                this.addressInfo = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo"/>
             *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
             *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "custKey",
                "custInfo",
                "individualInfo",
                "orgInfo"
            })
            public static class UserCustomer {

                @XmlElement(name = "CustKey", required = true)
                protected String custKey;
                @XmlElement(name = "CustInfo", required = true)
                protected CustInfo custInfo;
                @XmlElement(name = "IndividualInfo")
                protected IndividualInfo individualInfo;
                @XmlElement(name = "OrgInfo")
                protected OrgInfo orgInfo;

                /**
                 * Obtiene el valor de la propiedad custKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustKey() {
                    return custKey;
                }

                /**
                 * Define el valor de la propiedad custKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustKey(String value) {
                    this.custKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad custInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link CustInfo }
                 *     
                 */
                public CustInfo getCustInfo() {
                    return custInfo;
                }

                /**
                 * Define el valor de la propiedad custInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link CustInfo }
                 *     
                 */
                public void setCustInfo(CustInfo value) {
                    this.custInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad individualInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link IndividualInfo }
                 *     
                 */
                public IndividualInfo getIndividualInfo() {
                    return individualInfo;
                }

                /**
                 * Define el valor de la propiedad individualInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link IndividualInfo }
                 *     
                 */
                public void setIndividualInfo(IndividualInfo value) {
                    this.individualInfo = value;
                }

                /**
                 * Obtiene el valor de la propiedad orgInfo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrgInfo }
                 *     
                 */
                public OrgInfo getOrgInfo() {
                    return orgInfo;
                }

                /**
                 * Define el valor de la propiedad orgInfo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrgInfo }
                 *     
                 */
                public void setOrgInfo(OrgInfo value) {
                    this.orgInfo = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
         *       &lt;sequence>
         *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ActivationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "effectiveTime",
            "expirationTime",
            "activationMode",
            "activationTime",
            "activeTimeLimit"
        })
        public static class SupplementaryOffering
            extends com.huawei.bme.cbsinterface.bccommon.OfferingInst
        {

            @XmlElement(name = "EffectiveTime", required = true)
            protected String effectiveTime;
            @XmlElement(name = "ExpirationTime", required = true)
            protected String expirationTime;
            @XmlElement(name = "ActivationMode", required = true)
            protected String activationMode;
            @XmlElement(name = "ActivationTime")
            protected String activationTime;
            @XmlElement(name = "ActiveTimeLimit", required = true)
            protected String activeTimeLimit;

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffectiveTime(String value) {
                this.effectiveTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationTime(String value) {
                this.expirationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad activationMode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationMode() {
                return activationMode;
            }

            /**
             * Define el valor de la propiedad activationMode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationMode(String value) {
                this.activationMode = value;
            }

            /**
             * Obtiene el valor de la propiedad activationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActivationTime() {
                return activationTime;
            }

            /**
             * Define el valor de la propiedad activationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActivationTime(String value) {
                this.activationTime = value;
            }

            /**
             * Obtiene el valor de la propiedad activeTimeLimit.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getActiveTimeLimit() {
                return activeTimeLimit;
            }

            /**
             * Define el valor de la propiedad activeTimeLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setActiveTimeLimit(String value) {
                this.activeTimeLimit = value;
            }

        }

    }

}
