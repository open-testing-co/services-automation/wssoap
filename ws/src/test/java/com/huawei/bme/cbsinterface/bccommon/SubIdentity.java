
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SubIdentity complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubIdentity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="PrimaryFlag" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="RelatedSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubIdentity", propOrder = {
    "subIdentityType",
    "subIdentity",
    "primaryFlag",
    "relatedSubIdentity"
})
public class SubIdentity {

    @XmlElement(name = "SubIdentityType", required = true)
    protected String subIdentityType;
    @XmlElement(name = "SubIdentity", required = true)
    protected String subIdentity;
    @XmlElement(name = "PrimaryFlag", required = true)
    protected String primaryFlag;
    @XmlElement(name = "RelatedSubIdentity")
    protected String relatedSubIdentity;

    /**
     * Obtiene el valor de la propiedad subIdentityType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubIdentityType() {
        return subIdentityType;
    }

    /**
     * Define el valor de la propiedad subIdentityType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubIdentityType(String value) {
        this.subIdentityType = value;
    }

    /**
     * Obtiene el valor de la propiedad subIdentity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubIdentity() {
        return subIdentity;
    }

    /**
     * Define el valor de la propiedad subIdentity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubIdentity(String value) {
        this.subIdentity = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryFlag() {
        return primaryFlag;
    }

    /**
     * Define el valor de la propiedad primaryFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryFlag(String value) {
        this.primaryFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad relatedSubIdentity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelatedSubIdentity() {
        return relatedSubIdentity;
    }

    /**
     * Define el valor de la propiedad relatedSubIdentity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelatedSubIdentity(String value) {
        this.relatedSubIdentity = value;
    }

}
