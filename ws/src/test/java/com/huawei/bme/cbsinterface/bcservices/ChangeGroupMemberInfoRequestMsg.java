
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader" form="unqualified"/>
 *         &lt;element name="ChangeGroupMemberInfoRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeGroupMemberInfoRequest" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "changeGroupMemberInfoRequest"
})
@XmlRootElement(name = "ChangeGroupMemberInfoRequestMsg")
public class ChangeGroupMemberInfoRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "ChangeGroupMemberInfoRequest", namespace = "", required = true)
    protected ChangeGroupMemberInfoRequest changeGroupMemberInfoRequest;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad changeGroupMemberInfoRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeGroupMemberInfoRequest }
     *     
     */
    public ChangeGroupMemberInfoRequest getChangeGroupMemberInfoRequest() {
        return changeGroupMemberInfoRequest;
    }

    /**
     * Define el valor de la propiedad changeGroupMemberInfoRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeGroupMemberInfoRequest }
     *     
     */
    public void setChangeGroupMemberInfoRequest(ChangeGroupMemberInfoRequest value) {
        this.changeGroupMemberInfoRequest = value;
    }

}
