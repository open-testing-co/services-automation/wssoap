
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FreeUnitChgInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FreeUnitChgInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/>
 *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="FreeUnitTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *         &lt;element name="MeasureUnitName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OldAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *         &lt;element name="NewAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FreeUnitChgInfo", propOrder = {
    "freeUnitInstanceID",
    "freeUnitType",
    "freeUnitTypeName",
    "measureUnit",
    "measureUnitName",
    "oldAmt",
    "newAmt"
})
public class FreeUnitChgInfo {

    @XmlElement(name = "FreeUnitInstanceID")
    protected Long freeUnitInstanceID;
    @XmlElement(name = "FreeUnitType", required = true)
    protected String freeUnitType;
    @XmlElement(name = "FreeUnitTypeName")
    protected String freeUnitTypeName;
    @XmlElement(name = "MeasureUnit", required = true)
    protected BigInteger measureUnit;
    @XmlElement(name = "MeasureUnitName")
    protected String measureUnitName;
    @XmlElement(name = "OldAmt")
    protected long oldAmt;
    @XmlElement(name = "NewAmt")
    protected long newAmt;

    /**
     * Obtiene el valor de la propiedad freeUnitInstanceID.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFreeUnitInstanceID() {
        return freeUnitInstanceID;
    }

    /**
     * Define el valor de la propiedad freeUnitInstanceID.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFreeUnitInstanceID(Long value) {
        this.freeUnitInstanceID = value;
    }

    /**
     * Obtiene el valor de la propiedad freeUnitType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeUnitType() {
        return freeUnitType;
    }

    /**
     * Define el valor de la propiedad freeUnitType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeUnitType(String value) {
        this.freeUnitType = value;
    }

    /**
     * Obtiene el valor de la propiedad freeUnitTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreeUnitTypeName() {
        return freeUnitTypeName;
    }

    /**
     * Define el valor de la propiedad freeUnitTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreeUnitTypeName(String value) {
        this.freeUnitTypeName = value;
    }

    /**
     * Obtiene el valor de la propiedad measureUnit.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMeasureUnit() {
        return measureUnit;
    }

    /**
     * Define el valor de la propiedad measureUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMeasureUnit(BigInteger value) {
        this.measureUnit = value;
    }

    /**
     * Obtiene el valor de la propiedad measureUnitName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasureUnitName() {
        return measureUnitName;
    }

    /**
     * Define el valor de la propiedad measureUnitName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasureUnitName(String value) {
        this.measureUnitName = value;
    }

    /**
     * Obtiene el valor de la propiedad oldAmt.
     * 
     */
    public long getOldAmt() {
        return oldAmt;
    }

    /**
     * Define el valor de la propiedad oldAmt.
     * 
     */
    public void setOldAmt(long value) {
        this.oldAmt = value;
    }

    /**
     * Obtiene el valor de la propiedad newAmt.
     * 
     */
    public long getNewAmt() {
        return newAmt;
    }

    /**
     * Define el valor de la propiedad newAmt.
     * 
     */
    public void setNewAmt(long value) {
        this.newAmt = value;
    }

}
