
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QuerySubInforToMicroResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QuerySubInforToMicroResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Subscriber" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ManagementStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BlacklistStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubInforToMicroResult", propOrder = {
    "subscriber"
})
public class QuerySubInforToMicroResult {

    @XmlElement(name = "Subscriber")
    protected QuerySubInforToMicroResult.Subscriber subscriber;

    /**
     * Obtiene el valor de la propiedad subscriber.
     * 
     * @return
     *     possible object is
     *     {@link QuerySubInforToMicroResult.Subscriber }
     *     
     */
    public QuerySubInforToMicroResult.Subscriber getSubscriber() {
        return subscriber;
    }

    /**
     * Define el valor de la propiedad subscriber.
     * 
     * @param value
     *     allowed object is
     *     {@link QuerySubInforToMicroResult.Subscriber }
     *     
     */
    public void setSubscriber(QuerySubInforToMicroResult.Subscriber value) {
        this.subscriber = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ManagementStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BlacklistStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subType",
        "language",
        "subStatus",
        "effectiveDate",
        "managementStatus",
        "blacklistStatus"
    })
    public static class Subscriber {

        @XmlElement(name = "SubType")
        protected String subType;
        @XmlElement(name = "Language", required = true)
        protected String language;
        @XmlElement(name = "SubStatus")
        protected String subStatus;
        @XmlElement(name = "EffectiveDate")
        protected String effectiveDate;
        @XmlElement(name = "ManagementStatus")
        protected String managementStatus;
        @XmlElement(name = "BlacklistStatus")
        protected String blacklistStatus;

        /**
         * Obtiene el valor de la propiedad subType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubType() {
            return subType;
        }

        /**
         * Define el valor de la propiedad subType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubType(String value) {
            this.subType = value;
        }

        /**
         * Obtiene el valor de la propiedad language.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguage() {
            return language;
        }

        /**
         * Define el valor de la propiedad language.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguage(String value) {
            this.language = value;
        }

        /**
         * Obtiene el valor de la propiedad subStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubStatus() {
            return subStatus;
        }

        /**
         * Define el valor de la propiedad subStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubStatus(String value) {
            this.subStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * Define el valor de la propiedad effectiveDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffectiveDate(String value) {
            this.effectiveDate = value;
        }

        /**
         * Obtiene el valor de la propiedad managementStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getManagementStatus() {
            return managementStatus;
        }

        /**
         * Define el valor de la propiedad managementStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setManagementStatus(String value) {
            this.managementStatus = value;
        }

        /**
         * Obtiene el valor de la propiedad blacklistStatus.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBlacklistStatus() {
            return blacklistStatus;
        }

        /**
         * Define el valor de la propiedad blacklistStatus.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBlacklistStatus(String value) {
            this.blacklistStatus = value;
        }

    }

}
