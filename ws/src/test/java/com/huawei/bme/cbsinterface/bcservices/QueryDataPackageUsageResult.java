
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryDataPackageUsageResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryDataPackageUsageResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UsageList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="InitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="TodayUsedAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BillCycleOpenDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillCycleEndDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillCycleID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryDataPackageUsageResult", propOrder = {
    "usageList",
    "billCycleOpenDate",
    "billCycleEndDate",
    "billCycleID"
})
public class QueryDataPackageUsageResult {

    @XmlElement(name = "UsageList")
    protected List<QueryDataPackageUsageResult.UsageList> usageList;
    @XmlElement(name = "BillCycleOpenDate")
    protected String billCycleOpenDate;
    @XmlElement(name = "BillCycleEndDate")
    protected String billCycleEndDate;
    @XmlElement(name = "BillCycleID")
    protected BigInteger billCycleID;

    /**
     * Gets the value of the usageList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usageList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsageList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryDataPackageUsageResult.UsageList }
     * 
     * 
     */
    public List<QueryDataPackageUsageResult.UsageList> getUsageList() {
        if (usageList == null) {
            usageList = new ArrayList<QueryDataPackageUsageResult.UsageList>();
        }
        return this.usageList;
    }

    /**
     * Obtiene el valor de la propiedad billCycleOpenDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleOpenDate() {
        return billCycleOpenDate;
    }

    /**
     * Define el valor de la propiedad billCycleOpenDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleOpenDate(String value) {
        this.billCycleOpenDate = value;
    }

    /**
     * Obtiene el valor de la propiedad billCycleEndDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleEndDate() {
        return billCycleEndDate;
    }

    /**
     * Define el valor de la propiedad billCycleEndDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleEndDate(String value) {
        this.billCycleEndDate = value;
    }

    /**
     * Obtiene el valor de la propiedad billCycleID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBillCycleID() {
        return billCycleID;
    }

    /**
     * Define el valor de la propiedad billCycleID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBillCycleID(BigInteger value) {
        this.billCycleID = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="InitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="UsedAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="TodayUsedAmount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "freeUnitType",
        "initialAmount",
        "currentAmount",
        "usedAmount",
        "measureUnit",
        "todayUsedAmount"
    })
    public static class UsageList {

        @XmlElement(name = "FreeUnitType", required = true)
        protected String freeUnitType;
        @XmlElement(name = "InitialAmount")
        protected long initialAmount;
        @XmlElement(name = "CurrentAmount")
        protected long currentAmount;
        @XmlElement(name = "UsedAmount")
        protected long usedAmount;
        @XmlElement(name = "MeasureUnit", required = true)
        protected BigInteger measureUnit;
        @XmlElement(name = "TodayUsedAmount")
        protected Long todayUsedAmount;

        /**
         * Obtiene el valor de la propiedad freeUnitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFreeUnitType() {
            return freeUnitType;
        }

        /**
         * Define el valor de la propiedad freeUnitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFreeUnitType(String value) {
            this.freeUnitType = value;
        }

        /**
         * Obtiene el valor de la propiedad initialAmount.
         * 
         */
        public long getInitialAmount() {
            return initialAmount;
        }

        /**
         * Define el valor de la propiedad initialAmount.
         * 
         */
        public void setInitialAmount(long value) {
            this.initialAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad currentAmount.
         * 
         */
        public long getCurrentAmount() {
            return currentAmount;
        }

        /**
         * Define el valor de la propiedad currentAmount.
         * 
         */
        public void setCurrentAmount(long value) {
            this.currentAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad usedAmount.
         * 
         */
        public long getUsedAmount() {
            return usedAmount;
        }

        /**
         * Define el valor de la propiedad usedAmount.
         * 
         */
        public void setUsedAmount(long value) {
            this.usedAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad measureUnit.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMeasureUnit() {
            return measureUnit;
        }

        /**
         * Define el valor de la propiedad measureUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMeasureUnit(BigInteger value) {
            this.measureUnit = value;
        }

        /**
         * Obtiene el valor de la propiedad todayUsedAmount.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getTodayUsedAmount() {
            return todayUsedAmount;
        }

        /**
         * Define el valor de la propiedad todayUsedAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setTodayUsedAmount(Long value) {
            this.todayUsedAmount = value;
        }

    }

}
