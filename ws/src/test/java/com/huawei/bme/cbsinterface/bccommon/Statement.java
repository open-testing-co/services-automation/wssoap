
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Statement complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Statement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SmtKey" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="InvoiceType" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *         &lt;element name="SmtClass" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="SmtInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}StatementInfo" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Statement", propOrder = {
    "smtKey",
    "invoiceType",
    "smtClass",
    "smtInfo"
})
public class Statement {

    @XmlElement(name = "SmtKey", required = true)
    protected String smtKey;
    @XmlElement(name = "InvoiceType", required = true)
    protected BigInteger invoiceType;
    @XmlElement(name = "SmtClass", required = true)
    protected String smtClass;
    @XmlElement(name = "SmtInfo", required = true)
    protected StatementInfo smtInfo;

    /**
     * Obtiene el valor de la propiedad smtKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmtKey() {
        return smtKey;
    }

    /**
     * Define el valor de la propiedad smtKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmtKey(String value) {
        this.smtKey = value;
    }

    /**
     * Obtiene el valor de la propiedad invoiceType.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInvoiceType() {
        return invoiceType;
    }

    /**
     * Define el valor de la propiedad invoiceType.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInvoiceType(BigInteger value) {
        this.invoiceType = value;
    }

    /**
     * Obtiene el valor de la propiedad smtClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmtClass() {
        return smtClass;
    }

    /**
     * Define el valor de la propiedad smtClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmtClass(String value) {
        this.smtClass = value;
    }

    /**
     * Obtiene el valor de la propiedad smtInfo.
     * 
     * @return
     *     possible object is
     *     {@link StatementInfo }
     *     
     */
    public StatementInfo getSmtInfo() {
        return smtInfo;
    }

    /**
     * Define el valor de la propiedad smtInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link StatementInfo }
     *     
     */
    public void setSmtInfo(StatementInfo value) {
        this.smtInfo = value;
    }

}
