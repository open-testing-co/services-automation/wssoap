
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BatchAdjustmentRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BatchAdjustmentRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OpType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AdjustmentInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExtendDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FreeUnitAdjustmentInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExtendDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdjustmentReasonCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatchAdjustmentRequest", propOrder = {
    "opType",
    "fileName",
    "adjustmentInfo",
    "freeUnitAdjustmentInfo",
    "adjustmentReasonCode"
})
public class BatchAdjustmentRequest {

    @XmlElement(name = "OpType", required = true)
    protected String opType;
    @XmlElement(name = "FileName", required = true)
    protected String fileName;
    @XmlElement(name = "AdjustmentInfo")
    protected List<BatchAdjustmentRequest.AdjustmentInfo> adjustmentInfo;
    @XmlElement(name = "FreeUnitAdjustmentInfo")
    protected List<BatchAdjustmentRequest.FreeUnitAdjustmentInfo> freeUnitAdjustmentInfo;
    @XmlElement(name = "AdjustmentReasonCode")
    protected String adjustmentReasonCode;

    /**
     * Obtiene el valor de la propiedad opType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpType() {
        return opType;
    }

    /**
     * Define el valor de la propiedad opType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpType(String value) {
        this.opType = value;
    }

    /**
     * Obtiene el valor de la propiedad fileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Define el valor de la propiedad fileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the adjustmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjustmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjustmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BatchAdjustmentRequest.AdjustmentInfo }
     * 
     * 
     */
    public List<BatchAdjustmentRequest.AdjustmentInfo> getAdjustmentInfo() {
        if (adjustmentInfo == null) {
            adjustmentInfo = new ArrayList<BatchAdjustmentRequest.AdjustmentInfo>();
        }
        return this.adjustmentInfo;
    }

    /**
     * Gets the value of the freeUnitAdjustmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the freeUnitAdjustmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreeUnitAdjustmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BatchAdjustmentRequest.FreeUnitAdjustmentInfo }
     * 
     * 
     */
    public List<BatchAdjustmentRequest.FreeUnitAdjustmentInfo> getFreeUnitAdjustmentInfo() {
        if (freeUnitAdjustmentInfo == null) {
            freeUnitAdjustmentInfo = new ArrayList<BatchAdjustmentRequest.FreeUnitAdjustmentInfo>();
        }
        return this.freeUnitAdjustmentInfo;
    }

    /**
     * Obtiene el valor de la propiedad adjustmentReasonCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdjustmentReasonCode() {
        return adjustmentReasonCode;
    }

    /**
     * Define el valor de la propiedad adjustmentReasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdjustmentReasonCode(String value) {
        this.adjustmentReasonCode = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="BalanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BalanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExtendDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "balanceType",
        "balanceID",
        "adjustmentType",
        "adjustmentAmt",
        "currencyID",
        "effectiveTime",
        "expireTime",
        "extendDays"
    })
    public static class AdjustmentInfo {

        @XmlElement(name = "BalanceType")
        protected String balanceType;
        @XmlElement(name = "BalanceID")
        protected Long balanceID;
        @XmlElement(name = "AdjustmentType")
        protected String adjustmentType;
        @XmlElement(name = "AdjustmentAmt")
        protected Long adjustmentAmt;
        @XmlElement(name = "CurrencyID")
        protected BigInteger currencyID;
        @XmlElement(name = "EffectiveTime")
        protected String effectiveTime;
        @XmlElement(name = "ExpireTime")
        protected String expireTime;
        @XmlElement(name = "ExtendDays")
        protected Integer extendDays;

        /**
         * Obtiene el valor de la propiedad balanceType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBalanceType() {
            return balanceType;
        }

        /**
         * Define el valor de la propiedad balanceType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBalanceType(String value) {
            this.balanceType = value;
        }

        /**
         * Obtiene el valor de la propiedad balanceID.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getBalanceID() {
            return balanceID;
        }

        /**
         * Define el valor de la propiedad balanceID.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setBalanceID(Long value) {
            this.balanceID = value;
        }

        /**
         * Obtiene el valor de la propiedad adjustmentType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdjustmentType() {
            return adjustmentType;
        }

        /**
         * Define el valor de la propiedad adjustmentType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdjustmentType(String value) {
            this.adjustmentType = value;
        }

        /**
         * Obtiene el valor de la propiedad adjustmentAmt.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getAdjustmentAmt() {
            return adjustmentAmt;
        }

        /**
         * Define el valor de la propiedad adjustmentAmt.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setAdjustmentAmt(Long value) {
            this.adjustmentAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Define el valor de la propiedad effectiveTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffectiveTime(String value) {
            this.effectiveTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expireTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpireTime() {
            return expireTime;
        }

        /**
         * Define el valor de la propiedad expireTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpireTime(String value) {
            this.expireTime = value;
        }

        /**
         * Obtiene el valor de la propiedad extendDays.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getExtendDays() {
            return extendDays;
        }

        /**
         * Define el valor de la propiedad extendDays.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setExtendDays(Integer value) {
            this.extendDays = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FreeUnitInstanceID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="FreeUnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AdjustmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AdjustmentAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExtendDays" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "freeUnitInstanceID",
        "freeUnitType",
        "adjustmentType",
        "adjustmentAmt",
        "effectiveTime",
        "expireTime",
        "extendDays"
    })
    public static class FreeUnitAdjustmentInfo {

        @XmlElement(name = "FreeUnitInstanceID")
        protected Long freeUnitInstanceID;
        @XmlElement(name = "FreeUnitType")
        protected String freeUnitType;
        @XmlElement(name = "AdjustmentType")
        protected String adjustmentType;
        @XmlElement(name = "AdjustmentAmt")
        protected Long adjustmentAmt;
        @XmlElement(name = "EffectiveTime")
        protected String effectiveTime;
        @XmlElement(name = "ExpireTime")
        protected String expireTime;
        @XmlElement(name = "ExtendDays")
        protected Integer extendDays;

        /**
         * Obtiene el valor de la propiedad freeUnitInstanceID.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getFreeUnitInstanceID() {
            return freeUnitInstanceID;
        }

        /**
         * Define el valor de la propiedad freeUnitInstanceID.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setFreeUnitInstanceID(Long value) {
            this.freeUnitInstanceID = value;
        }

        /**
         * Obtiene el valor de la propiedad freeUnitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFreeUnitType() {
            return freeUnitType;
        }

        /**
         * Define el valor de la propiedad freeUnitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFreeUnitType(String value) {
            this.freeUnitType = value;
        }

        /**
         * Obtiene el valor de la propiedad adjustmentType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdjustmentType() {
            return adjustmentType;
        }

        /**
         * Define el valor de la propiedad adjustmentType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdjustmentType(String value) {
            this.adjustmentType = value;
        }

        /**
         * Obtiene el valor de la propiedad adjustmentAmt.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getAdjustmentAmt() {
            return adjustmentAmt;
        }

        /**
         * Define el valor de la propiedad adjustmentAmt.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setAdjustmentAmt(Long value) {
            this.adjustmentAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Define el valor de la propiedad effectiveTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffectiveTime(String value) {
            this.effectiveTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expireTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpireTime() {
            return expireTime;
        }

        /**
         * Define el valor de la propiedad expireTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpireTime(String value) {
            this.expireTime = value;
        }

        /**
         * Obtiene el valor de la propiedad extendDays.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getExtendDays() {
            return extendDays;
        }

        /**
         * Define el valor de la propiedad extendDays.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setExtendDays(Integer value) {
            this.extendDays = value;
        }

    }

}
