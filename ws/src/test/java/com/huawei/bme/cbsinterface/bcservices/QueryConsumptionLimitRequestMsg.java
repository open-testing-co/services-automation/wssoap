
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader" form="unqualified"/>
 *         &lt;element name="QueryConsumptionLimitRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryConsumptionLimitRequest" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "queryConsumptionLimitRequest"
})
@XmlRootElement(name = "QueryConsumptionLimitRequestMsg")
public class QueryConsumptionLimitRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "QueryConsumptionLimitRequest", namespace = "", required = true)
    protected QueryConsumptionLimitRequest queryConsumptionLimitRequest;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad queryConsumptionLimitRequest.
     * 
     * @return
     *     possible object is
     *     {@link QueryConsumptionLimitRequest }
     *     
     */
    public QueryConsumptionLimitRequest getQueryConsumptionLimitRequest() {
        return queryConsumptionLimitRequest;
    }

    /**
     * Define el valor de la propiedad queryConsumptionLimitRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryConsumptionLimitRequest }
     *     
     */
    public void setQueryConsumptionLimitRequest(QueryConsumptionLimitRequest value) {
        this.queryConsumptionLimitRequest = value;
    }

}
