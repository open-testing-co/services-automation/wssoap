
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;


/**
 * <p>Clase Java para QueryOfferingRentFailedFeeResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryOfferingRentFailedFeeResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingRentFailedFee" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OfferingOwner">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
 *                             &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
 *                             &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                   &lt;element name="BillCycleId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BillCycleBeginTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BillCycleEndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UnpaidRentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryOfferingRentFailedFeeResult", propOrder = {
    "offeringRentFailedFee"
})
public class QueryOfferingRentFailedFeeResult {

    @XmlElement(name = "OfferingRentFailedFee")
    protected List<QueryOfferingRentFailedFeeResult.OfferingRentFailedFee> offeringRentFailedFee;

    /**
     * Gets the value of the offeringRentFailedFee property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offeringRentFailedFee property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOfferingRentFailedFee().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryOfferingRentFailedFeeResult.OfferingRentFailedFee }
     * 
     * 
     */
    public List<QueryOfferingRentFailedFeeResult.OfferingRentFailedFee> getOfferingRentFailedFee() {
        if (offeringRentFailedFee == null) {
            offeringRentFailedFee = new ArrayList<QueryOfferingRentFailedFeeResult.OfferingRentFailedFee>();
        }
        return this.offeringRentFailedFee;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OfferingOwner">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
     *                   &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
     *                   &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *         &lt;element name="BillCycleId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BillCycleBeginTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BillCycleEndTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UnpaidRentAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offeringOwner",
        "offeringKey",
        "billCycleId",
        "billCycleBeginTime",
        "billCycleEndTime",
        "unpaidRentAmount",
        "currencyID"
    })
    public static class OfferingRentFailedFee {

        @XmlElement(name = "OfferingOwner", required = true)
        protected QueryOfferingRentFailedFeeResult.OfferingRentFailedFee.OfferingOwner offeringOwner;
        @XmlElement(name = "OfferingKey", required = true)
        protected OfferingKey offeringKey;
        @XmlElement(name = "BillCycleId", required = true)
        protected String billCycleId;
        @XmlElement(name = "BillCycleBeginTime", required = true)
        protected String billCycleBeginTime;
        @XmlElement(name = "BillCycleEndTime", required = true)
        protected String billCycleEndTime;
        @XmlElement(name = "UnpaidRentAmount")
        protected long unpaidRentAmount;
        @XmlElement(name = "CurrencyID")
        protected BigInteger currencyID;

        /**
         * Obtiene el valor de la propiedad offeringOwner.
         * 
         * @return
         *     possible object is
         *     {@link QueryOfferingRentFailedFeeResult.OfferingRentFailedFee.OfferingOwner }
         *     
         */
        public QueryOfferingRentFailedFeeResult.OfferingRentFailedFee.OfferingOwner getOfferingOwner() {
            return offeringOwner;
        }

        /**
         * Define el valor de la propiedad offeringOwner.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryOfferingRentFailedFeeResult.OfferingRentFailedFee.OfferingOwner }
         *     
         */
        public void setOfferingOwner(QueryOfferingRentFailedFeeResult.OfferingRentFailedFee.OfferingOwner value) {
            this.offeringOwner = value;
        }

        /**
         * Obtiene el valor de la propiedad offeringKey.
         * 
         * @return
         *     possible object is
         *     {@link OfferingKey }
         *     
         */
        public OfferingKey getOfferingKey() {
            return offeringKey;
        }

        /**
         * Define el valor de la propiedad offeringKey.
         * 
         * @param value
         *     allowed object is
         *     {@link OfferingKey }
         *     
         */
        public void setOfferingKey(OfferingKey value) {
            this.offeringKey = value;
        }

        /**
         * Obtiene el valor de la propiedad billCycleId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillCycleId() {
            return billCycleId;
        }

        /**
         * Define el valor de la propiedad billCycleId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillCycleId(String value) {
            this.billCycleId = value;
        }

        /**
         * Obtiene el valor de la propiedad billCycleBeginTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillCycleBeginTime() {
            return billCycleBeginTime;
        }

        /**
         * Define el valor de la propiedad billCycleBeginTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillCycleBeginTime(String value) {
            this.billCycleBeginTime = value;
        }

        /**
         * Obtiene el valor de la propiedad billCycleEndTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillCycleEndTime() {
            return billCycleEndTime;
        }

        /**
         * Define el valor de la propiedad billCycleEndTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillCycleEndTime(String value) {
            this.billCycleEndTime = value;
        }

        /**
         * Obtiene el valor de la propiedad unpaidRentAmount.
         * 
         */
        public long getUnpaidRentAmount() {
            return unpaidRentAmount;
        }

        /**
         * Define el valor de la propiedad unpaidRentAmount.
         * 
         */
        public void setUnpaidRentAmount(long value) {
            this.unpaidRentAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
         *         &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
         *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "subAccessCode",
            "custAccessCode",
            "subGroupAccessCode"
        })
        public static class OfferingOwner {

            @XmlElement(name = "SubAccessCode")
            protected SubAccessCode subAccessCode;
            @XmlElement(name = "CustAccessCode")
            protected CustAccessCode custAccessCode;
            @XmlElement(name = "SubGroupAccessCode")
            protected SubGroupAccessCode subGroupAccessCode;

            /**
             * Obtiene el valor de la propiedad subAccessCode.
             * 
             * @return
             *     possible object is
             *     {@link SubAccessCode }
             *     
             */
            public SubAccessCode getSubAccessCode() {
                return subAccessCode;
            }

            /**
             * Define el valor de la propiedad subAccessCode.
             * 
             * @param value
             *     allowed object is
             *     {@link SubAccessCode }
             *     
             */
            public void setSubAccessCode(SubAccessCode value) {
                this.subAccessCode = value;
            }

            /**
             * Obtiene el valor de la propiedad custAccessCode.
             * 
             * @return
             *     possible object is
             *     {@link CustAccessCode }
             *     
             */
            public CustAccessCode getCustAccessCode() {
                return custAccessCode;
            }

            /**
             * Define el valor de la propiedad custAccessCode.
             * 
             * @param value
             *     allowed object is
             *     {@link CustAccessCode }
             *     
             */
            public void setCustAccessCode(CustAccessCode value) {
                this.custAccessCode = value;
            }

            /**
             * Obtiene el valor de la propiedad subGroupAccessCode.
             * 
             * @return
             *     possible object is
             *     {@link SubGroupAccessCode }
             *     
             */
            public SubGroupAccessCode getSubGroupAccessCode() {
                return subGroupAccessCode;
            }

            /**
             * Define el valor de la propiedad subGroupAccessCode.
             * 
             * @param value
             *     allowed object is
             *     {@link SubGroupAccessCode }
             *     
             */
            public void setSubGroupAccessCode(SubGroupAccessCode value) {
                this.subGroupAccessCode = value;
            }

        }

    }

}
