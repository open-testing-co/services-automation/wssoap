
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;


/**
 * <p>Clase Java para QuerySubscribedOfferingsRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QuerySubscribedOfferingsRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
 *         &lt;element name="QueryMode" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="TotalNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubscribedOfferingsRequest", propOrder = {
    "subAccessCode",
    "queryMode",
    "totalNum",
    "beginRowNum",
    "fetchRowNum"
})
public class QuerySubscribedOfferingsRequest {

    @XmlElement(name = "SubAccessCode", required = true)
    protected SubAccessCode subAccessCode;
    @XmlElement(name = "QueryMode", required = true)
    protected BigInteger queryMode;
    @XmlElement(name = "TotalNum", required = true)
    protected BigInteger totalNum;
    @XmlElement(name = "BeginRowNum", required = true)
    protected BigInteger beginRowNum;
    @XmlElement(name = "FetchRowNum", required = true)
    protected BigInteger fetchRowNum;

    /**
     * Obtiene el valor de la propiedad subAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubAccessCode }
     *     
     */
    public SubAccessCode getSubAccessCode() {
        return subAccessCode;
    }

    /**
     * Define el valor de la propiedad subAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubAccessCode }
     *     
     */
    public void setSubAccessCode(SubAccessCode value) {
        this.subAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad queryMode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQueryMode() {
        return queryMode;
    }

    /**
     * Define el valor de la propiedad queryMode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQueryMode(BigInteger value) {
        this.queryMode = value;
    }

    /**
     * Obtiene el valor de la propiedad totalNum.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalNum() {
        return totalNum;
    }

    /**
     * Define el valor de la propiedad totalNum.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalNum(BigInteger value) {
        this.totalNum = value;
    }

    /**
     * Obtiene el valor de la propiedad beginRowNum.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Define el valor de la propiedad beginRowNum.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBeginRowNum(BigInteger value) {
        this.beginRowNum = value;
    }

    /**
     * Obtiene el valor de la propiedad fetchRowNum.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Define el valor de la propiedad fetchRowNum.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFetchRowNum(BigInteger value) {
        this.fetchRowNum = value;
    }

}
