
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubGroupBasicInfo;


/**
 * <p>Clase Java para ChangeGroupBasicInfoRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeGroupBasicInfoRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode"/>
 *         &lt;element name="SubGroupBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupBasicInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeGroupBasicInfoRequest", propOrder = {
    "subGroupAccessCode",
    "subGroupBasicInfo"
})
public class ChangeGroupBasicInfoRequest {

    @XmlElement(name = "SubGroupAccessCode", required = true)
    protected SubGroupAccessCode subGroupAccessCode;
    @XmlElement(name = "SubGroupBasicInfo", required = true)
    protected SubGroupBasicInfo subGroupBasicInfo;

    /**
     * Obtiene el valor de la propiedad subGroupAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public SubGroupAccessCode getSubGroupAccessCode() {
        return subGroupAccessCode;
    }

    /**
     * Define el valor de la propiedad subGroupAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public void setSubGroupAccessCode(SubGroupAccessCode value) {
        this.subGroupAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad subGroupBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupBasicInfo }
     *     
     */
    public SubGroupBasicInfo getSubGroupBasicInfo() {
        return subGroupBasicInfo;
    }

    /**
     * Define el valor de la propiedad subGroupBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupBasicInfo }
     *     
     */
    public void setSubGroupBasicInfo(SubGroupBasicInfo value) {
        this.subGroupBasicInfo = value;
    }

}
