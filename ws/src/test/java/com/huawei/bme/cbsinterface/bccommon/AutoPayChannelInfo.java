
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AutoPayChannelInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AutoPayChannelInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="BankBranchCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="AcctType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="CreditCardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="AcctNo" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="CVVNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ExpDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *         &lt;element name="PaymentPlan" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AutoPayType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="BillCycleSchema" minOccurs="0" form="qualified">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AutoPayDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                             &lt;element name="AutoPayMaxAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TimeSchema" minOccurs="0" form="qualified">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PeriodType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                             &lt;element name="AutoRechargeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                             &lt;element name="AutoRechargeAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="LowBalanceSchema" minOccurs="0" form="qualified">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="BalanceThreshold" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                             &lt;element name="AutoRechargeAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                             &lt;element name="ControlPeriodType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                             &lt;element name="MaxTimes" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoPayChannelInfo", propOrder = {
    "bankCode",
    "bankBranchCode",
    "acctType",
    "creditCardType",
    "acctNo",
    "acctName",
    "cvvNumber",
    "expDate",
    "priority",
    "chargeCode",
    "paymentPlan"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo.class,
    com.huawei.bme.cbsinterface.bcservices.SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo.class,
    com.huawei.bme.cbsinterface.bccommon.AccountInfo.AutoPayChannel.AutoPayChannelInfo.class
})
public class AutoPayChannelInfo {

    @XmlElement(name = "BankCode", required = true)
    protected String bankCode;
    @XmlElement(name = "BankBranchCode")
    protected String bankBranchCode;
    @XmlElement(name = "AcctType", required = true)
    protected String acctType;
    @XmlElement(name = "CreditCardType")
    protected String creditCardType;
    @XmlElement(name = "AcctNo", required = true)
    protected String acctNo;
    @XmlElement(name = "AcctName", required = true)
    protected String acctName;
    @XmlElement(name = "CVVNumber")
    protected String cvvNumber;
    @XmlElement(name = "ExpDate")
    protected String expDate;
    @XmlElement(name = "Priority")
    protected BigInteger priority;
    @XmlElement(name = "ChargeCode")
    protected List<String> chargeCode;
    @XmlElement(name = "PaymentPlan")
    protected List<AutoPayChannelInfo.PaymentPlan> paymentPlan;

    /**
     * Obtiene el valor de la propiedad bankCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * Define el valor de la propiedad bankCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankCode(String value) {
        this.bankCode = value;
    }

    /**
     * Obtiene el valor de la propiedad bankBranchCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankBranchCode() {
        return bankBranchCode;
    }

    /**
     * Define el valor de la propiedad bankBranchCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankBranchCode(String value) {
        this.bankBranchCode = value;
    }

    /**
     * Obtiene el valor de la propiedad acctType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctType() {
        return acctType;
    }

    /**
     * Define el valor de la propiedad acctType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctType(String value) {
        this.acctType = value;
    }

    /**
     * Obtiene el valor de la propiedad creditCardType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditCardType() {
        return creditCardType;
    }

    /**
     * Define el valor de la propiedad creditCardType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditCardType(String value) {
        this.creditCardType = value;
    }

    /**
     * Obtiene el valor de la propiedad acctNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctNo() {
        return acctNo;
    }

    /**
     * Define el valor de la propiedad acctNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctNo(String value) {
        this.acctNo = value;
    }

    /**
     * Obtiene el valor de la propiedad acctName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctName() {
        return acctName;
    }

    /**
     * Define el valor de la propiedad acctName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctName(String value) {
        this.acctName = value;
    }

    /**
     * Obtiene el valor de la propiedad cvvNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCVVNumber() {
        return cvvNumber;
    }

    /**
     * Define el valor de la propiedad cvvNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCVVNumber(String value) {
        this.cvvNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad expDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpDate() {
        return expDate;
    }

    /**
     * Define el valor de la propiedad expDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpDate(String value) {
        this.expDate = value;
    }

    /**
     * Obtiene el valor de la propiedad priority.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPriority() {
        return priority;
    }

    /**
     * Define el valor de la propiedad priority.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPriority(BigInteger value) {
        this.priority = value;
    }

    /**
     * Gets the value of the chargeCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chargeCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChargeCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getChargeCode() {
        if (chargeCode == null) {
            chargeCode = new ArrayList<String>();
        }
        return this.chargeCode;
    }

    /**
     * Gets the value of the paymentPlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentPlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentPlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AutoPayChannelInfo.PaymentPlan }
     * 
     * 
     */
    public List<AutoPayChannelInfo.PaymentPlan> getPaymentPlan() {
        if (paymentPlan == null) {
            paymentPlan = new ArrayList<AutoPayChannelInfo.PaymentPlan>();
        }
        return this.paymentPlan;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AutoPayType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="BillCycleSchema" minOccurs="0" form="qualified">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AutoPayDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *                   &lt;element name="AutoPayMaxAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TimeSchema" minOccurs="0" form="qualified">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PeriodType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *                   &lt;element name="AutoRechargeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *                   &lt;element name="AutoRechargeAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="LowBalanceSchema" minOccurs="0" form="qualified">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="BalanceThreshold" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *                   &lt;element name="AutoRechargeAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *                   &lt;element name="ControlPeriodType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *                   &lt;element name="MaxTimes" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "autoPayType",
        "billCycleSchema",
        "timeSchema",
        "lowBalanceSchema"
    })
    public static class PaymentPlan {

        @XmlElement(name = "AutoPayType", required = true)
        protected String autoPayType;
        @XmlElement(name = "BillCycleSchema")
        protected AutoPayChannelInfo.PaymentPlan.BillCycleSchema billCycleSchema;
        @XmlElement(name = "TimeSchema")
        protected AutoPayChannelInfo.PaymentPlan.TimeSchema timeSchema;
        @XmlElement(name = "LowBalanceSchema")
        protected AutoPayChannelInfo.PaymentPlan.LowBalanceSchema lowBalanceSchema;

        /**
         * Obtiene el valor de la propiedad autoPayType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAutoPayType() {
            return autoPayType;
        }

        /**
         * Define el valor de la propiedad autoPayType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAutoPayType(String value) {
            this.autoPayType = value;
        }

        /**
         * Obtiene el valor de la propiedad billCycleSchema.
         * 
         * @return
         *     possible object is
         *     {@link AutoPayChannelInfo.PaymentPlan.BillCycleSchema }
         *     
         */
        public AutoPayChannelInfo.PaymentPlan.BillCycleSchema getBillCycleSchema() {
            return billCycleSchema;
        }

        /**
         * Define el valor de la propiedad billCycleSchema.
         * 
         * @param value
         *     allowed object is
         *     {@link AutoPayChannelInfo.PaymentPlan.BillCycleSchema }
         *     
         */
        public void setBillCycleSchema(AutoPayChannelInfo.PaymentPlan.BillCycleSchema value) {
            this.billCycleSchema = value;
        }

        /**
         * Obtiene el valor de la propiedad timeSchema.
         * 
         * @return
         *     possible object is
         *     {@link AutoPayChannelInfo.PaymentPlan.TimeSchema }
         *     
         */
        public AutoPayChannelInfo.PaymentPlan.TimeSchema getTimeSchema() {
            return timeSchema;
        }

        /**
         * Define el valor de la propiedad timeSchema.
         * 
         * @param value
         *     allowed object is
         *     {@link AutoPayChannelInfo.PaymentPlan.TimeSchema }
         *     
         */
        public void setTimeSchema(AutoPayChannelInfo.PaymentPlan.TimeSchema value) {
            this.timeSchema = value;
        }

        /**
         * Obtiene el valor de la propiedad lowBalanceSchema.
         * 
         * @return
         *     possible object is
         *     {@link AutoPayChannelInfo.PaymentPlan.LowBalanceSchema }
         *     
         */
        public AutoPayChannelInfo.PaymentPlan.LowBalanceSchema getLowBalanceSchema() {
            return lowBalanceSchema;
        }

        /**
         * Define el valor de la propiedad lowBalanceSchema.
         * 
         * @param value
         *     allowed object is
         *     {@link AutoPayChannelInfo.PaymentPlan.LowBalanceSchema }
         *     
         */
        public void setLowBalanceSchema(AutoPayChannelInfo.PaymentPlan.LowBalanceSchema value) {
            this.lowBalanceSchema = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AutoPayDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
         *         &lt;element name="AutoPayMaxAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "autoPayDate",
            "autoPayMaxAmt"
        })
        public static class BillCycleSchema {

            @XmlElement(name = "AutoPayDate")
            protected String autoPayDate;
            @XmlElement(name = "AutoPayMaxAmt")
            protected Long autoPayMaxAmt;

            /**
             * Obtiene el valor de la propiedad autoPayDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAutoPayDate() {
                return autoPayDate;
            }

            /**
             * Define el valor de la propiedad autoPayDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAutoPayDate(String value) {
                this.autoPayDate = value;
            }

            /**
             * Obtiene el valor de la propiedad autoPayMaxAmt.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getAutoPayMaxAmt() {
                return autoPayMaxAmt;
            }

            /**
             * Define el valor de la propiedad autoPayMaxAmt.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setAutoPayMaxAmt(Long value) {
                this.autoPayMaxAmt = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="BalanceThreshold" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
         *         &lt;element name="AutoRechargeAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
         *         &lt;element name="ControlPeriodType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
         *         &lt;element name="MaxTimes" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "balanceThreshold",
            "autoRechargeAmt",
            "controlPeriodType",
            "maxTimes"
        })
        public static class LowBalanceSchema {

            @XmlElement(name = "BalanceThreshold")
            protected long balanceThreshold;
            @XmlElement(name = "AutoRechargeAmt")
            protected long autoRechargeAmt;
            @XmlElement(name = "ControlPeriodType")
            protected String controlPeriodType;
            @XmlElement(name = "MaxTimes")
            protected BigInteger maxTimes;

            /**
             * Obtiene el valor de la propiedad balanceThreshold.
             * 
             */
            public long getBalanceThreshold() {
                return balanceThreshold;
            }

            /**
             * Define el valor de la propiedad balanceThreshold.
             * 
             */
            public void setBalanceThreshold(long value) {
                this.balanceThreshold = value;
            }

            /**
             * Obtiene el valor de la propiedad autoRechargeAmt.
             * 
             */
            public long getAutoRechargeAmt() {
                return autoRechargeAmt;
            }

            /**
             * Define el valor de la propiedad autoRechargeAmt.
             * 
             */
            public void setAutoRechargeAmt(long value) {
                this.autoRechargeAmt = value;
            }

            /**
             * Obtiene el valor de la propiedad controlPeriodType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getControlPeriodType() {
                return controlPeriodType;
            }

            /**
             * Define el valor de la propiedad controlPeriodType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setControlPeriodType(String value) {
                this.controlPeriodType = value;
            }

            /**
             * Obtiene el valor de la propiedad maxTimes.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getMaxTimes() {
                return maxTimes;
            }

            /**
             * Define el valor de la propiedad maxTimes.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setMaxTimes(BigInteger value) {
                this.maxTimes = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PeriodType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
         *         &lt;element name="AutoRechargeDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
         *         &lt;element name="AutoRechargeAmt" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "periodType",
            "autoRechargeDate",
            "autoRechargeAmt"
        })
        public static class TimeSchema {

            @XmlElement(name = "PeriodType", required = true)
            protected String periodType;
            @XmlElement(name = "AutoRechargeDate")
            protected String autoRechargeDate;
            @XmlElement(name = "AutoRechargeAmt")
            protected long autoRechargeAmt;

            /**
             * Obtiene el valor de la propiedad periodType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPeriodType() {
                return periodType;
            }

            /**
             * Define el valor de la propiedad periodType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPeriodType(String value) {
                this.periodType = value;
            }

            /**
             * Obtiene el valor de la propiedad autoRechargeDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAutoRechargeDate() {
                return autoRechargeDate;
            }

            /**
             * Define el valor de la propiedad autoRechargeDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAutoRechargeDate(String value) {
                this.autoRechargeDate = value;
            }

            /**
             * Obtiene el valor de la propiedad autoRechargeAmt.
             * 
             */
            public long getAutoRechargeAmt() {
                return autoRechargeAmt;
            }

            /**
             * Define el valor de la propiedad autoRechargeAmt.
             * 
             */
            public void setAutoRechargeAmt(long value) {
                this.autoRechargeAmt = value;
            }

        }

    }

}
