
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;
import com.huawei.bme.cbsinterface.bccommon.Tax;


/**
 * <p>Clase Java para FeeDeductionRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FeeDeductionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DeductSerialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeductObj">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
 *                   &lt;element name="AcctAccessCode" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
 *                           &lt;sequence>
 *                             &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
 *                   &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OperationInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OperationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OperationProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DeductInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ChargeSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SalesTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="InvoiceTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ChargeAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="DiscountAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="WaiveAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="Tax" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Tax" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FeeDeductionRequest", propOrder = {
    "deductSerialNo",
    "deductObj",
    "operationInfo",
    "deductInfo",
    "additionalProperty"
})
public class FeeDeductionRequest {

    @XmlElement(name = "DeductSerialNo")
    protected String deductSerialNo;
    @XmlElement(name = "DeductObj", required = true)
    protected FeeDeductionRequest.DeductObj deductObj;
    @XmlElement(name = "OperationInfo")
    protected FeeDeductionRequest.OperationInfo operationInfo;
    @XmlElement(name = "DeductInfo")
    protected List<FeeDeductionRequest.DeductInfo> deductInfo;
    @XmlElement(name = "AdditionalProperty")
    protected List<SimpleProperty> additionalProperty;

    /**
     * Obtiene el valor de la propiedad deductSerialNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeductSerialNo() {
        return deductSerialNo;
    }

    /**
     * Define el valor de la propiedad deductSerialNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeductSerialNo(String value) {
        this.deductSerialNo = value;
    }

    /**
     * Obtiene el valor de la propiedad deductObj.
     * 
     * @return
     *     possible object is
     *     {@link FeeDeductionRequest.DeductObj }
     *     
     */
    public FeeDeductionRequest.DeductObj getDeductObj() {
        return deductObj;
    }

    /**
     * Define el valor de la propiedad deductObj.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeDeductionRequest.DeductObj }
     *     
     */
    public void setDeductObj(FeeDeductionRequest.DeductObj value) {
        this.deductObj = value;
    }

    /**
     * Obtiene el valor de la propiedad operationInfo.
     * 
     * @return
     *     possible object is
     *     {@link FeeDeductionRequest.OperationInfo }
     *     
     */
    public FeeDeductionRequest.OperationInfo getOperationInfo() {
        return operationInfo;
    }

    /**
     * Define el valor de la propiedad operationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeDeductionRequest.OperationInfo }
     *     
     */
    public void setOperationInfo(FeeDeductionRequest.OperationInfo value) {
        this.operationInfo = value;
    }

    /**
     * Gets the value of the deductInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the deductInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDeductInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeeDeductionRequest.DeductInfo }
     * 
     * 
     */
    public List<FeeDeductionRequest.DeductInfo> getDeductInfo() {
        if (deductInfo == null) {
            deductInfo = new ArrayList<FeeDeductionRequest.DeductInfo>();
        }
        return this.deductInfo;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<SimpleProperty>();
        }
        return this.additionalProperty;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ChargeSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SalesTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="InvoiceTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ChargeAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="DiscountAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="WaiveAmt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="Tax" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Tax" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="AdditionalInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AdditionalProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "chargeSeq",
        "chargeCode",
        "salesTime",
        "invoiceTime",
        "chargeAmt",
        "discountAmt",
        "waiveAmt",
        "currencyID",
        "tax",
        "additionalInfo",
        "additionalProperty"
    })
    public static class DeductInfo {

        @XmlElement(name = "ChargeSeq")
        protected String chargeSeq;
        @XmlElement(name = "ChargeCode", required = true)
        protected String chargeCode;
        @XmlElement(name = "SalesTime")
        protected String salesTime;
        @XmlElement(name = "InvoiceTime")
        protected String invoiceTime;
        @XmlElement(name = "ChargeAmt")
        protected long chargeAmt;
        @XmlElement(name = "DiscountAmt")
        protected Long discountAmt;
        @XmlElement(name = "WaiveAmt")
        protected Long waiveAmt;
        @XmlElement(name = "CurrencyID", required = true)
        protected BigInteger currencyID;
        @XmlElement(name = "Tax")
        protected List<Tax> tax;
        @XmlElement(name = "AdditionalInfo")
        protected String additionalInfo;
        @XmlElement(name = "AdditionalProperty")
        protected List<SimpleProperty> additionalProperty;

        /**
         * Obtiene el valor de la propiedad chargeSeq.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChargeSeq() {
            return chargeSeq;
        }

        /**
         * Define el valor de la propiedad chargeSeq.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChargeSeq(String value) {
            this.chargeSeq = value;
        }

        /**
         * Obtiene el valor de la propiedad chargeCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChargeCode() {
            return chargeCode;
        }

        /**
         * Define el valor de la propiedad chargeCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChargeCode(String value) {
            this.chargeCode = value;
        }

        /**
         * Obtiene el valor de la propiedad salesTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSalesTime() {
            return salesTime;
        }

        /**
         * Define el valor de la propiedad salesTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSalesTime(String value) {
            this.salesTime = value;
        }

        /**
         * Obtiene el valor de la propiedad invoiceTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvoiceTime() {
            return invoiceTime;
        }

        /**
         * Define el valor de la propiedad invoiceTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvoiceTime(String value) {
            this.invoiceTime = value;
        }

        /**
         * Obtiene el valor de la propiedad chargeAmt.
         * 
         */
        public long getChargeAmt() {
            return chargeAmt;
        }

        /**
         * Define el valor de la propiedad chargeAmt.
         * 
         */
        public void setChargeAmt(long value) {
            this.chargeAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad discountAmt.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getDiscountAmt() {
            return discountAmt;
        }

        /**
         * Define el valor de la propiedad discountAmt.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setDiscountAmt(Long value) {
            this.discountAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad waiveAmt.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getWaiveAmt() {
            return waiveAmt;
        }

        /**
         * Define el valor de la propiedad waiveAmt.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setWaiveAmt(Long value) {
            this.waiveAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Gets the value of the tax property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tax property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTax().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tax }
         * 
         * 
         */
        public List<Tax> getTax() {
            if (tax == null) {
                tax = new ArrayList<Tax>();
            }
            return this.tax;
        }

        /**
         * Obtiene el valor de la propiedad additionalInfo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAdditionalInfo() {
            return additionalInfo;
        }

        /**
         * Define el valor de la propiedad additionalInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAdditionalInfo(String value) {
            this.additionalInfo = value;
        }

        /**
         * Gets the value of the additionalProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdditionalProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getAdditionalProperty() {
            if (additionalProperty == null) {
                additionalProperty = new ArrayList<SimpleProperty>();
            }
            return this.additionalProperty;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
     *         &lt;element name="AcctAccessCode" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
     *                 &lt;sequence>
     *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode" minOccurs="0"/>
     *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subAccessCode",
        "acctAccessCode",
        "custAccessCode",
        "subGroupAccessCode"
    })
    public static class DeductObj {

        @XmlElement(name = "SubAccessCode")
        protected SubAccessCode subAccessCode;
        @XmlElement(name = "AcctAccessCode")
        protected FeeDeductionRequest.DeductObj.AcctAccessCode acctAccessCode;
        @XmlElement(name = "CustAccessCode")
        protected CustAccessCode custAccessCode;
        @XmlElement(name = "SubGroupAccessCode")
        protected SubGroupAccessCode subGroupAccessCode;

        /**
         * Obtiene el valor de la propiedad subAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubAccessCode }
         *     
         */
        public SubAccessCode getSubAccessCode() {
            return subAccessCode;
        }

        /**
         * Define el valor de la propiedad subAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubAccessCode }
         *     
         */
        public void setSubAccessCode(SubAccessCode value) {
            this.subAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad acctAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link FeeDeductionRequest.DeductObj.AcctAccessCode }
         *     
         */
        public FeeDeductionRequest.DeductObj.AcctAccessCode getAcctAccessCode() {
            return acctAccessCode;
        }

        /**
         * Define el valor de la propiedad acctAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link FeeDeductionRequest.DeductObj.AcctAccessCode }
         *     
         */
        public void setAcctAccessCode(FeeDeductionRequest.DeductObj.AcctAccessCode value) {
            this.acctAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad custAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link CustAccessCode }
         *     
         */
        public CustAccessCode getCustAccessCode() {
            return custAccessCode;
        }

        /**
         * Define el valor de la propiedad custAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link CustAccessCode }
         *     
         */
        public void setCustAccessCode(CustAccessCode value) {
            this.custAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public SubGroupAccessCode getSubGroupAccessCode() {
            return subGroupAccessCode;
        }

        /**
         * Define el valor de la propiedad subGroupAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public void setSubGroupAccessCode(SubGroupAccessCode value) {
            this.subGroupAccessCode = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
         *       &lt;sequence>
         *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "payType"
        })
        public static class AcctAccessCode
            extends com.huawei.bme.cbsinterface.bccommon.AcctAccessCode
        {

            @XmlElement(name = "PayType")
            protected String payType;

            /**
             * Obtiene el valor de la propiedad payType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayType() {
                return payType;
            }

            /**
             * Define el valor de la propiedad payType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayType(String value) {
                this.payType = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OperationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OperationProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "operationCode",
        "operationProperty"
    })
    public static class OperationInfo {

        @XmlElement(name = "OperationCode", required = true)
        protected String operationCode;
        @XmlElement(name = "OperationProperty")
        protected List<SimpleProperty> operationProperty;

        /**
         * Obtiene el valor de la propiedad operationCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperationCode() {
            return operationCode;
        }

        /**
         * Define el valor de la propiedad operationCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperationCode(String value) {
            this.operationCode = value;
        }

        /**
         * Gets the value of the operationProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the operationProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getOperationProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getOperationProperty() {
            if (operationProperty == null) {
                operationProperty = new ArrayList<SimpleProperty>();
            }
            return this.operationProperty;
        }

    }

}
