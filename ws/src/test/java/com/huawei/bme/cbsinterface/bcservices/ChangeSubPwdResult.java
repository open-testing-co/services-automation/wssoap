
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.BalanceChgInfo;
import com.huawei.bme.cbsinterface.bccommon.FreeUnitChgInfo;


/**
 * <p>Clase Java para ChangeSubPwdResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeSubPwdResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctBalanceChangeList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="BalanceChgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}BalanceChgInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FreeUnitChangeList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OwnerType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OwnerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FreeUnitChgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}FreeUnitChgInfo" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CreditLimitChangeList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="OriginLimitAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="PaidAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrentAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="AccmBeginDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AccmEndDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeSubPwdResult", propOrder = {
    "acctBalanceChangeList",
    "freeUnitChangeList",
    "creditLimitChangeList"
})
public class ChangeSubPwdResult {

    @XmlElement(name = "AcctBalanceChangeList")
    protected List<ChangeSubPwdResult.AcctBalanceChangeList> acctBalanceChangeList;
    @XmlElement(name = "FreeUnitChangeList", nillable = true)
    protected List<ChangeSubPwdResult.FreeUnitChangeList> freeUnitChangeList;
    @XmlElement(name = "CreditLimitChangeList")
    protected List<ChangeSubPwdResult.CreditLimitChangeList> creditLimitChangeList;

    /**
     * Gets the value of the acctBalanceChangeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acctBalanceChangeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcctBalanceChangeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeSubPwdResult.AcctBalanceChangeList }
     * 
     * 
     */
    public List<ChangeSubPwdResult.AcctBalanceChangeList> getAcctBalanceChangeList() {
        if (acctBalanceChangeList == null) {
            acctBalanceChangeList = new ArrayList<ChangeSubPwdResult.AcctBalanceChangeList>();
        }
        return this.acctBalanceChangeList;
    }

    /**
     * Gets the value of the freeUnitChangeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the freeUnitChangeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreeUnitChangeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeSubPwdResult.FreeUnitChangeList }
     * 
     * 
     */
    public List<ChangeSubPwdResult.FreeUnitChangeList> getFreeUnitChangeList() {
        if (freeUnitChangeList == null) {
            freeUnitChangeList = new ArrayList<ChangeSubPwdResult.FreeUnitChangeList>();
        }
        return this.freeUnitChangeList;
    }

    /**
     * Gets the value of the creditLimitChangeList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditLimitChangeList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditLimitChangeList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeSubPwdResult.CreditLimitChangeList }
     * 
     * 
     */
    public List<ChangeSubPwdResult.CreditLimitChangeList> getCreditLimitChangeList() {
        if (creditLimitChangeList == null) {
            creditLimitChangeList = new ArrayList<ChangeSubPwdResult.CreditLimitChangeList>();
        }
        return this.creditLimitChangeList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="BalanceChgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}BalanceChgInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "balanceChgInfo"
    })
    public static class AcctBalanceChangeList {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "BalanceChgInfo", required = true, nillable = true)
        protected List<BalanceChgInfo> balanceChgInfo;

        /**
         * Obtiene el valor de la propiedad acctKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Define el valor de la propiedad acctKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Gets the value of the balanceChgInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the balanceChgInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBalanceChgInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BalanceChgInfo }
         * 
         * 
         */
        public List<BalanceChgInfo> getBalanceChgInfo() {
            if (balanceChgInfo == null) {
                balanceChgInfo = new ArrayList<BalanceChgInfo>();
            }
            return this.balanceChgInfo;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CreditLimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CreditInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="OriginLimitAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="PaidAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrentAmt" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="AccmBeginDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AccmEndDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "creditLimitType",
        "creditInstID",
        "originLimitAmt",
        "paidAmt",
        "currentAmt",
        "currencyID",
        "accmBeginDate",
        "accmEndDate"
    })
    public static class CreditLimitChangeList {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "CreditLimitType", required = true)
        protected String creditLimitType;
        @XmlElement(name = "CreditInstID")
        protected long creditInstID;
        @XmlElement(name = "OriginLimitAmt")
        protected long originLimitAmt;
        @XmlElement(name = "PaidAmt")
        protected long paidAmt;
        @XmlElement(name = "CurrentAmt")
        protected long currentAmt;
        @XmlElement(name = "CurrencyID", required = true)
        protected BigInteger currencyID;
        @XmlElement(name = "AccmBeginDate", required = true)
        protected String accmBeginDate;
        @XmlElement(name = "AccmEndDate", required = true)
        protected String accmEndDate;

        /**
         * Obtiene el valor de la propiedad acctKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Define el valor de la propiedad acctKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Obtiene el valor de la propiedad creditLimitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCreditLimitType() {
            return creditLimitType;
        }

        /**
         * Define el valor de la propiedad creditLimitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCreditLimitType(String value) {
            this.creditLimitType = value;
        }

        /**
         * Obtiene el valor de la propiedad creditInstID.
         * 
         */
        public long getCreditInstID() {
            return creditInstID;
        }

        /**
         * Define el valor de la propiedad creditInstID.
         * 
         */
        public void setCreditInstID(long value) {
            this.creditInstID = value;
        }

        /**
         * Obtiene el valor de la propiedad originLimitAmt.
         * 
         */
        public long getOriginLimitAmt() {
            return originLimitAmt;
        }

        /**
         * Define el valor de la propiedad originLimitAmt.
         * 
         */
        public void setOriginLimitAmt(long value) {
            this.originLimitAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad paidAmt.
         * 
         */
        public long getPaidAmt() {
            return paidAmt;
        }

        /**
         * Define el valor de la propiedad paidAmt.
         * 
         */
        public void setPaidAmt(long value) {
            this.paidAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad currentAmt.
         * 
         */
        public long getCurrentAmt() {
            return currentAmt;
        }

        /**
         * Define el valor de la propiedad currentAmt.
         * 
         */
        public void setCurrentAmt(long value) {
            this.currentAmt = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Obtiene el valor de la propiedad accmBeginDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccmBeginDate() {
            return accmBeginDate;
        }

        /**
         * Define el valor de la propiedad accmBeginDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccmBeginDate(String value) {
            this.accmBeginDate = value;
        }

        /**
         * Obtiene el valor de la propiedad accmEndDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccmEndDate() {
            return accmEndDate;
        }

        /**
         * Define el valor de la propiedad accmEndDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccmEndDate(String value) {
            this.accmEndDate = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OwnerType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OwnerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FreeUnitChgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}FreeUnitChgInfo" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ownerType",
        "ownerKey",
        "freeUnitChgInfo"
    })
    public static class FreeUnitChangeList {

        @XmlElement(name = "OwnerType", required = true)
        protected String ownerType;
        @XmlElement(name = "OwnerKey", required = true)
        protected String ownerKey;
        @XmlElement(name = "FreeUnitChgInfo", required = true, nillable = true)
        protected List<FreeUnitChgInfo> freeUnitChgInfo;

        /**
         * Obtiene el valor de la propiedad ownerType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOwnerType() {
            return ownerType;
        }

        /**
         * Define el valor de la propiedad ownerType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOwnerType(String value) {
            this.ownerType = value;
        }

        /**
         * Obtiene el valor de la propiedad ownerKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOwnerKey() {
            return ownerKey;
        }

        /**
         * Define el valor de la propiedad ownerKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOwnerKey(String value) {
            this.ownerKey = value;
        }

        /**
         * Gets the value of the freeUnitChgInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the freeUnitChgInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFreeUnitChgInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link FreeUnitChgInfo }
         * 
         * 
         */
        public List<FreeUnitChgInfo> getFreeUnitChgInfo() {
            if (freeUnitChgInfo == null) {
                freeUnitChgInfo = new ArrayList<FreeUnitChgInfo>();
            }
            return this.freeUnitChgInfo;
        }

    }

}
