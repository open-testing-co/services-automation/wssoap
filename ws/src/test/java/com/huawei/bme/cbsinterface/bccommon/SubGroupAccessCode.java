
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SubGroupAccessCode complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubGroupAccessCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubGroupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubGroupAccessCode", propOrder = {
    "subGroupKey",
    "subGroupCode"
})
public class SubGroupAccessCode {

    @XmlElement(name = "SubGroupKey")
    protected String subGroupKey;
    @XmlElement(name = "SubGroupCode")
    protected String subGroupCode;

    /**
     * Obtiene el valor de la propiedad subGroupKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubGroupKey() {
        return subGroupKey;
    }

    /**
     * Define el valor de la propiedad subGroupKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubGroupKey(String value) {
        this.subGroupKey = value;
    }

    /**
     * Obtiene el valor de la propiedad subGroupCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubGroupCode() {
        return subGroupCode;
    }

    /**
     * Define el valor de la propiedad subGroupCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubGroupCode(String value) {
        this.subGroupCode = value;
    }

}
