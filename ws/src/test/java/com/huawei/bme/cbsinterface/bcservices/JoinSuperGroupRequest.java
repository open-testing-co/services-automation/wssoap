
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;


/**
 * <p>Clase Java para JoinSuperGroupRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="JoinSuperGroupRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SuperGroupAccess">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SuperGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SuperGroupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="members" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JoinSuperGroupRequest", propOrder = {
    "superGroupAccess",
    "members"
})
public class JoinSuperGroupRequest {

    @XmlElement(name = "SuperGroupAccess", required = true)
    protected JoinSuperGroupRequest.SuperGroupAccess superGroupAccess;
    @XmlElement(nillable = true)
    protected List<JoinSuperGroupRequest.Members> members;

    /**
     * Obtiene el valor de la propiedad superGroupAccess.
     * 
     * @return
     *     possible object is
     *     {@link JoinSuperGroupRequest.SuperGroupAccess }
     *     
     */
    public JoinSuperGroupRequest.SuperGroupAccess getSuperGroupAccess() {
        return superGroupAccess;
    }

    /**
     * Define el valor de la propiedad superGroupAccess.
     * 
     * @param value
     *     allowed object is
     *     {@link JoinSuperGroupRequest.SuperGroupAccess }
     *     
     */
    public void setSuperGroupAccess(JoinSuperGroupRequest.SuperGroupAccess value) {
        this.superGroupAccess = value;
    }

    /**
     * Gets the value of the members property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the members property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMembers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JoinSuperGroupRequest.Members }
     * 
     * 
     */
    public List<JoinSuperGroupRequest.Members> getMembers() {
        if (members == null) {
            members = new ArrayList<JoinSuperGroupRequest.Members>();
        }
        return this.members;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subGroupAccessCode"
    })
    public static class Members {

        @XmlElement(name = "SubGroupAccessCode", required = true)
        protected SubGroupAccessCode subGroupAccessCode;

        /**
         * Obtiene el valor de la propiedad subGroupAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public SubGroupAccessCode getSubGroupAccessCode() {
            return subGroupAccessCode;
        }

        /**
         * Define el valor de la propiedad subGroupAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubGroupAccessCode }
         *     
         */
        public void setSubGroupAccessCode(SubGroupAccessCode value) {
            this.subGroupAccessCode = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SuperGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SuperGroupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "superGroupKey",
        "superGroupCode"
    })
    public static class SuperGroupAccess {

        @XmlElement(name = "SuperGroupKey")
        protected String superGroupKey;
        @XmlElement(name = "SuperGroupCode")
        protected String superGroupCode;

        /**
         * Obtiene el valor de la propiedad superGroupKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuperGroupKey() {
            return superGroupKey;
        }

        /**
         * Define el valor de la propiedad superGroupKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuperGroupKey(String value) {
            this.superGroupKey = value;
        }

        /**
         * Obtiene el valor de la propiedad superGroupCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuperGroupCode() {
            return superGroupCode;
        }

        /**
         * Define el valor de la propiedad superGroupCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuperGroupCode(String value) {
            this.superGroupCode = value;
        }

    }

}
