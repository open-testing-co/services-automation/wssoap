
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Subscriber complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Subscriber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubBasicInfo" minOccurs="0" form="qualified"/>
 *         &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubIdentity" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubIdentity" maxOccurs="unbounded" form="qualified"/>
 *         &lt;element name="Brand" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="NetworkType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ConsumptionLimit" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;choice maxOccurs="unbounded" minOccurs="0">
 *                     &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                     &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *                     &lt;element name="LimitParam" form="qualified">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                               &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="MesureID" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *                     &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                     &lt;element name="UnitType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                     &lt;element name="MesureType" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *                   &lt;/choice>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Subscriber", propOrder = {
    "subBasicInfo",
    "userCustomerKey",
    "subIdentity",
    "brand",
    "subClass",
    "networkType",
    "subPassword",
    "status",
    "activeTimeLimit",
    "consumptionLimit"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoResult.Subscriber.SubscriberInfo.class
})
public class Subscriber {

    @XmlElement(name = "SubBasicInfo")
    protected SubBasicInfo subBasicInfo;
    @XmlElement(name = "UserCustomerKey")
    protected String userCustomerKey;
    @XmlElement(name = "SubIdentity", required = true)
    protected List<SubIdentity> subIdentity;
    @XmlElement(name = "Brand")
    protected String brand;
    @XmlElement(name = "SubClass")
    protected String subClass;
    @XmlElement(name = "NetworkType")
    protected String networkType;
    @XmlElement(name = "SubPassword")
    protected String subPassword;
    @XmlElement(name = "Status")
    protected String status;
    @XmlElement(name = "ActiveTimeLimit")
    protected String activeTimeLimit;
    @XmlElement(name = "ConsumptionLimit")
    protected List<Subscriber.ConsumptionLimit> consumptionLimit;

    /**
     * Obtiene el valor de la propiedad subBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link SubBasicInfo }
     *     
     */
    public SubBasicInfo getSubBasicInfo() {
        return subBasicInfo;
    }

    /**
     * Define el valor de la propiedad subBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SubBasicInfo }
     *     
     */
    public void setSubBasicInfo(SubBasicInfo value) {
        this.subBasicInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad userCustomerKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserCustomerKey() {
        return userCustomerKey;
    }

    /**
     * Define el valor de la propiedad userCustomerKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserCustomerKey(String value) {
        this.userCustomerKey = value;
    }

    /**
     * Gets the value of the subIdentity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subIdentity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubIdentity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubIdentity }
     * 
     * 
     */
    public List<SubIdentity> getSubIdentity() {
        if (subIdentity == null) {
            subIdentity = new ArrayList<SubIdentity>();
        }
        return this.subIdentity;
    }

    /**
     * Obtiene el valor de la propiedad brand.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Define el valor de la propiedad brand.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrand(String value) {
        this.brand = value;
    }

    /**
     * Obtiene el valor de la propiedad subClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubClass() {
        return subClass;
    }

    /**
     * Define el valor de la propiedad subClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubClass(String value) {
        this.subClass = value;
    }

    /**
     * Obtiene el valor de la propiedad networkType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkType() {
        return networkType;
    }

    /**
     * Define el valor de la propiedad networkType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkType(String value) {
        this.networkType = value;
    }

    /**
     * Obtiene el valor de la propiedad subPassword.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubPassword() {
        return subPassword;
    }

    /**
     * Define el valor de la propiedad subPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubPassword(String value) {
        this.subPassword = value;
    }

    /**
     * Obtiene el valor de la propiedad status.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Define el valor de la propiedad status.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Obtiene el valor de la propiedad activeTimeLimit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveTimeLimit() {
        return activeTimeLimit;
    }

    /**
     * Define el valor de la propiedad activeTimeLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveTimeLimit(String value) {
        this.activeTimeLimit = value;
    }

    /**
     * Gets the value of the consumptionLimit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consumptionLimit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsumptionLimit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Subscriber.ConsumptionLimit }
     * 
     * 
     */
    public List<Subscriber.ConsumptionLimit> getConsumptionLimit() {
        if (consumptionLimit == null) {
            consumptionLimit = new ArrayList<Subscriber.ConsumptionLimit>();
        }
        return this.consumptionLimit;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice maxOccurs="unbounded" minOccurs="0">
     *           &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *           &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
     *           &lt;element name="LimitParam" form="qualified">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *                     &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="MesureID" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
     *           &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *           &lt;element name="UnitType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *           &lt;element name="MesureType" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
     *         &lt;/choice>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitTypeOrCurrencyIDOrLimitParam"
    })
    public static class ConsumptionLimit {

        @XmlElementRefs({
            @XmlElementRef(name = "LimitValue", namespace = "http://www.huawei.com/bme/cbsinterface/bccommon", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LimitType", namespace = "http://www.huawei.com/bme/cbsinterface/bccommon", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "LimitParam", namespace = "http://www.huawei.com/bme/cbsinterface/bccommon", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "MesureType", namespace = "http://www.huawei.com/bme/cbsinterface/bccommon", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "UnitType", namespace = "http://www.huawei.com/bme/cbsinterface/bccommon", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "CurrencyID", namespace = "http://www.huawei.com/bme/cbsinterface/bccommon", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "MesureID", namespace = "http://www.huawei.com/bme/cbsinterface/bccommon", type = JAXBElement.class, required = false)
        })
        protected List<JAXBElement<?>> limitTypeOrCurrencyIDOrLimitParam;

        /**
         * Gets the value of the limitTypeOrCurrencyIDOrLimitParam property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the limitTypeOrCurrencyIDOrLimitParam property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLimitTypeOrCurrencyIDOrLimitParam().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link Long }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link Subscriber.ConsumptionLimit.LimitParam }{@code >}
         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
         * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
         * 
         * 
         */
        public List<JAXBElement<?>> getLimitTypeOrCurrencyIDOrLimitParam() {
            if (limitTypeOrCurrencyIDOrLimitParam == null) {
                limitTypeOrCurrencyIDOrLimitParam = new ArrayList<JAXBElement<?>>();
            }
            return this.limitTypeOrCurrencyIDOrLimitParam;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
         *         &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "paramCode",
            "paramValue"
        })
        public static class LimitParam {

            @XmlElement(name = "ParamCode", required = true)
            protected String paramCode;
            @XmlElement(name = "ParamValue")
            protected long paramValue;

            /**
             * Obtiene el valor de la propiedad paramCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParamCode() {
                return paramCode;
            }

            /**
             * Define el valor de la propiedad paramCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParamCode(String value) {
                this.paramCode = value;
            }

            /**
             * Obtiene el valor de la propiedad paramValue.
             * 
             */
            public long getParamValue() {
                return paramValue;
            }

            /**
             * Define el valor de la propiedad paramValue.
             * 
             */
            public void setParamValue(long value) {
                this.paramValue = value;
            }

        }

    }

}
