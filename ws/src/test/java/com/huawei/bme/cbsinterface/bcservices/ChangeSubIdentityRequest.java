
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;


/**
 * <p>Clase Java para ChangeSubIdentityRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeSubIdentityRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
 *         &lt;element name="AddSubIdentity" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DelSubIdentity" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ModifySubIdentity" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OldSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OldSubIdentityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NewSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeSubIdentityRequest", propOrder = {
    "subAccessCode",
    "addSubIdentity",
    "delSubIdentity",
    "modifySubIdentity"
})
public class ChangeSubIdentityRequest {

    @XmlElement(name = "SubAccessCode", required = true)
    protected SubAccessCode subAccessCode;
    @XmlElement(name = "AddSubIdentity")
    protected List<ChangeSubIdentityRequest.AddSubIdentity> addSubIdentity;
    @XmlElement(name = "DelSubIdentity")
    protected List<ChangeSubIdentityRequest.DelSubIdentity> delSubIdentity;
    @XmlElement(name = "ModifySubIdentity")
    protected List<ChangeSubIdentityRequest.ModifySubIdentity> modifySubIdentity;

    /**
     * Obtiene el valor de la propiedad subAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubAccessCode }
     *     
     */
    public SubAccessCode getSubAccessCode() {
        return subAccessCode;
    }

    /**
     * Define el valor de la propiedad subAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubAccessCode }
     *     
     */
    public void setSubAccessCode(SubAccessCode value) {
        this.subAccessCode = value;
    }

    /**
     * Gets the value of the addSubIdentity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addSubIdentity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddSubIdentity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeSubIdentityRequest.AddSubIdentity }
     * 
     * 
     */
    public List<ChangeSubIdentityRequest.AddSubIdentity> getAddSubIdentity() {
        if (addSubIdentity == null) {
            addSubIdentity = new ArrayList<ChangeSubIdentityRequest.AddSubIdentity>();
        }
        return this.addSubIdentity;
    }

    /**
     * Gets the value of the delSubIdentity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the delSubIdentity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDelSubIdentity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeSubIdentityRequest.DelSubIdentity }
     * 
     * 
     */
    public List<ChangeSubIdentityRequest.DelSubIdentity> getDelSubIdentity() {
        if (delSubIdentity == null) {
            delSubIdentity = new ArrayList<ChangeSubIdentityRequest.DelSubIdentity>();
        }
        return this.delSubIdentity;
    }

    /**
     * Gets the value of the modifySubIdentity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modifySubIdentity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModifySubIdentity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeSubIdentityRequest.ModifySubIdentity }
     * 
     * 
     */
    public List<ChangeSubIdentityRequest.ModifySubIdentity> getModifySubIdentity() {
        if (modifySubIdentity == null) {
            modifySubIdentity = new ArrayList<ChangeSubIdentityRequest.ModifySubIdentity>();
        }
        return this.modifySubIdentity;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subIdentityType",
        "subIdentity"
    })
    public static class AddSubIdentity {

        @XmlElement(name = "SubIdentityType", required = true)
        protected String subIdentityType;
        @XmlElement(name = "SubIdentity", required = true)
        protected String subIdentity;

        /**
         * Obtiene el valor de la propiedad subIdentityType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubIdentityType() {
            return subIdentityType;
        }

        /**
         * Define el valor de la propiedad subIdentityType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubIdentityType(String value) {
            this.subIdentityType = value;
        }

        /**
         * Obtiene el valor de la propiedad subIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubIdentity() {
            return subIdentity;
        }

        /**
         * Define el valor de la propiedad subIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubIdentity(String value) {
            this.subIdentity = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subIdentityType",
        "subIdentity"
    })
    public static class DelSubIdentity {

        @XmlElement(name = "SubIdentityType", required = true)
        protected String subIdentityType;
        @XmlElement(name = "SubIdentity", required = true)
        protected String subIdentity;

        /**
         * Obtiene el valor de la propiedad subIdentityType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubIdentityType() {
            return subIdentityType;
        }

        /**
         * Define el valor de la propiedad subIdentityType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubIdentityType(String value) {
            this.subIdentityType = value;
        }

        /**
         * Obtiene el valor de la propiedad subIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubIdentity() {
            return subIdentity;
        }

        /**
         * Define el valor de la propiedad subIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubIdentity(String value) {
            this.subIdentity = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OldSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OldSubIdentityType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NewSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "oldSubIdentity",
        "oldSubIdentityType",
        "newSubIdentity"
    })
    public static class ModifySubIdentity {

        @XmlElement(name = "OldSubIdentity", required = true)
        protected String oldSubIdentity;
        @XmlElement(name = "OldSubIdentityType", required = true)
        protected String oldSubIdentityType;
        @XmlElement(name = "NewSubIdentity", required = true)
        protected String newSubIdentity;

        /**
         * Obtiene el valor de la propiedad oldSubIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOldSubIdentity() {
            return oldSubIdentity;
        }

        /**
         * Define el valor de la propiedad oldSubIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOldSubIdentity(String value) {
            this.oldSubIdentity = value;
        }

        /**
         * Obtiene el valor de la propiedad oldSubIdentityType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOldSubIdentityType() {
            return oldSubIdentityType;
        }

        /**
         * Define el valor de la propiedad oldSubIdentityType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOldSubIdentityType(String value) {
            this.oldSubIdentityType = value;
        }

        /**
         * Obtiene el valor de la propiedad newSubIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewSubIdentity() {
            return newSubIdentity;
        }

        /**
         * Define el valor de la propiedad newSubIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewSubIdentity(String value) {
            this.newSubIdentity = value;
        }

    }

}
