
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SimpleProperty complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SimpleProperty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleProperty", propOrder = {
    "code",
    "value"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.CustDeactivationRequest.AdditionalProperty.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeCustOfferingRequest.AdditionalProperty.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeAccountOfferingRequest.AdditionalProperty.class,
    com.huawei.bme.cbsinterface.bcservices.SubDeactivationRequest.AdditionalProperty.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeSubStatusRequest.AdditionalProperty.class,
    com.huawei.bme.cbsinterface.bcservices.AcctDeactivationRequest.AdditionalProperty.class
})
public class SimpleProperty {

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Value", required = true)
    protected String value;

    /**
     * Obtiene el valor de la propiedad code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

}
