
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BatchChangePayRelationRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BatchChangePayRelationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentAcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayRelation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AddPayRelation" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ControlRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PayLimit" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Limit">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="LimitValueType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="LimitMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                                 &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ModPayRelation" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="ControlRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatchChangePayRelationRequest", propOrder = {
    "paymentAcctKey",
    "payRelation",
    "fileName"
})
public class BatchChangePayRelationRequest {

    @XmlElement(name = "PaymentAcctKey", required = true)
    protected String paymentAcctKey;
    @XmlElement(name = "PayRelation")
    protected BatchChangePayRelationRequest.PayRelation payRelation;
    @XmlElement(name = "FileName", required = true)
    protected String fileName;

    /**
     * Obtiene el valor de la propiedad paymentAcctKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentAcctKey() {
        return paymentAcctKey;
    }

    /**
     * Define el valor de la propiedad paymentAcctKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentAcctKey(String value) {
        this.paymentAcctKey = value;
    }

    /**
     * Obtiene el valor de la propiedad payRelation.
     * 
     * @return
     *     possible object is
     *     {@link BatchChangePayRelationRequest.PayRelation }
     *     
     */
    public BatchChangePayRelationRequest.PayRelation getPayRelation() {
        return payRelation;
    }

    /**
     * Define el valor de la propiedad payRelation.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchChangePayRelationRequest.PayRelation }
     *     
     */
    public void setPayRelation(BatchChangePayRelationRequest.PayRelation value) {
        this.payRelation = value;
    }

    /**
     * Obtiene el valor de la propiedad fileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Define el valor de la propiedad fileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AddPayRelation" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ControlRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PayLimit" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Limit">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="LimitValueType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="LimitMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                                       &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ModPayRelation" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="ControlRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "addPayRelation",
        "modPayRelation"
    })
    public static class PayRelation {

        @XmlElement(name = "AddPayRelation")
        protected List<BatchChangePayRelationRequest.PayRelation.AddPayRelation> addPayRelation;
        @XmlElement(name = "ModPayRelation")
        protected List<BatchChangePayRelationRequest.PayRelation.ModPayRelation> modPayRelation;

        /**
         * Gets the value of the addPayRelation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addPayRelation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddPayRelation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BatchChangePayRelationRequest.PayRelation.AddPayRelation }
         * 
         * 
         */
        public List<BatchChangePayRelationRequest.PayRelation.AddPayRelation> getAddPayRelation() {
            if (addPayRelation == null) {
                addPayRelation = new ArrayList<BatchChangePayRelationRequest.PayRelation.AddPayRelation>();
            }
            return this.addPayRelation;
        }

        /**
         * Gets the value of the modPayRelation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the modPayRelation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getModPayRelation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BatchChangePayRelationRequest.PayRelation.ModPayRelation }
         * 
         * 
         */
        public List<BatchChangePayRelationRequest.PayRelation.ModPayRelation> getModPayRelation() {
            if (modPayRelation == null) {
                modPayRelation = new ArrayList<BatchChangePayRelationRequest.PayRelation.ModPayRelation>();
            }
            return this.modPayRelation;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ControlRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PayLimit" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Limit">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="LimitValueType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="LimitMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                             &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "priority",
            "onlyPayRelFlag",
            "chargeCode",
            "controlRule",
            "payLimit"
        })
        public static class AddPayRelation {

            @XmlElement(name = "Priority")
            protected BigInteger priority;
            @XmlElement(name = "OnlyPayRelFlag")
            protected String onlyPayRelFlag;
            @XmlElement(name = "ChargeCode")
            protected String chargeCode;
            @XmlElement(name = "ControlRule")
            protected String controlRule;
            @XmlElement(name = "PayLimit")
            protected BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit payLimit;

            /**
             * Obtiene el valor de la propiedad priority.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPriority() {
                return priority;
            }

            /**
             * Define el valor de la propiedad priority.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPriority(BigInteger value) {
                this.priority = value;
            }

            /**
             * Obtiene el valor de la propiedad onlyPayRelFlag.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOnlyPayRelFlag() {
                return onlyPayRelFlag;
            }

            /**
             * Define el valor de la propiedad onlyPayRelFlag.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOnlyPayRelFlag(String value) {
                this.onlyPayRelFlag = value;
            }

            /**
             * Obtiene el valor de la propiedad chargeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChargeCode() {
                return chargeCode;
            }

            /**
             * Define el valor de la propiedad chargeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChargeCode(String value) {
                this.chargeCode = value;
            }

            /**
             * Obtiene el valor de la propiedad controlRule.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getControlRule() {
                return controlRule;
            }

            /**
             * Define el valor de la propiedad controlRule.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setControlRule(String value) {
                this.controlRule = value;
            }

            /**
             * Obtiene el valor de la propiedad payLimit.
             * 
             * @return
             *     possible object is
             *     {@link BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit }
             *     
             */
            public BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit getPayLimit() {
                return payLimit;
            }

            /**
             * Define el valor de la propiedad payLimit.
             * 
             * @param value
             *     allowed object is
             *     {@link BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit }
             *     
             */
            public void setPayLimit(BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit value) {
                this.payLimit = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Limit">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="LimitValueType" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="LimitMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "limitCycleType",
                "limit",
                "currencyID"
            })
            public static class PayLimit {

                @XmlElement(name = "LimitCycleType", required = true)
                protected String limitCycleType;
                @XmlElement(name = "Limit", required = true)
                protected BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit.Limit limit;
                @XmlElement(name = "CurrencyID")
                protected BigInteger currencyID;

                /**
                 * Obtiene el valor de la propiedad limitCycleType.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLimitCycleType() {
                    return limitCycleType;
                }

                /**
                 * Define el valor de la propiedad limitCycleType.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLimitCycleType(String value) {
                    this.limitCycleType = value;
                }

                /**
                 * Obtiene el valor de la propiedad limit.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit.Limit }
                 *     
                 */
                public BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit.Limit getLimit() {
                    return limit;
                }

                /**
                 * Define el valor de la propiedad limit.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit.Limit }
                 *     
                 */
                public void setLimit(BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit.Limit value) {
                    this.limit = value;
                }

                /**
                 * Obtiene el valor de la propiedad currencyID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getCurrencyID() {
                    return currencyID;
                }

                /**
                 * Define el valor de la propiedad currencyID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setCurrencyID(BigInteger value) {
                    this.currencyID = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="LimitValueType" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="LimitMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                 *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "limitType",
                    "limitValueType",
                    "limitMeasureUnit",
                    "limitValue"
                })
                public static class Limit {

                    @XmlElement(name = "LimitType", required = true)
                    protected String limitType;
                    @XmlElement(name = "LimitValueType", required = true)
                    protected String limitValueType;
                    @XmlElement(name = "LimitMeasureUnit")
                    protected BigInteger limitMeasureUnit;
                    @XmlElement(name = "LimitValue")
                    protected long limitValue;

                    /**
                     * Obtiene el valor de la propiedad limitType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLimitType() {
                        return limitType;
                    }

                    /**
                     * Define el valor de la propiedad limitType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLimitType(String value) {
                        this.limitType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad limitValueType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLimitValueType() {
                        return limitValueType;
                    }

                    /**
                     * Define el valor de la propiedad limitValueType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLimitValueType(String value) {
                        this.limitValueType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad limitMeasureUnit.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getLimitMeasureUnit() {
                        return limitMeasureUnit;
                    }

                    /**
                     * Define el valor de la propiedad limitMeasureUnit.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setLimitMeasureUnit(BigInteger value) {
                        this.limitMeasureUnit = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad limitValue.
                     * 
                     */
                    public long getLimitValue() {
                        return limitValue;
                    }

                    /**
                     * Define el valor de la propiedad limitValue.
                     * 
                     */
                    public void setLimitValue(long value) {
                        this.limitValue = value;
                    }

                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ChargeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="ControlRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "priority",
            "onlyPayRelFlag",
            "chargeCode",
            "controlRule",
            "limitValue"
        })
        public static class ModPayRelation {

            @XmlElement(name = "Priority")
            protected BigInteger priority;
            @XmlElement(name = "OnlyPayRelFlag")
            protected String onlyPayRelFlag;
            @XmlElement(name = "ChargeCode")
            protected String chargeCode;
            @XmlElement(name = "ControlRule")
            protected String controlRule;
            @XmlElement(name = "LimitValue")
            protected Long limitValue;

            /**
             * Obtiene el valor de la propiedad priority.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPriority() {
                return priority;
            }

            /**
             * Define el valor de la propiedad priority.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPriority(BigInteger value) {
                this.priority = value;
            }

            /**
             * Obtiene el valor de la propiedad onlyPayRelFlag.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOnlyPayRelFlag() {
                return onlyPayRelFlag;
            }

            /**
             * Define el valor de la propiedad onlyPayRelFlag.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOnlyPayRelFlag(String value) {
                this.onlyPayRelFlag = value;
            }

            /**
             * Obtiene el valor de la propiedad chargeCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getChargeCode() {
                return chargeCode;
            }

            /**
             * Define el valor de la propiedad chargeCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setChargeCode(String value) {
                this.chargeCode = value;
            }

            /**
             * Obtiene el valor de la propiedad controlRule.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getControlRule() {
                return controlRule;
            }

            /**
             * Define el valor de la propiedad controlRule.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setControlRule(String value) {
                this.controlRule = value;
            }

            /**
             * Obtiene el valor de la propiedad limitValue.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getLimitValue() {
                return limitValue;
            }

            /**
             * Define el valor de la propiedad limitValue.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setLimitValue(Long value) {
                this.limitValue = value;
            }

        }

    }

}
