
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader" form="unqualified"/>
 *         &lt;element name="DelGroupMemberRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}DelGroupMemberRequest" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "delGroupMemberRequest"
})
@XmlRootElement(name = "DelGroupMemberRequestMsg")
public class DelGroupMemberRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "DelGroupMemberRequest", namespace = "", required = true)
    protected DelGroupMemberRequest delGroupMemberRequest;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad delGroupMemberRequest.
     * 
     * @return
     *     possible object is
     *     {@link DelGroupMemberRequest }
     *     
     */
    public DelGroupMemberRequest getDelGroupMemberRequest() {
        return delGroupMemberRequest;
    }

    /**
     * Define el valor de la propiedad delGroupMemberRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link DelGroupMemberRequest }
     *     
     */
    public void setDelGroupMemberRequest(DelGroupMemberRequest value) {
        this.delGroupMemberRequest = value;
    }

}
