
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TaxException complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TaxException">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TaxChargeCode" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *         &lt;element name="ExceptionRate" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxException", propOrder = {
    "taxChargeCode",
    "exceptionRate"
})
public class TaxException {

    @XmlElement(name = "TaxChargeCode", required = true)
    protected BigInteger taxChargeCode;
    @XmlElement(name = "ExceptionRate", required = true)
    protected BigInteger exceptionRate;

    /**
     * Obtiene el valor de la propiedad taxChargeCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTaxChargeCode() {
        return taxChargeCode;
    }

    /**
     * Define el valor de la propiedad taxChargeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTaxChargeCode(BigInteger value) {
        this.taxChargeCode = value;
    }

    /**
     * Obtiene el valor de la propiedad exceptionRate.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getExceptionRate() {
        return exceptionRate;
    }

    /**
     * Define el valor de la propiedad exceptionRate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setExceptionRate(BigInteger value) {
        this.exceptionRate = value;
    }

}
