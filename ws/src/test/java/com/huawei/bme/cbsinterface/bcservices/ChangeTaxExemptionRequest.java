
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;


/**
 * <p>Clase Java para ChangeTaxExemptionRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeTaxExemptionRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegisterCust" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode"/>
 *         &lt;element name="AddExemption" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ExptPlanCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ObjType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ObjKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DelExemption" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ExptPlanCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ObjType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ObjKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeTaxExemptionRequest", propOrder = {
    "registerCust",
    "addExemption",
    "delExemption"
})
public class ChangeTaxExemptionRequest {

    @XmlElement(name = "RegisterCust", required = true)
    protected CustAccessCode registerCust;
    @XmlElement(name = "AddExemption")
    protected List<ChangeTaxExemptionRequest.AddExemption> addExemption;
    @XmlElement(name = "DelExemption")
    protected List<ChangeTaxExemptionRequest.DelExemption> delExemption;

    /**
     * Obtiene el valor de la propiedad registerCust.
     * 
     * @return
     *     possible object is
     *     {@link CustAccessCode }
     *     
     */
    public CustAccessCode getRegisterCust() {
        return registerCust;
    }

    /**
     * Define el valor de la propiedad registerCust.
     * 
     * @param value
     *     allowed object is
     *     {@link CustAccessCode }
     *     
     */
    public void setRegisterCust(CustAccessCode value) {
        this.registerCust = value;
    }

    /**
     * Gets the value of the addExemption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addExemption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddExemption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeTaxExemptionRequest.AddExemption }
     * 
     * 
     */
    public List<ChangeTaxExemptionRequest.AddExemption> getAddExemption() {
        if (addExemption == null) {
            addExemption = new ArrayList<ChangeTaxExemptionRequest.AddExemption>();
        }
        return this.addExemption;
    }

    /**
     * Gets the value of the delExemption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the delExemption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDelExemption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChangeTaxExemptionRequest.DelExemption }
     * 
     * 
     */
    public List<ChangeTaxExemptionRequest.DelExemption> getDelExemption() {
        if (delExemption == null) {
            delExemption = new ArrayList<ChangeTaxExemptionRequest.DelExemption>();
        }
        return this.delExemption;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ExptPlanCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ObjType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ObjKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "exptPlanCODE",
        "objType",
        "objKey"
    })
    public static class AddExemption {

        @XmlElement(name = "ExptPlanCODE", required = true)
        protected String exptPlanCODE;
        @XmlElement(name = "ObjType")
        protected String objType;
        @XmlElement(name = "ObjKey")
        protected String objKey;

        /**
         * Obtiene el valor de la propiedad exptPlanCODE.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExptPlanCODE() {
            return exptPlanCODE;
        }

        /**
         * Define el valor de la propiedad exptPlanCODE.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExptPlanCODE(String value) {
            this.exptPlanCODE = value;
        }

        /**
         * Obtiene el valor de la propiedad objType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObjType() {
            return objType;
        }

        /**
         * Define el valor de la propiedad objType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObjType(String value) {
            this.objType = value;
        }

        /**
         * Obtiene el valor de la propiedad objKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObjKey() {
            return objKey;
        }

        /**
         * Define el valor de la propiedad objKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObjKey(String value) {
            this.objKey = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ExptPlanCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ObjType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ObjKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "exptPlanCODE",
        "objType",
        "objKey"
    })
    public static class DelExemption {

        @XmlElement(name = "ExptPlanCODE", required = true)
        protected String exptPlanCODE;
        @XmlElement(name = "ObjType")
        protected String objType;
        @XmlElement(name = "ObjKey")
        protected String objKey;

        /**
         * Obtiene el valor de la propiedad exptPlanCODE.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExptPlanCODE() {
            return exptPlanCODE;
        }

        /**
         * Define el valor de la propiedad exptPlanCODE.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExptPlanCODE(String value) {
            this.exptPlanCODE = value;
        }

        /**
         * Obtiene el valor de la propiedad objType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObjType() {
            return objType;
        }

        /**
         * Define el valor de la propiedad objType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObjType(String value) {
            this.objType = value;
        }

        /**
         * Obtiene el valor de la propiedad objKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObjKey() {
            return objKey;
        }

        /**
         * Define el valor de la propiedad objKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObjKey(String value) {
            this.objKey = value;
        }

    }

}
