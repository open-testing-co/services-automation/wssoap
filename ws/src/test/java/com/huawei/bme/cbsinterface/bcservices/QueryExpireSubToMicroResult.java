
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryExpireSubToMicroResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryExpireSubToMicroResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExpireSubscriber" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="ServiceNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PagingInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryExpireSubToMicroResult", propOrder = {
    "expireSubscriber",
    "pagingInfo"
})
public class QueryExpireSubToMicroResult {

    @XmlElement(name = "ExpireSubscriber")
    protected List<QueryExpireSubToMicroResult.ExpireSubscriber> expireSubscriber;
    @XmlElement(name = "PagingInfo")
    protected QueryExpireSubToMicroResult.PagingInfo pagingInfo;

    /**
     * Gets the value of the expireSubscriber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expireSubscriber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpireSubscriber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryExpireSubToMicroResult.ExpireSubscriber }
     * 
     * 
     */
    public List<QueryExpireSubToMicroResult.ExpireSubscriber> getExpireSubscriber() {
        if (expireSubscriber == null) {
            expireSubscriber = new ArrayList<QueryExpireSubToMicroResult.ExpireSubscriber>();
        }
        return this.expireSubscriber;
    }

    /**
     * Obtiene el valor de la propiedad pagingInfo.
     * 
     * @return
     *     possible object is
     *     {@link QueryExpireSubToMicroResult.PagingInfo }
     *     
     */
    public QueryExpireSubToMicroResult.PagingInfo getPagingInfo() {
        return pagingInfo;
    }

    /**
     * Define el valor de la propiedad pagingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryExpireSubToMicroResult.PagingInfo }
     *     
     */
    public void setPagingInfo(QueryExpireSubToMicroResult.PagingInfo value) {
        this.pagingInfo = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="ServiceNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class ExpireSubscriber {

        @XmlElement(name = "ServiceNum", required = true, nillable = true)
        protected String serviceNum;
        @XmlElement(name = "ExpireTime", required = true)
        protected String expireTime;

        /**
         * Obtiene el valor de la propiedad serviceNum.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServiceNum() {
            return serviceNum;
        }

        /**
         * Define el valor de la propiedad serviceNum.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServiceNum(String value) {
            this.serviceNum = value;
        }

        /**
         * Obtiene el valor de la propiedad expireTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpireTime() {
            return expireTime;
        }

        /**
         * Define el valor de la propiedad expireTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpireTime(String value) {
            this.expireTime = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="TotalRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class PagingInfo {

        @XmlElement(name = "TotalRowNum", required = true)
        protected BigInteger totalRowNum;
        @XmlElement(name = "BeginRowNum", required = true)
        protected BigInteger beginRowNum;
        @XmlElement(name = "FetchRowNum", required = true)
        protected BigInteger fetchRowNum;

        /**
         * Obtiene el valor de la propiedad totalRowNum.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTotalRowNum() {
            return totalRowNum;
        }

        /**
         * Define el valor de la propiedad totalRowNum.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTotalRowNum(BigInteger value) {
            this.totalRowNum = value;
        }

        /**
         * Obtiene el valor de la propiedad beginRowNum.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getBeginRowNum() {
            return beginRowNum;
        }

        /**
         * Define el valor de la propiedad beginRowNum.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setBeginRowNum(BigInteger value) {
            this.beginRowNum = value;
        }

        /**
         * Obtiene el valor de la propiedad fetchRowNum.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getFetchRowNum() {
            return fetchRowNum;
        }

        /**
         * Define el valor de la propiedad fetchRowNum.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setFetchRowNum(BigInteger value) {
            this.fetchRowNum = value;
        }

    }

}
