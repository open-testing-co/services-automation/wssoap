
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader" form="unqualified"/>
 *         &lt;element name="QueryZoneMappingResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryZoneMappingResult" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "queryZoneMappingResult"
})
@XmlRootElement(name = "QueryZoneMappingResultMsg")
public class QueryZoneMappingResultMsg {

    @XmlElement(name = "ResultHeader", namespace = "", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "QueryZoneMappingResult", namespace = "", required = true)
    protected QueryZoneMappingResult queryZoneMappingResult;

    /**
     * Obtiene el valor de la propiedad resultHeader.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Define el valor de la propiedad resultHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad queryZoneMappingResult.
     * 
     * @return
     *     possible object is
     *     {@link QueryZoneMappingResult }
     *     
     */
    public QueryZoneMappingResult getQueryZoneMappingResult() {
        return queryZoneMappingResult;
    }

    /**
     * Define el valor de la propiedad queryZoneMappingResult.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryZoneMappingResult }
     *     
     */
    public void setQueryZoneMappingResult(QueryZoneMappingResult value) {
        this.queryZoneMappingResult = value;
    }

}
