
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;


/**
 * <p>Clase Java para QueryGroupListBySubscriberResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryGroupListBySubscriberResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubGroupKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubGroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubGroupType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MemberTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MemberShortNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MemberProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryGroupListBySubscriberResult", propOrder = {
    "groupList"
})
public class QueryGroupListBySubscriberResult {

    @XmlElement(name = "GroupList")
    protected List<QueryGroupListBySubscriberResult.GroupList> groupList;

    /**
     * Gets the value of the groupList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryGroupListBySubscriberResult.GroupList }
     * 
     * 
     */
    public List<QueryGroupListBySubscriberResult.GroupList> getGroupList() {
        if (groupList == null) {
            groupList = new ArrayList<QueryGroupListBySubscriberResult.GroupList>();
        }
        return this.groupList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubGroupKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubGroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubGroupType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MemberTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MemberShortNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MemberProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subGroupKey",
        "subGroupCode",
        "subGroupType",
        "memberTypeCode",
        "memberShortNo",
        "memberProperty",
        "effectiveTime",
        "expirationTime"
    })
    public static class GroupList {

        @XmlElement(name = "SubGroupKey", required = true)
        protected String subGroupKey;
        @XmlElement(name = "SubGroupCode", required = true)
        protected String subGroupCode;
        @XmlElement(name = "SubGroupType", required = true)
        protected String subGroupType;
        @XmlElement(name = "MemberTypeCode", required = true)
        protected String memberTypeCode;
        @XmlElement(name = "MemberShortNo")
        protected String memberShortNo;
        @XmlElement(name = "MemberProperty")
        protected List<SimpleProperty> memberProperty;
        @XmlElement(name = "EffectiveTime", required = true)
        protected String effectiveTime;
        @XmlElement(name = "ExpirationTime", required = true)
        protected String expirationTime;

        /**
         * Obtiene el valor de la propiedad subGroupKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubGroupKey() {
            return subGroupKey;
        }

        /**
         * Define el valor de la propiedad subGroupKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubGroupKey(String value) {
            this.subGroupKey = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubGroupCode() {
            return subGroupCode;
        }

        /**
         * Define el valor de la propiedad subGroupCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubGroupCode(String value) {
            this.subGroupCode = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubGroupType() {
            return subGroupType;
        }

        /**
         * Define el valor de la propiedad subGroupType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubGroupType(String value) {
            this.subGroupType = value;
        }

        /**
         * Obtiene el valor de la propiedad memberTypeCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMemberTypeCode() {
            return memberTypeCode;
        }

        /**
         * Define el valor de la propiedad memberTypeCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMemberTypeCode(String value) {
            this.memberTypeCode = value;
        }

        /**
         * Obtiene el valor de la propiedad memberShortNo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMemberShortNo() {
            return memberShortNo;
        }

        /**
         * Define el valor de la propiedad memberShortNo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMemberShortNo(String value) {
            this.memberShortNo = value;
        }

        /**
         * Gets the value of the memberProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the memberProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMemberProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getMemberProperty() {
            if (memberProperty == null) {
                memberProperty = new ArrayList<SimpleProperty>();
            }
            return this.memberProperty;
        }

        /**
         * Obtiene el valor de la propiedad effectiveTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Define el valor de la propiedad effectiveTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffectiveTime(String value) {
            this.effectiveTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationTime() {
            return expirationTime;
        }

        /**
         * Define el valor de la propiedad expirationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationTime(String value) {
            this.expirationTime = value;
        }

    }

}
