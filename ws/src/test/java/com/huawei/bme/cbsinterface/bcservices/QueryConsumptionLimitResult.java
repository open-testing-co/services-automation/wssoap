
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryConsumptionLimitResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryConsumptionLimitResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LimitUsageList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="LimitTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="UsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="LimitCtrlType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="MeasureType" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="MeasureID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="BeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EndDay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LimitParam" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryConsumptionLimitResult", propOrder = {
    "limitUsageList"
})
public class QueryConsumptionLimitResult {

    @XmlElement(name = "LimitUsageList")
    protected List<QueryConsumptionLimitResult.LimitUsageList> limitUsageList;

    /**
     * Gets the value of the limitUsageList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the limitUsageList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLimitUsageList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryConsumptionLimitResult.LimitUsageList }
     * 
     * 
     */
    public List<QueryConsumptionLimitResult.LimitUsageList> getLimitUsageList() {
        if (limitUsageList == null) {
            limitUsageList = new ArrayList<QueryConsumptionLimitResult.LimitUsageList>();
        }
        return this.limitUsageList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="LimitTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="UsageAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="LimitCtrlType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="MeasureType" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="MeasureID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="BeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EndDay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LimitParam" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitType",
        "limitTypeName",
        "amount",
        "usageAmount",
        "limitCtrlType",
        "currencyID",
        "measureType",
        "measureID",
        "beginDate",
        "endDay",
        "limitParam"
    })
    public static class LimitUsageList {

        @XmlElement(name = "LimitType", required = true)
        protected String limitType;
        @XmlElement(name = "LimitTypeName", required = true)
        protected String limitTypeName;
        @XmlElement(name = "Amount")
        protected long amount;
        @XmlElement(name = "UsageAmount")
        protected long usageAmount;
        @XmlElement(name = "LimitCtrlType", required = true)
        protected String limitCtrlType;
        @XmlElement(name = "CurrencyID")
        protected BigInteger currencyID;
        @XmlElement(name = "MeasureType")
        protected BigInteger measureType;
        @XmlElement(name = "MeasureID")
        protected BigInteger measureID;
        @XmlElement(name = "BeginDate")
        protected String beginDate;
        @XmlElement(name = "EndDay")
        protected String endDay;
        @XmlElement(name = "LimitParam")
        protected List<QueryConsumptionLimitResult.LimitUsageList.LimitParam> limitParam;

        /**
         * Obtiene el valor de la propiedad limitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitType() {
            return limitType;
        }

        /**
         * Define el valor de la propiedad limitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitType(String value) {
            this.limitType = value;
        }

        /**
         * Obtiene el valor de la propiedad limitTypeName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitTypeName() {
            return limitTypeName;
        }

        /**
         * Define el valor de la propiedad limitTypeName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitTypeName(String value) {
            this.limitTypeName = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         */
        public long getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         */
        public void setAmount(long value) {
            this.amount = value;
        }

        /**
         * Obtiene el valor de la propiedad usageAmount.
         * 
         */
        public long getUsageAmount() {
            return usageAmount;
        }

        /**
         * Define el valor de la propiedad usageAmount.
         * 
         */
        public void setUsageAmount(long value) {
            this.usageAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad limitCtrlType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitCtrlType() {
            return limitCtrlType;
        }

        /**
         * Define el valor de la propiedad limitCtrlType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitCtrlType(String value) {
            this.limitCtrlType = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Obtiene el valor de la propiedad measureType.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMeasureType() {
            return measureType;
        }

        /**
         * Define el valor de la propiedad measureType.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMeasureType(BigInteger value) {
            this.measureType = value;
        }

        /**
         * Obtiene el valor de la propiedad measureID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMeasureID() {
            return measureID;
        }

        /**
         * Define el valor de la propiedad measureID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMeasureID(BigInteger value) {
            this.measureID = value;
        }

        /**
         * Obtiene el valor de la propiedad beginDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBeginDate() {
            return beginDate;
        }

        /**
         * Define el valor de la propiedad beginDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBeginDate(String value) {
            this.beginDate = value;
        }

        /**
         * Obtiene el valor de la propiedad endDay.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndDay() {
            return endDay;
        }

        /**
         * Define el valor de la propiedad endDay.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndDay(String value) {
            this.endDay = value;
        }

        /**
         * Gets the value of the limitParam property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the limitParam property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLimitParam().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link QueryConsumptionLimitResult.LimitUsageList.LimitParam }
         * 
         * 
         */
        public List<QueryConsumptionLimitResult.LimitUsageList.LimitParam> getLimitParam() {
            if (limitParam == null) {
                limitParam = new ArrayList<QueryConsumptionLimitResult.LimitUsageList.LimitParam>();
            }
            return this.limitParam;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ParamCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ParamValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "paramCode",
            "paramValue"
        })
        public static class LimitParam {

            @XmlElement(name = "ParamCode", required = true)
            protected String paramCode;
            @XmlElement(name = "ParamValue")
            protected long paramValue;

            /**
             * Obtiene el valor de la propiedad paramCode.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParamCode() {
                return paramCode;
            }

            /**
             * Define el valor de la propiedad paramCode.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParamCode(String value) {
                this.paramCode = value;
            }

            /**
             * Obtiene el valor de la propiedad paramValue.
             * 
             */
            public long getParamValue() {
                return paramValue;
            }

            /**
             * Define el valor de la propiedad paramValue.
             * 
             */
            public void setParamValue(long value) {
                this.paramValue = value;
            }

        }

    }

}
