
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.AccountInfo;
import com.huawei.bme.cbsinterface.bccommon.ActiveMode;
import com.huawei.bme.cbsinterface.bccommon.Address;
import com.huawei.bme.cbsinterface.bccommon.CustInfo;
import com.huawei.bme.cbsinterface.bccommon.EffectMode;
import com.huawei.bme.cbsinterface.bccommon.IndividualInfo;
import com.huawei.bme.cbsinterface.bccommon.OfferingInst;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;
import com.huawei.bme.cbsinterface.bccommon.OrgInfo;
import com.huawei.bme.cbsinterface.bccommon.PayRelExtRule;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;


/**
 * <p>Clase Java para ChangeSubOwnershipRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeSubOwnershipRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OldOwnership">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NewOwnership">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RegisterCustomer">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
 *                             &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                             &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                           &lt;/sequence>
 *                           &lt;attribute name="OpType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="UserCustomer" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
 *                             &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                             &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="Account" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AcctInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountInfo" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="Subscriber">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="SubDFTAcct">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="AcctList" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="DEFAcctFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="PayRelation" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                                 &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
 *                                                 &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                 &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="PaymentLimit" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="PaymentLimitInfo">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
 *                                                         &lt;sequence>
 *                                                         &lt;/sequence>
 *                                                       &lt;/extension>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="ShiftPayRelation" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PrimaryOffering" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="SupplementaryOffering" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ShiftOffering" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OldOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                                       &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
 *                                     &lt;sequence>
 *                                       &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
 *                                       &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/extension>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ControlProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeSubOwnershipRequest", propOrder = {
    "oldOwnership",
    "newOwnership"
})
public class ChangeSubOwnershipRequest {

    @XmlElement(name = "OldOwnership", required = true)
    protected ChangeSubOwnershipRequest.OldOwnership oldOwnership;
    @XmlElement(name = "NewOwnership", required = true)
    protected ChangeSubOwnershipRequest.NewOwnership newOwnership;

    /**
     * Obtiene el valor de la propiedad oldOwnership.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubOwnershipRequest.OldOwnership }
     *     
     */
    public ChangeSubOwnershipRequest.OldOwnership getOldOwnership() {
        return oldOwnership;
    }

    /**
     * Define el valor de la propiedad oldOwnership.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubOwnershipRequest.OldOwnership }
     *     
     */
    public void setOldOwnership(ChangeSubOwnershipRequest.OldOwnership value) {
        this.oldOwnership = value;
    }

    /**
     * Obtiene el valor de la propiedad newOwnership.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubOwnershipRequest.NewOwnership }
     *     
     */
    public ChangeSubOwnershipRequest.NewOwnership getNewOwnership() {
        return newOwnership;
    }

    /**
     * Define el valor de la propiedad newOwnership.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubOwnershipRequest.NewOwnership }
     *     
     */
    public void setNewOwnership(ChangeSubOwnershipRequest.NewOwnership value) {
        this.newOwnership = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RegisterCustomer">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
     *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *                 &lt;/sequence>
     *                 &lt;attribute name="OpType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="UserCustomer" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
     *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="Account" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AcctInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountInfo" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="Subscriber">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="SubDFTAcct">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="AcctList" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="DEFAcctFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="PayRelation" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *                                       &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
     *                                       &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                       &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="PaymentLimit" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="PaymentLimitInfo">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
     *                                               &lt;sequence>
     *                                               &lt;/sequence>
     *                                             &lt;/extension>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="ShiftPayRelation" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PrimaryOffering" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="SupplementaryOffering" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ShiftOffering" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OldOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *                             &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
     *                           &lt;sequence>
     *                             &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
     *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/extension>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ControlProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "registerCustomer",
        "userCustomer",
        "account",
        "addressInfo",
        "subscriber",
        "primaryOffering",
        "supplementaryOffering",
        "controlProperty"
    })
    public static class NewOwnership {

        @XmlElement(name = "RegisterCustomer", required = true)
        protected ChangeSubOwnershipRequest.NewOwnership.RegisterCustomer registerCustomer;
        @XmlElement(name = "UserCustomer")
        protected ChangeSubOwnershipRequest.NewOwnership.UserCustomer userCustomer;
        @XmlElement(name = "Account")
        protected List<ChangeSubOwnershipRequest.NewOwnership.Account> account;
        @XmlElement(name = "AddressInfo")
        protected List<Address> addressInfo;
        @XmlElement(name = "Subscriber", required = true)
        protected ChangeSubOwnershipRequest.NewOwnership.Subscriber subscriber;
        @XmlElement(name = "PrimaryOffering")
        protected ChangeSubOwnershipRequest.NewOwnership.PrimaryOffering primaryOffering;
        @XmlElement(name = "SupplementaryOffering")
        protected ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering supplementaryOffering;
        @XmlElement(name = "ControlProperty")
        protected List<SimpleProperty> controlProperty;

        /**
         * Obtiene el valor de la propiedad registerCustomer.
         * 
         * @return
         *     possible object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.RegisterCustomer }
         *     
         */
        public ChangeSubOwnershipRequest.NewOwnership.RegisterCustomer getRegisterCustomer() {
            return registerCustomer;
        }

        /**
         * Define el valor de la propiedad registerCustomer.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.RegisterCustomer }
         *     
         */
        public void setRegisterCustomer(ChangeSubOwnershipRequest.NewOwnership.RegisterCustomer value) {
            this.registerCustomer = value;
        }

        /**
         * Obtiene el valor de la propiedad userCustomer.
         * 
         * @return
         *     possible object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.UserCustomer }
         *     
         */
        public ChangeSubOwnershipRequest.NewOwnership.UserCustomer getUserCustomer() {
            return userCustomer;
        }

        /**
         * Define el valor de la propiedad userCustomer.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.UserCustomer }
         *     
         */
        public void setUserCustomer(ChangeSubOwnershipRequest.NewOwnership.UserCustomer value) {
            this.userCustomer = value;
        }

        /**
         * Gets the value of the account property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the account property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAccount().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeSubOwnershipRequest.NewOwnership.Account }
         * 
         * 
         */
        public List<ChangeSubOwnershipRequest.NewOwnership.Account> getAccount() {
            if (account == null) {
                account = new ArrayList<ChangeSubOwnershipRequest.NewOwnership.Account>();
            }
            return this.account;
        }

        /**
         * Gets the value of the addressInfo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addressInfo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddressInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Address }
         * 
         * 
         */
        public List<Address> getAddressInfo() {
            if (addressInfo == null) {
                addressInfo = new ArrayList<Address>();
            }
            return this.addressInfo;
        }

        /**
         * Obtiene el valor de la propiedad subscriber.
         * 
         * @return
         *     possible object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber }
         *     
         */
        public ChangeSubOwnershipRequest.NewOwnership.Subscriber getSubscriber() {
            return subscriber;
        }

        /**
         * Define el valor de la propiedad subscriber.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber }
         *     
         */
        public void setSubscriber(ChangeSubOwnershipRequest.NewOwnership.Subscriber value) {
            this.subscriber = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryOffering.
         * 
         * @return
         *     possible object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.PrimaryOffering }
         *     
         */
        public ChangeSubOwnershipRequest.NewOwnership.PrimaryOffering getPrimaryOffering() {
            return primaryOffering;
        }

        /**
         * Define el valor de la propiedad primaryOffering.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.PrimaryOffering }
         *     
         */
        public void setPrimaryOffering(ChangeSubOwnershipRequest.NewOwnership.PrimaryOffering value) {
            this.primaryOffering = value;
        }

        /**
         * Obtiene el valor de la propiedad supplementaryOffering.
         * 
         * @return
         *     possible object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering }
         *     
         */
        public ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering getSupplementaryOffering() {
            return supplementaryOffering;
        }

        /**
         * Define el valor de la propiedad supplementaryOffering.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering }
         *     
         */
        public void setSupplementaryOffering(ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering value) {
            this.supplementaryOffering = value;
        }

        /**
         * Gets the value of the controlProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the controlProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getControlProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getControlProperty() {
            if (controlProperty == null) {
                controlProperty = new ArrayList<SimpleProperty>();
            }
            return this.controlProperty;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AcctInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountInfo" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "acctKey",
            "acctInfo"
        })
        public static class Account {

            @XmlElement(name = "AcctKey", required = true)
            protected String acctKey;
            @XmlElement(name = "AcctInfo")
            protected AccountInfo acctInfo;

            /**
             * Obtiene el valor de la propiedad acctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctKey() {
                return acctKey;
            }

            /**
             * Define el valor de la propiedad acctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctKey(String value) {
                this.acctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad acctInfo.
             * 
             * @return
             *     possible object is
             *     {@link AccountInfo }
             *     
             */
            public AccountInfo getAcctInfo() {
                return acctInfo;
            }

            /**
             * Define el valor de la propiedad acctInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link AccountInfo }
             *     
             */
            public void setAcctInfo(AccountInfo value) {
                this.acctInfo = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "newOfferingKey"
        })
        public static class PrimaryOffering {

            @XmlElement(name = "NewOfferingKey")
            protected OfferingKey newOfferingKey;

            /**
             * Obtiene el valor de la propiedad newOfferingKey.
             * 
             * @return
             *     possible object is
             *     {@link OfferingKey }
             *     
             */
            public OfferingKey getNewOfferingKey() {
                return newOfferingKey;
            }

            /**
             * Define el valor de la propiedad newOfferingKey.
             * 
             * @param value
             *     allowed object is
             *     {@link OfferingKey }
             *     
             */
            public void setNewOfferingKey(OfferingKey value) {
                this.newOfferingKey = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
         *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
         *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
         *       &lt;/sequence>
         *       &lt;attribute name="OpType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "custKey",
            "custInfo",
            "individualInfo",
            "orgInfo"
        })
        public static class RegisterCustomer {

            @XmlElement(name = "CustKey", required = true)
            protected String custKey;
            @XmlElement(name = "CustInfo")
            protected CustInfo custInfo;
            @XmlElement(name = "IndividualInfo")
            protected IndividualInfo individualInfo;
            @XmlElement(name = "OrgInfo")
            protected OrgInfo orgInfo;
            @XmlAttribute(name = "OpType", required = true)
            protected String opType;

            /**
             * Obtiene el valor de la propiedad custKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustKey() {
                return custKey;
            }

            /**
             * Define el valor de la propiedad custKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustKey(String value) {
                this.custKey = value;
            }

            /**
             * Obtiene el valor de la propiedad custInfo.
             * 
             * @return
             *     possible object is
             *     {@link CustInfo }
             *     
             */
            public CustInfo getCustInfo() {
                return custInfo;
            }

            /**
             * Define el valor de la propiedad custInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link CustInfo }
             *     
             */
            public void setCustInfo(CustInfo value) {
                this.custInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad individualInfo.
             * 
             * @return
             *     possible object is
             *     {@link IndividualInfo }
             *     
             */
            public IndividualInfo getIndividualInfo() {
                return individualInfo;
            }

            /**
             * Define el valor de la propiedad individualInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link IndividualInfo }
             *     
             */
            public void setIndividualInfo(IndividualInfo value) {
                this.individualInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad orgInfo.
             * 
             * @return
             *     possible object is
             *     {@link OrgInfo }
             *     
             */
            public OrgInfo getOrgInfo() {
                return orgInfo;
            }

            /**
             * Define el valor de la propiedad orgInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OrgInfo }
             *     
             */
            public void setOrgInfo(OrgInfo value) {
                this.orgInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad opType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOpType() {
                return opType;
            }

            /**
             * Define el valor de la propiedad opType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOpType(String value) {
                this.opType = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="SubDFTAcct">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="AcctList" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="DEFAcctFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="PayRelation" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
         *                             &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
         *                             &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                             &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="PaymentLimit" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="PaymentLimitInfo">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
         *                                     &lt;sequence>
         *                                     &lt;/sequence>
         *                                   &lt;/extension>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="ShiftPayRelation" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "subscriberKey",
            "userCustomerKey",
            "writtenLang",
            "ivrLang",
            "subPassword",
            "subDFTAcct",
            "shiftPayRelation"
        })
        public static class Subscriber {

            @XmlElement(name = "SubscriberKey")
            protected String subscriberKey;
            @XmlElement(name = "UserCustomerKey")
            protected String userCustomerKey;
            @XmlElement(name = "WrittenLang")
            protected String writtenLang;
            @XmlElement(name = "IVRLang")
            protected String ivrLang;
            @XmlElement(name = "SubPassword")
            protected String subPassword;
            @XmlElement(name = "SubDFTAcct", required = true)
            protected ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct subDFTAcct;
            @XmlElement(name = "ShiftPayRelation")
            protected List<ChangeSubOwnershipRequest.NewOwnership.Subscriber.ShiftPayRelation> shiftPayRelation;

            /**
             * Obtiene el valor de la propiedad subscriberKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubscriberKey() {
                return subscriberKey;
            }

            /**
             * Define el valor de la propiedad subscriberKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubscriberKey(String value) {
                this.subscriberKey = value;
            }

            /**
             * Obtiene el valor de la propiedad userCustomerKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUserCustomerKey() {
                return userCustomerKey;
            }

            /**
             * Define el valor de la propiedad userCustomerKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUserCustomerKey(String value) {
                this.userCustomerKey = value;
            }

            /**
             * Obtiene el valor de la propiedad writtenLang.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getWrittenLang() {
                return writtenLang;
            }

            /**
             * Define el valor de la propiedad writtenLang.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setWrittenLang(String value) {
                this.writtenLang = value;
            }

            /**
             * Obtiene el valor de la propiedad ivrLang.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIVRLang() {
                return ivrLang;
            }

            /**
             * Define el valor de la propiedad ivrLang.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIVRLang(String value) {
                this.ivrLang = value;
            }

            /**
             * Obtiene el valor de la propiedad subPassword.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSubPassword() {
                return subPassword;
            }

            /**
             * Define el valor de la propiedad subPassword.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSubPassword(String value) {
                this.subPassword = value;
            }

            /**
             * Obtiene el valor de la propiedad subDFTAcct.
             * 
             * @return
             *     possible object is
             *     {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct }
             *     
             */
            public ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct getSubDFTAcct() {
                return subDFTAcct;
            }

            /**
             * Define el valor de la propiedad subDFTAcct.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct }
             *     
             */
            public void setSubDFTAcct(ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct value) {
                this.subDFTAcct = value;
            }

            /**
             * Gets the value of the shiftPayRelation property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the shiftPayRelation property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getShiftPayRelation().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.ShiftPayRelation }
             * 
             * 
             */
            public List<ChangeSubOwnershipRequest.NewOwnership.Subscriber.ShiftPayRelation> getShiftPayRelation() {
                if (shiftPayRelation == null) {
                    shiftPayRelation = new ArrayList<ChangeSubOwnershipRequest.NewOwnership.Subscriber.ShiftPayRelation>();
                }
                return this.shiftPayRelation;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OldPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="NewPayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "oldPayRelationKey",
                "newPayRelationKey"
            })
            public static class ShiftPayRelation {

                @XmlElement(name = "OldPayRelationKey", required = true)
                protected String oldPayRelationKey;
                @XmlElement(name = "NewPayRelationKey", required = true)
                protected String newPayRelationKey;

                /**
                 * Obtiene el valor de la propiedad oldPayRelationKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOldPayRelationKey() {
                    return oldPayRelationKey;
                }

                /**
                 * Define el valor de la propiedad oldPayRelationKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOldPayRelationKey(String value) {
                    this.oldPayRelationKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad newPayRelationKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNewPayRelationKey() {
                    return newPayRelationKey;
                }

                /**
                 * Define el valor de la propiedad newPayRelationKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNewPayRelationKey(String value) {
                    this.newPayRelationKey = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="AcctList" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="DEFAcctFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="PayRelation" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
             *                   &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
             *                   &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="PaymentLimit" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="PaymentLimitInfo">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
             *                           &lt;sequence>
             *                           &lt;/sequence>
             *                         &lt;/extension>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "payRelationKey",
                "acctKey",
                "acctList",
                "payRelation",
                "paymentLimit"
            })
            public static class SubDFTAcct {

                @XmlElement(name = "PayRelationKey")
                protected String payRelationKey;
                @XmlElement(name = "AcctKey")
                protected String acctKey;
                @XmlElement(name = "AcctList")
                protected List<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.AcctList> acctList;
                @XmlElement(name = "PayRelation")
                protected List<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PayRelation> payRelation;
                @XmlElement(name = "PaymentLimit")
                protected List<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit> paymentLimit;

                /**
                 * Obtiene el valor de la propiedad payRelationKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPayRelationKey() {
                    return payRelationKey;
                }

                /**
                 * Define el valor de la propiedad payRelationKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPayRelationKey(String value) {
                    this.payRelationKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad acctKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAcctKey() {
                    return acctKey;
                }

                /**
                 * Define el valor de la propiedad acctKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAcctKey(String value) {
                    this.acctKey = value;
                }

                /**
                 * Gets the value of the acctList property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the acctList property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getAcctList().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.AcctList }
                 * 
                 * 
                 */
                public List<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.AcctList> getAcctList() {
                    if (acctList == null) {
                        acctList = new ArrayList<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.AcctList>();
                    }
                    return this.acctList;
                }

                /**
                 * Gets the value of the payRelation property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the payRelation property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPayRelation().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PayRelation }
                 * 
                 * 
                 */
                public List<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PayRelation> getPayRelation() {
                    if (payRelation == null) {
                        payRelation = new ArrayList<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PayRelation>();
                    }
                    return this.payRelation;
                }

                /**
                 * Gets the value of the paymentLimit property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the paymentLimit property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getPaymentLimit().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit }
                 * 
                 * 
                 */
                public List<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit> getPaymentLimit() {
                    if (paymentLimit == null) {
                        paymentLimit = new ArrayList<ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit>();
                    }
                    return this.paymentLimit;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="DEFAcctFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "acctKey",
                    "defAcctFlag"
                })
                public static class AcctList {

                    @XmlElement(name = "AcctKey", required = true)
                    protected String acctKey;
                    @XmlElement(name = "DEFAcctFlag", required = true)
                    protected String defAcctFlag;

                    /**
                     * Obtiene el valor de la propiedad acctKey.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAcctKey() {
                        return acctKey;
                    }

                    /**
                     * Define el valor de la propiedad acctKey.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAcctKey(String value) {
                        this.acctKey = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad defAcctFlag.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getDEFAcctFlag() {
                        return defAcctFlag;
                    }

                    /**
                     * Define el valor de la propiedad defAcctFlag.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setDEFAcctFlag(String value) {
                        this.defAcctFlag = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer"/>
                 *         &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
                 *         &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "payRelationKey",
                    "acctKey",
                    "priority",
                    "payRelExtRule",
                    "onlyPayRelFlag",
                    "paymentLimitKey"
                })
                public static class PayRelation {

                    @XmlElement(name = "PayRelationKey", required = true)
                    protected String payRelationKey;
                    @XmlElement(name = "AcctKey", required = true)
                    protected String acctKey;
                    @XmlElement(name = "Priority", required = true)
                    protected BigInteger priority;
                    @XmlElement(name = "PayRelExtRule")
                    protected PayRelExtRule payRelExtRule;
                    @XmlElement(name = "OnlyPayRelFlag")
                    protected String onlyPayRelFlag;
                    @XmlElement(name = "PaymentLimitKey")
                    protected String paymentLimitKey;

                    /**
                     * Obtiene el valor de la propiedad payRelationKey.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPayRelationKey() {
                        return payRelationKey;
                    }

                    /**
                     * Define el valor de la propiedad payRelationKey.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPayRelationKey(String value) {
                        this.payRelationKey = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad acctKey.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getAcctKey() {
                        return acctKey;
                    }

                    /**
                     * Define el valor de la propiedad acctKey.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setAcctKey(String value) {
                        this.acctKey = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad priority.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getPriority() {
                        return priority;
                    }

                    /**
                     * Define el valor de la propiedad priority.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setPriority(BigInteger value) {
                        this.priority = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad payRelExtRule.
                     * 
                     * @return
                     *     possible object is
                     *     {@link PayRelExtRule }
                     *     
                     */
                    public PayRelExtRule getPayRelExtRule() {
                        return payRelExtRule;
                    }

                    /**
                     * Define el valor de la propiedad payRelExtRule.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link PayRelExtRule }
                     *     
                     */
                    public void setPayRelExtRule(PayRelExtRule value) {
                        this.payRelExtRule = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad onlyPayRelFlag.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getOnlyPayRelFlag() {
                        return onlyPayRelFlag;
                    }

                    /**
                     * Define el valor de la propiedad onlyPayRelFlag.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setOnlyPayRelFlag(String value) {
                        this.onlyPayRelFlag = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad paymentLimitKey.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPaymentLimitKey() {
                        return paymentLimitKey;
                    }

                    /**
                     * Define el valor de la propiedad paymentLimitKey.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPaymentLimitKey(String value) {
                        this.paymentLimitKey = value;
                    }

                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="PaymentLimitInfo">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
                 *                 &lt;sequence>
                 *                 &lt;/sequence>
                 *               &lt;/extension>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "paymentLimitKey",
                    "paymentLimitInfo"
                })
                public static class PaymentLimit {

                    @XmlElement(name = "PaymentLimitKey", required = true)
                    protected String paymentLimitKey;
                    @XmlElement(name = "PaymentLimitInfo", required = true)
                    protected ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo paymentLimitInfo;

                    /**
                     * Obtiene el valor de la propiedad paymentLimitKey.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPaymentLimitKey() {
                        return paymentLimitKey;
                    }

                    /**
                     * Define el valor de la propiedad paymentLimitKey.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPaymentLimitKey(String value) {
                        this.paymentLimitKey = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad paymentLimitInfo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo }
                     *     
                     */
                    public ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo getPaymentLimitInfo() {
                        return paymentLimitInfo;
                    }

                    /**
                     * Define el valor de la propiedad paymentLimitInfo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo }
                     *     
                     */
                    public void setPaymentLimitInfo(ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo value) {
                        this.paymentLimitInfo = value;
                    }


                    /**
                     * <p>Clase Java para anonymous complex type.
                     * 
                     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
                     *       &lt;sequence>
                     *       &lt;/sequence>
                     *     &lt;/extension>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class PaymentLimitInfo
                        extends com.huawei.bme.cbsinterface.bccommon.PaymentLimit
                    {


                    }

                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ShiftOffering" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OldOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
         *                   &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="DelOffering" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="AddOffering" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
         *                 &lt;sequence>
         *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
         *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/extension>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "shiftOffering",
            "delOffering",
            "addOffering"
        })
        public static class SupplementaryOffering {

            @XmlElement(name = "ShiftOffering")
            protected List<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering> shiftOffering;
            @XmlElement(name = "DelOffering")
            protected List<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering> delOffering;
            @XmlElement(name = "AddOffering")
            protected List<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering> addOffering;

            /**
             * Gets the value of the shiftOffering property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the shiftOffering property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getShiftOffering().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering }
             * 
             * 
             */
            public List<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering> getShiftOffering() {
                if (shiftOffering == null) {
                    shiftOffering = new ArrayList<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering>();
                }
                return this.shiftOffering;
            }

            /**
             * Gets the value of the delOffering property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the delOffering property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDelOffering().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering }
             * 
             * 
             */
            public List<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering> getDelOffering() {
                if (delOffering == null) {
                    delOffering = new ArrayList<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering>();
                }
                return this.delOffering;
            }

            /**
             * Gets the value of the addOffering property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the addOffering property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAddOffering().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering }
             * 
             * 
             */
            public List<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering> getAddOffering() {
                if (addOffering == null) {
                    addOffering = new ArrayList<ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering>();
                }
                return this.addOffering;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
             *       &lt;sequence>
             *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
             *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/extension>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "effectiveTime",
                "expirationTime",
                "activationTime"
            })
            public static class AddOffering
                extends OfferingInst
            {

                @XmlElement(name = "EffectiveTime", required = true)
                protected EffectMode effectiveTime;
                @XmlElement(name = "ExpirationTime", required = true)
                protected String expirationTime;
                @XmlElement(name = "ActivationTime")
                protected ActiveMode activationTime;

                /**
                 * Obtiene el valor de la propiedad effectiveTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link EffectMode }
                 *     
                 */
                public EffectMode getEffectiveTime() {
                    return effectiveTime;
                }

                /**
                 * Define el valor de la propiedad effectiveTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link EffectMode }
                 *     
                 */
                public void setEffectiveTime(EffectMode value) {
                    this.effectiveTime = value;
                }

                /**
                 * Obtiene el valor de la propiedad expirationTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExpirationTime() {
                    return expirationTime;
                }

                /**
                 * Define el valor de la propiedad expirationTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExpirationTime(String value) {
                    this.expirationTime = value;
                }

                /**
                 * Obtiene el valor de la propiedad activationTime.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ActiveMode }
                 *     
                 */
                public ActiveMode getActivationTime() {
                    return activationTime;
                }

                /**
                 * Define el valor de la propiedad activationTime.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ActiveMode }
                 *     
                 */
                public void setActivationTime(ActiveMode value) {
                    this.activationTime = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offeringKey"
            })
            public static class DelOffering {

                @XmlElement(name = "OfferingKey", required = true)
                protected OfferingKey offeringKey;

                /**
                 * Obtiene el valor de la propiedad offeringKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getOfferingKey() {
                    return offeringKey;
                }

                /**
                 * Define el valor de la propiedad offeringKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setOfferingKey(OfferingKey value) {
                    this.offeringKey = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OldOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
             *         &lt;element name="NewOfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "oldOfferingKey",
                "newOfferingKey"
            })
            public static class ShiftOffering {

                @XmlElement(name = "OldOfferingKey", required = true, nillable = true)
                protected OfferingKey oldOfferingKey;
                @XmlElement(name = "NewOfferingKey", required = true, nillable = true)
                protected OfferingKey newOfferingKey;

                /**
                 * Obtiene el valor de la propiedad oldOfferingKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getOldOfferingKey() {
                    return oldOfferingKey;
                }

                /**
                 * Define el valor de la propiedad oldOfferingKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setOldOfferingKey(OfferingKey value) {
                    this.oldOfferingKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad newOfferingKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getNewOfferingKey() {
                    return newOfferingKey;
                }

                /**
                 * Define el valor de la propiedad newOfferingKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setNewOfferingKey(OfferingKey value) {
                    this.newOfferingKey = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
         *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
         *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "custKey",
            "custInfo",
            "individualInfo",
            "orgInfo"
        })
        public static class UserCustomer {

            @XmlElement(name = "CustKey", required = true)
            protected String custKey;
            @XmlElement(name = "CustInfo")
            protected CustInfo custInfo;
            @XmlElement(name = "IndividualInfo")
            protected IndividualInfo individualInfo;
            @XmlElement(name = "OrgInfo")
            protected OrgInfo orgInfo;

            /**
             * Obtiene el valor de la propiedad custKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustKey() {
                return custKey;
            }

            /**
             * Define el valor de la propiedad custKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustKey(String value) {
                this.custKey = value;
            }

            /**
             * Obtiene el valor de la propiedad custInfo.
             * 
             * @return
             *     possible object is
             *     {@link CustInfo }
             *     
             */
            public CustInfo getCustInfo() {
                return custInfo;
            }

            /**
             * Define el valor de la propiedad custInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link CustInfo }
             *     
             */
            public void setCustInfo(CustInfo value) {
                this.custInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad individualInfo.
             * 
             * @return
             *     possible object is
             *     {@link IndividualInfo }
             *     
             */
            public IndividualInfo getIndividualInfo() {
                return individualInfo;
            }

            /**
             * Define el valor de la propiedad individualInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link IndividualInfo }
             *     
             */
            public void setIndividualInfo(IndividualInfo value) {
                this.individualInfo = value;
            }

            /**
             * Obtiene el valor de la propiedad orgInfo.
             * 
             * @return
             *     possible object is
             *     {@link OrgInfo }
             *     
             */
            public OrgInfo getOrgInfo() {
                return orgInfo;
            }

            /**
             * Define el valor de la propiedad orgInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link OrgInfo }
             *     
             */
            public void setOrgInfo(OrgInfo value) {
                this.orgInfo = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubscriberKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subscriberKey"
    })
    public static class OldOwnership {

        @XmlElement(name = "SubscriberKey", required = true)
        protected String subscriberKey;

        /**
         * Obtiene el valor de la propiedad subscriberKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubscriberKey() {
            return subscriberKey;
        }

        /**
         * Define el valor de la propiedad subscriberKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubscriberKey(String value) {
            this.subscriberKey = value;
        }

    }

}
