
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader" form="unqualified"/>
 *         &lt;element name="FeeDeductionRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}FeeDeductionRequest" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "feeDeductionRequest"
})
@XmlRootElement(name = "FeeDeductionRequestMsg")
public class FeeDeductionRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "FeeDeductionRequest", namespace = "", required = true)
    protected FeeDeductionRequest feeDeductionRequest;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad feeDeductionRequest.
     * 
     * @return
     *     possible object is
     *     {@link FeeDeductionRequest }
     *     
     */
    public FeeDeductionRequest getFeeDeductionRequest() {
        return feeDeductionRequest;
    }

    /**
     * Define el valor de la propiedad feeDeductionRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link FeeDeductionRequest }
     *     
     */
    public void setFeeDeductionRequest(FeeDeductionRequest value) {
        this.feeDeductionRequest = value;
    }

}
