
package com.huawei.bme.cbsinterface.bccommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InstProperty complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InstProperty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PropCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="PropType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubPropInst" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubPropCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstProperty", propOrder = {
    "propCode",
    "propType",
    "value",
    "subPropInst"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty.AddProperty.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty.DelProperty.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeOfferingPropertyRequest.OfferingInst.ProductInst.AddProperty.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeOfferingPropertyRequest.OfferingInst.ProductInst.DelProperty.class,
    com.huawei.bme.cbsinterface.bcservices.BatchChangeOfferingPropertyRequest.OfferingInstProperty.AddProperty.class,
    com.huawei.bme.cbsinterface.bcservices.BatchChangeOfferingPropertyRequest.OfferingInstProperty.DelProperty.class,
    com.huawei.bme.cbsinterface.bcservices.BatchChangeOfferingPropertyRequest.ProductInst.AddProperty.class,
    com.huawei.bme.cbsinterface.bcservices.BatchChangeOfferingPropertyRequest.ProductInst.DelProperty.class,
    com.huawei.bme.cbsinterface.bcservices.FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.OInstProperty.class,
    com.huawei.bme.cbsinterface.bcservices.FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst.PInstProperty.class,
    com.huawei.bme.cbsinterface.bcservices.QueryOfferingInstPropertyResult.OfferingInst.OfferingInstProperty.class,
    com.huawei.bme.cbsinterface.bcservices.QueryOfferingInstPropertyResult.OfferingInst.ProductInst.ProductInstProperty.class,
    com.huawei.bme.cbsinterface.bccommon.POfferingInst.OfferingInstProperty.class,
    com.huawei.bme.cbsinterface.bccommon.ProductInst.PInstProperty.class,
    com.huawei.bme.cbsinterface.bccommon.OfferingInst.OInstProperty.class
})
public class InstProperty {

    @XmlElement(name = "PropCode", required = true)
    protected String propCode;
    @XmlElement(name = "PropType", required = true)
    protected String propType;
    @XmlElement(name = "Value")
    protected String value;
    @XmlElement(name = "SubPropInst")
    protected List<InstProperty.SubPropInst> subPropInst;

    /**
     * Obtiene el valor de la propiedad propCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropCode() {
        return propCode;
    }

    /**
     * Define el valor de la propiedad propCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropCode(String value) {
        this.propCode = value;
    }

    /**
     * Obtiene el valor de la propiedad propType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropType() {
        return propType;
    }

    /**
     * Define el valor de la propiedad propType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropType(String value) {
        this.propType = value;
    }

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the subPropInst property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subPropInst property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubPropInst().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstProperty.SubPropInst }
     * 
     * 
     */
    public List<InstProperty.SubPropInst> getSubPropInst() {
        if (subPropInst == null) {
            subPropInst = new ArrayList<InstProperty.SubPropInst>();
        }
        return this.subPropInst;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubPropCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subPropCode",
        "value"
    })
    public static class SubPropInst {

        @XmlElement(name = "SubPropCode", required = true)
        protected String subPropCode;
        @XmlElement(name = "Value", required = true)
        protected String value;

        /**
         * Obtiene el valor de la propiedad subPropCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubPropCode() {
            return subPropCode;
        }

        /**
         * Define el valor de la propiedad subPropCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubPropCode(String value) {
            this.subPropCode = value;
        }

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }

}
