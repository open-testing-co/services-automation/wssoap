
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.bme.cbsinterface.bcservices package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ChangeConsumptionLimitRequestAddLimitExpirationTime_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "ExpirationTime");
    private final static QName _ChangeConsumptionLimitRequestAddLimitLimitValue_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "LimitValue");
    private final static QName _ChangeConsumptionLimitRequestAddLimitCurrencyID_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "CurrencyID");
    private final static QName _ChangeConsumptionLimitRequestAddLimitLimitType_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "LimitType");
    private final static QName _ChangeConsumptionLimitRequestAddLimitMesureID_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "MesureID");
    private final static QName _ChangeConsumptionLimitRequestAddLimitEffectiveTime_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "EffectiveTime");
    private final static QName _ChangeConsumptionLimitRequestAddLimitLimitParam_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "LimitParam");
    private final static QName _ChangeConsumptionLimitRequestAddLimitUnitType_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "UnitType");
    private final static QName _ChangeConsumptionLimitRequestAddLimitMesureType_QNAME = new QName("http://www.huawei.com/bme/cbsinterface/bcservices", "MesureType");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.bme.cbsinterface.bcservices
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActivateProductRequest }
     * 
     */
    public ActivateProductRequest createActivateProductRequest() {
        return new ActivateProductRequest();
    }

    /**
     * Create an instance of {@link ActivateProductRequest.OfferingOwner }
     * 
     */
    public ActivateProductRequest.OfferingOwner createActivateProductRequestOfferingOwner() {
        return new ActivateProductRequest.OfferingOwner();
    }

    /**
     * Create an instance of {@link ChangeTaxExemptionRequest }
     * 
     */
    public ChangeTaxExemptionRequest createChangeTaxExemptionRequest() {
        return new ChangeTaxExemptionRequest();
    }

    /**
     * Create an instance of {@link QueryOfferingRentFailedFeeResult }
     * 
     */
    public QueryOfferingRentFailedFeeResult createQueryOfferingRentFailedFeeResult() {
        return new QueryOfferingRentFailedFeeResult();
    }

    /**
     * Create an instance of {@link QueryOfferingRentFailedFeeResult.OfferingRentFailedFee }
     * 
     */
    public QueryOfferingRentFailedFeeResult.OfferingRentFailedFee createQueryOfferingRentFailedFeeResultOfferingRentFailedFee() {
        return new QueryOfferingRentFailedFeeResult.OfferingRentFailedFee();
    }

    /**
     * Create an instance of {@link QueryRscRelationRequest }
     * 
     */
    public QueryRscRelationRequest createQueryRscRelationRequest() {
        return new QueryRscRelationRequest();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackResult }
     * 
     */
    public FeeDeductionRollBackResult createFeeDeductionRollBackResult() {
        return new FeeDeductionRollBackResult();
    }

    /**
     * Create an instance of {@link SubDeactivationResult }
     * 
     */
    public SubDeactivationResult createSubDeactivationResult() {
        return new SubDeactivationResult();
    }

    /**
     * Create an instance of {@link SubDeactivationResult.AcctBalance }
     * 
     */
    public SubDeactivationResult.AcctBalance createSubDeactivationResultAcctBalance() {
        return new SubDeactivationResult.AcctBalance();
    }

    /**
     * Create an instance of {@link FeeDeductionResult }
     * 
     */
    public FeeDeductionResult createFeeDeductionResult() {
        return new FeeDeductionResult();
    }

    /**
     * Create an instance of {@link BatchChangeSubOfferingRequest }
     * 
     */
    public BatchChangeSubOfferingRequest createBatchChangeSubOfferingRequest() {
        return new BatchChangeSubOfferingRequest();
    }

    /**
     * Create an instance of {@link BatchChangeSubOfferingRequest.SupplementaryOffering }
     * 
     */
    public BatchChangeSubOfferingRequest.SupplementaryOffering createBatchChangeSubOfferingRequestSupplementaryOffering() {
        return new BatchChangeSubOfferingRequest.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupMemberRequest }
     * 
     */
    public DeleteSuperGroupMemberRequest createDeleteSuperGroupMemberRequest() {
        return new DeleteSuperGroupMemberRequest();
    }

    /**
     * Create an instance of {@link QueryGroupMemberListResult }
     * 
     */
    public QueryGroupMemberListResult createQueryGroupMemberListResult() {
        return new QueryGroupMemberListResult();
    }

    /**
     * Create an instance of {@link BatchAdjustmentRequest }
     * 
     */
    public BatchAdjustmentRequest createBatchAdjustmentRequest() {
        return new BatchAdjustmentRequest();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitResult }
     * 
     */
    public ChangeAcctCreditLimitResult createChangeAcctCreditLimitResult() {
        return new ChangeAcctCreditLimitResult();
    }

    /**
     * Create an instance of {@link ChangeProductStatusRequest }
     * 
     */
    public ChangeProductStatusRequest createChangeProductStatusRequest() {
        return new ChangeProductStatusRequest();
    }

    /**
     * Create an instance of {@link ChangeProductStatusRequest.OfferingInst }
     * 
     */
    public ChangeProductStatusRequest.OfferingInst createChangeProductStatusRequestOfferingInst() {
        return new ChangeProductStatusRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link ChangeProductStatusRequest.OfferingInst.OfferingOwner }
     * 
     */
    public ChangeProductStatusRequest.OfferingInst.OfferingOwner createChangeProductStatusRequestOfferingInstOfferingOwner() {
        return new ChangeProductStatusRequest.OfferingInst.OfferingOwner();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleResult }
     * 
     */
    public QuerySubLifeCycleResult createQuerySubLifeCycleResult() {
        return new QuerySubLifeCycleResult();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest }
     * 
     */
    public SupplementProfileRequest createSupplementProfileRequest() {
        return new SupplementProfileRequest();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest.Account }
     * 
     */
    public SupplementProfileRequest.Account createSupplementProfileRequestAccount() {
        return new SupplementProfileRequest.Account();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest.Account.AutoPayChannel }
     * 
     */
    public SupplementProfileRequest.Account.AutoPayChannel createSupplementProfileRequestAccountAutoPayChannel() {
        return new SupplementProfileRequest.Account.AutoPayChannel();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitRequest }
     * 
     */
    public ChangeAcctCreditLimitRequest createChangeAcctCreditLimitRequest() {
        return new ChangeAcctCreditLimitRequest();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitRequest.AccountCredit }
     * 
     */
    public ChangeAcctCreditLimitRequest.AccountCredit createChangeAcctCreditLimitRequestAccountCredit() {
        return new ChangeAcctCreditLimitRequest.AccountCredit();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingRequest }
     * 
     */
    public ChangeSubOfferingRequest createChangeSubOfferingRequest() {
        return new ChangeSubOfferingRequest();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingRequest.SupplementaryOffering }
     * 
     */
    public ChangeSubOfferingRequest.SupplementaryOffering createChangeSubOfferingRequestSupplementaryOffering() {
        return new ChangeSubOfferingRequest.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResult }
     * 
     */
    public QueryOfferingRentCycleResult createQueryOfferingRentCycleResult() {
        return new QueryOfferingRentCycleResult();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResult.OfferingRentCycle }
     * 
     */
    public QueryOfferingRentCycleResult.OfferingRentCycle createQueryOfferingRentCycleResultOfferingRentCycle() {
        return new QueryOfferingRentCycleResult.OfferingRentCycle();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner }
     * 
     */
    public QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner createQueryOfferingRentCycleResultOfferingRentCycleOfferingOwner() {
        return new QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner();
    }

    /**
     * Create an instance of {@link ChangeGroupMemOffNetNumberRequest }
     * 
     */
    public ChangeGroupMemOffNetNumberRequest createChangeGroupMemOffNetNumberRequest() {
        return new ChangeGroupMemOffNetNumberRequest();
    }

    /**
     * Create an instance of {@link QuerySubInforToMicroResult }
     * 
     */
    public QuerySubInforToMicroResult createQuerySubInforToMicroResult() {
        return new QuerySubInforToMicroResult();
    }

    /**
     * Create an instance of {@link QueryGroupListBySubscriberResult }
     * 
     */
    public QueryGroupListBySubscriberResult createQueryGroupListBySubscriberResult() {
        return new QueryGroupListBySubscriberResult();
    }

    /**
     * Create an instance of {@link FeeQuotationResult }
     * 
     */
    public FeeQuotationResult createFeeQuotationResult() {
        return new FeeQuotationResult();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyResult }
     * 
     */
    public QueryOfferingInstPropertyResult createQueryOfferingInstPropertyResult() {
        return new QueryOfferingInstPropertyResult();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyResult.OfferingInst }
     * 
     */
    public QueryOfferingInstPropertyResult.OfferingInst createQueryOfferingInstPropertyResultOfferingInst() {
        return new QueryOfferingInstPropertyResult.OfferingInst();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyResult.OfferingInst.ProductInst }
     * 
     */
    public QueryOfferingInstPropertyResult.OfferingInst.ProductInst createQueryOfferingInstPropertyResultOfferingInstProductInst() {
        return new QueryOfferingInstPropertyResult.OfferingInst.ProductInst();
    }

    /**
     * Create an instance of {@link ChangeCustHierachyRequest }
     * 
     */
    public ChangeCustHierachyRequest createChangeCustHierachyRequest() {
        return new ChangeCustHierachyRequest();
    }

    /**
     * Create an instance of {@link ApplyInstallmentRequest }
     * 
     */
    public ApplyInstallmentRequest createApplyInstallmentRequest() {
        return new ApplyInstallmentRequest();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest }
     * 
     */
    public BatchSwitchGroupMemberRequest createBatchSwitchGroupMemberRequest() {
        return new BatchSwitchGroupMemberRequest();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest.PaymentRelation }
     * 
     */
    public BatchSwitchGroupMemberRequest.PaymentRelation createBatchSwitchGroupMemberRequestPaymentRelation() {
        return new BatchSwitchGroupMemberRequest.PaymentRelation();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation }
     * 
     */
    public BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation createBatchSwitchGroupMemberRequestPaymentRelationAddPayRelation() {
        return new BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit }
     * 
     */
    public BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit createBatchSwitchGroupMemberRequestPaymentRelationAddPayRelationPaymentLimit() {
        return new BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct }
     * 
     */
    public BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct createBatchSwitchGroupMemberRequestPaymentRelationNewDFTAcct() {
        return new BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit }
     * 
     */
    public BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit createBatchSwitchGroupMemberRequestPaymentRelationNewDFTAcctPaymentLimit() {
        return new BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit();
    }

    /**
     * Create an instance of {@link QueryOfferingRentFailedFeeRequest }
     * 
     */
    public QueryOfferingRentFailedFeeRequest createQueryOfferingRentFailedFeeRequest() {
        return new QueryOfferingRentFailedFeeRequest();
    }

    /**
     * Create an instance of {@link QueryOfferingRentFailedFeeRequest.OfferingInst }
     * 
     */
    public QueryOfferingRentFailedFeeRequest.OfferingInst createQueryOfferingRentFailedFeeRequestOfferingInst() {
        return new QueryOfferingRentFailedFeeRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link ChangeCustDFTAcctRequest }
     * 
     */
    public ChangeCustDFTAcctRequest createChangeCustDFTAcctRequest() {
        return new ChangeCustDFTAcctRequest();
    }

    /**
     * Create an instance of {@link QueryGroupMemOffNetNumberResult }
     * 
     */
    public QueryGroupMemOffNetNumberResult createQueryGroupMemOffNetNumberResult() {
        return new QueryGroupMemOffNetNumberResult();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingResult }
     * 
     */
    public ChangeGroupOfferingResult createChangeGroupOfferingResult() {
        return new ChangeGroupOfferingResult();
    }

    /**
     * Create an instance of {@link BatchChangePayRelationRequest }
     * 
     */
    public BatchChangePayRelationRequest createBatchChangePayRelationRequest() {
        return new BatchChangePayRelationRequest();
    }

    /**
     * Create an instance of {@link BatchChangePayRelationRequest.PayRelation }
     * 
     */
    public BatchChangePayRelationRequest.PayRelation createBatchChangePayRelationRequestPayRelation() {
        return new BatchChangePayRelationRequest.PayRelation();
    }

    /**
     * Create an instance of {@link BatchChangePayRelationRequest.PayRelation.AddPayRelation }
     * 
     */
    public BatchChangePayRelationRequest.PayRelation.AddPayRelation createBatchChangePayRelationRequestPayRelationAddPayRelation() {
        return new BatchChangePayRelationRequest.PayRelation.AddPayRelation();
    }

    /**
     * Create an instance of {@link BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit }
     * 
     */
    public BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit createBatchChangePayRelationRequestPayRelationAddPayRelationPayLimit() {
        return new BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit();
    }

    /**
     * Create an instance of {@link CheckSubscribersGroupResult }
     * 
     */
    public CheckSubscribersGroupResult createCheckSubscribersGroupResult() {
        return new CheckSubscribersGroupResult();
    }

    /**
     * Create an instance of {@link AcctDeactivationRequest }
     * 
     */
    public AcctDeactivationRequest createAcctDeactivationRequest() {
        return new AcctDeactivationRequest();
    }

    /**
     * Create an instance of {@link ApplyPrepaymentRequest }
     * 
     */
    public ApplyPrepaymentRequest createApplyPrepaymentRequest() {
        return new ApplyPrepaymentRequest();
    }

    /**
     * Create an instance of {@link ChangeSubStatusRequest }
     * 
     */
    public ChangeSubStatusRequest createChangeSubStatusRequest() {
        return new ChangeSubStatusRequest();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingResult }
     * 
     */
    public ChangeSubOfferingResult createChangeSubOfferingResult() {
        return new ChangeSubOfferingResult();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendResult }
     * 
     */
    public ApplyInstallmentExtendResult createApplyInstallmentExtendResult() {
        return new ApplyInstallmentExtendResult();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendResult.NewInstallmentInfo }
     * 
     */
    public ApplyInstallmentExtendResult.NewInstallmentInfo createApplyInstallmentExtendResultNewInstallmentInfo() {
        return new ApplyInstallmentExtendResult.NewInstallmentInfo();
    }

    /**
     * Create an instance of {@link FeeDeductionRequest }
     * 
     */
    public FeeDeductionRequest createFeeDeductionRequest() {
        return new FeeDeductionRequest();
    }

    /**
     * Create an instance of {@link FeeDeductionRequest.DeductObj }
     * 
     */
    public FeeDeductionRequest.DeductObj createFeeDeductionRequestDeductObj() {
        return new FeeDeductionRequest.DeductObj();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingRequest }
     * 
     */
    public ChangeGroupOfferingRequest createChangeGroupOfferingRequest() {
        return new ChangeGroupOfferingRequest();
    }

    /**
     * Create an instance of {@link QueryZoneMappingResult }
     * 
     */
    public QueryZoneMappingResult createQueryZoneMappingResult() {
        return new QueryZoneMappingResult();
    }

    /**
     * Create an instance of {@link ChangeSubGrpDFTAcctRequest }
     * 
     */
    public ChangeSubGrpDFTAcctRequest createChangeSubGrpDFTAcctRequest() {
        return new ChangeSubGrpDFTAcctRequest();
    }

    /**
     * Create an instance of {@link QueryInstallmentResult }
     * 
     */
    public QueryInstallmentResult createQueryInstallmentResult() {
        return new QueryInstallmentResult();
    }

    /**
     * Create an instance of {@link QueryExpireSubToMicroResult }
     * 
     */
    public QueryExpireSubToMicroResult createQueryExpireSubToMicroResult() {
        return new QueryExpireSubToMicroResult();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest }
     * 
     */
    public CreateSubscriberRequest createCreateSubscriberRequest() {
        return new CreateSubscriberRequest();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.Subscriber }
     * 
     */
    public CreateSubscriberRequest.Subscriber createCreateSubscriberRequestSubscriber() {
        return new CreateSubscriberRequest.Subscriber();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.Subscriber.SubPaymentMode }
     * 
     */
    public CreateSubscriberRequest.Subscriber.SubPaymentMode createCreateSubscriberRequestSubscriberSubPaymentMode() {
        return new CreateSubscriberRequest.Subscriber.SubPaymentMode();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.Subscriber.SubPaymentMode.PaymentLimit }
     * 
     */
    public CreateSubscriberRequest.Subscriber.SubPaymentMode.PaymentLimit createCreateSubscriberRequestSubscriberSubPaymentModePaymentLimit() {
        return new CreateSubscriberRequest.Subscriber.SubPaymentMode.PaymentLimit();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoRequest }
     * 
     */
    public QueryCustomerInfoRequest createQueryCustomerInfoRequest() {
        return new QueryCustomerInfoRequest();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoRequest.QueryObj }
     * 
     */
    public QueryCustomerInfoRequest.QueryObj createQueryCustomerInfoRequestQueryObj() {
        return new QueryCustomerInfoRequest.QueryObj();
    }

    /**
     * Create an instance of {@link SubDeactivationRequest }
     * 
     */
    public SubDeactivationRequest createSubDeactivationRequest() {
        return new SubDeactivationRequest();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitResult }
     * 
     */
    public QueryConsumptionLimitResult createQueryConsumptionLimitResult() {
        return new QueryConsumptionLimitResult();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitResult.LimitUsageList }
     * 
     */
    public QueryConsumptionLimitResult.LimitUsageList createQueryConsumptionLimitResultLimitUsageList() {
        return new QueryConsumptionLimitResult.LimitUsageList();
    }

    /**
     * Create an instance of {@link QueryZoneInfoResult }
     * 
     */
    public QueryZoneInfoResult createQueryZoneInfoResult() {
        return new QueryZoneInfoResult();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult }
     * 
     */
    public QueryPaymentRelationResult createQueryPaymentRelationResult() {
        return new QueryPaymentRelationResult();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult.PaymentRelationList }
     * 
     */
    public QueryPaymentRelationResult.PaymentRelationList createQueryPaymentRelationResultPaymentRelationList() {
        return new QueryPaymentRelationResult.PaymentRelationList();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult.PaymentRelationList.PaymentLimit }
     * 
     */
    public QueryPaymentRelationResult.PaymentRelationList.PaymentLimit createQueryPaymentRelationResultPaymentRelationListPaymentLimit() {
        return new QueryPaymentRelationResult.PaymentRelationList.PaymentLimit();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest }
     * 
     */
    public ChangeRscRelationRequest createChangeRscRelationRequest() {
        return new ChangeRscRelationRequest();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation }
     * 
     */
    public ChangeRscRelationRequest.RscRelation createChangeRscRelationRequestRscRelation() {
        return new ChangeRscRelationRequest.RscRelation();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation.ModRelation }
     * 
     */
    public ChangeRscRelationRequest.RscRelation.ModRelation createChangeRscRelationRequestRscRelationModRelation() {
        return new ChangeRscRelationRequest.RscRelation.ModRelation();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation }
     * 
     */
    public ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation createChangeRscRelationRequestRscRelationModRelationNewRelation() {
        return new ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation.DelRelation }
     * 
     */
    public ChangeRscRelationRequest.RscRelation.DelRelation createChangeRscRelationRequestRscRelationDelRelation() {
        return new ChangeRscRelationRequest.RscRelation.DelRelation();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingRequest }
     * 
     */
    public ChangeAccountOfferingRequest createChangeAccountOfferingRequest() {
        return new ChangeAccountOfferingRequest();
    }

    /**
     * Create an instance of {@link QueryExpireSubToMicroRequest }
     * 
     */
    public QueryExpireSubToMicroRequest createQueryExpireSubToMicroRequest() {
        return new QueryExpireSubToMicroRequest();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycleRequest }
     * 
     */
    public ChangeAcctBillCycleRequest createChangeAcctBillCycleRequest() {
        return new ChangeAcctBillCycleRequest();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycleRequest.Account }
     * 
     */
    public ChangeAcctBillCycleRequest.Account createChangeAcctBillCycleRequestAccount() {
        return new ChangeAcctBillCycleRequest.Account();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult }
     * 
     */
    public QueryCustomerInfoResult createQueryCustomerInfoResult() {
        return new QueryCustomerInfoResult();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Account }
     * 
     */
    public QueryCustomerInfoResult.Account createQueryCustomerInfoResultAccount() {
        return new QueryCustomerInfoResult.Account();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Account.AcctInfo }
     * 
     */
    public QueryCustomerInfoResult.Account.AcctInfo createQueryCustomerInfoResultAccountAcctInfo() {
        return new QueryCustomerInfoResult.Account.AcctInfo();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel }
     * 
     */
    public QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel createQueryCustomerInfoResultAccountAcctInfoAutoPayChannel() {
        return new QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.SubGroup }
     * 
     */
    public QueryCustomerInfoResult.SubGroup createQueryCustomerInfoResultSubGroup() {
        return new QueryCustomerInfoResult.SubGroup();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.SubGroup.SubGroupInfo }
     * 
     */
    public QueryCustomerInfoResult.SubGroup.SubGroupInfo createQueryCustomerInfoResultSubGroupSubGroupInfo() {
        return new QueryCustomerInfoResult.SubGroup.SubGroupInfo();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Subscriber }
     * 
     */
    public QueryCustomerInfoResult.Subscriber createQueryCustomerInfoResultSubscriber() {
        return new QueryCustomerInfoResult.Subscriber();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Subscriber.SubscriberInfo }
     * 
     */
    public QueryCustomerInfoResult.Subscriber.SubscriberInfo createQueryCustomerInfoResultSubscriberSubscriberInfo() {
        return new QueryCustomerInfoResult.Subscriber.SubscriberInfo();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Customer }
     * 
     */
    public QueryCustomerInfoResult.Customer createQueryCustomerInfoResultCustomer() {
        return new QueryCustomerInfoResult.Customer();
    }

    /**
     * Create an instance of {@link QuerySubInforToMicroRequest }
     * 
     */
    public QuerySubInforToMicroRequest createQuerySubInforToMicroRequest() {
        return new QuerySubInforToMicroRequest();
    }

    /**
     * Create an instance of {@link QueryPaymentLimitUsageResult }
     * 
     */
    public QueryPaymentLimitUsageResult createQueryPaymentLimitUsageResult() {
        return new QueryPaymentLimitUsageResult();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitRequest }
     * 
     */
    public QueryConsumptionLimitRequest createQueryConsumptionLimitRequest() {
        return new QueryConsumptionLimitRequest();
    }

    /**
     * Create an instance of {@link SubActivationRequest }
     * 
     */
    public SubActivationRequest createSubActivationRequest() {
        return new SubActivationRequest();
    }

    /**
     * Create an instance of {@link SubActivationRequest.OfferingInst }
     * 
     */
    public SubActivationRequest.OfferingInst createSubActivationRequestOfferingInst() {
        return new SubActivationRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link QueryAccumulationUsageResult }
     * 
     */
    public QueryAccumulationUsageResult createQueryAccumulationUsageResult() {
        return new QueryAccumulationUsageResult();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest }
     * 
     */
    public FeeQuotationRequest createFeeQuotationRequest() {
        return new FeeQuotationRequest();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement }
     * 
     */
    public FeeQuotationRequest.ChargeElement createFeeQuotationRequestChargeElement() {
        return new FeeQuotationRequest.ChargeElement();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement.OfferingFee }
     * 
     */
    public FeeQuotationRequest.ChargeElement.OfferingFee createFeeQuotationRequestChargeElementOfferingFee() {
        return new FeeQuotationRequest.ChargeElement.OfferingFee();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering }
     * 
     */
    public FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering createFeeQuotationRequestChargeElementOfferingFeeAddOffering() {
        return new FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst }
     * 
     */
    public FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst createFeeQuotationRequestChargeElementOfferingFeeAddOfferingProductInst() {
        return new FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeObj }
     * 
     */
    public FeeQuotationRequest.ChargeObj createFeeQuotationRequestChargeObj() {
        return new FeeQuotationRequest.ChargeObj();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest }
     * 
     */
    public AddGroupMemberRequest createAddGroupMemberRequest() {
        return new AddGroupMemberRequest();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.PaymentRelation }
     * 
     */
    public AddGroupMemberRequest.PaymentRelation createAddGroupMemberRequestPaymentRelation() {
        return new AddGroupMemberRequest.PaymentRelation();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.PaymentRelation.AddPayRelation }
     * 
     */
    public AddGroupMemberRequest.PaymentRelation.AddPayRelation createAddGroupMemberRequestPaymentRelationAddPayRelation() {
        return new AddGroupMemberRequest.PaymentRelation.AddPayRelation();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit }
     * 
     */
    public AddGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit createAddGroupMemberRequestPaymentRelationAddPayRelationPaymentLimit() {
        return new AddGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.PaymentRelation.NewDFTAcct }
     * 
     */
    public AddGroupMemberRequest.PaymentRelation.NewDFTAcct createAddGroupMemberRequestPaymentRelationNewDFTAcct() {
        return new AddGroupMemberRequest.PaymentRelation.NewDFTAcct();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit }
     * 
     */
    public AddGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit createAddGroupMemberRequestPaymentRelationNewDFTAcctPaymentLimit() {
        return new AddGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.GroupMember }
     * 
     */
    public AddGroupMemberRequest.GroupMember createAddGroupMemberRequestGroupMember() {
        return new AddGroupMemberRequest.GroupMember();
    }

    /**
     * Create an instance of {@link ApplyInstallmentResult }
     * 
     */
    public ApplyInstallmentResult createApplyInstallmentResult() {
        return new ApplyInstallmentResult();
    }

    /**
     * Create an instance of {@link QueryGrpCallScreenNoResult }
     * 
     */
    public QueryGrpCallScreenNoResult createQueryGrpCallScreenNoResult() {
        return new QueryGrpCallScreenNoResult();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupRequest }
     * 
     */
    public DeleteSuperGroupRequest createDeleteSuperGroupRequest() {
        return new DeleteSuperGroupRequest();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendRequest }
     * 
     */
    public ApplyInstallmentExtendRequest createApplyInstallmentExtendRequest() {
        return new ApplyInstallmentExtendRequest();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendRequest.NewInstallment }
     * 
     */
    public ApplyInstallmentExtendRequest.NewInstallment createApplyInstallmentExtendRequestNewInstallment() {
        return new ApplyInstallmentExtendRequest.NewInstallment();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberOfferingResult }
     * 
     */
    public ChangeGroupMemberOfferingResult createChangeGroupMemberOfferingResult() {
        return new ChangeGroupMemberOfferingResult();
    }

    /**
     * Create an instance of {@link ChangeOfferingStatusRequest }
     * 
     */
    public ChangeOfferingStatusRequest createChangeOfferingStatusRequest() {
        return new ChangeOfferingStatusRequest();
    }

    /**
     * Create an instance of {@link ChangeOfferingStatusRequest.OfferingInst }
     * 
     */
    public ChangeOfferingStatusRequest.OfferingInst createChangeOfferingStatusRequestOfferingInst() {
        return new ChangeOfferingStatusRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link ChangeOfferingStatusRequest.OfferingInst.OfferingOwner }
     * 
     */
    public ChangeOfferingStatusRequest.OfferingInst.OfferingOwner createChangeOfferingStatusRequestOfferingInstOfferingOwner() {
        return new ChangeOfferingStatusRequest.OfferingInst.OfferingOwner();
    }

    /**
     * Create an instance of {@link ManageGrpCallScreenNoRequest }
     * 
     */
    public ManageGrpCallScreenNoRequest createManageGrpCallScreenNoRequest() {
        return new ManageGrpCallScreenNoRequest();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoRequest }
     * 
     */
    public ChangeAcctInfoRequest createChangeAcctInfoRequest() {
        return new ChangeAcctInfoRequest();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoRequest.AcctPayMethod }
     * 
     */
    public ChangeAcctInfoRequest.AcctPayMethod createChangeAcctInfoRequestAcctPayMethod() {
        return new ChangeAcctInfoRequest.AcctPayMethod();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationRequest }
     * 
     */
    public QueryPaymentRelationRequest createQueryPaymentRelationRequest() {
        return new QueryPaymentRelationRequest();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeResult }
     * 
     */
    public ChangeSubPaymentModeResult createChangeSubPaymentModeResult() {
        return new ChangeSubPaymentModeResult();
    }

    /**
     * Create an instance of {@link ManAppendSubIdentityRequest }
     * 
     */
    public ManAppendSubIdentityRequest createManAppendSubIdentityRequest() {
        return new ManAppendSubIdentityRequest();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyRequest }
     * 
     */
    public BatchChangeOfferingPropertyRequest createBatchChangeOfferingPropertyRequest() {
        return new BatchChangeOfferingPropertyRequest();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyRequest.ProductInst }
     * 
     */
    public BatchChangeOfferingPropertyRequest.ProductInst createBatchChangeOfferingPropertyRequestProductInst() {
        return new BatchChangeOfferingPropertyRequest.ProductInst();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyRequest.OfferingInstProperty }
     * 
     */
    public BatchChangeOfferingPropertyRequest.OfferingInstProperty createBatchChangeOfferingPropertyRequestOfferingInstProperty() {
        return new BatchChangeOfferingPropertyRequest.OfferingInstProperty();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingResult }
     * 
     */
    public ChangeCustOfferingResult createChangeCustOfferingResult() {
        return new ChangeCustOfferingResult();
    }

    /**
     * Create an instance of {@link QueryRebateResult }
     * 
     */
    public QueryRebateResult createQueryRebateResult() {
        return new QueryRebateResult();
    }

    /**
     * Create an instance of {@link QueryGroupOffNetNumberResult }
     * 
     */
    public QueryGroupOffNetNumberResult createQueryGroupOffNetNumberResult() {
        return new QueryGroupOffNetNumberResult();
    }

    /**
     * Create an instance of {@link QueryAppendSubIdentityResult }
     * 
     */
    public QueryAppendSubIdentityResult createQueryAppendSubIdentityResult() {
        return new QueryAppendSubIdentityResult();
    }

    /**
     * Create an instance of {@link ActivateOfferingRequest }
     * 
     */
    public ActivateOfferingRequest createActivateOfferingRequest() {
        return new ActivateOfferingRequest();
    }

    /**
     * Create an instance of {@link ActivateOfferingRequest.OfferingOwner }
     * 
     */
    public ActivateOfferingRequest.OfferingOwner createActivateOfferingRequestOfferingOwner() {
        return new ActivateOfferingRequest.OfferingOwner();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberInfoRequest }
     * 
     */
    public ChangeGroupMemberInfoRequest createChangeGroupMemberInfoRequest() {
        return new ChangeGroupMemberInfoRequest();
    }

    /**
     * Create an instance of {@link ChangeSubIdentityRequest }
     * 
     */
    public ChangeSubIdentityRequest createChangeSubIdentityRequest() {
        return new ChangeSubIdentityRequest();
    }

    /**
     * Create an instance of {@link BatchSubActivationRequest }
     * 
     */
    public BatchSubActivationRequest createBatchSubActivationRequest() {
        return new BatchSubActivationRequest();
    }

    /**
     * Create an instance of {@link QueryAccumulationUsageRequest }
     * 
     */
    public QueryAccumulationUsageRequest createQueryAccumulationUsageRequest() {
        return new QueryAccumulationUsageRequest();
    }

    /**
     * Create an instance of {@link QueryAccumulationUsageRequest.QueryObj }
     * 
     */
    public QueryAccumulationUsageRequest.QueryObj createQueryAccumulationUsageRequestQueryObj() {
        return new QueryAccumulationUsageRequest.QueryObj();
    }

    /**
     * Create an instance of {@link BatchDelGroupMemberRequest }
     * 
     */
    public BatchDelGroupMemberRequest createBatchDelGroupMemberRequest() {
        return new BatchDelGroupMemberRequest();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest }
     * 
     */
    public ChangeConsumptionLimitRequest createChangeConsumptionLimitRequest() {
        return new ChangeConsumptionLimitRequest();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.ModifyLimit }
     * 
     */
    public ChangeConsumptionLimitRequest.ModifyLimit createChangeConsumptionLimitRequestModifyLimit() {
        return new ChangeConsumptionLimitRequest.ModifyLimit();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam }
     * 
     */
    public ChangeConsumptionLimitRequest.ModifyLimit.LimitParam createChangeConsumptionLimitRequestModifyLimitLimitParam() {
        return new ChangeConsumptionLimitRequest.ModifyLimit.LimitParam();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.AddLimit }
     * 
     */
    public ChangeConsumptionLimitRequest.AddLimit createChangeConsumptionLimitRequestAddLimit() {
        return new ChangeConsumptionLimitRequest.AddLimit();
    }

    /**
     * Create an instance of {@link ChangeSubPwdResult }
     * 
     */
    public ChangeSubPwdResult createChangeSubPwdResult() {
        return new ChangeSubPwdResult();
    }

    /**
     * Create an instance of {@link ChangeGroupOffNetNumberRequest }
     * 
     */
    public ChangeGroupOffNetNumberRequest createChangeGroupOffNetNumberRequest() {
        return new ChangeGroupOffNetNumberRequest();
    }

    /**
     * Create an instance of {@link QuerySubInfoExToCubeRequest }
     * 
     */
    public QuerySubInfoExToCubeRequest createQuerySubInfoExToCubeRequest() {
        return new QuerySubInfoExToCubeRequest();
    }

    /**
     * Create an instance of {@link QuerySubInfoExToCubeResult }
     * 
     */
    public QuerySubInfoExToCubeResult createQuerySubInfoExToCubeResult() {
        return new QuerySubInfoExToCubeResult();
    }

    /**
     * Create an instance of {@link QuerySubInfoExToCubeResult.Subscriber }
     * 
     */
    public QuerySubInfoExToCubeResult.Subscriber createQuerySubInfoExToCubeResultSubscriber() {
        return new QuerySubInfoExToCubeResult.Subscriber();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingRequest }
     * 
     */
    public ChangeCustOfferingRequest createChangeCustOfferingRequest() {
        return new ChangeCustOfferingRequest();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest }
     * 
     */
    public ChangePayRelationRequest createChangePayRelationRequest() {
        return new ChangePayRelationRequest();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation createChangePayRelationRequestPaymentRelation() {
        return new ChangePayRelationRequest.PaymentRelation();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.ModPayRelation }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.ModPayRelation createChangePayRelationRequestPaymentRelationModPayRelation() {
        return new ChangePayRelationRequest.PaymentRelation.ModPayRelation();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.ModPayRelation.PaymentLimit }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.ModPayRelation.PaymentLimit createChangePayRelationRequestPaymentRelationModPayRelationPaymentLimit() {
        return new ChangePayRelationRequest.PaymentRelation.ModPayRelation.PaymentLimit();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.ModPayRelation.PayRelation }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.ModPayRelation.PayRelation createChangePayRelationRequestPaymentRelationModPayRelationPayRelation() {
        return new ChangePayRelationRequest.PaymentRelation.ModPayRelation.PayRelation();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.AddPayRelation }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.AddPayRelation createChangePayRelationRequestPaymentRelationAddPayRelation() {
        return new ChangePayRelationRequest.PaymentRelation.AddPayRelation();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.AddPayRelation.PaymentLimit }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.AddPayRelation.PaymentLimit createChangePayRelationRequestPaymentRelationAddPayRelationPaymentLimit() {
        return new ChangePayRelationRequest.PaymentRelation.AddPayRelation.PaymentLimit();
    }

    /**
     * Create an instance of {@link CreateAccountRequest }
     * 
     */
    public CreateAccountRequest createCreateAccountRequest() {
        return new CreateAccountRequest();
    }

    /**
     * Create an instance of {@link JoinSuperGroupRequest }
     * 
     */
    public JoinSuperGroupRequest createJoinSuperGroupRequest() {
        return new JoinSuperGroupRequest();
    }

    /**
     * Create an instance of {@link DelGroupMemberRequest }
     * 
     */
    public DelGroupMemberRequest createDelGroupMemberRequest() {
        return new DelGroupMemberRequest();
    }

    /**
     * Create an instance of {@link DelGroupMemberRequest.PaymentRelation }
     * 
     */
    public DelGroupMemberRequest.PaymentRelation createDelGroupMemberRequestPaymentRelation() {
        return new DelGroupMemberRequest.PaymentRelation();
    }

    /**
     * Create an instance of {@link QueryRscRelationResult }
     * 
     */
    public QueryRscRelationResult createQueryRscRelationResult() {
        return new QueryRscRelationResult();
    }

    /**
     * Create an instance of {@link QueryRscRelationResult.RscRelation }
     * 
     */
    public QueryRscRelationResult.RscRelation createQueryRscRelationResultRscRelation() {
        return new QueryRscRelationResult.RscRelation();
    }

    /**
     * Create an instance of {@link CustDeactivationRequest }
     * 
     */
    public CustDeactivationRequest createCustDeactivationRequest() {
        return new CustDeactivationRequest();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingResult }
     * 
     */
    public ChangeAccountOfferingResult createChangeAccountOfferingResult() {
        return new ChangeAccountOfferingResult();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyRequest }
     * 
     */
    public QueryOfferingInstPropertyRequest createQueryOfferingInstPropertyRequest() {
        return new QueryOfferingInstPropertyRequest();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyRequest.OfferingInst }
     * 
     */
    public QueryOfferingInstPropertyRequest.OfferingInst createQueryOfferingInstPropertyRequestOfferingInst() {
        return new QueryOfferingInstPropertyRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyRequest.OfferingOwner }
     * 
     */
    public QueryOfferingInstPropertyRequest.OfferingOwner createQueryOfferingInstPropertyRequestOfferingOwner() {
        return new QueryOfferingInstPropertyRequest.OfferingOwner();
    }

    /**
     * Create an instance of {@link QueryDataPackageUsageResult }
     * 
     */
    public QueryDataPackageUsageResult createQueryDataPackageUsageResult() {
        return new QueryDataPackageUsageResult();
    }

    /**
     * Create an instance of {@link ChangeCustInfoRequest }
     * 
     */
    public ChangeCustInfoRequest createChangeCustInfoRequest() {
        return new ChangeCustInfoRequest();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequest }
     * 
     */
    public QueryOfferingRentCycleRequest createQueryOfferingRentCycleRequest() {
        return new QueryOfferingRentCycleRequest();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequest.OfferingInst }
     * 
     */
    public QueryOfferingRentCycleRequest.OfferingInst createQueryOfferingRentCycleRequestOfferingInst() {
        return new QueryOfferingRentCycleRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner }
     * 
     */
    public QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner createQueryOfferingRentCycleRequestOfferingInstOfferingOwner() {
        return new QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest }
     * 
     */
    public ChangeOfferingPropertyRequest createChangeOfferingPropertyRequest() {
        return new ChangeOfferingPropertyRequest();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst createChangeOfferingPropertyRequestOfferingInst() {
        return new ChangeOfferingPropertyRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst.ProductInst }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst.ProductInst createChangeOfferingPropertyRequestOfferingInstProductInst() {
        return new ChangeOfferingPropertyRequest.OfferingInst.ProductInst();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty createChangeOfferingPropertyRequestOfferingInstOfferingInstProperty() {
        return new ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst.OfferingOwner }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst.OfferingOwner createChangeOfferingPropertyRequestOfferingInstOfferingOwner() {
        return new ChangeOfferingPropertyRequest.OfferingInst.OfferingOwner();
    }

    /**
     * Create an instance of {@link CancelPreDeactivationRequest }
     * 
     */
    public CancelPreDeactivationRequest createCancelPreDeactivationRequest() {
        return new CancelPreDeactivationRequest();
    }

    /**
     * Create an instance of {@link ModifyStatementRequest }
     * 
     */
    public ModifyStatementRequest createModifyStatementRequest() {
        return new ModifyStatementRequest();
    }

    /**
     * Create an instance of {@link ModifyStatementRequest.StatementScenario }
     * 
     */
    public ModifyStatementRequest.StatementScenario createModifyStatementRequestStatementScenario() {
        return new ModifyStatementRequest.StatementScenario();
    }

    /**
     * Create an instance of {@link CreateCustomerRequest }
     * 
     */
    public CreateCustomerRequest createCreateCustomerRequest() {
        return new CreateCustomerRequest();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest }
     * 
     */
    public ChangeSubDFTAcctRequest createChangeSubDFTAcctRequest() {
        return new ChangeSubDFTAcctRequest();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest.DFTPayRelation }
     * 
     */
    public ChangeSubDFTAcctRequest.DFTPayRelation createChangeSubDFTAcctRequestDFTPayRelation() {
        return new ChangeSubDFTAcctRequest.DFTPayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit }
     * 
     */
    public ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit createChangeSubDFTAcctRequestDFTPayRelationPaymentLimit() {
        return new ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest.SubDFTAccount }
     * 
     */
    public ChangeSubDFTAcctRequest.SubDFTAccount createChangeSubDFTAcctRequestSubDFTAccount() {
        return new ChangeSubDFTAcctRequest.SubDFTAccount();
    }

    /**
     * Create an instance of {@link QueryRebateRequest }
     * 
     */
    public QueryRebateRequest createQueryRebateRequest() {
        return new QueryRebateRequest();
    }

    /**
     * Create an instance of {@link QueryRebateRequest.OfferingOwner }
     * 
     */
    public QueryRebateRequest.OfferingOwner createQueryRebateRequestOfferingOwner() {
        return new QueryRebateRequest.OfferingOwner();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest }
     * 
     */
    public ChangeSubOwnershipRequest createChangeSubOwnershipRequest() {
        return new ChangeSubOwnershipRequest();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership createChangeSubOwnershipRequestNewOwnership() {
        return new ChangeSubOwnershipRequest.NewOwnership();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering createChangeSubOwnershipRequestNewOwnershipSupplementaryOffering() {
        return new ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.Subscriber createChangeSubOwnershipRequestNewOwnershipSubscriber() {
        return new ChangeSubOwnershipRequest.NewOwnership.Subscriber();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct createChangeSubOwnershipRequestNewOwnershipSubscriberSubDFTAcct() {
        return new ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit createChangeSubOwnershipRequestNewOwnershipSubscriberSubDFTAcctPaymentLimit() {
        return new ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit();
    }

    /**
     * Create an instance of {@link CreateGroupRequest }
     * 
     */
    public CreateGroupRequest createCreateGroupRequest() {
        return new CreateGroupRequest();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberOfferingRequest }
     * 
     */
    public ChangeGroupMemberOfferingRequest createChangeGroupMemberOfferingRequest() {
        return new ChangeGroupMemberOfferingRequest();
    }

    /**
     * Create an instance of {@link QueryPaymentLimitUsageRequest }
     * 
     */
    public QueryPaymentLimitUsageRequest createQueryPaymentLimitUsageRequest() {
        return new QueryPaymentLimitUsageRequest();
    }

    /**
     * Create an instance of {@link ApplyPrepaymentResult }
     * 
     */
    public ApplyPrepaymentResult createApplyPrepaymentResult() {
        return new ApplyPrepaymentResult();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest }
     * 
     */
    public ChangeSubPaymentModeRequest createChangeSubPaymentModeRequest() {
        return new ChangeSubPaymentModeRequest();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.SupplementaryOffering }
     * 
     */
    public ChangeSubPaymentModeRequest.SupplementaryOffering createChangeSubPaymentModeRequestSupplementaryOffering() {
        return new ChangeSubPaymentModeRequest.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange createChangeSubPaymentModeRequestPaymentModeChange() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation createChangeSubPaymentModeRequestPaymentModeChangeDFTPayRelation() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.PaymentLimit }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.PaymentLimit createChangeSubPaymentModeRequestPaymentModeChangeDFTPayRelationPaymentLimit() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.PaymentLimit();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount createChangeSubPaymentModeRequestPaymentModeChangeSubDFTAccount() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount();
    }

    /**
     * Create an instance of {@link ChangeRedFlagRequest }
     * 
     */
    public ChangeRedFlagRequest createChangeRedFlagRequest() {
        return new ChangeRedFlagRequest();
    }

    /**
     * Create an instance of {@link ChangeRedFlagRequest.ChangeObj }
     * 
     */
    public ChangeRedFlagRequest.ChangeObj createChangeRedFlagRequestChangeObj() {
        return new ChangeRedFlagRequest.ChangeObj();
    }

    /**
     * Create an instance of {@link BatchChangeAcctOfferingRequest }
     * 
     */
    public BatchChangeAcctOfferingRequest createBatchChangeAcctOfferingRequest() {
        return new BatchChangeAcctOfferingRequest();
    }

    /**
     * Create an instance of {@link BatchChangeAcctOfferingRequest.AcctOffering }
     * 
     */
    public BatchChangeAcctOfferingRequest.AcctOffering createBatchChangeAcctOfferingRequestAcctOffering() {
        return new BatchChangeAcctOfferingRequest.AcctOffering();
    }

    /**
     * Create an instance of {@link QueryInstallmentRequest }
     * 
     */
    public QueryInstallmentRequest createQueryInstallmentRequest() {
        return new QueryInstallmentRequest();
    }

    /**
     * Create an instance of {@link BatchScatteredSubActivationRequest }
     * 
     */
    public BatchScatteredSubActivationRequest createBatchScatteredSubActivationRequest() {
        return new BatchScatteredSubActivationRequest();
    }

    /**
     * Create an instance of {@link ChangeSubInfoRequest }
     * 
     */
    public ChangeSubInfoRequest createChangeSubInfoRequest() {
        return new ChangeSubInfoRequest();
    }

    /**
     * Create an instance of {@link ChangeCustNoticeSuppressRequest }
     * 
     */
    public ChangeCustNoticeSuppressRequest createChangeCustNoticeSuppressRequest() {
        return new ChangeCustNoticeSuppressRequest();
    }

    /**
     * Create an instance of {@link ChangeCustNoticeSuppressRequest.SuppressSetting }
     * 
     */
    public ChangeCustNoticeSuppressRequest.SuppressSetting createChangeCustNoticeSuppressRequestSuppressSetting() {
        return new ChangeCustNoticeSuppressRequest.SuppressSetting();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest }
     * 
     */
    public ChangeAcctOwnershipRequest createChangeAcctOwnershipRequest() {
        return new ChangeAcctOwnershipRequest();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership createChangeAcctOwnershipRequestNewOwnership() {
        return new ChangeAcctOwnershipRequest.NewOwnership();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering createChangeAcctOwnershipRequestNewOwnershipSupplementaryOffering() {
        return new ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.Subscriber }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.Subscriber createChangeAcctOwnershipRequestNewOwnershipSubscriber() {
        return new ChangeAcctOwnershipRequest.NewOwnership.Subscriber();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation createChangeAcctOwnershipRequestNewOwnershipSubscriberSubPayRelation() {
        return new ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.Account }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.Account createChangeAcctOwnershipRequestNewOwnershipAccount() {
        return new ChangeAcctOwnershipRequest.NewOwnership.Account();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.OldOwnership }
     * 
     */
    public ChangeAcctOwnershipRequest.OldOwnership createChangeAcctOwnershipRequestOldOwnership() {
        return new ChangeAcctOwnershipRequest.OldOwnership();
    }

    /**
     * Create an instance of {@link BatchCreateSubscriberRequest }
     * 
     */
    public BatchCreateSubscriberRequest createBatchCreateSubscriberRequest() {
        return new BatchCreateSubscriberRequest();
    }

    /**
     * Create an instance of {@link CreateSuperGroupRequest }
     * 
     */
    public CreateSuperGroupRequest createCreateSuperGroupRequest() {
        return new CreateSuperGroupRequest();
    }

    /**
     * Create an instance of {@link QuerySubscribedOfferingsResult }
     * 
     */
    public QuerySubscribedOfferingsResult createQuerySubscribedOfferingsResult() {
        return new QuerySubscribedOfferingsResult();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackRequest }
     * 
     */
    public FeeDeductionRollBackRequest createFeeDeductionRollBackRequest() {
        return new FeeDeductionRollBackRequest();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackRequest.DeductObj }
     * 
     */
    public FeeDeductionRollBackRequest.DeductObj createFeeDeductionRollBackRequestDeductObj() {
        return new FeeDeductionRollBackRequest.DeductObj();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackRequestMsg }
     * 
     */
    public FeeDeductionRollBackRequestMsg createFeeDeductionRollBackRequestMsg() {
        return new FeeDeductionRollBackRequestMsg();
    }

    /**
     * Create an instance of {@link GroupDeactivationRequestMsg }
     * 
     */
    public GroupDeactivationRequestMsg createGroupDeactivationRequestMsg() {
        return new GroupDeactivationRequestMsg();
    }

    /**
     * Create an instance of {@link GroupDeactivationRequest }
     * 
     */
    public GroupDeactivationRequest createGroupDeactivationRequest() {
        return new GroupDeactivationRequest();
    }

    /**
     * Create an instance of {@link ChangeSubIdentityResultMsg }
     * 
     */
    public ChangeSubIdentityResultMsg createChangeSubIdentityResultMsg() {
        return new ChangeSubIdentityResultMsg();
    }

    /**
     * Create an instance of {@link SupplementProfileResultMsg }
     * 
     */
    public SupplementProfileResultMsg createSupplementProfileResultMsg() {
        return new SupplementProfileResultMsg();
    }

    /**
     * Create an instance of {@link QuerySubscribedOfferingsResultMsg }
     * 
     */
    public QuerySubscribedOfferingsResultMsg createQuerySubscribedOfferingsResultMsg() {
        return new QuerySubscribedOfferingsResultMsg();
    }

    /**
     * Create an instance of {@link QueryCallHuntingResponseMsg }
     * 
     */
    public QueryCallHuntingResponseMsg createQueryCallHuntingResponseMsg() {
        return new QueryCallHuntingResponseMsg();
    }

    /**
     * Create an instance of {@link QueryCallHuntingResponse }
     * 
     */
    public QueryCallHuntingResponse createQueryCallHuntingResponse() {
        return new QueryCallHuntingResponse();
    }

    /**
     * Create an instance of {@link CreateSuperGroupRequestMsg }
     * 
     */
    public CreateSuperGroupRequestMsg createCreateSuperGroupRequestMsg() {
        return new CreateSuperGroupRequestMsg();
    }

    /**
     * Create an instance of {@link BatchCreateSubscriberRequestMsg }
     * 
     */
    public BatchCreateSubscriberRequestMsg createBatchCreateSubscriberRequestMsg() {
        return new BatchCreateSubscriberRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequestMsg }
     * 
     */
    public ChangeAcctOwnershipRequestMsg createChangeAcctOwnershipRequestMsg() {
        return new ChangeAcctOwnershipRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeCustNoticeSuppressRequestMsg }
     * 
     */
    public ChangeCustNoticeSuppressRequestMsg createChangeCustNoticeSuppressRequestMsg() {
        return new ChangeCustNoticeSuppressRequestMsg();
    }

    /**
     * Create an instance of {@link AddGroupMemberResultMsg }
     * 
     */
    public AddGroupMemberResultMsg createAddGroupMemberResultMsg() {
        return new AddGroupMemberResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeInitCreditLimitResultMsg }
     * 
     */
    public BatchChangeInitCreditLimitResultMsg createBatchChangeInitCreditLimitResultMsg() {
        return new BatchChangeInitCreditLimitResultMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupStatusRequestMsg }
     * 
     */
    public ChangeGroupStatusRequestMsg createChangeGroupStatusRequestMsg() {
        return new ChangeGroupStatusRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupStatusRequest }
     * 
     */
    public ChangeGroupStatusRequest createChangeGroupStatusRequest() {
        return new ChangeGroupStatusRequest();
    }

    /**
     * Create an instance of {@link CancelPreDeactivationResultMsg }
     * 
     */
    public CancelPreDeactivationResultMsg createCancelPreDeactivationResultMsg() {
        return new CancelPreDeactivationResultMsg();
    }

    /**
     * Create an instance of {@link AcctDeactivationResultMsg }
     * 
     */
    public AcctDeactivationResultMsg createAcctDeactivationResultMsg() {
        return new AcctDeactivationResultMsg();
    }

    /**
     * Create an instance of {@link ManageCallHuntingRequestMsg }
     * 
     */
    public ManageCallHuntingRequestMsg createManageCallHuntingRequestMsg() {
        return new ManageCallHuntingRequestMsg();
    }

    /**
     * Create an instance of {@link ManageCallHuntingRequest }
     * 
     */
    public ManageCallHuntingRequest createManageCallHuntingRequest() {
        return new ManageCallHuntingRequest();
    }

    /**
     * Create an instance of {@link ChangeSubInfoRequestMsg }
     * 
     */
    public ChangeSubInfoRequestMsg createChangeSubInfoRequestMsg() {
        return new ChangeSubInfoRequestMsg();
    }

    /**
     * Create an instance of {@link BatchScatteredSubActivationRequestMsg }
     * 
     */
    public BatchScatteredSubActivationRequestMsg createBatchScatteredSubActivationRequestMsg() {
        return new BatchScatteredSubActivationRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeProductStatusResultMsg }
     * 
     */
    public ChangeProductStatusResultMsg createChangeProductStatusResultMsg() {
        return new ChangeProductStatusResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeCustInfoResultMsg }
     * 
     */
    public BatchChangeCustInfoResultMsg createBatchChangeCustInfoResultMsg() {
        return new BatchChangeCustInfoResultMsg();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipResultMsg }
     * 
     */
    public ChangeAcctOwnershipResultMsg createChangeAcctOwnershipResultMsg() {
        return new ChangeAcctOwnershipResultMsg();
    }

    /**
     * Create an instance of {@link QueryZoneInfoRequestMsg }
     * 
     */
    public QueryZoneInfoRequestMsg createQueryZoneInfoRequestMsg() {
        return new QueryZoneInfoRequestMsg();
    }

    /**
     * Create an instance of {@link QueryZoneInfoRequest }
     * 
     */
    public QueryZoneInfoRequest createQueryZoneInfoRequest() {
        return new QueryZoneInfoRequest();
    }

    /**
     * Create an instance of {@link QueryInstallmentRequestMsg }
     * 
     */
    public QueryInstallmentRequestMsg createQueryInstallmentRequestMsg() {
        return new QueryInstallmentRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeAcctOfferingRequestMsg }
     * 
     */
    public BatchChangeAcctOfferingRequestMsg createBatchChangeAcctOfferingRequestMsg() {
        return new BatchChangeAcctOfferingRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeRedFlagRequestMsg }
     * 
     */
    public ChangeRedFlagRequestMsg createChangeRedFlagRequestMsg() {
        return new ChangeRedFlagRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubInfoResultMsg }
     * 
     */
    public ChangeSubInfoResultMsg createChangeSubInfoResultMsg() {
        return new ChangeSubInfoResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubActiveLimitDateResultMsg }
     * 
     */
    public BatchChangeSubActiveLimitDateResultMsg createBatchChangeSubActiveLimitDateResultMsg() {
        return new BatchChangeSubActiveLimitDateResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeScatteredAcctInfoRequestMsg }
     * 
     */
    public BatchChangeScatteredAcctInfoRequestMsg createBatchChangeScatteredAcctInfoRequestMsg() {
        return new BatchChangeScatteredAcctInfoRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeScatteredAcctInfoRequest }
     * 
     */
    public BatchChangeScatteredAcctInfoRequest createBatchChangeScatteredAcctInfoRequest() {
        return new BatchChangeScatteredAcctInfoRequest();
    }

    /**
     * Create an instance of {@link QueryGroupMemberListRequestMsg }
     * 
     */
    public QueryGroupMemberListRequestMsg createQueryGroupMemberListRequestMsg() {
        return new QueryGroupMemberListRequestMsg();
    }

    /**
     * Create an instance of {@link QueryGroupMemberListRequest }
     * 
     */
    public QueryGroupMemberListRequest createQueryGroupMemberListRequest() {
        return new QueryGroupMemberListRequest();
    }

    /**
     * Create an instance of {@link BatchChangeScatteredAcctInfoResultMsg }
     * 
     */
    public BatchChangeScatteredAcctInfoResultMsg createBatchChangeScatteredAcctInfoResultMsg() {
        return new BatchChangeScatteredAcctInfoResultMsg();
    }

    /**
     * Create an instance of {@link QueryGroupOffNetNumberRequestMsg }
     * 
     */
    public QueryGroupOffNetNumberRequestMsg createQueryGroupOffNetNumberRequestMsg() {
        return new QueryGroupOffNetNumberRequestMsg();
    }

    /**
     * Create an instance of {@link QueryGroupOffNetNumberRequest }
     * 
     */
    public QueryGroupOffNetNumberRequest createQueryGroupOffNetNumberRequest() {
        return new QueryGroupOffNetNumberRequest();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequestMsg }
     * 
     */
    public ChangeSubPaymentModeRequestMsg createChangeSubPaymentModeRequestMsg() {
        return new ChangeSubPaymentModeRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyResultMsg }
     * 
     */
    public BatchChangeOfferingPropertyResultMsg createBatchChangeOfferingPropertyResultMsg() {
        return new BatchChangeOfferingPropertyResultMsg();
    }

    /**
     * Create an instance of {@link QueryGroupMemOffNetNumberRequestMsg }
     * 
     */
    public QueryGroupMemOffNetNumberRequestMsg createQueryGroupMemOffNetNumberRequestMsg() {
        return new QueryGroupMemOffNetNumberRequestMsg();
    }

    /**
     * Create an instance of {@link QueryGroupMemOffNetNumberRequest }
     * 
     */
    public QueryGroupMemOffNetNumberRequest createQueryGroupMemOffNetNumberRequest() {
        return new QueryGroupMemOffNetNumberRequest();
    }

    /**
     * Create an instance of {@link BatchChangeCustInfoRequestMsg }
     * 
     */
    public BatchChangeCustInfoRequestMsg createBatchChangeCustInfoRequestMsg() {
        return new BatchChangeCustInfoRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeCustInfoRequest }
     * 
     */
    public BatchChangeCustInfoRequest createBatchChangeCustInfoRequest() {
        return new BatchChangeCustInfoRequest();
    }

    /**
     * Create an instance of {@link ApplyPrepaymentResultMsg }
     * 
     */
    public ApplyPrepaymentResultMsg createApplyPrepaymentResultMsg() {
        return new ApplyPrepaymentResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubBasicInfoRequestMsg }
     * 
     */
    public BatchChangeSubBasicInfoRequestMsg createBatchChangeSubBasicInfoRequestMsg() {
        return new BatchChangeSubBasicInfoRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubBasicInfoRequest }
     * 
     */
    public BatchChangeSubBasicInfoRequest createBatchChangeSubBasicInfoRequest() {
        return new BatchChangeSubBasicInfoRequest();
    }

    /**
     * Create an instance of {@link QueryPaymentLimitUsageRequestMsg }
     * 
     */
    public QueryPaymentLimitUsageRequestMsg createQueryPaymentLimitUsageRequestMsg() {
        return new QueryPaymentLimitUsageRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberOfferingRequestMsg }
     * 
     */
    public ChangeGroupMemberOfferingRequestMsg createChangeGroupMemberOfferingRequestMsg() {
        return new ChangeGroupMemberOfferingRequestMsg();
    }

    /**
     * Create an instance of {@link CustDeactivationResultMsg }
     * 
     */
    public CustDeactivationResultMsg createCustDeactivationResultMsg() {
        return new CustDeactivationResultMsg();
    }

    /**
     * Create an instance of {@link SynDTUserGroupRequestMsg }
     * 
     */
    public SynDTUserGroupRequestMsg createSynDTUserGroupRequestMsg() {
        return new SynDTUserGroupRequestMsg();
    }

    /**
     * Create an instance of {@link SynDTUserGroupRequest }
     * 
     */
    public SynDTUserGroupRequest createSynDTUserGroupRequest() {
        return new SynDTUserGroupRequest();
    }

    /**
     * Create an instance of {@link ChangeMemberStatusRequestMsg }
     * 
     */
    public ChangeMemberStatusRequestMsg createChangeMemberStatusRequestMsg() {
        return new ChangeMemberStatusRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeMemberStatusRequest }
     * 
     */
    public ChangeMemberStatusRequest createChangeMemberStatusRequest() {
        return new ChangeMemberStatusRequest();
    }

    /**
     * Create an instance of {@link QueryDataPackageUsageRequestMsg }
     * 
     */
    public QueryDataPackageUsageRequestMsg createQueryDataPackageUsageRequestMsg() {
        return new QueryDataPackageUsageRequestMsg();
    }

    /**
     * Create an instance of {@link QueryDataPackageUsageRequest }
     * 
     */
    public QueryDataPackageUsageRequest createQueryDataPackageUsageRequest() {
        return new QueryDataPackageUsageRequest();
    }

    /**
     * Create an instance of {@link CreateGroupRequestMsg }
     * 
     */
    public CreateGroupRequestMsg createCreateGroupRequestMsg() {
        return new CreateGroupRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequestMsg }
     * 
     */
    public ChangeSubOwnershipRequestMsg createChangeSubOwnershipRequestMsg() {
        return new ChangeSubOwnershipRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubOfferingResultMsg }
     * 
     */
    public BatchChangeSubOfferingResultMsg createBatchChangeSubOfferingResultMsg() {
        return new BatchChangeSubOfferingResultMsg();
    }

    /**
     * Create an instance of {@link QueryRebateRequestMsg }
     * 
     */
    public QueryRebateRequestMsg createQueryRebateRequestMsg() {
        return new QueryRebateRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequestMsg }
     * 
     */
    public ChangeSubDFTAcctRequestMsg createChangeSubDFTAcctRequestMsg() {
        return new ChangeSubDFTAcctRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeCustInfoResultMsg }
     * 
     */
    public ChangeCustInfoResultMsg createChangeCustInfoResultMsg() {
        return new ChangeCustInfoResultMsg();
    }

    /**
     * Create an instance of {@link BatchAddGroupMemberResultMsg }
     * 
     */
    public BatchAddGroupMemberResultMsg createBatchAddGroupMemberResultMsg() {
        return new BatchAddGroupMemberResultMsg();
    }

    /**
     * Create an instance of {@link GroupDelMemberRequestMsg }
     * 
     */
    public GroupDelMemberRequestMsg createGroupDelMemberRequestMsg() {
        return new GroupDelMemberRequestMsg();
    }

    /**
     * Create an instance of {@link GroupDelMemberRequest }
     * 
     */
    public GroupDelMemberRequest createGroupDelMemberRequest() {
        return new GroupDelMemberRequest();
    }

    /**
     * Create an instance of {@link CheckSubscribersGroupRequestMsg }
     * 
     */
    public CheckSubscribersGroupRequestMsg createCheckSubscribersGroupRequestMsg() {
        return new CheckSubscribersGroupRequestMsg();
    }

    /**
     * Create an instance of {@link CheckSubscribersGroupRequest }
     * 
     */
    public CheckSubscribersGroupRequest createCheckSubscribersGroupRequest() {
        return new CheckSubscribersGroupRequest();
    }

    /**
     * Create an instance of {@link BatchChangeSubValidityResultMsg }
     * 
     */
    public BatchChangeSubValidityResultMsg createBatchChangeSubValidityResultMsg() {
        return new BatchChangeSubValidityResultMsg();
    }

    /**
     * Create an instance of {@link CreateCustomerRequestMsg }
     * 
     */
    public CreateCustomerRequestMsg createCreateCustomerRequestMsg() {
        return new CreateCustomerRequestMsg();
    }

    /**
     * Create an instance of {@link ModifyStatementRequestMsg }
     * 
     */
    public ModifyStatementRequestMsg createModifyStatementRequestMsg() {
        return new ModifyStatementRequestMsg();
    }

    /**
     * Create an instance of {@link CancelPreDeactivationRequestMsg }
     * 
     */
    public CancelPreDeactivationRequestMsg createCancelPreDeactivationRequestMsg() {
        return new CancelPreDeactivationRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupBasicInfoRequestMsg }
     * 
     */
    public ChangeGroupBasicInfoRequestMsg createChangeGroupBasicInfoRequestMsg() {
        return new ChangeGroupBasicInfoRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupBasicInfoRequest }
     * 
     */
    public ChangeGroupBasicInfoRequest createChangeGroupBasicInfoRequest() {
        return new ChangeGroupBasicInfoRequest();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequestMsg }
     * 
     */
    public ChangeOfferingPropertyRequestMsg createChangeOfferingPropertyRequestMsg() {
        return new ChangeOfferingPropertyRequestMsg();
    }

    /**
     * Create an instance of {@link AuthSubPwdRequestMsg }
     * 
     */
    public AuthSubPwdRequestMsg createAuthSubPwdRequestMsg() {
        return new AuthSubPwdRequestMsg();
    }

    /**
     * Create an instance of {@link AuthSubPwdRequest }
     * 
     */
    public AuthSubPwdRequest createAuthSubPwdRequest() {
        return new AuthSubPwdRequest();
    }

    /**
     * Create an instance of {@link QueryCallHuntingRequestMsg }
     * 
     */
    public QueryCallHuntingRequestMsg createQueryCallHuntingRequestMsg() {
        return new QueryCallHuntingRequestMsg();
    }

    /**
     * Create an instance of {@link QueryCallHuntingRequest }
     * 
     */
    public QueryCallHuntingRequest createQueryCallHuntingRequest() {
        return new QueryCallHuntingRequest();
    }

    /**
     * Create an instance of {@link AddStatementRequestMsg }
     * 
     */
    public AddStatementRequestMsg createAddStatementRequestMsg() {
        return new AddStatementRequestMsg();
    }

    /**
     * Create an instance of {@link AddStatementRequest }
     * 
     */
    public AddStatementRequest createAddStatementRequest() {
        return new AddStatementRequest();
    }

    /**
     * Create an instance of {@link ManFphCallerResultMsg }
     * 
     */
    public ManFphCallerResultMsg createManFphCallerResultMsg() {
        return new ManFphCallerResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeMemberStatusResultMsg }
     * 
     */
    public BatchChangeMemberStatusResultMsg createBatchChangeMemberStatusResultMsg() {
        return new BatchChangeMemberStatusResultMsg();
    }

    /**
     * Create an instance of {@link ChangeSubValidityRequestMsg }
     * 
     */
    public ChangeSubValidityRequestMsg createChangeSubValidityRequestMsg() {
        return new ChangeSubValidityRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubValidityRequest }
     * 
     */
    public ChangeSubValidityRequest createChangeSubValidityRequest() {
        return new ChangeSubValidityRequest();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequestMsg }
     * 
     */
    public QueryOfferingRentCycleRequestMsg createQueryOfferingRentCycleRequestMsg() {
        return new QueryOfferingRentCycleRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeCustInfoRequestMsg }
     * 
     */
    public ChangeCustInfoRequestMsg createChangeCustInfoRequestMsg() {
        return new ChangeCustInfoRequestMsg();
    }

    /**
     * Create an instance of {@link QueryDataPackageUsageResultMsg }
     * 
     */
    public QueryDataPackageUsageResultMsg createQueryDataPackageUsageResultMsg() {
        return new QueryDataPackageUsageResultMsg();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyRequestMsg }
     * 
     */
    public QueryOfferingInstPropertyRequestMsg createQueryOfferingInstPropertyRequestMsg() {
        return new QueryOfferingInstPropertyRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubStatusResultMsg }
     * 
     */
    public BatchChangeSubStatusResultMsg createBatchChangeSubStatusResultMsg() {
        return new BatchChangeSubStatusResultMsg();
    }

    /**
     * Create an instance of {@link GroupDelMemberResultMsg }
     * 
     */
    public GroupDelMemberResultMsg createGroupDelMemberResultMsg() {
        return new GroupDelMemberResultMsg();
    }

    /**
     * Create an instance of {@link ManFphCallerRequestMsg }
     * 
     */
    public ManFphCallerRequestMsg createManFphCallerRequestMsg() {
        return new ManFphCallerRequestMsg();
    }

    /**
     * Create an instance of {@link ManFphCallerRequest }
     * 
     */
    public ManFphCallerRequest createManFphCallerRequest() {
        return new ManFphCallerRequest();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupMemberResultMsg }
     * 
     */
    public DeleteSuperGroupMemberResultMsg createDeleteSuperGroupMemberResultMsg() {
        return new DeleteSuperGroupMemberResultMsg();
    }

    /**
     * Create an instance of {@link QueryFphCallerRequestMsg }
     * 
     */
    public QueryFphCallerRequestMsg createQueryFphCallerRequestMsg() {
        return new QueryFphCallerRequestMsg();
    }

    /**
     * Create an instance of {@link QueryFphCallerRequest }
     * 
     */
    public QueryFphCallerRequest createQueryFphCallerRequest() {
        return new QueryFphCallerRequest();
    }

    /**
     * Create an instance of {@link ChangeRechageBlacklistRequestMsg }
     * 
     */
    public ChangeRechageBlacklistRequestMsg createChangeRechageBlacklistRequestMsg() {
        return new ChangeRechageBlacklistRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeRechageBlacklistRequest }
     * 
     */
    public ChangeRechageBlacklistRequest createChangeRechageBlacklistRequest() {
        return new ChangeRechageBlacklistRequest();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingResultMsg }
     * 
     */
    public ChangeAccountOfferingResultMsg createChangeAccountOfferingResultMsg() {
        return new ChangeAccountOfferingResultMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupStatusResultMsg }
     * 
     */
    public ChangeGroupStatusResultMsg createChangeGroupStatusResultMsg() {
        return new ChangeGroupStatusResultMsg();
    }

    /**
     * Create an instance of {@link ChangeMemberShortNoRequestMsg }
     * 
     */
    public ChangeMemberShortNoRequestMsg createChangeMemberShortNoRequestMsg() {
        return new ChangeMemberShortNoRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeMemberShortNoRequest }
     * 
     */
    public ChangeMemberShortNoRequest createChangeMemberShortNoRequest() {
        return new ChangeMemberShortNoRequest();
    }

    /**
     * Create an instance of {@link DelGroupMemberResultMsg }
     * 
     */
    public DelGroupMemberResultMsg createDelGroupMemberResultMsg() {
        return new DelGroupMemberResultMsg();
    }

    /**
     * Create an instance of {@link CustDeactivationRequestMsg }
     * 
     */
    public CustDeactivationRequestMsg createCustDeactivationRequestMsg() {
        return new CustDeactivationRequestMsg();
    }

    /**
     * Create an instance of {@link QueryRscRelationResultMsg }
     * 
     */
    public QueryRscRelationResultMsg createQueryRscRelationResultMsg() {
        return new QueryRscRelationResultMsg();
    }

    /**
     * Create an instance of {@link DelGroupMemberRequestMsg }
     * 
     */
    public DelGroupMemberRequestMsg createDelGroupMemberRequestMsg() {
        return new DelGroupMemberRequestMsg();
    }

    /**
     * Create an instance of {@link JoinSuperGroupRequestMsg }
     * 
     */
    public JoinSuperGroupRequestMsg createJoinSuperGroupRequestMsg() {
        return new JoinSuperGroupRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubPwdRequestMsg }
     * 
     */
    public ChangeSubPwdRequestMsg createChangeSubPwdRequestMsg() {
        return new ChangeSubPwdRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubPwdRequest }
     * 
     */
    public ChangeSubPwdRequest createChangeSubPwdRequest() {
        return new ChangeSubPwdRequest();
    }

    /**
     * Create an instance of {@link BatchChangeMemberShortNoRequestMsg }
     * 
     */
    public BatchChangeMemberShortNoRequestMsg createBatchChangeMemberShortNoRequestMsg() {
        return new BatchChangeMemberShortNoRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeMemberShortNoRequest }
     * 
     */
    public BatchChangeMemberShortNoRequest createBatchChangeMemberShortNoRequest() {
        return new BatchChangeMemberShortNoRequest();
    }

    /**
     * Create an instance of {@link BatchChangeSubActiveLimitDateRequestMsg }
     * 
     */
    public BatchChangeSubActiveLimitDateRequestMsg createBatchChangeSubActiveLimitDateRequestMsg() {
        return new BatchChangeSubActiveLimitDateRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubActiveLimitDateRequest }
     * 
     */
    public BatchChangeSubActiveLimitDateRequest createBatchChangeSubActiveLimitDateRequest() {
        return new BatchChangeSubActiveLimitDateRequest();
    }

    /**
     * Create an instance of {@link CreateAccountRequestMsg }
     * 
     */
    public CreateAccountRequestMsg createCreateAccountRequestMsg() {
        return new CreateAccountRequestMsg();
    }

    /**
     * Create an instance of {@link ActivateOfferingResultMsg }
     * 
     */
    public ActivateOfferingResultMsg createActivateOfferingResultMsg() {
        return new ActivateOfferingResultMsg();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequestMsg }
     * 
     */
    public ChangePayRelationRequestMsg createChangePayRelationRequestMsg() {
        return new ChangePayRelationRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubStatusResultMsg }
     * 
     */
    public ChangeSubStatusResultMsg createChangeSubStatusResultMsg() {
        return new ChangeSubStatusResultMsg();
    }

    /**
     * Create an instance of {@link ChangeSubStatusResult }
     * 
     */
    public ChangeSubStatusResult createChangeSubStatusResult() {
        return new ChangeSubStatusResult();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingRequestMsg }
     * 
     */
    public ChangeCustOfferingRequestMsg createChangeCustOfferingRequestMsg() {
        return new ChangeCustOfferingRequestMsg();
    }

    /**
     * Create an instance of {@link BatchSubActivationResultMsg }
     * 
     */
    public BatchSubActivationResultMsg createBatchSubActivationResultMsg() {
        return new BatchSubActivationResultMsg();
    }

    /**
     * Create an instance of {@link QuerySubInfoExToCubeResultMsg }
     * 
     */
    public QuerySubInfoExToCubeResultMsg createQuerySubInfoExToCubeResultMsg() {
        return new QuerySubInfoExToCubeResultMsg();
    }

    /**
     * Create an instance of {@link QuerySubInfoExToCubeRequestMsg }
     * 
     */
    public QuerySubInfoExToCubeRequestMsg createQuerySubInfoExToCubeRequestMsg() {
        return new QuerySubInfoExToCubeRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupOffNetNumberRequestMsg }
     * 
     */
    public ChangeGroupOffNetNumberRequestMsg createChangeGroupOffNetNumberRequestMsg() {
        return new ChangeGroupOffNetNumberRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubPwdResultMsg }
     * 
     */
    public ChangeSubPwdResultMsg createChangeSubPwdResultMsg() {
        return new ChangeSubPwdResultMsg();
    }

    /**
     * Create an instance of {@link BatchSubDeactivationRequestMsg }
     * 
     */
    public BatchSubDeactivationRequestMsg createBatchSubDeactivationRequestMsg() {
        return new BatchSubDeactivationRequestMsg();
    }

    /**
     * Create an instance of {@link BatchSubDeactivationRequest }
     * 
     */
    public BatchSubDeactivationRequest createBatchSubDeactivationRequest() {
        return new BatchSubDeactivationRequest();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequestMsg }
     * 
     */
    public ChangeConsumptionLimitRequestMsg createChangeConsumptionLimitRequestMsg() {
        return new ChangeConsumptionLimitRequestMsg();
    }

    /**
     * Create an instance of {@link BatchDelGroupMemberRequestMsg }
     * 
     */
    public BatchDelGroupMemberRequestMsg createBatchDelGroupMemberRequestMsg() {
        return new BatchDelGroupMemberRequestMsg();
    }

    /**
     * Create an instance of {@link ApplyProdFreeValidityRequestMsg }
     * 
     */
    public ApplyProdFreeValidityRequestMsg createApplyProdFreeValidityRequestMsg() {
        return new ApplyProdFreeValidityRequestMsg();
    }

    /**
     * Create an instance of {@link ApplyProdFreeValidityRequest }
     * 
     */
    public ApplyProdFreeValidityRequest createApplyProdFreeValidityRequest() {
        return new ApplyProdFreeValidityRequest();
    }

    /**
     * Create an instance of {@link BatchAdjustmentResultMsg }
     * 
     */
    public BatchAdjustmentResultMsg createBatchAdjustmentResultMsg() {
        return new BatchAdjustmentResultMsg();
    }

    /**
     * Create an instance of {@link QueryAccumulationUsageRequestMsg }
     * 
     */
    public QueryAccumulationUsageRequestMsg createQueryAccumulationUsageRequestMsg() {
        return new QueryAccumulationUsageRequestMsg();
    }

    /**
     * Create an instance of {@link BatchSubActivationRequestMsg }
     * 
     */
    public BatchSubActivationRequestMsg createBatchSubActivationRequestMsg() {
        return new BatchSubActivationRequestMsg();
    }

    /**
     * Create an instance of {@link ApplyProdFreeValidityResultMsg }
     * 
     */
    public ApplyProdFreeValidityResultMsg createApplyProdFreeValidityResultMsg() {
        return new ApplyProdFreeValidityResultMsg();
    }

    /**
     * Create an instance of {@link ApplyProdFreeValidityResult }
     * 
     */
    public ApplyProdFreeValidityResult createApplyProdFreeValidityResult() {
        return new ApplyProdFreeValidityResult();
    }

    /**
     * Create an instance of {@link ChangeSubIdentityRequestMsg }
     * 
     */
    public ChangeSubIdentityRequestMsg createChangeSubIdentityRequestMsg() {
        return new ChangeSubIdentityRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberInfoRequestMsg }
     * 
     */
    public ChangeGroupMemberInfoRequestMsg createChangeGroupMemberInfoRequestMsg() {
        return new ChangeGroupMemberInfoRequestMsg();
    }

    /**
     * Create an instance of {@link ActivateOfferingRequestMsg }
     * 
     */
    public ActivateOfferingRequestMsg createActivateOfferingRequestMsg() {
        return new ActivateOfferingRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubInitBalanceResultMsg }
     * 
     */
    public BatchChangeSubInitBalanceResultMsg createBatchChangeSubInitBalanceResultMsg() {
        return new BatchChangeSubInitBalanceResultMsg();
    }

    /**
     * Create an instance of {@link QueryAppendSubIdentityResultMsg }
     * 
     */
    public QueryAppendSubIdentityResultMsg createQueryAppendSubIdentityResultMsg() {
        return new QueryAppendSubIdentityResultMsg();
    }

    /**
     * Create an instance of {@link QueryGroupOffNetNumberResultMsg }
     * 
     */
    public QueryGroupOffNetNumberResultMsg createQueryGroupOffNetNumberResultMsg() {
        return new QueryGroupOffNetNumberResultMsg();
    }

    /**
     * Create an instance of {@link QueryRebateResultMsg }
     * 
     */
    public QueryRebateResultMsg createQueryRebateResultMsg() {
        return new QueryRebateResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubInitBalanceRequestMsg }
     * 
     */
    public BatchChangeSubInitBalanceRequestMsg createBatchChangeSubInitBalanceRequestMsg() {
        return new BatchChangeSubInitBalanceRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubInitBalanceRequest }
     * 
     */
    public BatchChangeSubInitBalanceRequest createBatchChangeSubInitBalanceRequest() {
        return new BatchChangeSubInitBalanceRequest();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingResultMsg }
     * 
     */
    public ChangeCustOfferingResultMsg createChangeCustOfferingResultMsg() {
        return new ChangeCustOfferingResultMsg();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleRequestMsg }
     * 
     */
    public QuerySubLifeCycleRequestMsg createQuerySubLifeCycleRequestMsg() {
        return new QuerySubLifeCycleRequestMsg();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleRequest }
     * 
     */
    public QuerySubLifeCycleRequest createQuerySubLifeCycleRequest() {
        return new QuerySubLifeCycleRequest();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyRequestMsg }
     * 
     */
    public BatchChangeOfferingPropertyRequestMsg createBatchChangeOfferingPropertyRequestMsg() {
        return new BatchChangeOfferingPropertyRequestMsg();
    }

    /**
     * Create an instance of {@link QueryBatchTaskStatusResultMsg }
     * 
     */
    public QueryBatchTaskStatusResultMsg createQueryBatchTaskStatusResultMsg() {
        return new QueryBatchTaskStatusResultMsg();
    }

    /**
     * Create an instance of {@link QueryBatchTaskStatusResult }
     * 
     */
    public QueryBatchTaskStatusResult createQueryBatchTaskStatusResult() {
        return new QueryBatchTaskStatusResult();
    }

    /**
     * Create an instance of {@link ManAppendSubIdentityRequestMsg }
     * 
     */
    public ManAppendSubIdentityRequestMsg createManAppendSubIdentityRequestMsg() {
        return new ManAppendSubIdentityRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeResultMsg }
     * 
     */
    public ChangeSubPaymentModeResultMsg createChangeSubPaymentModeResultMsg() {
        return new ChangeSubPaymentModeResultMsg();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationRequestMsg }
     * 
     */
    public QueryPaymentRelationRequestMsg createQueryPaymentRelationRequestMsg() {
        return new QueryPaymentRelationRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoRequestMsg }
     * 
     */
    public ChangeAcctInfoRequestMsg createChangeAcctInfoRequestMsg() {
        return new ChangeAcctInfoRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeTaxExemptionResultMsg }
     * 
     */
    public ChangeTaxExemptionResultMsg createChangeTaxExemptionResultMsg() {
        return new ChangeTaxExemptionResultMsg();
    }

    /**
     * Create an instance of {@link ManageGrpCallScreenNoRequestMsg }
     * 
     */
    public ManageGrpCallScreenNoRequestMsg createManageGrpCallScreenNoRequestMsg() {
        return new ManageGrpCallScreenNoRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeOfferingStatusRequestMsg }
     * 
     */
    public ChangeOfferingStatusRequestMsg createChangeOfferingStatusRequestMsg() {
        return new ChangeOfferingStatusRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubBasicInfoResultMsg }
     * 
     */
    public BatchChangeSubBasicInfoResultMsg createBatchChangeSubBasicInfoResultMsg() {
        return new BatchChangeSubBasicInfoResultMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberOfferingResultMsg }
     * 
     */
    public ChangeGroupMemberOfferingResultMsg createChangeGroupMemberOfferingResultMsg() {
        return new ChangeGroupMemberOfferingResultMsg();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendRequestMsg }
     * 
     */
    public ApplyInstallmentExtendRequestMsg createApplyInstallmentExtendRequestMsg() {
        return new ApplyInstallmentExtendRequestMsg();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupRequestMsg }
     * 
     */
    public DeleteSuperGroupRequestMsg createDeleteSuperGroupRequestMsg() {
        return new DeleteSuperGroupRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipResultMsg }
     * 
     */
    public ChangeSubOwnershipResultMsg createChangeSubOwnershipResultMsg() {
        return new ChangeSubOwnershipResultMsg();
    }

    /**
     * Create an instance of {@link QueryGrpCallScreenNoResultMsg }
     * 
     */
    public QueryGrpCallScreenNoResultMsg createQueryGrpCallScreenNoResultMsg() {
        return new QueryGrpCallScreenNoResultMsg();
    }

    /**
     * Create an instance of {@link ApplyInstallmentResultMsg }
     * 
     */
    public ApplyInstallmentResultMsg createApplyInstallmentResultMsg() {
        return new ApplyInstallmentResultMsg();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequestMsg }
     * 
     */
    public AddGroupMemberRequestMsg createAddGroupMemberRequestMsg() {
        return new AddGroupMemberRequestMsg();
    }

    /**
     * Create an instance of {@link FeeQuotationRequestMsg }
     * 
     */
    public FeeQuotationRequestMsg createFeeQuotationRequestMsg() {
        return new FeeQuotationRequestMsg();
    }

    /**
     * Create an instance of {@link QueryAccumulationUsageResultMsg }
     * 
     */
    public QueryAccumulationUsageResultMsg createQueryAccumulationUsageResultMsg() {
        return new QueryAccumulationUsageResultMsg();
    }

    /**
     * Create an instance of {@link ManageCallHuntingResultMsg }
     * 
     */
    public ManageCallHuntingResultMsg createManageCallHuntingResultMsg() {
        return new ManageCallHuntingResultMsg();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitResultMsg }
     * 
     */
    public ChangeConsumptionLimitResultMsg createChangeConsumptionLimitResultMsg() {
        return new ChangeConsumptionLimitResultMsg();
    }

    /**
     * Create an instance of {@link SynDTUserGroupResultMsg }
     * 
     */
    public SynDTUserGroupResultMsg createSynDTUserGroupResultMsg() {
        return new SynDTUserGroupResultMsg();
    }

    /**
     * Create an instance of {@link SubActivationRequestMsg }
     * 
     */
    public SubActivationRequestMsg createSubActivationRequestMsg() {
        return new SubActivationRequestMsg();
    }

    /**
     * Create an instance of {@link ManCrossGroupRequestMsg }
     * 
     */
    public ManCrossGroupRequestMsg createManCrossGroupRequestMsg() {
        return new ManCrossGroupRequestMsg();
    }

    /**
     * Create an instance of {@link ManCrossGroupRequest }
     * 
     */
    public ManCrossGroupRequest createManCrossGroupRequest() {
        return new ManCrossGroupRequest();
    }

    /**
     * Create an instance of {@link ChangeCustNoticeSuppressResultMsg }
     * 
     */
    public ChangeCustNoticeSuppressResultMsg createChangeCustNoticeSuppressResultMsg() {
        return new ChangeCustNoticeSuppressResultMsg();
    }

    /**
     * Create an instance of {@link ManageGrpCallScreenNoResultMsg }
     * 
     */
    public ManageGrpCallScreenNoResultMsg createManageGrpCallScreenNoResultMsg() {
        return new ManageGrpCallScreenNoResultMsg();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitRequestMsg }
     * 
     */
    public QueryConsumptionLimitRequestMsg createQueryConsumptionLimitRequestMsg() {
        return new QueryConsumptionLimitRequestMsg();
    }

    /**
     * Create an instance of {@link QueryPaymentLimitUsageResultMsg }
     * 
     */
    public QueryPaymentLimitUsageResultMsg createQueryPaymentLimitUsageResultMsg() {
        return new QueryPaymentLimitUsageResultMsg();
    }

    /**
     * Create an instance of {@link QuerySubInforToMicroRequestMsg }
     * 
     */
    public QuerySubInforToMicroRequestMsg createQuerySubInforToMicroRequestMsg() {
        return new QuerySubInforToMicroRequestMsg();
    }

    /**
     * Create an instance of {@link QueryBatchTaskStatusRequestMsg }
     * 
     */
    public QueryBatchTaskStatusRequestMsg createQueryBatchTaskStatusRequestMsg() {
        return new QueryBatchTaskStatusRequestMsg();
    }

    /**
     * Create an instance of {@link QueryBatchTaskStatusRequest }
     * 
     */
    public QueryBatchTaskStatusRequest createQueryBatchTaskStatusRequest() {
        return new QueryBatchTaskStatusRequest();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResultMsg }
     * 
     */
    public QueryCustomerInfoResultMsg createQueryCustomerInfoResultMsg() {
        return new QueryCustomerInfoResultMsg();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycleRequestMsg }
     * 
     */
    public ChangeAcctBillCycleRequestMsg createChangeAcctBillCycleRequestMsg() {
        return new ChangeAcctBillCycleRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubGrpDFTAcctResultMsg }
     * 
     */
    public ChangeSubGrpDFTAcctResultMsg createChangeSubGrpDFTAcctResultMsg() {
        return new ChangeSubGrpDFTAcctResultMsg();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyResultMsg }
     * 
     */
    public ChangeOfferingPropertyResultMsg createChangeOfferingPropertyResultMsg() {
        return new ChangeOfferingPropertyResultMsg();
    }

    /**
     * Create an instance of {@link QueryFphCallerResultMsg }
     * 
     */
    public QueryFphCallerResultMsg createQueryFphCallerResultMsg() {
        return new QueryFphCallerResultMsg();
    }

    /**
     * Create an instance of {@link QueryFphCallerResult }
     * 
     */
    public QueryFphCallerResult createQueryFphCallerResult() {
        return new QueryFphCallerResult();
    }

    /**
     * Create an instance of {@link ModifyStatementResultMsg }
     * 
     */
    public ModifyStatementResultMsg createModifyStatementResultMsg() {
        return new ModifyStatementResultMsg();
    }

    /**
     * Create an instance of {@link QueryExpireSubToMicroRequestMsg }
     * 
     */
    public QueryExpireSubToMicroRequestMsg createQueryExpireSubToMicroRequestMsg() {
        return new QueryExpireSubToMicroRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeMemberShortNoResultMsg }
     * 
     */
    public ChangeMemberShortNoResultMsg createChangeMemberShortNoResultMsg() {
        return new ChangeMemberShortNoResultMsg();
    }

    /**
     * Create an instance of {@link QueryZoneMappingRequestMsg }
     * 
     */
    public QueryZoneMappingRequestMsg createQueryZoneMappingRequestMsg() {
        return new QueryZoneMappingRequestMsg();
    }

    /**
     * Create an instance of {@link QueryZoneMappingRequest }
     * 
     */
    public QueryZoneMappingRequest createQueryZoneMappingRequest() {
        return new QueryZoneMappingRequest();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingRequestMsg }
     * 
     */
    public ChangeAccountOfferingRequestMsg createChangeAccountOfferingRequestMsg() {
        return new ChangeAccountOfferingRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupOffNetNumberResultMsg }
     * 
     */
    public ChangeGroupOffNetNumberResultMsg createChangeGroupOffNetNumberResultMsg() {
        return new ChangeGroupOffNetNumberResultMsg();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequestMsg }
     * 
     */
    public ChangeRscRelationRequestMsg createChangeRscRelationRequestMsg() {
        return new ChangeRscRelationRequestMsg();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResultMsg }
     * 
     */
    public QueryPaymentRelationResultMsg createQueryPaymentRelationResultMsg() {
        return new QueryPaymentRelationResultMsg();
    }

    /**
     * Create an instance of {@link QueryZoneInfoResultMsg }
     * 
     */
    public QueryZoneInfoResultMsg createQueryZoneInfoResultMsg() {
        return new QueryZoneInfoResultMsg();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitResultMsg }
     * 
     */
    public QueryConsumptionLimitResultMsg createQueryConsumptionLimitResultMsg() {
        return new QueryConsumptionLimitResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeMemberStatusRequestMsg }
     * 
     */
    public BatchChangeMemberStatusRequestMsg createBatchChangeMemberStatusRequestMsg() {
        return new BatchChangeMemberStatusRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeMemberStatusRequest }
     * 
     */
    public BatchChangeMemberStatusRequest createBatchChangeMemberStatusRequest() {
        return new BatchChangeMemberStatusRequest();
    }

    /**
     * Create an instance of {@link SubDeactivationRequestMsg }
     * 
     */
    public SubDeactivationRequestMsg createSubDeactivationRequestMsg() {
        return new SubDeactivationRequestMsg();
    }

    /**
     * Create an instance of {@link QuerySubscribedOfferingsRequestMsg }
     * 
     */
    public QuerySubscribedOfferingsRequestMsg createQuerySubscribedOfferingsRequestMsg() {
        return new QuerySubscribedOfferingsRequestMsg();
    }

    /**
     * Create an instance of {@link QuerySubscribedOfferingsRequest }
     * 
     */
    public QuerySubscribedOfferingsRequest createQuerySubscribedOfferingsRequest() {
        return new QuerySubscribedOfferingsRequest();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoRequestMsg }
     * 
     */
    public QueryCustomerInfoRequestMsg createQueryCustomerInfoRequestMsg() {
        return new QueryCustomerInfoRequestMsg();
    }

    /**
     * Create an instance of {@link SynDTDiscountRequestMsg }
     * 
     */
    public SynDTDiscountRequestMsg createSynDTDiscountRequestMsg() {
        return new SynDTDiscountRequestMsg();
    }

    /**
     * Create an instance of {@link SynDTDiscountRequest }
     * 
     */
    public SynDTDiscountRequest createSynDTDiscountRequest() {
        return new SynDTDiscountRequest();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequestMsg }
     * 
     */
    public CreateSubscriberRequestMsg createCreateSubscriberRequestMsg() {
        return new CreateSubscriberRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubValidityResultMsg }
     * 
     */
    public ChangeSubValidityResultMsg createChangeSubValidityResultMsg() {
        return new ChangeSubValidityResultMsg();
    }

    /**
     * Create an instance of {@link QueryExpireSubToMicroResultMsg }
     * 
     */
    public QueryExpireSubToMicroResultMsg createQueryExpireSubToMicroResultMsg() {
        return new QueryExpireSubToMicroResultMsg();
    }

    /**
     * Create an instance of {@link CreateAccountResultMsg }
     * 
     */
    public CreateAccountResultMsg createCreateAccountResultMsg() {
        return new CreateAccountResultMsg();
    }

    /**
     * Create an instance of {@link QueryInstallmentResultMsg }
     * 
     */
    public QueryInstallmentResultMsg createQueryInstallmentResultMsg() {
        return new QueryInstallmentResultMsg();
    }

    /**
     * Create an instance of {@link ChangeSubGrpDFTAcctRequestMsg }
     * 
     */
    public ChangeSubGrpDFTAcctRequestMsg createChangeSubGrpDFTAcctRequestMsg() {
        return new ChangeSubGrpDFTAcctRequestMsg();
    }

    /**
     * Create an instance of {@link ManAppendSubIdentityResultMsg }
     * 
     */
    public ManAppendSubIdentityResultMsg createManAppendSubIdentityResultMsg() {
        return new ManAppendSubIdentityResultMsg();
    }

    /**
     * Create an instance of {@link QueryZoneMappingResultMsg }
     * 
     */
    public QueryZoneMappingResultMsg createQueryZoneMappingResultMsg() {
        return new QueryZoneMappingResultMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingRequestMsg }
     * 
     */
    public ChangeGroupOfferingRequestMsg createChangeGroupOfferingRequestMsg() {
        return new ChangeGroupOfferingRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeAcctOfferingResultMsg }
     * 
     */
    public BatchChangeAcctOfferingResultMsg createBatchChangeAcctOfferingResultMsg() {
        return new BatchChangeAcctOfferingResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeMemberShortNoResultMsg }
     * 
     */
    public BatchChangeMemberShortNoResultMsg createBatchChangeMemberShortNoResultMsg() {
        return new BatchChangeMemberShortNoResultMsg();
    }

    /**
     * Create an instance of {@link GroupDeactivationResultMsg }
     * 
     */
    public GroupDeactivationResultMsg createGroupDeactivationResultMsg() {
        return new GroupDeactivationResultMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupBasicInfoResultMsg }
     * 
     */
    public ChangeGroupBasicInfoResultMsg createChangeGroupBasicInfoResultMsg() {
        return new ChangeGroupBasicInfoResultMsg();
    }

    /**
     * Create an instance of {@link FeeDeductionRequestMsg }
     * 
     */
    public FeeDeductionRequestMsg createFeeDeductionRequestMsg() {
        return new FeeDeductionRequestMsg();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendResultMsg }
     * 
     */
    public ApplyInstallmentExtendResultMsg createApplyInstallmentExtendResultMsg() {
        return new ApplyInstallmentExtendResultMsg();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingResultMsg }
     * 
     */
    public ChangeSubOfferingResultMsg createChangeSubOfferingResultMsg() {
        return new ChangeSubOfferingResultMsg();
    }

    /**
     * Create an instance of {@link CleanUpCustDataResultMsg }
     * 
     */
    public CleanUpCustDataResultMsg createCleanUpCustDataResultMsg() {
        return new CleanUpCustDataResultMsg();
    }

    /**
     * Create an instance of {@link ChangeOfferingStatusResultMsg }
     * 
     */
    public ChangeOfferingStatusResultMsg createChangeOfferingStatusResultMsg() {
        return new ChangeOfferingStatusResultMsg();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycleResultMsg }
     * 
     */
    public ChangeAcctBillCycleResultMsg createChangeAcctBillCycleResultMsg() {
        return new ChangeAcctBillCycleResultMsg();
    }

    /**
     * Create an instance of {@link ChangeSubStatusRequestMsg }
     * 
     */
    public ChangeSubStatusRequestMsg createChangeSubStatusRequestMsg() {
        return new ChangeSubStatusRequestMsg();
    }

    /**
     * Create an instance of {@link ApplyPrepaymentRequestMsg }
     * 
     */
    public ApplyPrepaymentRequestMsg createApplyPrepaymentRequestMsg() {
        return new ApplyPrepaymentRequestMsg();
    }

    /**
     * Create an instance of {@link AcctDeactivationRequestMsg }
     * 
     */
    public AcctDeactivationRequestMsg createAcctDeactivationRequestMsg() {
        return new AcctDeactivationRequestMsg();
    }

    /**
     * Create an instance of {@link BatchSubDeactivationResultMsg }
     * 
     */
    public BatchSubDeactivationResultMsg createBatchSubDeactivationResultMsg() {
        return new BatchSubDeactivationResultMsg();
    }

    /**
     * Create an instance of {@link CheckSubscribersGroupResultMsg }
     * 
     */
    public CheckSubscribersGroupResultMsg createCheckSubscribersGroupResultMsg() {
        return new CheckSubscribersGroupResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangePayRelationRequestMsg }
     * 
     */
    public BatchChangePayRelationRequestMsg createBatchChangePayRelationRequestMsg() {
        return new BatchChangePayRelationRequestMsg();
    }

    /**
     * Create an instance of {@link JoinSuperGroupResultMsg }
     * 
     */
    public JoinSuperGroupResultMsg createJoinSuperGroupResultMsg() {
        return new JoinSuperGroupResultMsg();
    }

    /**
     * Create an instance of {@link AuthSubPwdResultMsg }
     * 
     */
    public AuthSubPwdResultMsg createAuthSubPwdResultMsg() {
        return new AuthSubPwdResultMsg();
    }

    /**
     * Create an instance of {@link ChangeRscRelationResultMsg }
     * 
     */
    public ChangeRscRelationResultMsg createChangeRscRelationResultMsg() {
        return new ChangeRscRelationResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubStatusRequestMsg }
     * 
     */
    public BatchChangeSubStatusRequestMsg createBatchChangeSubStatusRequestMsg() {
        return new BatchChangeSubStatusRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubStatusRequest }
     * 
     */
    public BatchChangeSubStatusRequest createBatchChangeSubStatusRequest() {
        return new BatchChangeSubStatusRequest();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingResultMsg }
     * 
     */
    public ChangeGroupOfferingResultMsg createChangeGroupOfferingResultMsg() {
        return new ChangeGroupOfferingResultMsg();
    }

    /**
     * Create an instance of {@link ChangePayRelationResultMsg }
     * 
     */
    public ChangePayRelationResultMsg createChangePayRelationResultMsg() {
        return new ChangePayRelationResultMsg();
    }

    /**
     * Create an instance of {@link QueryGroupMemOffNetNumberResultMsg }
     * 
     */
    public QueryGroupMemOffNetNumberResultMsg createQueryGroupMemOffNetNumberResultMsg() {
        return new QueryGroupMemOffNetNumberResultMsg();
    }

    /**
     * Create an instance of {@link ChangeCustDFTAcctRequestMsg }
     * 
     */
    public ChangeCustDFTAcctRequestMsg createChangeCustDFTAcctRequestMsg() {
        return new ChangeCustDFTAcctRequestMsg();
    }

    /**
     * Create an instance of {@link QueryOfferingRentFailedFeeRequestMsg }
     * 
     */
    public QueryOfferingRentFailedFeeRequestMsg createQueryOfferingRentFailedFeeRequestMsg() {
        return new QueryOfferingRentFailedFeeRequestMsg();
    }

    /**
     * Create an instance of {@link CreateSubscriberResultMsg }
     * 
     */
    public CreateSubscriberResultMsg createCreateSubscriberResultMsg() {
        return new CreateSubscriberResultMsg();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequestMsg }
     * 
     */
    public BatchSwitchGroupMemberRequestMsg createBatchSwitchGroupMemberRequestMsg() {
        return new BatchSwitchGroupMemberRequestMsg();
    }

    /**
     * Create an instance of {@link ApplyInstallmentRequestMsg }
     * 
     */
    public ApplyInstallmentRequestMsg createApplyInstallmentRequestMsg() {
        return new ApplyInstallmentRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctResultMsg }
     * 
     */
    public ChangeSubDFTAcctResultMsg createChangeSubDFTAcctResultMsg() {
        return new ChangeSubDFTAcctResultMsg();
    }

    /**
     * Create an instance of {@link SubActivationResultMsg }
     * 
     */
    public SubActivationResultMsg createSubActivationResultMsg() {
        return new SubActivationResultMsg();
    }

    /**
     * Create an instance of {@link ChangeCustHierachyRequestMsg }
     * 
     */
    public ChangeCustHierachyRequestMsg createChangeCustHierachyRequestMsg() {
        return new ChangeCustHierachyRequestMsg();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyResultMsg }
     * 
     */
    public QueryOfferingInstPropertyResultMsg createQueryOfferingInstPropertyResultMsg() {
        return new QueryOfferingInstPropertyResultMsg();
    }

    /**
     * Create an instance of {@link BatchCreateSubscriberResultMsg }
     * 
     */
    public BatchCreateSubscriberResultMsg createBatchCreateSubscriberResultMsg() {
        return new BatchCreateSubscriberResultMsg();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoResultMsg }
     * 
     */
    public ChangeAcctInfoResultMsg createChangeAcctInfoResultMsg() {
        return new ChangeAcctInfoResultMsg();
    }

    /**
     * Create an instance of {@link ManCrossGroupResultMsg }
     * 
     */
    public ManCrossGroupResultMsg createManCrossGroupResultMsg() {
        return new ManCrossGroupResultMsg();
    }

    /**
     * Create an instance of {@link FeeQuotationResultMsg }
     * 
     */
    public FeeQuotationResultMsg createFeeQuotationResultMsg() {
        return new FeeQuotationResultMsg();
    }

    /**
     * Create an instance of {@link AddStatementResultMsg }
     * 
     */
    public AddStatementResultMsg createAddStatementResultMsg() {
        return new AddStatementResultMsg();
    }

    /**
     * Create an instance of {@link QueryGroupListBySubscriberResultMsg }
     * 
     */
    public QueryGroupListBySubscriberResultMsg createQueryGroupListBySubscriberResultMsg() {
        return new QueryGroupListBySubscriberResultMsg();
    }

    /**
     * Create an instance of {@link CreateCustomerResultMsg }
     * 
     */
    public CreateCustomerResultMsg createCreateCustomerResultMsg() {
        return new CreateCustomerResultMsg();
    }

    /**
     * Create an instance of {@link CreateCustomerResult }
     * 
     */
    public CreateCustomerResult createCreateCustomerResult() {
        return new CreateCustomerResult();
    }

    /**
     * Create an instance of {@link QuerySubInforToMicroResultMsg }
     * 
     */
    public QuerySubInforToMicroResultMsg createQuerySubInforToMicroResultMsg() {
        return new QuerySubInforToMicroResultMsg();
    }

    /**
     * Create an instance of {@link SynDTDiscountResultMsg }
     * 
     */
    public SynDTDiscountResultMsg createSynDTDiscountResultMsg() {
        return new SynDTDiscountResultMsg();
    }

    /**
     * Create an instance of {@link ChangeRedFlagResultMsg }
     * 
     */
    public ChangeRedFlagResultMsg createChangeRedFlagResultMsg() {
        return new ChangeRedFlagResultMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupMemOffNetNumberRequestMsg }
     * 
     */
    public ChangeGroupMemOffNetNumberRequestMsg createChangeGroupMemOffNetNumberRequestMsg() {
        return new ChangeGroupMemOffNetNumberRequestMsg();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResultMsg }
     * 
     */
    public QueryOfferingRentCycleResultMsg createQueryOfferingRentCycleResultMsg() {
        return new QueryOfferingRentCycleResultMsg();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingRequestMsg }
     * 
     */
    public ChangeSubOfferingRequestMsg createChangeSubOfferingRequestMsg() {
        return new ChangeSubOfferingRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitRequestMsg }
     * 
     */
    public ChangeAcctCreditLimitRequestMsg createChangeAcctCreditLimitRequestMsg() {
        return new ChangeAcctCreditLimitRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeCustHierachyResultMsg }
     * 
     */
    public ChangeCustHierachyResultMsg createChangeCustHierachyResultMsg() {
        return new ChangeCustHierachyResultMsg();
    }

    /**
     * Create an instance of {@link CleanUpCustDataRequestMsg }
     * 
     */
    public CleanUpCustDataRequestMsg createCleanUpCustDataRequestMsg() {
        return new CleanUpCustDataRequestMsg();
    }

    /**
     * Create an instance of {@link CleanUpCustDataRequest }
     * 
     */
    public CleanUpCustDataRequest createCleanUpCustDataRequest() {
        return new CleanUpCustDataRequest();
    }

    /**
     * Create an instance of {@link ChangeRechageBlacklistResultMsg }
     * 
     */
    public ChangeRechageBlacklistResultMsg createChangeRechageBlacklistResultMsg() {
        return new ChangeRechageBlacklistResultMsg();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupResultMsg }
     * 
     */
    public DeleteSuperGroupResultMsg createDeleteSuperGroupResultMsg() {
        return new DeleteSuperGroupResultMsg();
    }

    /**
     * Create an instance of {@link SupplementProfileRequestMsg }
     * 
     */
    public SupplementProfileRequestMsg createSupplementProfileRequestMsg() {
        return new SupplementProfileRequestMsg();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberResultMsg }
     * 
     */
    public BatchSwitchGroupMemberResultMsg createBatchSwitchGroupMemberResultMsg() {
        return new BatchSwitchGroupMemberResultMsg();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleResultMsg }
     * 
     */
    public QuerySubLifeCycleResultMsg createQuerySubLifeCycleResultMsg() {
        return new QuerySubLifeCycleResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangePayRelationResultMsg }
     * 
     */
    public BatchChangePayRelationResultMsg createBatchChangePayRelationResultMsg() {
        return new BatchChangePayRelationResultMsg();
    }

    /**
     * Create an instance of {@link CreateSuperGroupResultMsg }
     * 
     */
    public CreateSuperGroupResultMsg createCreateSuperGroupResultMsg() {
        return new CreateSuperGroupResultMsg();
    }

    /**
     * Create an instance of {@link ChangeProductStatusRequestMsg }
     * 
     */
    public ChangeProductStatusRequestMsg createChangeProductStatusRequestMsg() {
        return new ChangeProductStatusRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubValidityRequestMsg }
     * 
     */
    public BatchChangeSubValidityRequestMsg createBatchChangeSubValidityRequestMsg() {
        return new BatchChangeSubValidityRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubValidityRequest }
     * 
     */
    public BatchChangeSubValidityRequest createBatchChangeSubValidityRequest() {
        return new BatchChangeSubValidityRequest();
    }

    /**
     * Create an instance of {@link QueryGroupListBySubscriberRequestMsg }
     * 
     */
    public QueryGroupListBySubscriberRequestMsg createQueryGroupListBySubscriberRequestMsg() {
        return new QueryGroupListBySubscriberRequestMsg();
    }

    /**
     * Create an instance of {@link QueryGroupListBySubscriberRequest }
     * 
     */
    public QueryGroupListBySubscriberRequest createQueryGroupListBySubscriberRequest() {
        return new QueryGroupListBySubscriberRequest();
    }

    /**
     * Create an instance of {@link BatchScatteredSubActivationResultMsg }
     * 
     */
    public BatchScatteredSubActivationResultMsg createBatchScatteredSubActivationResultMsg() {
        return new BatchScatteredSubActivationResultMsg();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitResultMsg }
     * 
     */
    public ChangeAcctCreditLimitResultMsg createChangeAcctCreditLimitResultMsg() {
        return new ChangeAcctCreditLimitResultMsg();
    }

    /**
     * Create an instance of {@link BatchDelGroupMemberResultMsg }
     * 
     */
    public BatchDelGroupMemberResultMsg createBatchDelGroupMemberResultMsg() {
        return new BatchDelGroupMemberResultMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberInfoResultMsg }
     * 
     */
    public ChangeGroupMemberInfoResultMsg createChangeGroupMemberInfoResultMsg() {
        return new ChangeGroupMemberInfoResultMsg();
    }

    /**
     * Create an instance of {@link ChangeCustDFTAcctResultMsg }
     * 
     */
    public ChangeCustDFTAcctResultMsg createChangeCustDFTAcctResultMsg() {
        return new ChangeCustDFTAcctResultMsg();
    }

    /**
     * Create an instance of {@link BatchAdjustmentRequestMsg }
     * 
     */
    public BatchAdjustmentRequestMsg createBatchAdjustmentRequestMsg() {
        return new BatchAdjustmentRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeMemberStatusResultMsg }
     * 
     */
    public ChangeMemberStatusResultMsg createChangeMemberStatusResultMsg() {
        return new ChangeMemberStatusResultMsg();
    }

    /**
     * Create an instance of {@link QueryGroupMemberListResultMsg }
     * 
     */
    public QueryGroupMemberListResultMsg createQueryGroupMemberListResultMsg() {
        return new QueryGroupMemberListResultMsg();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupMemberRequestMsg }
     * 
     */
    public DeleteSuperGroupMemberRequestMsg createDeleteSuperGroupMemberRequestMsg() {
        return new DeleteSuperGroupMemberRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeSubOfferingRequestMsg }
     * 
     */
    public BatchChangeSubOfferingRequestMsg createBatchChangeSubOfferingRequestMsg() {
        return new BatchChangeSubOfferingRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeGroupMemOffNetNumberResultMsg }
     * 
     */
    public ChangeGroupMemOffNetNumberResultMsg createChangeGroupMemOffNetNumberResultMsg() {
        return new ChangeGroupMemOffNetNumberResultMsg();
    }

    /**
     * Create an instance of {@link BatchChangeInitCreditLimitRequestMsg }
     * 
     */
    public BatchChangeInitCreditLimitRequestMsg createBatchChangeInitCreditLimitRequestMsg() {
        return new BatchChangeInitCreditLimitRequestMsg();
    }

    /**
     * Create an instance of {@link BatchChangeInitCreditLimitRequest }
     * 
     */
    public BatchChangeInitCreditLimitRequest createBatchChangeInitCreditLimitRequest() {
        return new BatchChangeInitCreditLimitRequest();
    }

    /**
     * Create an instance of {@link FeeDeductionResultMsg }
     * 
     */
    public FeeDeductionResultMsg createFeeDeductionResultMsg() {
        return new FeeDeductionResultMsg();
    }

    /**
     * Create an instance of {@link QueryAppendSubIdentityRequestMsg }
     * 
     */
    public QueryAppendSubIdentityRequestMsg createQueryAppendSubIdentityRequestMsg() {
        return new QueryAppendSubIdentityRequestMsg();
    }

    /**
     * Create an instance of {@link QueryAppendSubIdentityRequest }
     * 
     */
    public QueryAppendSubIdentityRequest createQueryAppendSubIdentityRequest() {
        return new QueryAppendSubIdentityRequest();
    }

    /**
     * Create an instance of {@link ChangeSubGrpPwdRequestMsg }
     * 
     */
    public ChangeSubGrpPwdRequestMsg createChangeSubGrpPwdRequestMsg() {
        return new ChangeSubGrpPwdRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeSubGrpPwdRequest }
     * 
     */
    public ChangeSubGrpPwdRequest createChangeSubGrpPwdRequest() {
        return new ChangeSubGrpPwdRequest();
    }

    /**
     * Create an instance of {@link BatchAddGroupMemberRequestMsg }
     * 
     */
    public BatchAddGroupMemberRequestMsg createBatchAddGroupMemberRequestMsg() {
        return new BatchAddGroupMemberRequestMsg();
    }

    /**
     * Create an instance of {@link BatchAddGroupMemberRequest }
     * 
     */
    public BatchAddGroupMemberRequest createBatchAddGroupMemberRequest() {
        return new BatchAddGroupMemberRequest();
    }

    /**
     * Create an instance of {@link QueryGrpCallScreenNoRequestMsg }
     * 
     */
    public QueryGrpCallScreenNoRequestMsg createQueryGrpCallScreenNoRequestMsg() {
        return new QueryGrpCallScreenNoRequestMsg();
    }

    /**
     * Create an instance of {@link QueryGrpCallScreenNoRequest }
     * 
     */
    public QueryGrpCallScreenNoRequest createQueryGrpCallScreenNoRequest() {
        return new QueryGrpCallScreenNoRequest();
    }

    /**
     * Create an instance of {@link SubDeactivationResultMsg }
     * 
     */
    public SubDeactivationResultMsg createSubDeactivationResultMsg() {
        return new SubDeactivationResultMsg();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackResultMsg }
     * 
     */
    public FeeDeductionRollBackResultMsg createFeeDeductionRollBackResultMsg() {
        return new FeeDeductionRollBackResultMsg();
    }

    /**
     * Create an instance of {@link CreateGroupResultMsg }
     * 
     */
    public CreateGroupResultMsg createCreateGroupResultMsg() {
        return new CreateGroupResultMsg();
    }

    /**
     * Create an instance of {@link DelStatementRequestMsg }
     * 
     */
    public DelStatementRequestMsg createDelStatementRequestMsg() {
        return new DelStatementRequestMsg();
    }

    /**
     * Create an instance of {@link DelStatementRequest }
     * 
     */
    public DelStatementRequest createDelStatementRequest() {
        return new DelStatementRequest();
    }

    /**
     * Create an instance of {@link QueryRscRelationRequestMsg }
     * 
     */
    public QueryRscRelationRequestMsg createQueryRscRelationRequestMsg() {
        return new QueryRscRelationRequestMsg();
    }

    /**
     * Create an instance of {@link QueryOfferingRentFailedFeeResultMsg }
     * 
     */
    public QueryOfferingRentFailedFeeResultMsg createQueryOfferingRentFailedFeeResultMsg() {
        return new QueryOfferingRentFailedFeeResultMsg();
    }

    /**
     * Create an instance of {@link DelStatementResultMsg }
     * 
     */
    public DelStatementResultMsg createDelStatementResultMsg() {
        return new DelStatementResultMsg();
    }

    /**
     * Create an instance of {@link ChangeSubGrpPwdResultMsg }
     * 
     */
    public ChangeSubGrpPwdResultMsg createChangeSubGrpPwdResultMsg() {
        return new ChangeSubGrpPwdResultMsg();
    }

    /**
     * Create an instance of {@link ChangeTaxExemptionRequestMsg }
     * 
     */
    public ChangeTaxExemptionRequestMsg createChangeTaxExemptionRequestMsg() {
        return new ChangeTaxExemptionRequestMsg();
    }

    /**
     * Create an instance of {@link BatchCancelPreDeactivationRequest }
     * 
     */
    public BatchCancelPreDeactivationRequest createBatchCancelPreDeactivationRequest() {
        return new BatchCancelPreDeactivationRequest();
    }

    /**
     * Create an instance of {@link FphCaller }
     * 
     */
    public FphCaller createFphCaller() {
        return new FphCaller();
    }

    /**
     * Create an instance of {@link ActivateProductRequest.Product }
     * 
     */
    public ActivateProductRequest.Product createActivateProductRequestProduct() {
        return new ActivateProductRequest.Product();
    }

    /**
     * Create an instance of {@link ActivateProductRequest.OfferingOwner.AcctAccessCode }
     * 
     */
    public ActivateProductRequest.OfferingOwner.AcctAccessCode createActivateProductRequestOfferingOwnerAcctAccessCode() {
        return new ActivateProductRequest.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeTaxExemptionRequest.AddExemption }
     * 
     */
    public ChangeTaxExemptionRequest.AddExemption createChangeTaxExemptionRequestAddExemption() {
        return new ChangeTaxExemptionRequest.AddExemption();
    }

    /**
     * Create an instance of {@link ChangeTaxExemptionRequest.DelExemption }
     * 
     */
    public ChangeTaxExemptionRequest.DelExemption createChangeTaxExemptionRequestDelExemption() {
        return new ChangeTaxExemptionRequest.DelExemption();
    }

    /**
     * Create an instance of {@link QueryOfferingRentFailedFeeResult.OfferingRentFailedFee.OfferingOwner }
     * 
     */
    public QueryOfferingRentFailedFeeResult.OfferingRentFailedFee.OfferingOwner createQueryOfferingRentFailedFeeResultOfferingRentFailedFeeOfferingOwner() {
        return new QueryOfferingRentFailedFeeResult.OfferingRentFailedFee.OfferingOwner();
    }

    /**
     * Create an instance of {@link QueryRscRelationRequest.PayObj }
     * 
     */
    public QueryRscRelationRequest.PayObj createQueryRscRelationRequestPayObj() {
        return new QueryRscRelationRequest.PayObj();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackResult.BalanceRollBack }
     * 
     */
    public FeeDeductionRollBackResult.BalanceRollBack createFeeDeductionRollBackResultBalanceRollBack() {
        return new FeeDeductionRollBackResult.BalanceRollBack();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackResult.FreeUnitRollBack }
     * 
     */
    public FeeDeductionRollBackResult.FreeUnitRollBack createFeeDeductionRollBackResultFreeUnitRollBack() {
        return new FeeDeductionRollBackResult.FreeUnitRollBack();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackResult.CreditLimitChangeList }
     * 
     */
    public FeeDeductionRollBackResult.CreditLimitChangeList createFeeDeductionRollBackResultCreditLimitChangeList() {
        return new FeeDeductionRollBackResult.CreditLimitChangeList();
    }

    /**
     * Create an instance of {@link SubDeactivationResult.AcctBalance.AmountList }
     * 
     */
    public SubDeactivationResult.AcctBalance.AmountList createSubDeactivationResultAcctBalanceAmountList() {
        return new SubDeactivationResult.AcctBalance.AmountList();
    }

    /**
     * Create an instance of {@link FeeDeductionResult.AcctBalanceChangeList }
     * 
     */
    public FeeDeductionResult.AcctBalanceChangeList createFeeDeductionResultAcctBalanceChangeList() {
        return new FeeDeductionResult.AcctBalanceChangeList();
    }

    /**
     * Create an instance of {@link FeeDeductionResult.FreeUnitChangeList }
     * 
     */
    public FeeDeductionResult.FreeUnitChangeList createFeeDeductionResultFreeUnitChangeList() {
        return new FeeDeductionResult.FreeUnitChangeList();
    }

    /**
     * Create an instance of {@link FeeDeductionResult.CreditLimitChangeList }
     * 
     */
    public FeeDeductionResult.CreditLimitChangeList createFeeDeductionResultCreditLimitChangeList() {
        return new FeeDeductionResult.CreditLimitChangeList();
    }

    /**
     * Create an instance of {@link BatchChangeSubOfferingRequest.PrimaryOffering }
     * 
     */
    public BatchChangeSubOfferingRequest.PrimaryOffering createBatchChangeSubOfferingRequestPrimaryOffering() {
        return new BatchChangeSubOfferingRequest.PrimaryOffering();
    }

    /**
     * Create an instance of {@link BatchChangeSubOfferingRequest.SupplementaryOffering.AddOffering }
     * 
     */
    public BatchChangeSubOfferingRequest.SupplementaryOffering.AddOffering createBatchChangeSubOfferingRequestSupplementaryOfferingAddOffering() {
        return new BatchChangeSubOfferingRequest.SupplementaryOffering.AddOffering();
    }

    /**
     * Create an instance of {@link BatchChangeSubOfferingRequest.SupplementaryOffering.DelOffering }
     * 
     */
    public BatchChangeSubOfferingRequest.SupplementaryOffering.DelOffering createBatchChangeSubOfferingRequestSupplementaryOfferingDelOffering() {
        return new BatchChangeSubOfferingRequest.SupplementaryOffering.DelOffering();
    }

    /**
     * Create an instance of {@link BatchChangeSubOfferingRequest.SupplementaryOffering.ModifyOffering }
     * 
     */
    public BatchChangeSubOfferingRequest.SupplementaryOffering.ModifyOffering createBatchChangeSubOfferingRequestSupplementaryOfferingModifyOffering() {
        return new BatchChangeSubOfferingRequest.SupplementaryOffering.ModifyOffering();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupMemberRequest.SuperGroupAccess }
     * 
     */
    public DeleteSuperGroupMemberRequest.SuperGroupAccess createDeleteSuperGroupMemberRequestSuperGroupAccess() {
        return new DeleteSuperGroupMemberRequest.SuperGroupAccess();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupMemberRequest.Members }
     * 
     */
    public DeleteSuperGroupMemberRequest.Members createDeleteSuperGroupMemberRequestMembers() {
        return new DeleteSuperGroupMemberRequest.Members();
    }

    /**
     * Create an instance of {@link QueryGroupMemberListResult.GroupMemberList }
     * 
     */
    public QueryGroupMemberListResult.GroupMemberList createQueryGroupMemberListResultGroupMemberList() {
        return new QueryGroupMemberListResult.GroupMemberList();
    }

    /**
     * Create an instance of {@link BatchAdjustmentRequest.AdjustmentInfo }
     * 
     */
    public BatchAdjustmentRequest.AdjustmentInfo createBatchAdjustmentRequestAdjustmentInfo() {
        return new BatchAdjustmentRequest.AdjustmentInfo();
    }

    /**
     * Create an instance of {@link BatchAdjustmentRequest.FreeUnitAdjustmentInfo }
     * 
     */
    public BatchAdjustmentRequest.FreeUnitAdjustmentInfo createBatchAdjustmentRequestFreeUnitAdjustmentInfo() {
        return new BatchAdjustmentRequest.FreeUnitAdjustmentInfo();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitResult.CreditLimit }
     * 
     */
    public ChangeAcctCreditLimitResult.CreditLimit createChangeAcctCreditLimitResultCreditLimit() {
        return new ChangeAcctCreditLimitResult.CreditLimit();
    }

    /**
     * Create an instance of {@link ChangeProductStatusRequest.OfferingInst.ProductInst }
     * 
     */
    public ChangeProductStatusRequest.OfferingInst.ProductInst createChangeProductStatusRequestOfferingInstProductInst() {
        return new ChangeProductStatusRequest.OfferingInst.ProductInst();
    }

    /**
     * Create an instance of {@link ChangeProductStatusRequest.OfferingInst.OfferingOwner.AcctAccessCode }
     * 
     */
    public ChangeProductStatusRequest.OfferingInst.OfferingOwner.AcctAccessCode createChangeProductStatusRequestOfferingInstOfferingOwnerAcctAccessCode() {
        return new ChangeProductStatusRequest.OfferingInst.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QuerySubLifeCycleResult.LifeCycleStatus }
     * 
     */
    public QuerySubLifeCycleResult.LifeCycleStatus createQuerySubLifeCycleResultLifeCycleStatus() {
        return new QuerySubLifeCycleResult.LifeCycleStatus();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest.NewOwnership }
     * 
     */
    public SupplementProfileRequest.NewOwnership createSupplementProfileRequestNewOwnership() {
        return new SupplementProfileRequest.NewOwnership();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest.RegisterCustomer }
     * 
     */
    public SupplementProfileRequest.RegisterCustomer createSupplementProfileRequestRegisterCustomer() {
        return new SupplementProfileRequest.RegisterCustomer();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest.UserCustomer }
     * 
     */
    public SupplementProfileRequest.UserCustomer createSupplementProfileRequestUserCustomer() {
        return new SupplementProfileRequest.UserCustomer();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest.AddressInfo }
     * 
     */
    public SupplementProfileRequest.AddressInfo createSupplementProfileRequestAddressInfo() {
        return new SupplementProfileRequest.AddressInfo();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest.Account.CreditLimit }
     * 
     */
    public SupplementProfileRequest.Account.CreditLimit createSupplementProfileRequestAccountCreditLimit() {
        return new SupplementProfileRequest.Account.CreditLimit();
    }

    /**
     * Create an instance of {@link SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo }
     * 
     */
    public SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo createSupplementProfileRequestAccountAutoPayChannelAutoPayChannelInfo() {
        return new SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitRequest.AcctAccessCode }
     * 
     */
    public ChangeAcctCreditLimitRequest.AcctAccessCode createChangeAcctCreditLimitRequestAcctAccessCode() {
        return new ChangeAcctCreditLimitRequest.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitRequest.AddAccountCredit }
     * 
     */
    public ChangeAcctCreditLimitRequest.AddAccountCredit createChangeAcctCreditLimitRequestAddAccountCredit() {
        return new ChangeAcctCreditLimitRequest.AddAccountCredit();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitRequest.DelAccountCredit }
     * 
     */
    public ChangeAcctCreditLimitRequest.DelAccountCredit createChangeAcctCreditLimitRequestDelAccountCredit() {
        return new ChangeAcctCreditLimitRequest.DelAccountCredit();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitRequest.AccountCredit.CommonCreditLimit }
     * 
     */
    public ChangeAcctCreditLimitRequest.AccountCredit.CommonCreditLimit createChangeAcctCreditLimitRequestAccountCreditCommonCreditLimit() {
        return new ChangeAcctCreditLimitRequest.AccountCredit.CommonCreditLimit();
    }

    /**
     * Create an instance of {@link ChangeAcctCreditLimitRequest.AccountCredit.TmpCreditLimit }
     * 
     */
    public ChangeAcctCreditLimitRequest.AccountCredit.TmpCreditLimit createChangeAcctCreditLimitRequestAccountCreditTmpCreditLimit() {
        return new ChangeAcctCreditLimitRequest.AccountCredit.TmpCreditLimit();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingRequest.PrimaryOffering }
     * 
     */
    public ChangeSubOfferingRequest.PrimaryOffering createChangeSubOfferingRequestPrimaryOffering() {
        return new ChangeSubOfferingRequest.PrimaryOffering();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingRequest.SupplementaryOffering.AddOffering }
     * 
     */
    public ChangeSubOfferingRequest.SupplementaryOffering.AddOffering createChangeSubOfferingRequestSupplementaryOfferingAddOffering() {
        return new ChangeSubOfferingRequest.SupplementaryOffering.AddOffering();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingRequest.SupplementaryOffering.DelOffering }
     * 
     */
    public ChangeSubOfferingRequest.SupplementaryOffering.DelOffering createChangeSubOfferingRequestSupplementaryOfferingDelOffering() {
        return new ChangeSubOfferingRequest.SupplementaryOffering.DelOffering();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingRequest.SupplementaryOffering.ModifyOffering }
     * 
     */
    public ChangeSubOfferingRequest.SupplementaryOffering.ModifyOffering createChangeSubOfferingRequestSupplementaryOfferingModifyOffering() {
        return new ChangeSubOfferingRequest.SupplementaryOffering.ModifyOffering();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner.AcctAccessCode }
     * 
     */
    public QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner.AcctAccessCode createQueryOfferingRentCycleResultOfferingRentCycleOfferingOwnerAcctAccessCode() {
        return new QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeGroupMemOffNetNumberRequest.GroupMemOffNetNumber }
     * 
     */
    public ChangeGroupMemOffNetNumberRequest.GroupMemOffNetNumber createChangeGroupMemOffNetNumberRequestGroupMemOffNetNumber() {
        return new ChangeGroupMemOffNetNumberRequest.GroupMemOffNetNumber();
    }

    /**
     * Create an instance of {@link QuerySubInforToMicroResult.Subscriber }
     * 
     */
    public QuerySubInforToMicroResult.Subscriber createQuerySubInforToMicroResultSubscriber() {
        return new QuerySubInforToMicroResult.Subscriber();
    }

    /**
     * Create an instance of {@link QueryGroupListBySubscriberResult.GroupList }
     * 
     */
    public QueryGroupListBySubscriberResult.GroupList createQueryGroupListBySubscriberResultGroupList() {
        return new QueryGroupListBySubscriberResult.GroupList();
    }

    /**
     * Create an instance of {@link FeeQuotationResult.AcctBalanceChangeList }
     * 
     */
    public FeeQuotationResult.AcctBalanceChangeList createFeeQuotationResultAcctBalanceChangeList() {
        return new FeeQuotationResult.AcctBalanceChangeList();
    }

    /**
     * Create an instance of {@link FeeQuotationResult.FreeUnitChangeList }
     * 
     */
    public FeeQuotationResult.FreeUnitChangeList createFeeQuotationResultFreeUnitChangeList() {
        return new FeeQuotationResult.FreeUnitChangeList();
    }

    /**
     * Create an instance of {@link FeeQuotationResult.ChargeAmountList }
     * 
     */
    public FeeQuotationResult.ChargeAmountList createFeeQuotationResultChargeAmountList() {
        return new FeeQuotationResult.ChargeAmountList();
    }

    /**
     * Create an instance of {@link FeeQuotationResult.CreditLimitChangeList }
     * 
     */
    public FeeQuotationResult.CreditLimitChangeList createFeeQuotationResultCreditLimitChangeList() {
        return new FeeQuotationResult.CreditLimitChangeList();
    }

    /**
     * Create an instance of {@link FeeQuotationResult.ConsumptionLimitChangeList }
     * 
     */
    public FeeQuotationResult.ConsumptionLimitChangeList createFeeQuotationResultConsumptionLimitChangeList() {
        return new FeeQuotationResult.ConsumptionLimitChangeList();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyResult.OfferingInst.OfferingInstProperty }
     * 
     */
    public QueryOfferingInstPropertyResult.OfferingInst.OfferingInstProperty createQueryOfferingInstPropertyResultOfferingInstOfferingInstProperty() {
        return new QueryOfferingInstPropertyResult.OfferingInst.OfferingInstProperty();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyResult.OfferingInst.ProductInst.ProductInstProperty }
     * 
     */
    public QueryOfferingInstPropertyResult.OfferingInst.ProductInst.ProductInstProperty createQueryOfferingInstPropertyResultOfferingInstProductInstProductInstProperty() {
        return new QueryOfferingInstPropertyResult.OfferingInst.ProductInst.ProductInstProperty();
    }

    /**
     * Create an instance of {@link ChangeCustHierachyRequest.AcctAccessCode }
     * 
     */
    public ChangeCustHierachyRequest.AcctAccessCode createChangeCustHierachyRequestAcctAccessCode() {
        return new ChangeCustHierachyRequest.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeCustHierachyRequest.NewParentAcct }
     * 
     */
    public ChangeCustHierachyRequest.NewParentAcct createChangeCustHierachyRequestNewParentAcct() {
        return new ChangeCustHierachyRequest.NewParentAcct();
    }

    /**
     * Create an instance of {@link ApplyInstallmentRequest.ApplyObj }
     * 
     */
    public ApplyInstallmentRequest.ApplyObj createApplyInstallmentRequestApplyObj() {
        return new ApplyInstallmentRequest.ApplyObj();
    }

    /**
     * Create an instance of {@link ApplyInstallmentRequest.InatallmentPlan }
     * 
     */
    public ApplyInstallmentRequest.InatallmentPlan createApplyInstallmentRequestInatallmentPlan() {
        return new ApplyInstallmentRequest.InatallmentPlan();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PayRelation }
     * 
     */
    public BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PayRelation createBatchSwitchGroupMemberRequestPaymentRelationAddPayRelationPayRelation() {
        return new BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PayRelation();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo createBatchSwitchGroupMemberRequestPaymentRelationAddPayRelationPaymentLimitPaymentLimitInfo() {
        return new BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit.PaymentLimitInfo createBatchSwitchGroupMemberRequestPaymentRelationNewDFTAcctPaymentLimitPaymentLimitInfo() {
        return new BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link QueryOfferingRentFailedFeeRequest.OfferingInst.OfferingOwner }
     * 
     */
    public QueryOfferingRentFailedFeeRequest.OfferingInst.OfferingOwner createQueryOfferingRentFailedFeeRequestOfferingInstOfferingOwner() {
        return new QueryOfferingRentFailedFeeRequest.OfferingInst.OfferingOwner();
    }

    /**
     * Create an instance of {@link ChangeCustDFTAcctRequest.NewDFTAcct }
     * 
     */
    public ChangeCustDFTAcctRequest.NewDFTAcct createChangeCustDFTAcctRequestNewDFTAcct() {
        return new ChangeCustDFTAcctRequest.NewDFTAcct();
    }

    /**
     * Create an instance of {@link QueryGroupMemOffNetNumberResult.GroupMemOffNetNumber }
     * 
     */
    public QueryGroupMemOffNetNumberResult.GroupMemOffNetNumber createQueryGroupMemOffNetNumberResultGroupMemOffNetNumber() {
        return new QueryGroupMemOffNetNumberResult.GroupMemOffNetNumber();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingResult.ModifyOffering }
     * 
     */
    public ChangeGroupOfferingResult.ModifyOffering createChangeGroupOfferingResultModifyOffering() {
        return new ChangeGroupOfferingResult.ModifyOffering();
    }

    /**
     * Create an instance of {@link BatchChangePayRelationRequest.PayRelation.ModPayRelation }
     * 
     */
    public BatchChangePayRelationRequest.PayRelation.ModPayRelation createBatchChangePayRelationRequestPayRelationModPayRelation() {
        return new BatchChangePayRelationRequest.PayRelation.ModPayRelation();
    }

    /**
     * Create an instance of {@link BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit.Limit }
     * 
     */
    public BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit.Limit createBatchChangePayRelationRequestPayRelationAddPayRelationPayLimitLimit() {
        return new BatchChangePayRelationRequest.PayRelation.AddPayRelation.PayLimit.Limit();
    }

    /**
     * Create an instance of {@link CheckSubscribersGroupResult.UserGroup }
     * 
     */
    public CheckSubscribersGroupResult.UserGroup createCheckSubscribersGroupResultUserGroup() {
        return new CheckSubscribersGroupResult.UserGroup();
    }

    /**
     * Create an instance of {@link AcctDeactivationRequest.AcctAccessCode }
     * 
     */
    public AcctDeactivationRequest.AcctAccessCode createAcctDeactivationRequestAcctAccessCode() {
        return new AcctDeactivationRequest.AcctAccessCode();
    }

    /**
     * Create an instance of {@link AcctDeactivationRequest.AdditionalProperty }
     * 
     */
    public AcctDeactivationRequest.AdditionalProperty createAcctDeactivationRequestAdditionalProperty() {
        return new AcctDeactivationRequest.AdditionalProperty();
    }

    /**
     * Create an instance of {@link ApplyPrepaymentRequest.ApplyObj }
     * 
     */
    public ApplyPrepaymentRequest.ApplyObj createApplyPrepaymentRequestApplyObj() {
        return new ApplyPrepaymentRequest.ApplyObj();
    }

    /**
     * Create an instance of {@link ChangeSubStatusRequest.AdditionalProperty }
     * 
     */
    public ChangeSubStatusRequest.AdditionalProperty createChangeSubStatusRequestAdditionalProperty() {
        return new ChangeSubStatusRequest.AdditionalProperty();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingResult.ModifyOffering }
     * 
     */
    public ChangeSubOfferingResult.ModifyOffering createChangeSubOfferingResultModifyOffering() {
        return new ChangeSubOfferingResult.ModifyOffering();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendResult.NewInstallmentInfo.InatallmentDetail }
     * 
     */
    public ApplyInstallmentExtendResult.NewInstallmentInfo.InatallmentDetail createApplyInstallmentExtendResultNewInstallmentInfoInatallmentDetail() {
        return new ApplyInstallmentExtendResult.NewInstallmentInfo.InatallmentDetail();
    }

    /**
     * Create an instance of {@link FeeDeductionRequest.OperationInfo }
     * 
     */
    public FeeDeductionRequest.OperationInfo createFeeDeductionRequestOperationInfo() {
        return new FeeDeductionRequest.OperationInfo();
    }

    /**
     * Create an instance of {@link FeeDeductionRequest.DeductInfo }
     * 
     */
    public FeeDeductionRequest.DeductInfo createFeeDeductionRequestDeductInfo() {
        return new FeeDeductionRequest.DeductInfo();
    }

    /**
     * Create an instance of {@link FeeDeductionRequest.DeductObj.AcctAccessCode }
     * 
     */
    public FeeDeductionRequest.DeductObj.AcctAccessCode createFeeDeductionRequestDeductObjAcctAccessCode() {
        return new FeeDeductionRequest.DeductObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingRequest.PrimaryOffering }
     * 
     */
    public ChangeGroupOfferingRequest.PrimaryOffering createChangeGroupOfferingRequestPrimaryOffering() {
        return new ChangeGroupOfferingRequest.PrimaryOffering();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingRequest.AddOffering }
     * 
     */
    public ChangeGroupOfferingRequest.AddOffering createChangeGroupOfferingRequestAddOffering() {
        return new ChangeGroupOfferingRequest.AddOffering();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingRequest.DelOffering }
     * 
     */
    public ChangeGroupOfferingRequest.DelOffering createChangeGroupOfferingRequestDelOffering() {
        return new ChangeGroupOfferingRequest.DelOffering();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingRequest.ModifyOffering }
     * 
     */
    public ChangeGroupOfferingRequest.ModifyOffering createChangeGroupOfferingRequestModifyOffering() {
        return new ChangeGroupOfferingRequest.ModifyOffering();
    }

    /**
     * Create an instance of {@link QueryZoneMappingResult.ZoneDetail }
     * 
     */
    public QueryZoneMappingResult.ZoneDetail createQueryZoneMappingResultZoneDetail() {
        return new QueryZoneMappingResult.ZoneDetail();
    }

    /**
     * Create an instance of {@link ChangeSubGrpDFTAcctRequest.NewDFTAcct }
     * 
     */
    public ChangeSubGrpDFTAcctRequest.NewDFTAcct createChangeSubGrpDFTAcctRequestNewDFTAcct() {
        return new ChangeSubGrpDFTAcctRequest.NewDFTAcct();
    }

    /**
     * Create an instance of {@link QueryInstallmentResult.InatallmentDetail }
     * 
     */
    public QueryInstallmentResult.InatallmentDetail createQueryInstallmentResultInatallmentDetail() {
        return new QueryInstallmentResult.InatallmentDetail();
    }

    /**
     * Create an instance of {@link QueryExpireSubToMicroResult.ExpireSubscriber }
     * 
     */
    public QueryExpireSubToMicroResult.ExpireSubscriber createQueryExpireSubToMicroResultExpireSubscriber() {
        return new QueryExpireSubToMicroResult.ExpireSubscriber();
    }

    /**
     * Create an instance of {@link QueryExpireSubToMicroResult.PagingInfo }
     * 
     */
    public QueryExpireSubToMicroResult.PagingInfo createQueryExpireSubToMicroResultPagingInfo() {
        return new QueryExpireSubToMicroResult.PagingInfo();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.RegisterCustomer }
     * 
     */
    public CreateSubscriberRequest.RegisterCustomer createCreateSubscriberRequestRegisterCustomer() {
        return new CreateSubscriberRequest.RegisterCustomer();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.UserCustomer }
     * 
     */
    public CreateSubscriberRequest.UserCustomer createCreateSubscriberRequestUserCustomer() {
        return new CreateSubscriberRequest.UserCustomer();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.Account }
     * 
     */
    public CreateSubscriberRequest.Account createCreateSubscriberRequestAccount() {
        return new CreateSubscriberRequest.Account();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.PrimaryOffering }
     * 
     */
    public CreateSubscriberRequest.PrimaryOffering createCreateSubscriberRequestPrimaryOffering() {
        return new CreateSubscriberRequest.PrimaryOffering();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.SupplementaryOffering }
     * 
     */
    public CreateSubscriberRequest.SupplementaryOffering createCreateSubscriberRequestSupplementaryOffering() {
        return new CreateSubscriberRequest.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.Subscriber.SubPaymentMode.AcctList }
     * 
     */
    public CreateSubscriberRequest.Subscriber.SubPaymentMode.AcctList createCreateSubscriberRequestSubscriberSubPaymentModeAcctList() {
        return new CreateSubscriberRequest.Subscriber.SubPaymentMode.AcctList();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.Subscriber.SubPaymentMode.PayRelation }
     * 
     */
    public CreateSubscriberRequest.Subscriber.SubPaymentMode.PayRelation createCreateSubscriberRequestSubscriberSubPaymentModePayRelation() {
        return new CreateSubscriberRequest.Subscriber.SubPaymentMode.PayRelation();
    }

    /**
     * Create an instance of {@link CreateSubscriberRequest.Subscriber.SubPaymentMode.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public CreateSubscriberRequest.Subscriber.SubPaymentMode.PaymentLimit.PaymentLimitInfo createCreateSubscriberRequestSubscriberSubPaymentModePaymentLimitPaymentLimitInfo() {
        return new CreateSubscriberRequest.Subscriber.SubPaymentMode.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoRequest.QueryObj.SubAccessCode }
     * 
     */
    public QueryCustomerInfoRequest.QueryObj.SubAccessCode createQueryCustomerInfoRequestQueryObjSubAccessCode() {
        return new QueryCustomerInfoRequest.QueryObj.SubAccessCode();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoRequest.QueryObj.AcctAccessCode }
     * 
     */
    public QueryCustomerInfoRequest.QueryObj.AcctAccessCode createQueryCustomerInfoRequestQueryObjAcctAccessCode() {
        return new QueryCustomerInfoRequest.QueryObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link SubDeactivationRequest.AdditionalProperty }
     * 
     */
    public SubDeactivationRequest.AdditionalProperty createSubDeactivationRequestAdditionalProperty() {
        return new SubDeactivationRequest.AdditionalProperty();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitResult.LimitUsageList.LimitParam }
     * 
     */
    public QueryConsumptionLimitResult.LimitUsageList.LimitParam createQueryConsumptionLimitResultLimitUsageListLimitParam() {
        return new QueryConsumptionLimitResult.LimitUsageList.LimitParam();
    }

    /**
     * Create an instance of {@link QueryZoneInfoResult.ZoneList }
     * 
     */
    public QueryZoneInfoResult.ZoneList createQueryZoneInfoResultZoneList() {
        return new QueryZoneInfoResult.ZoneList();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult.PaymentRelationList.PayRelation }
     * 
     */
    public QueryPaymentRelationResult.PaymentRelationList.PayRelation createQueryPaymentRelationResultPaymentRelationListPayRelation() {
        return new QueryPaymentRelationResult.PaymentRelationList.PayRelation();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo createQueryPaymentRelationResultPaymentRelationListPaymentLimitPaymentLimitInfo() {
        return new QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.SubAccessCode }
     * 
     */
    public ChangeRscRelationRequest.SubAccessCode createChangeRscRelationRequestSubAccessCode() {
        return new ChangeRscRelationRequest.SubAccessCode();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation.ModRelation.RelationDestIdentify }
     * 
     */
    public ChangeRscRelationRequest.RscRelation.ModRelation.RelationDestIdentify createChangeRscRelationRequestRscRelationModRelationRelationDestIdentify() {
        return new ChangeRscRelationRequest.RscRelation.ModRelation.RelationDestIdentify();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation.ModRelation.OldRelation }
     * 
     */
    public ChangeRscRelationRequest.RscRelation.ModRelation.OldRelation createChangeRscRelationRequestRscRelationModRelationOldRelation() {
        return new ChangeRscRelationRequest.RscRelation.ModRelation.OldRelation();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewDestIdentify }
     * 
     */
    public ChangeRscRelationRequest.RscRelation.ModRelation.NewDestIdentify createChangeRscRelationRequestRscRelationModRelationNewDestIdentify() {
        return new ChangeRscRelationRequest.RscRelation.ModRelation.NewDestIdentify();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation.ShareLimit }
     * 
     */
    public ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation.ShareLimit createChangeRscRelationRequestRscRelationModRelationNewRelationShareLimit() {
        return new ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation.ShareLimit();
    }

    /**
     * Create an instance of {@link ChangeRscRelationRequest.RscRelation.DelRelation.RelationDestIdentify }
     * 
     */
    public ChangeRscRelationRequest.RscRelation.DelRelation.RelationDestIdentify createChangeRscRelationRequestRscRelationDelRelationRelationDestIdentify() {
        return new ChangeRscRelationRequest.RscRelation.DelRelation.RelationDestIdentify();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingRequest.AcctAccessCode }
     * 
     */
    public ChangeAccountOfferingRequest.AcctAccessCode createChangeAccountOfferingRequestAcctAccessCode() {
        return new ChangeAccountOfferingRequest.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingRequest.AddOffering }
     * 
     */
    public ChangeAccountOfferingRequest.AddOffering createChangeAccountOfferingRequestAddOffering() {
        return new ChangeAccountOfferingRequest.AddOffering();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingRequest.DelOffering }
     * 
     */
    public ChangeAccountOfferingRequest.DelOffering createChangeAccountOfferingRequestDelOffering() {
        return new ChangeAccountOfferingRequest.DelOffering();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingRequest.ModifyOffering }
     * 
     */
    public ChangeAccountOfferingRequest.ModifyOffering createChangeAccountOfferingRequestModifyOffering() {
        return new ChangeAccountOfferingRequest.ModifyOffering();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingRequest.AdditionalProperty }
     * 
     */
    public ChangeAccountOfferingRequest.AdditionalProperty createChangeAccountOfferingRequestAdditionalProperty() {
        return new ChangeAccountOfferingRequest.AdditionalProperty();
    }

    /**
     * Create an instance of {@link QueryExpireSubToMicroRequest.TimePeriod }
     * 
     */
    public QueryExpireSubToMicroRequest.TimePeriod createQueryExpireSubToMicroRequestTimePeriod() {
        return new QueryExpireSubToMicroRequest.TimePeriod();
    }

    /**
     * Create an instance of {@link QueryExpireSubToMicroRequest.PagingInfo }
     * 
     */
    public QueryExpireSubToMicroRequest.PagingInfo createQueryExpireSubToMicroRequestPagingInfo() {
        return new QueryExpireSubToMicroRequest.PagingInfo();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycleRequest.Account.RootAccount }
     * 
     */
    public ChangeAcctBillCycleRequest.Account.RootAccount createChangeAcctBillCycleRequestAccountRootAccount() {
        return new ChangeAcctBillCycleRequest.Account.RootAccount();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycleRequest.Account.SubAccount }
     * 
     */
    public ChangeAcctBillCycleRequest.Account.SubAccount createChangeAcctBillCycleRequestAccountSubAccount() {
        return new ChangeAcctBillCycleRequest.Account.SubAccount();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Account.OfferingInst }
     * 
     */
    public QueryCustomerInfoResult.Account.OfferingInst createQueryCustomerInfoResultAccountOfferingInst() {
        return new QueryCustomerInfoResult.Account.OfferingInst();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Account.AcctInfo.UserCustomer }
     * 
     */
    public QueryCustomerInfoResult.Account.AcctInfo.UserCustomer createQueryCustomerInfoResultAccountAcctInfoUserCustomer() {
        return new QueryCustomerInfoResult.Account.AcctInfo.UserCustomer();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo }
     * 
     */
    public QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo createQueryCustomerInfoResultAccountAcctInfoAutoPayChannelAutoPayChannelInfo() {
        return new QueryCustomerInfoResult.Account.AcctInfo.AutoPayChannel.AutoPayChannelInfo();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.SubGroup.SupplementaryOffering }
     * 
     */
    public QueryCustomerInfoResult.SubGroup.SupplementaryOffering createQueryCustomerInfoResultSubGroupSupplementaryOffering() {
        return new QueryCustomerInfoResult.SubGroup.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.SubGroup.SubGroupInfo.UserCustomer }
     * 
     */
    public QueryCustomerInfoResult.SubGroup.SubGroupInfo.UserCustomer createQueryCustomerInfoResultSubGroupSubGroupInfoUserCustomer() {
        return new QueryCustomerInfoResult.SubGroup.SubGroupInfo.UserCustomer();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Subscriber.SupplementaryOffering }
     * 
     */
    public QueryCustomerInfoResult.Subscriber.SupplementaryOffering createQueryCustomerInfoResultSubscriberSupplementaryOffering() {
        return new QueryCustomerInfoResult.Subscriber.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Subscriber.SubscriberInfo.UserCustomer }
     * 
     */
    public QueryCustomerInfoResult.Subscriber.SubscriberInfo.UserCustomer createQueryCustomerInfoResultSubscriberSubscriberInfoUserCustomer() {
        return new QueryCustomerInfoResult.Subscriber.SubscriberInfo.UserCustomer();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResult.Customer.OfferingInst }
     * 
     */
    public QueryCustomerInfoResult.Customer.OfferingInst createQueryCustomerInfoResultCustomerOfferingInst() {
        return new QueryCustomerInfoResult.Customer.OfferingInst();
    }

    /**
     * Create an instance of {@link QuerySubInforToMicroRequest.AccessInfo }
     * 
     */
    public QuerySubInforToMicroRequest.AccessInfo createQuerySubInforToMicroRequestAccessInfo() {
        return new QuerySubInforToMicroRequest.AccessInfo();
    }

    /**
     * Create an instance of {@link QueryPaymentLimitUsageResult.LimitUsageList }
     * 
     */
    public QueryPaymentLimitUsageResult.LimitUsageList createQueryPaymentLimitUsageResultLimitUsageList() {
        return new QueryPaymentLimitUsageResult.LimitUsageList();
    }

    /**
     * Create an instance of {@link QueryConsumptionLimitRequest.QueryObj }
     * 
     */
    public QueryConsumptionLimitRequest.QueryObj createQueryConsumptionLimitRequestQueryObj() {
        return new QueryConsumptionLimitRequest.QueryObj();
    }

    /**
     * Create an instance of {@link SubActivationRequest.SubBasicInfo }
     * 
     */
    public SubActivationRequest.SubBasicInfo createSubActivationRequestSubBasicInfo() {
        return new SubActivationRequest.SubBasicInfo();
    }

    /**
     * Create an instance of {@link SubActivationRequest.OfferingInst.OfferingKey }
     * 
     */
    public SubActivationRequest.OfferingInst.OfferingKey createSubActivationRequestOfferingInstOfferingKey() {
        return new SubActivationRequest.OfferingInst.OfferingKey();
    }

    /**
     * Create an instance of {@link QueryAccumulationUsageResult.AccmUsageList }
     * 
     */
    public QueryAccumulationUsageResult.AccmUsageList createQueryAccumulationUsageResultAccmUsageList() {
        return new QueryAccumulationUsageResult.AccmUsageList();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement.FeeAmount }
     * 
     */
    public FeeQuotationRequest.ChargeElement.FeeAmount createFeeQuotationRequestChargeElementFeeAmount() {
        return new FeeQuotationRequest.ChargeElement.FeeAmount();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement.OperationFee }
     * 
     */
    public FeeQuotationRequest.ChargeElement.OperationFee createFeeQuotationRequestChargeElementOperationFee() {
        return new FeeQuotationRequest.ChargeElement.OperationFee();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement.OfferingFee.DelOffering }
     * 
     */
    public FeeQuotationRequest.ChargeElement.OfferingFee.DelOffering createFeeQuotationRequestChargeElementOfferingFeeDelOffering() {
        return new FeeQuotationRequest.ChargeElement.OfferingFee.DelOffering();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.OInstProperty }
     * 
     */
    public FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.OInstProperty createFeeQuotationRequestChargeElementOfferingFeeAddOfferingOInstProperty() {
        return new FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.OInstProperty();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst.PInstProperty }
     * 
     */
    public FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst.PInstProperty createFeeQuotationRequestChargeElementOfferingFeeAddOfferingProductInstPInstProperty() {
        return new FeeQuotationRequest.ChargeElement.OfferingFee.AddOffering.ProductInst.PInstProperty();
    }

    /**
     * Create an instance of {@link FeeQuotationRequest.ChargeObj.AcctAccessCode }
     * 
     */
    public FeeQuotationRequest.ChargeObj.AcctAccessCode createFeeQuotationRequestChargeObjAcctAccessCode() {
        return new FeeQuotationRequest.ChargeObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.GroupMemberOffering }
     * 
     */
    public AddGroupMemberRequest.GroupMemberOffering createAddGroupMemberRequestGroupMemberOffering() {
        return new AddGroupMemberRequest.GroupMemberOffering();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.PaymentRelation.AddPayRelation.PayRelation }
     * 
     */
    public AddGroupMemberRequest.PaymentRelation.AddPayRelation.PayRelation createAddGroupMemberRequestPaymentRelationAddPayRelationPayRelation() {
        return new AddGroupMemberRequest.PaymentRelation.AddPayRelation.PayRelation();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public AddGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo createAddGroupMemberRequestPaymentRelationAddPayRelationPaymentLimitPaymentLimitInfo() {
        return new AddGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public AddGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit.PaymentLimitInfo createAddGroupMemberRequestPaymentRelationNewDFTAcctPaymentLimitPaymentLimitInfo() {
        return new AddGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link AddGroupMemberRequest.GroupMember.SubInfo }
     * 
     */
    public AddGroupMemberRequest.GroupMember.SubInfo createAddGroupMemberRequestGroupMemberSubInfo() {
        return new AddGroupMemberRequest.GroupMember.SubInfo();
    }

    /**
     * Create an instance of {@link ApplyInstallmentResult.InatallmentDetail }
     * 
     */
    public ApplyInstallmentResult.InatallmentDetail createApplyInstallmentResultInatallmentDetail() {
        return new ApplyInstallmentResult.InatallmentDetail();
    }

    /**
     * Create an instance of {@link QueryGrpCallScreenNoResult.CallScreenNoInfo }
     * 
     */
    public QueryGrpCallScreenNoResult.CallScreenNoInfo createQueryGrpCallScreenNoResultCallScreenNoInfo() {
        return new QueryGrpCallScreenNoResult.CallScreenNoInfo();
    }

    /**
     * Create an instance of {@link DeleteSuperGroupRequest.SuperGroupAccess }
     * 
     */
    public DeleteSuperGroupRequest.SuperGroupAccess createDeleteSuperGroupRequestSuperGroupAccess() {
        return new DeleteSuperGroupRequest.SuperGroupAccess();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendRequest.ApplyObj }
     * 
     */
    public ApplyInstallmentExtendRequest.ApplyObj createApplyInstallmentExtendRequestApplyObj() {
        return new ApplyInstallmentExtendRequest.ApplyObj();
    }

    /**
     * Create an instance of {@link ApplyInstallmentExtendRequest.NewInstallment.InatallmentPlan }
     * 
     */
    public ApplyInstallmentExtendRequest.NewInstallment.InatallmentPlan createApplyInstallmentExtendRequestNewInstallmentInatallmentPlan() {
        return new ApplyInstallmentExtendRequest.NewInstallment.InatallmentPlan();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberOfferingResult.ModifyOffering }
     * 
     */
    public ChangeGroupMemberOfferingResult.ModifyOffering createChangeGroupMemberOfferingResultModifyOffering() {
        return new ChangeGroupMemberOfferingResult.ModifyOffering();
    }

    /**
     * Create an instance of {@link ChangeOfferingStatusRequest.OfferingInst.OfferingOwner.AcctAccessCode }
     * 
     */
    public ChangeOfferingStatusRequest.OfferingInst.OfferingOwner.AcctAccessCode createChangeOfferingStatusRequestOfferingInstOfferingOwnerAcctAccessCode() {
        return new ChangeOfferingStatusRequest.OfferingInst.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ManageGrpCallScreenNoRequest.CallScreenNoInfo }
     * 
     */
    public ManageGrpCallScreenNoRequest.CallScreenNoInfo createManageGrpCallScreenNoRequestCallScreenNoInfo() {
        return new ManageGrpCallScreenNoRequest.CallScreenNoInfo();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoRequest.AcctAccessCode }
     * 
     */
    public ChangeAcctInfoRequest.AcctAccessCode createChangeAcctInfoRequestAcctAccessCode() {
        return new ChangeAcctInfoRequest.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoRequest.AcctPayMethod.AddAutoPayChannel }
     * 
     */
    public ChangeAcctInfoRequest.AcctPayMethod.AddAutoPayChannel createChangeAcctInfoRequestAcctPayMethodAddAutoPayChannel() {
        return new ChangeAcctInfoRequest.AcctPayMethod.AddAutoPayChannel();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoRequest.AcctPayMethod.DelAutoPayChannel }
     * 
     */
    public ChangeAcctInfoRequest.AcctPayMethod.DelAutoPayChannel createChangeAcctInfoRequestAcctPayMethodDelAutoPayChannel() {
        return new ChangeAcctInfoRequest.AcctPayMethod.DelAutoPayChannel();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationRequest.PayAccount }
     * 
     */
    public QueryPaymentRelationRequest.PayAccount createQueryPaymentRelationRequestPayAccount() {
        return new QueryPaymentRelationRequest.PayAccount();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationRequest.PaymentObj }
     * 
     */
    public QueryPaymentRelationRequest.PaymentObj createQueryPaymentRelationRequestPaymentObj() {
        return new QueryPaymentRelationRequest.PaymentObj();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeResult.ModifyOffering }
     * 
     */
    public ChangeSubPaymentModeResult.ModifyOffering createChangeSubPaymentModeResultModifyOffering() {
        return new ChangeSubPaymentModeResult.ModifyOffering();
    }

    /**
     * Create an instance of {@link ManAppendSubIdentityRequest.AppendSubIdentityList }
     * 
     */
    public ManAppendSubIdentityRequest.AppendSubIdentityList createManAppendSubIdentityRequestAppendSubIdentityList() {
        return new ManAppendSubIdentityRequest.AppendSubIdentityList();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyRequest.ProductInst.AddProperty }
     * 
     */
    public BatchChangeOfferingPropertyRequest.ProductInst.AddProperty createBatchChangeOfferingPropertyRequestProductInstAddProperty() {
        return new BatchChangeOfferingPropertyRequest.ProductInst.AddProperty();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyRequest.ProductInst.DelProperty }
     * 
     */
    public BatchChangeOfferingPropertyRequest.ProductInst.DelProperty createBatchChangeOfferingPropertyRequestProductInstDelProperty() {
        return new BatchChangeOfferingPropertyRequest.ProductInst.DelProperty();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyRequest.OfferingInstProperty.AddProperty }
     * 
     */
    public BatchChangeOfferingPropertyRequest.OfferingInstProperty.AddProperty createBatchChangeOfferingPropertyRequestOfferingInstPropertyAddProperty() {
        return new BatchChangeOfferingPropertyRequest.OfferingInstProperty.AddProperty();
    }

    /**
     * Create an instance of {@link BatchChangeOfferingPropertyRequest.OfferingInstProperty.DelProperty }
     * 
     */
    public BatchChangeOfferingPropertyRequest.OfferingInstProperty.DelProperty createBatchChangeOfferingPropertyRequestOfferingInstPropertyDelProperty() {
        return new BatchChangeOfferingPropertyRequest.OfferingInstProperty.DelProperty();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingResult.ModifyOffering }
     * 
     */
    public ChangeCustOfferingResult.ModifyOffering createChangeCustOfferingResultModifyOffering() {
        return new ChangeCustOfferingResult.ModifyOffering();
    }

    /**
     * Create an instance of {@link QueryRebateResult.Rebate }
     * 
     */
    public QueryRebateResult.Rebate createQueryRebateResultRebate() {
        return new QueryRebateResult.Rebate();
    }

    /**
     * Create an instance of {@link QueryGroupOffNetNumberResult.GroupOffNetNumber }
     * 
     */
    public QueryGroupOffNetNumberResult.GroupOffNetNumber createQueryGroupOffNetNumberResultGroupOffNetNumber() {
        return new QueryGroupOffNetNumberResult.GroupOffNetNumber();
    }

    /**
     * Create an instance of {@link QueryAppendSubIdentityResult.SubIdentityList }
     * 
     */
    public QueryAppendSubIdentityResult.SubIdentityList createQueryAppendSubIdentityResultSubIdentityList() {
        return new QueryAppendSubIdentityResult.SubIdentityList();
    }

    /**
     * Create an instance of {@link ActivateOfferingRequest.OfferingInst }
     * 
     */
    public ActivateOfferingRequest.OfferingInst createActivateOfferingRequestOfferingInst() {
        return new ActivateOfferingRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link ActivateOfferingRequest.OfferingOwner.AcctAccessCode }
     * 
     */
    public ActivateOfferingRequest.OfferingOwner.AcctAccessCode createActivateOfferingRequestOfferingOwnerAcctAccessCode() {
        return new ActivateOfferingRequest.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberInfoRequest.GroupMember }
     * 
     */
    public ChangeGroupMemberInfoRequest.GroupMember createChangeGroupMemberInfoRequestGroupMember() {
        return new ChangeGroupMemberInfoRequest.GroupMember();
    }

    /**
     * Create an instance of {@link ChangeSubIdentityRequest.AddSubIdentity }
     * 
     */
    public ChangeSubIdentityRequest.AddSubIdentity createChangeSubIdentityRequestAddSubIdentity() {
        return new ChangeSubIdentityRequest.AddSubIdentity();
    }

    /**
     * Create an instance of {@link ChangeSubIdentityRequest.DelSubIdentity }
     * 
     */
    public ChangeSubIdentityRequest.DelSubIdentity createChangeSubIdentityRequestDelSubIdentity() {
        return new ChangeSubIdentityRequest.DelSubIdentity();
    }

    /**
     * Create an instance of {@link ChangeSubIdentityRequest.ModifySubIdentity }
     * 
     */
    public ChangeSubIdentityRequest.ModifySubIdentity createChangeSubIdentityRequestModifySubIdentity() {
        return new ChangeSubIdentityRequest.ModifySubIdentity();
    }

    /**
     * Create an instance of {@link BatchSubActivationRequest.SubBasicInfo }
     * 
     */
    public BatchSubActivationRequest.SubBasicInfo createBatchSubActivationRequestSubBasicInfo() {
        return new BatchSubActivationRequest.SubBasicInfo();
    }

    /**
     * Create an instance of {@link BatchSubActivationRequest.OfferingInst }
     * 
     */
    public BatchSubActivationRequest.OfferingInst createBatchSubActivationRequestOfferingInst() {
        return new BatchSubActivationRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link QueryAccumulationUsageRequest.QueryObj.AcctAccessCode }
     * 
     */
    public QueryAccumulationUsageRequest.QueryObj.AcctAccessCode createQueryAccumulationUsageRequestQueryObjAcctAccessCode() {
        return new QueryAccumulationUsageRequest.QueryObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link BatchDelGroupMemberRequest.DelGroupMemberOffering }
     * 
     */
    public BatchDelGroupMemberRequest.DelGroupMemberOffering createBatchDelGroupMemberRequestDelGroupMemberOffering() {
        return new BatchDelGroupMemberRequest.DelGroupMemberOffering();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.LimitObj }
     * 
     */
    public ChangeConsumptionLimitRequest.LimitObj createChangeConsumptionLimitRequestLimitObj() {
        return new ChangeConsumptionLimitRequest.LimitObj();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.DelLimit }
     * 
     */
    public ChangeConsumptionLimitRequest.DelLimit createChangeConsumptionLimitRequestDelLimit() {
        return new ChangeConsumptionLimitRequest.DelLimit();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.AddLimitParam }
     * 
     */
    public ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.AddLimitParam createChangeConsumptionLimitRequestModifyLimitLimitParamAddLimitParam() {
        return new ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.AddLimitParam();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.DelLimitParam }
     * 
     */
    public ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.DelLimitParam createChangeConsumptionLimitRequestModifyLimitLimitParamDelLimitParam() {
        return new ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.DelLimitParam();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.ModifyLimitParam }
     * 
     */
    public ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.ModifyLimitParam createChangeConsumptionLimitRequestModifyLimitLimitParamModifyLimitParam() {
        return new ChangeConsumptionLimitRequest.ModifyLimit.LimitParam.ModifyLimitParam();
    }

    /**
     * Create an instance of {@link ChangeConsumptionLimitRequest.AddLimit.LimitParam }
     * 
     */
    public ChangeConsumptionLimitRequest.AddLimit.LimitParam createChangeConsumptionLimitRequestAddLimitLimitParam() {
        return new ChangeConsumptionLimitRequest.AddLimit.LimitParam();
    }

    /**
     * Create an instance of {@link ChangeSubPwdResult.AcctBalanceChangeList }
     * 
     */
    public ChangeSubPwdResult.AcctBalanceChangeList createChangeSubPwdResultAcctBalanceChangeList() {
        return new ChangeSubPwdResult.AcctBalanceChangeList();
    }

    /**
     * Create an instance of {@link ChangeSubPwdResult.FreeUnitChangeList }
     * 
     */
    public ChangeSubPwdResult.FreeUnitChangeList createChangeSubPwdResultFreeUnitChangeList() {
        return new ChangeSubPwdResult.FreeUnitChangeList();
    }

    /**
     * Create an instance of {@link ChangeSubPwdResult.CreditLimitChangeList }
     * 
     */
    public ChangeSubPwdResult.CreditLimitChangeList createChangeSubPwdResultCreditLimitChangeList() {
        return new ChangeSubPwdResult.CreditLimitChangeList();
    }

    /**
     * Create an instance of {@link ChangeGroupOffNetNumberRequest.GroupOffNetNumber }
     * 
     */
    public ChangeGroupOffNetNumberRequest.GroupOffNetNumber createChangeGroupOffNetNumberRequestGroupOffNetNumber() {
        return new ChangeGroupOffNetNumberRequest.GroupOffNetNumber();
    }

    /**
     * Create an instance of {@link QuerySubInfoExToCubeRequest.AccessInfo }
     * 
     */
    public QuerySubInfoExToCubeRequest.AccessInfo createQuerySubInfoExToCubeRequestAccessInfo() {
        return new QuerySubInfoExToCubeRequest.AccessInfo();
    }

    /**
     * Create an instance of {@link QuerySubInfoExToCubeResult.Subscriber.AdditionalProperty }
     * 
     */
    public QuerySubInfoExToCubeResult.Subscriber.AdditionalProperty createQuerySubInfoExToCubeResultSubscriberAdditionalProperty() {
        return new QuerySubInfoExToCubeResult.Subscriber.AdditionalProperty();
    }

    /**
     * Create an instance of {@link QuerySubInfoExToCubeResult.Subscriber.PaymentLimitUsage }
     * 
     */
    public QuerySubInfoExToCubeResult.Subscriber.PaymentLimitUsage createQuerySubInfoExToCubeResultSubscriberPaymentLimitUsage() {
        return new QuerySubInfoExToCubeResult.Subscriber.PaymentLimitUsage();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingRequest.AddOffering }
     * 
     */
    public ChangeCustOfferingRequest.AddOffering createChangeCustOfferingRequestAddOffering() {
        return new ChangeCustOfferingRequest.AddOffering();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingRequest.DelOffering }
     * 
     */
    public ChangeCustOfferingRequest.DelOffering createChangeCustOfferingRequestDelOffering() {
        return new ChangeCustOfferingRequest.DelOffering();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingRequest.ModifyOffering }
     * 
     */
    public ChangeCustOfferingRequest.ModifyOffering createChangeCustOfferingRequestModifyOffering() {
        return new ChangeCustOfferingRequest.ModifyOffering();
    }

    /**
     * Create an instance of {@link ChangeCustOfferingRequest.AdditionalProperty }
     * 
     */
    public ChangeCustOfferingRequest.AdditionalProperty createChangeCustOfferingRequestAdditionalProperty() {
        return new ChangeCustOfferingRequest.AdditionalProperty();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentObj }
     * 
     */
    public ChangePayRelationRequest.PaymentObj createChangePayRelationRequestPaymentObj() {
        return new ChangePayRelationRequest.PaymentObj();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.DelPayRelation }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.DelPayRelation createChangePayRelationRequestPaymentRelationDelPayRelation() {
        return new ChangePayRelationRequest.PaymentRelation.DelPayRelation();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.ModPayRelation.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.ModPayRelation.PaymentLimit.PaymentLimitInfo createChangePayRelationRequestPaymentRelationModPayRelationPaymentLimitPaymentLimitInfo() {
        return new ChangePayRelationRequest.PaymentRelation.ModPayRelation.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.ModPayRelation.PayRelation.PaymentLimit }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.ModPayRelation.PayRelation.PaymentLimit createChangePayRelationRequestPaymentRelationModPayRelationPayRelationPaymentLimit() {
        return new ChangePayRelationRequest.PaymentRelation.ModPayRelation.PayRelation.PaymentLimit();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.AddPayRelation.PayRelation }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.AddPayRelation.PayRelation createChangePayRelationRequestPaymentRelationAddPayRelationPayRelation() {
        return new ChangePayRelationRequest.PaymentRelation.AddPayRelation.PayRelation();
    }

    /**
     * Create an instance of {@link ChangePayRelationRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public ChangePayRelationRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo createChangePayRelationRequestPaymentRelationAddPayRelationPaymentLimitPaymentLimitInfo() {
        return new ChangePayRelationRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link CreateAccountRequest.Account }
     * 
     */
    public CreateAccountRequest.Account createCreateAccountRequestAccount() {
        return new CreateAccountRequest.Account();
    }

    /**
     * Create an instance of {@link CreateAccountRequest.Offering }
     * 
     */
    public CreateAccountRequest.Offering createCreateAccountRequestOffering() {
        return new CreateAccountRequest.Offering();
    }

    /**
     * Create an instance of {@link JoinSuperGroupRequest.SuperGroupAccess }
     * 
     */
    public JoinSuperGroupRequest.SuperGroupAccess createJoinSuperGroupRequestSuperGroupAccess() {
        return new JoinSuperGroupRequest.SuperGroupAccess();
    }

    /**
     * Create an instance of {@link JoinSuperGroupRequest.Members }
     * 
     */
    public JoinSuperGroupRequest.Members createJoinSuperGroupRequestMembers() {
        return new JoinSuperGroupRequest.Members();
    }

    /**
     * Create an instance of {@link DelGroupMemberRequest.DelGroupMemberOffering }
     * 
     */
    public DelGroupMemberRequest.DelGroupMemberOffering createDelGroupMemberRequestDelGroupMemberOffering() {
        return new DelGroupMemberRequest.DelGroupMemberOffering();
    }

    /**
     * Create an instance of {@link DelGroupMemberRequest.PaymentRelation.NewDFTAcct }
     * 
     */
    public DelGroupMemberRequest.PaymentRelation.NewDFTAcct createDelGroupMemberRequestPaymentRelationNewDFTAcct() {
        return new DelGroupMemberRequest.PaymentRelation.NewDFTAcct();
    }

    /**
     * Create an instance of {@link DelGroupMemberRequest.PaymentRelation.DelPayRelation }
     * 
     */
    public DelGroupMemberRequest.PaymentRelation.DelPayRelation createDelGroupMemberRequestPaymentRelationDelPayRelation() {
        return new DelGroupMemberRequest.PaymentRelation.DelPayRelation();
    }

    /**
     * Create an instance of {@link QueryRscRelationResult.RscRelation.RelationDestIdentify }
     * 
     */
    public QueryRscRelationResult.RscRelation.RelationDestIdentify createQueryRscRelationResultRscRelationRelationDestIdentify() {
        return new QueryRscRelationResult.RscRelation.RelationDestIdentify();
    }

    /**
     * Create an instance of {@link QueryRscRelationResult.RscRelation.RelationDestCust }
     * 
     */
    public QueryRscRelationResult.RscRelation.RelationDestCust createQueryRscRelationResultRscRelationRelationDestCust() {
        return new QueryRscRelationResult.RscRelation.RelationDestCust();
    }

    /**
     * Create an instance of {@link QueryRscRelationResult.RscRelation.ShareLimit }
     * 
     */
    public QueryRscRelationResult.RscRelation.ShareLimit createQueryRscRelationResultRscRelationShareLimit() {
        return new QueryRscRelationResult.RscRelation.ShareLimit();
    }

    /**
     * Create an instance of {@link QueryRscRelationResult.RscRelation.NotifyRule }
     * 
     */
    public QueryRscRelationResult.RscRelation.NotifyRule createQueryRscRelationResultRscRelationNotifyRule() {
        return new QueryRscRelationResult.RscRelation.NotifyRule();
    }

    /**
     * Create an instance of {@link CustDeactivationRequest.AdditionalProperty }
     * 
     */
    public CustDeactivationRequest.AdditionalProperty createCustDeactivationRequestAdditionalProperty() {
        return new CustDeactivationRequest.AdditionalProperty();
    }

    /**
     * Create an instance of {@link ChangeAccountOfferingResult.ModifyOffering }
     * 
     */
    public ChangeAccountOfferingResult.ModifyOffering createChangeAccountOfferingResultModifyOffering() {
        return new ChangeAccountOfferingResult.ModifyOffering();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyRequest.OfferingInst.ProductInst }
     * 
     */
    public QueryOfferingInstPropertyRequest.OfferingInst.ProductInst createQueryOfferingInstPropertyRequestOfferingInstProductInst() {
        return new QueryOfferingInstPropertyRequest.OfferingInst.ProductInst();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyRequest.OfferingOwner.AcctAccessCode }
     * 
     */
    public QueryOfferingInstPropertyRequest.OfferingOwner.AcctAccessCode createQueryOfferingInstPropertyRequestOfferingOwnerAcctAccessCode() {
        return new QueryOfferingInstPropertyRequest.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link QueryDataPackageUsageResult.UsageList }
     * 
     */
    public QueryDataPackageUsageResult.UsageList createQueryDataPackageUsageResultUsageList() {
        return new QueryDataPackageUsageResult.UsageList();
    }

    /**
     * Create an instance of {@link ChangeCustInfoRequest.CustInfo }
     * 
     */
    public ChangeCustInfoRequest.CustInfo createChangeCustInfoRequestCustInfo() {
        return new ChangeCustInfoRequest.CustInfo();
    }

    /**
     * Create an instance of {@link QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner.AcctAccessCode }
     * 
     */
    public QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner.AcctAccessCode createQueryOfferingRentCycleRequestOfferingInstOfferingOwnerAcctAccessCode() {
        return new QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst.ProductInst.AddProperty }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst.ProductInst.AddProperty createChangeOfferingPropertyRequestOfferingInstProductInstAddProperty() {
        return new ChangeOfferingPropertyRequest.OfferingInst.ProductInst.AddProperty();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst.ProductInst.DelProperty }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst.ProductInst.DelProperty createChangeOfferingPropertyRequestOfferingInstProductInstDelProperty() {
        return new ChangeOfferingPropertyRequest.OfferingInst.ProductInst.DelProperty();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty.AddProperty }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty.AddProperty createChangeOfferingPropertyRequestOfferingInstOfferingInstPropertyAddProperty() {
        return new ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty.AddProperty();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty.DelProperty }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty.DelProperty createChangeOfferingPropertyRequestOfferingInstOfferingInstPropertyDelProperty() {
        return new ChangeOfferingPropertyRequest.OfferingInst.OfferingInstProperty.DelProperty();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyRequest.OfferingInst.OfferingOwner.AcctAccessCode }
     * 
     */
    public ChangeOfferingPropertyRequest.OfferingInst.OfferingOwner.AcctAccessCode createChangeOfferingPropertyRequestOfferingInstOfferingOwnerAcctAccessCode() {
        return new ChangeOfferingPropertyRequest.OfferingInst.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link CancelPreDeactivationRequest.ResumeStatus }
     * 
     */
    public CancelPreDeactivationRequest.ResumeStatus createCancelPreDeactivationRequestResumeStatus() {
        return new CancelPreDeactivationRequest.ResumeStatus();
    }

    /**
     * Create an instance of {@link ModifyStatementRequest.StatementScenario.AddStatementScenario }
     * 
     */
    public ModifyStatementRequest.StatementScenario.AddStatementScenario createModifyStatementRequestStatementScenarioAddStatementScenario() {
        return new ModifyStatementRequest.StatementScenario.AddStatementScenario();
    }

    /**
     * Create an instance of {@link ModifyStatementRequest.StatementScenario.DelStatementScenario }
     * 
     */
    public ModifyStatementRequest.StatementScenario.DelStatementScenario createModifyStatementRequestStatementScenarioDelStatementScenario() {
        return new ModifyStatementRequest.StatementScenario.DelStatementScenario();
    }

    /**
     * Create an instance of {@link CreateCustomerRequest.Customer }
     * 
     */
    public CreateCustomerRequest.Customer createCreateCustomerRequestCustomer() {
        return new CreateCustomerRequest.Customer();
    }

    /**
     * Create an instance of {@link CreateCustomerRequest.DFTAccount }
     * 
     */
    public CreateCustomerRequest.DFTAccount createCreateCustomerRequestDFTAccount() {
        return new CreateCustomerRequest.DFTAccount();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest.DFTPayRelation.DelPayRelation }
     * 
     */
    public ChangeSubDFTAcctRequest.DFTPayRelation.DelPayRelation createChangeSubDFTAcctRequestDFTPayRelationDelPayRelation() {
        return new ChangeSubDFTAcctRequest.DFTPayRelation.DelPayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest.DFTPayRelation.AddPayRelation }
     * 
     */
    public ChangeSubDFTAcctRequest.DFTPayRelation.AddPayRelation createChangeSubDFTAcctRequestDFTPayRelationAddPayRelation() {
        return new ChangeSubDFTAcctRequest.DFTPayRelation.AddPayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo createChangeSubDFTAcctRequestDFTPayRelationPaymentLimitPaymentLimitInfo() {
        return new ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct }
     * 
     */
    public ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct createChangeSubDFTAcctRequestSubDFTAccountOldDFTAcct() {
        return new ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct }
     * 
     */
    public ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct createChangeSubDFTAcctRequestSubDFTAccountNewDFTAcct() {
        return new ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct();
    }

    /**
     * Create an instance of {@link QueryRebateRequest.OfferingInst }
     * 
     */
    public QueryRebateRequest.OfferingInst createQueryRebateRequestOfferingInst() {
        return new QueryRebateRequest.OfferingInst();
    }

    /**
     * Create an instance of {@link QueryRebateRequest.OfferingOwner.AcctAccessCode }
     * 
     */
    public QueryRebateRequest.OfferingOwner.AcctAccessCode createQueryRebateRequestOfferingOwnerAcctAccessCode() {
        return new QueryRebateRequest.OfferingOwner.AcctAccessCode();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.OldOwnership }
     * 
     */
    public ChangeSubOwnershipRequest.OldOwnership createChangeSubOwnershipRequestOldOwnership() {
        return new ChangeSubOwnershipRequest.OldOwnership();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.RegisterCustomer }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.RegisterCustomer createChangeSubOwnershipRequestNewOwnershipRegisterCustomer() {
        return new ChangeSubOwnershipRequest.NewOwnership.RegisterCustomer();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.UserCustomer }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.UserCustomer createChangeSubOwnershipRequestNewOwnershipUserCustomer() {
        return new ChangeSubOwnershipRequest.NewOwnership.UserCustomer();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.Account }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.Account createChangeSubOwnershipRequestNewOwnershipAccount() {
        return new ChangeSubOwnershipRequest.NewOwnership.Account();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.PrimaryOffering }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.PrimaryOffering createChangeSubOwnershipRequestNewOwnershipPrimaryOffering() {
        return new ChangeSubOwnershipRequest.NewOwnership.PrimaryOffering();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering createChangeSubOwnershipRequestNewOwnershipSupplementaryOfferingShiftOffering() {
        return new ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering createChangeSubOwnershipRequestNewOwnershipSupplementaryOfferingDelOffering() {
        return new ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering createChangeSubOwnershipRequestNewOwnershipSupplementaryOfferingAddOffering() {
        return new ChangeSubOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.ShiftPayRelation }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.Subscriber.ShiftPayRelation createChangeSubOwnershipRequestNewOwnershipSubscriberShiftPayRelation() {
        return new ChangeSubOwnershipRequest.NewOwnership.Subscriber.ShiftPayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.AcctList }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.AcctList createChangeSubOwnershipRequestNewOwnershipSubscriberSubDFTAcctAcctList() {
        return new ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.AcctList();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PayRelation }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PayRelation createChangeSubOwnershipRequestNewOwnershipSubscriberSubDFTAcctPayRelation() {
        return new ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo createChangeSubOwnershipRequestNewOwnershipSubscriberSubDFTAcctPaymentLimitPaymentLimitInfo() {
        return new ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link CreateGroupRequest.RegisterCustomer }
     * 
     */
    public CreateGroupRequest.RegisterCustomer createCreateGroupRequestRegisterCustomer() {
        return new CreateGroupRequest.RegisterCustomer();
    }

    /**
     * Create an instance of {@link CreateGroupRequest.UserCustomer }
     * 
     */
    public CreateGroupRequest.UserCustomer createCreateGroupRequestUserCustomer() {
        return new CreateGroupRequest.UserCustomer();
    }

    /**
     * Create an instance of {@link CreateGroupRequest.Account }
     * 
     */
    public CreateGroupRequest.Account createCreateGroupRequestAccount() {
        return new CreateGroupRequest.Account();
    }

    /**
     * Create an instance of {@link CreateGroupRequest.Group }
     * 
     */
    public CreateGroupRequest.Group createCreateGroupRequestGroup() {
        return new CreateGroupRequest.Group();
    }

    /**
     * Create an instance of {@link CreateGroupRequest.SupplementaryOffering }
     * 
     */
    public CreateGroupRequest.SupplementaryOffering createCreateGroupRequestSupplementaryOffering() {
        return new CreateGroupRequest.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberOfferingRequest.AddOffering }
     * 
     */
    public ChangeGroupMemberOfferingRequest.AddOffering createChangeGroupMemberOfferingRequestAddOffering() {
        return new ChangeGroupMemberOfferingRequest.AddOffering();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberOfferingRequest.DelOffering }
     * 
     */
    public ChangeGroupMemberOfferingRequest.DelOffering createChangeGroupMemberOfferingRequestDelOffering() {
        return new ChangeGroupMemberOfferingRequest.DelOffering();
    }

    /**
     * Create an instance of {@link ChangeGroupMemberOfferingRequest.ModifyOffering }
     * 
     */
    public ChangeGroupMemberOfferingRequest.ModifyOffering createChangeGroupMemberOfferingRequestModifyOffering() {
        return new ChangeGroupMemberOfferingRequest.ModifyOffering();
    }

    /**
     * Create an instance of {@link QueryPaymentLimitUsageRequest.PayAccount }
     * 
     */
    public QueryPaymentLimitUsageRequest.PayAccount createQueryPaymentLimitUsageRequestPayAccount() {
        return new QueryPaymentLimitUsageRequest.PayAccount();
    }

    /**
     * Create an instance of {@link QueryPaymentLimitUsageRequest.PayObj }
     * 
     */
    public QueryPaymentLimitUsageRequest.PayObj createQueryPaymentLimitUsageRequestPayObj() {
        return new QueryPaymentLimitUsageRequest.PayObj();
    }

    /**
     * Create an instance of {@link ApplyPrepaymentResult.InatallmentDetail }
     * 
     */
    public ApplyPrepaymentResult.InatallmentDetail createApplyPrepaymentResultInatallmentDetail() {
        return new ApplyPrepaymentResult.InatallmentDetail();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.SupplementaryOffering.AddOffering }
     * 
     */
    public ChangeSubPaymentModeRequest.SupplementaryOffering.AddOffering createChangeSubPaymentModeRequestSupplementaryOfferingAddOffering() {
        return new ChangeSubPaymentModeRequest.SupplementaryOffering.AddOffering();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.SupplementaryOffering.DelOffering }
     * 
     */
    public ChangeSubPaymentModeRequest.SupplementaryOffering.DelOffering createChangeSubPaymentModeRequestSupplementaryOfferingDelOffering() {
        return new ChangeSubPaymentModeRequest.SupplementaryOffering.DelOffering();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.SupplementaryOffering.ModifyOffering }
     * 
     */
    public ChangeSubPaymentModeRequest.SupplementaryOffering.ModifyOffering createChangeSubPaymentModeRequestSupplementaryOfferingModifyOffering() {
        return new ChangeSubPaymentModeRequest.SupplementaryOffering.ModifyOffering();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.PrimaryOffering }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.PrimaryOffering createChangeSubPaymentModeRequestPaymentModeChangePrimaryOffering() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.PrimaryOffering();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.Account }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.Account createChangeSubPaymentModeRequestPaymentModeChangeAccount() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.Account();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.AddressInfo }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.AddressInfo createChangeSubPaymentModeRequestPaymentModeChangeAddressInfo() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.AddressInfo();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.DelPayRelation }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.DelPayRelation createChangeSubPaymentModeRequestPaymentModeChangeDFTPayRelationDelPayRelation() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.DelPayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.AddPayRelation }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.AddPayRelation createChangeSubPaymentModeRequestPaymentModeChangeDFTPayRelationAddPayRelation() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.AddPayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.PaymentLimit.PaymentLimitInfo }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.PaymentLimit.PaymentLimitInfo createChangeSubPaymentModeRequestPaymentModeChangeDFTPayRelationPaymentLimitPaymentLimitInfo() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.PaymentLimit.PaymentLimitInfo();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount.OldDFTAcct }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount.OldDFTAcct createChangeSubPaymentModeRequestPaymentModeChangeSubDFTAccountOldDFTAcct() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount.OldDFTAcct();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount.NewDFTAcct }
     * 
     */
    public ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount.NewDFTAcct createChangeSubPaymentModeRequestPaymentModeChangeSubDFTAccountNewDFTAcct() {
        return new ChangeSubPaymentModeRequest.PaymentModeChange.SubDFTAccount.NewDFTAcct();
    }

    /**
     * Create an instance of {@link ChangeRedFlagRequest.ChangeObj.AcctAccessCode }
     * 
     */
    public ChangeRedFlagRequest.ChangeObj.AcctAccessCode createChangeRedFlagRequestChangeObjAcctAccessCode() {
        return new ChangeRedFlagRequest.ChangeObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link BatchChangeAcctOfferingRequest.AcctOffering.AddOffering }
     * 
     */
    public BatchChangeAcctOfferingRequest.AcctOffering.AddOffering createBatchChangeAcctOfferingRequestAcctOfferingAddOffering() {
        return new BatchChangeAcctOfferingRequest.AcctOffering.AddOffering();
    }

    /**
     * Create an instance of {@link BatchChangeAcctOfferingRequest.AcctOffering.DelOffering }
     * 
     */
    public BatchChangeAcctOfferingRequest.AcctOffering.DelOffering createBatchChangeAcctOfferingRequestAcctOfferingDelOffering() {
        return new BatchChangeAcctOfferingRequest.AcctOffering.DelOffering();
    }

    /**
     * Create an instance of {@link BatchChangeAcctOfferingRequest.AcctOffering.ModifyOffering }
     * 
     */
    public BatchChangeAcctOfferingRequest.AcctOffering.ModifyOffering createBatchChangeAcctOfferingRequestAcctOfferingModifyOffering() {
        return new BatchChangeAcctOfferingRequest.AcctOffering.ModifyOffering();
    }

    /**
     * Create an instance of {@link QueryInstallmentRequest.QueryObj }
     * 
     */
    public QueryInstallmentRequest.QueryObj createQueryInstallmentRequestQueryObj() {
        return new QueryInstallmentRequest.QueryObj();
    }

    /**
     * Create an instance of {@link BatchScatteredSubActivationRequest.SubBasicInfo }
     * 
     */
    public BatchScatteredSubActivationRequest.SubBasicInfo createBatchScatteredSubActivationRequestSubBasicInfo() {
        return new BatchScatteredSubActivationRequest.SubBasicInfo();
    }

    /**
     * Create an instance of {@link ChangeSubInfoRequest.UserCustomer }
     * 
     */
    public ChangeSubInfoRequest.UserCustomer createChangeSubInfoRequestUserCustomer() {
        return new ChangeSubInfoRequest.UserCustomer();
    }

    /**
     * Create an instance of {@link ChangeCustNoticeSuppressRequest.SuppressSetting.AddSuppressSet }
     * 
     */
    public ChangeCustNoticeSuppressRequest.SuppressSetting.AddSuppressSet createChangeCustNoticeSuppressRequestSuppressSettingAddSuppressSet() {
        return new ChangeCustNoticeSuppressRequest.SuppressSetting.AddSuppressSet();
    }

    /**
     * Create an instance of {@link ChangeCustNoticeSuppressRequest.SuppressSetting.DelSuppressSet }
     * 
     */
    public ChangeCustNoticeSuppressRequest.SuppressSetting.DelSuppressSet createChangeCustNoticeSuppressRequestSuppressSettingDelSuppressSet() {
        return new ChangeCustNoticeSuppressRequest.SuppressSetting.DelSuppressSet();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer createChangeAcctOwnershipRequestNewOwnershipRegisterCustomer() {
        return new ChangeAcctOwnershipRequest.NewOwnership.RegisterCustomer();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.UserCustomer }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.UserCustomer createChangeAcctOwnershipRequestNewOwnershipUserCustomer() {
        return new ChangeAcctOwnershipRequest.NewOwnership.UserCustomer();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering createChangeAcctOwnershipRequestNewOwnershipPrimaryOffering() {
        return new ChangeAcctOwnershipRequest.NewOwnership.PrimaryOffering();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering createChangeAcctOwnershipRequestNewOwnershipSupplementaryOfferingShiftOffering() {
        return new ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.ShiftOffering();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering createChangeAcctOwnershipRequestNewOwnershipSupplementaryOfferingDelOffering() {
        return new ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.DelOffering();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering createChangeAcctOwnershipRequestNewOwnershipSupplementaryOfferingAddOffering() {
        return new ChangeAcctOwnershipRequest.NewOwnership.SupplementaryOffering.AddOffering();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation.ShiftPayRelation }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation.ShiftPayRelation createChangeAcctOwnershipRequestNewOwnershipSubscriberSubPayRelationShiftPayRelation() {
        return new ChangeAcctOwnershipRequest.NewOwnership.Subscriber.SubPayRelation.ShiftPayRelation();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.NewOwnership.Account.AutoPayChannel }
     * 
     */
    public ChangeAcctOwnershipRequest.NewOwnership.Account.AutoPayChannel createChangeAcctOwnershipRequestNewOwnershipAccountAutoPayChannel() {
        return new ChangeAcctOwnershipRequest.NewOwnership.Account.AutoPayChannel();
    }

    /**
     * Create an instance of {@link ChangeAcctOwnershipRequest.OldOwnership.Account }
     * 
     */
    public ChangeAcctOwnershipRequest.OldOwnership.Account createChangeAcctOwnershipRequestOldOwnershipAccount() {
        return new ChangeAcctOwnershipRequest.OldOwnership.Account();
    }

    /**
     * Create an instance of {@link BatchCreateSubscriberRequest.Subscriber }
     * 
     */
    public BatchCreateSubscriberRequest.Subscriber createBatchCreateSubscriberRequestSubscriber() {
        return new BatchCreateSubscriberRequest.Subscriber();
    }

    /**
     * Create an instance of {@link BatchCreateSubscriberRequest.SupplementaryOffering }
     * 
     */
    public BatchCreateSubscriberRequest.SupplementaryOffering createBatchCreateSubscriberRequestSupplementaryOffering() {
        return new BatchCreateSubscriberRequest.SupplementaryOffering();
    }

    /**
     * Create an instance of {@link CreateSuperGroupRequest.SuperGroupAccessCode }
     * 
     */
    public CreateSuperGroupRequest.SuperGroupAccessCode createCreateSuperGroupRequestSuperGroupAccessCode() {
        return new CreateSuperGroupRequest.SuperGroupAccessCode();
    }

    /**
     * Create an instance of {@link QuerySubscribedOfferingsResult.OfferingInstanceInfo }
     * 
     */
    public QuerySubscribedOfferingsResult.OfferingInstanceInfo createQuerySubscribedOfferingsResultOfferingInstanceInfo() {
        return new QuerySubscribedOfferingsResult.OfferingInstanceInfo();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackRequest.RollBackFeeValues }
     * 
     */
    public FeeDeductionRollBackRequest.RollBackFeeValues createFeeDeductionRollBackRequestRollBackFeeValues() {
        return new FeeDeductionRollBackRequest.RollBackFeeValues();
    }

    /**
     * Create an instance of {@link FeeDeductionRollBackRequest.DeductObj.AcctAccessCode }
     * 
     */
    public FeeDeductionRollBackRequest.DeductObj.AcctAccessCode createFeeDeductionRollBackRequestDeductObjAcctAccessCode() {
        return new FeeDeductionRollBackRequest.DeductObj.AcctAccessCode();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "ExpirationTime", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<String> createChangeConsumptionLimitRequestAddLimitExpirationTime(String value) {
        return new JAXBElement<String>(_ChangeConsumptionLimitRequestAddLimitExpirationTime_QNAME, String.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "LimitValue", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<Long> createChangeConsumptionLimitRequestAddLimitLimitValue(Long value) {
        return new JAXBElement<Long>(_ChangeConsumptionLimitRequestAddLimitLimitValue_QNAME, Long.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "CurrencyID", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<BigInteger> createChangeConsumptionLimitRequestAddLimitCurrencyID(BigInteger value) {
        return new JAXBElement<BigInteger>(_ChangeConsumptionLimitRequestAddLimitCurrencyID_QNAME, BigInteger.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "LimitType", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<String> createChangeConsumptionLimitRequestAddLimitLimitType(String value) {
        return new JAXBElement<String>(_ChangeConsumptionLimitRequestAddLimitLimitType_QNAME, String.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "MesureID", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<BigInteger> createChangeConsumptionLimitRequestAddLimitMesureID(BigInteger value) {
        return new JAXBElement<BigInteger>(_ChangeConsumptionLimitRequestAddLimitMesureID_QNAME, BigInteger.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "EffectiveTime", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<String> createChangeConsumptionLimitRequestAddLimitEffectiveTime(String value) {
        return new JAXBElement<String>(_ChangeConsumptionLimitRequestAddLimitEffectiveTime_QNAME, String.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeConsumptionLimitRequest.AddLimit.LimitParam }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "LimitParam", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<ChangeConsumptionLimitRequest.AddLimit.LimitParam> createChangeConsumptionLimitRequestAddLimitLimitParam(ChangeConsumptionLimitRequest.AddLimit.LimitParam value) {
        return new JAXBElement<ChangeConsumptionLimitRequest.AddLimit.LimitParam>(_ChangeConsumptionLimitRequestAddLimitLimitParam_QNAME, ChangeConsumptionLimitRequest.AddLimit.LimitParam.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "UnitType", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<String> createChangeConsumptionLimitRequestAddLimitUnitType(String value) {
        return new JAXBElement<String>(_ChangeConsumptionLimitRequestAddLimitUnitType_QNAME, String.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.huawei.com/bme/cbsinterface/bcservices", name = "MesureType", scope = ChangeConsumptionLimitRequest.AddLimit.class)
    public JAXBElement<BigInteger> createChangeConsumptionLimitRequestAddLimitMesureType(BigInteger value) {
        return new JAXBElement<BigInteger>(_ChangeConsumptionLimitRequestAddLimitMesureType_QNAME, BigInteger.class, ChangeConsumptionLimitRequest.AddLimit.class, value);
    }

}
