
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubGroupMemberInfo;


/**
 * <p>Clase Java para ChangeGroupMemberInfoRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeGroupMemberInfoRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode"/>
 *         &lt;element name="GroupMember">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
 *                   &lt;element name="SubGroupMember" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupMemberInfo"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeGroupMemberInfoRequest", propOrder = {
    "subGroupAccessCode",
    "groupMember"
})
public class ChangeGroupMemberInfoRequest {

    @XmlElement(name = "SubGroupAccessCode", required = true)
    protected SubGroupAccessCode subGroupAccessCode;
    @XmlElement(name = "GroupMember", required = true)
    protected ChangeGroupMemberInfoRequest.GroupMember groupMember;

    /**
     * Obtiene el valor de la propiedad subGroupAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public SubGroupAccessCode getSubGroupAccessCode() {
        return subGroupAccessCode;
    }

    /**
     * Define el valor de la propiedad subGroupAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public void setSubGroupAccessCode(SubGroupAccessCode value) {
        this.subGroupAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad groupMember.
     * 
     * @return
     *     possible object is
     *     {@link ChangeGroupMemberInfoRequest.GroupMember }
     *     
     */
    public ChangeGroupMemberInfoRequest.GroupMember getGroupMember() {
        return groupMember;
    }

    /**
     * Define el valor de la propiedad groupMember.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeGroupMemberInfoRequest.GroupMember }
     *     
     */
    public void setGroupMember(ChangeGroupMemberInfoRequest.GroupMember value) {
        this.groupMember = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
     *         &lt;element name="SubGroupMember" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupMemberInfo"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subAccessCode",
        "subGroupMember"
    })
    public static class GroupMember {

        @XmlElement(name = "SubAccessCode", required = true)
        protected SubAccessCode subAccessCode;
        @XmlElement(name = "SubGroupMember", required = true)
        protected SubGroupMemberInfo subGroupMember;

        /**
         * Obtiene el valor de la propiedad subAccessCode.
         * 
         * @return
         *     possible object is
         *     {@link SubAccessCode }
         *     
         */
        public SubAccessCode getSubAccessCode() {
            return subAccessCode;
        }

        /**
         * Define el valor de la propiedad subAccessCode.
         * 
         * @param value
         *     allowed object is
         *     {@link SubAccessCode }
         *     
         */
        public void setSubAccessCode(SubAccessCode value) {
            this.subAccessCode = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupMember.
         * 
         * @return
         *     possible object is
         *     {@link SubGroupMemberInfo }
         *     
         */
        public SubGroupMemberInfo getSubGroupMember() {
            return subGroupMember;
        }

        /**
         * Define el valor de la propiedad subGroupMember.
         * 
         * @param value
         *     allowed object is
         *     {@link SubGroupMemberInfo }
         *     
         */
        public void setSubGroupMember(SubGroupMemberInfo value) {
            this.subGroupMember = value;
        }

    }

}
