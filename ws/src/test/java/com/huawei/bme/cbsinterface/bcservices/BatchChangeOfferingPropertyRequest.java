
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.EffectMode;
import com.huawei.bme.cbsinterface.bccommon.InstProperty;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;


/**
 * <p>Clase Java para BatchChangeOfferingPropertyRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BatchChangeOfferingPropertyRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *         &lt;element name="OfferingInstProperty" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AddProperty" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
 *                           &lt;sequence>
 *                             &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
 *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="DelProperty" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
 *                           &lt;sequence>
 *                             &lt;element name="ExpirationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ProductInst" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ProductID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AddProperty" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
 *                           &lt;sequence>
 *                             &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
 *                             &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="DelProperty" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
 *                           &lt;sequence>
 *                             &lt;element name="ExpirationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatchChangeOfferingPropertyRequest", propOrder = {
    "offeringKey",
    "offeringInstProperty",
    "productInst",
    "fileName"
})
public class BatchChangeOfferingPropertyRequest {

    @XmlElement(name = "OfferingKey")
    protected OfferingKey offeringKey;
    @XmlElement(name = "OfferingInstProperty")
    protected BatchChangeOfferingPropertyRequest.OfferingInstProperty offeringInstProperty;
    @XmlElement(name = "ProductInst")
    protected BatchChangeOfferingPropertyRequest.ProductInst productInst;
    @XmlElement(name = "FileName", required = true)
    protected Object fileName;

    /**
     * Obtiene el valor de la propiedad offeringKey.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getOfferingKey() {
        return offeringKey;
    }

    /**
     * Define el valor de la propiedad offeringKey.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setOfferingKey(OfferingKey value) {
        this.offeringKey = value;
    }

    /**
     * Obtiene el valor de la propiedad offeringInstProperty.
     * 
     * @return
     *     possible object is
     *     {@link BatchChangeOfferingPropertyRequest.OfferingInstProperty }
     *     
     */
    public BatchChangeOfferingPropertyRequest.OfferingInstProperty getOfferingInstProperty() {
        return offeringInstProperty;
    }

    /**
     * Define el valor de la propiedad offeringInstProperty.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchChangeOfferingPropertyRequest.OfferingInstProperty }
     *     
     */
    public void setOfferingInstProperty(BatchChangeOfferingPropertyRequest.OfferingInstProperty value) {
        this.offeringInstProperty = value;
    }

    /**
     * Obtiene el valor de la propiedad productInst.
     * 
     * @return
     *     possible object is
     *     {@link BatchChangeOfferingPropertyRequest.ProductInst }
     *     
     */
    public BatchChangeOfferingPropertyRequest.ProductInst getProductInst() {
        return productInst;
    }

    /**
     * Define el valor de la propiedad productInst.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchChangeOfferingPropertyRequest.ProductInst }
     *     
     */
    public void setProductInst(BatchChangeOfferingPropertyRequest.ProductInst value) {
        this.productInst = value;
    }

    /**
     * Obtiene el valor de la propiedad fileName.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getFileName() {
        return fileName;
    }

    /**
     * Define el valor de la propiedad fileName.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setFileName(Object value) {
        this.fileName = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AddProperty" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
     *                 &lt;sequence>
     *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
     *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DelProperty" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
     *                 &lt;sequence>
     *                   &lt;element name="ExpirationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "addProperty",
        "delProperty"
    })
    public static class OfferingInstProperty {

        @XmlElement(name = "AddProperty")
        protected List<BatchChangeOfferingPropertyRequest.OfferingInstProperty.AddProperty> addProperty;
        @XmlElement(name = "DelProperty")
        protected List<BatchChangeOfferingPropertyRequest.OfferingInstProperty.DelProperty> delProperty;

        /**
         * Gets the value of the addProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BatchChangeOfferingPropertyRequest.OfferingInstProperty.AddProperty }
         * 
         * 
         */
        public List<BatchChangeOfferingPropertyRequest.OfferingInstProperty.AddProperty> getAddProperty() {
            if (addProperty == null) {
                addProperty = new ArrayList<BatchChangeOfferingPropertyRequest.OfferingInstProperty.AddProperty>();
            }
            return this.addProperty;
        }

        /**
         * Gets the value of the delProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the delProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDelProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BatchChangeOfferingPropertyRequest.OfferingInstProperty.DelProperty }
         * 
         * 
         */
        public List<BatchChangeOfferingPropertyRequest.OfferingInstProperty.DelProperty> getDelProperty() {
            if (delProperty == null) {
                delProperty = new ArrayList<BatchChangeOfferingPropertyRequest.OfferingInstProperty.DelProperty>();
            }
            return this.delProperty;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
         *       &lt;sequence>
         *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
         *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "effectiveTime",
            "expirationTime"
        })
        public static class AddProperty
            extends InstProperty
        {

            @XmlElement(name = "EffectiveTime")
            protected EffectMode effectiveTime;
            @XmlElement(name = "ExpirationTime")
            protected String expirationTime;

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link EffectMode }
             *     
             */
            public EffectMode getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link EffectMode }
             *     
             */
            public void setEffectiveTime(EffectMode value) {
                this.effectiveTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationTime(String value) {
                this.expirationTime = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
         *       &lt;sequence>
         *         &lt;element name="ExpirationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "expirationTime"
        })
        public static class DelProperty
            extends InstProperty
        {

            @XmlElement(name = "ExpirationTime", required = true)
            protected EffectMode expirationTime;

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link EffectMode }
             *     
             */
            public EffectMode getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link EffectMode }
             *     
             */
            public void setExpirationTime(EffectMode value) {
                this.expirationTime = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ProductID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AddProperty" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
     *                 &lt;sequence>
     *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
     *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DelProperty" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
     *                 &lt;sequence>
     *                   &lt;element name="ExpirationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "productID",
        "addProperty",
        "delProperty"
    })
    public static class ProductInst {

        @XmlElement(name = "ProductID", required = true)
        protected String productID;
        @XmlElement(name = "AddProperty")
        protected List<BatchChangeOfferingPropertyRequest.ProductInst.AddProperty> addProperty;
        @XmlElement(name = "DelProperty")
        protected List<BatchChangeOfferingPropertyRequest.ProductInst.DelProperty> delProperty;

        /**
         * Obtiene el valor de la propiedad productID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductID() {
            return productID;
        }

        /**
         * Define el valor de la propiedad productID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductID(String value) {
            this.productID = value;
        }

        /**
         * Gets the value of the addProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BatchChangeOfferingPropertyRequest.ProductInst.AddProperty }
         * 
         * 
         */
        public List<BatchChangeOfferingPropertyRequest.ProductInst.AddProperty> getAddProperty() {
            if (addProperty == null) {
                addProperty = new ArrayList<BatchChangeOfferingPropertyRequest.ProductInst.AddProperty>();
            }
            return this.addProperty;
        }

        /**
         * Gets the value of the delProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the delProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDelProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BatchChangeOfferingPropertyRequest.ProductInst.DelProperty }
         * 
         * 
         */
        public List<BatchChangeOfferingPropertyRequest.ProductInst.DelProperty> getDelProperty() {
            if (delProperty == null) {
                delProperty = new ArrayList<BatchChangeOfferingPropertyRequest.ProductInst.DelProperty>();
            }
            return this.delProperty;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
         *       &lt;sequence>
         *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode" minOccurs="0"/>
         *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "effectiveTime",
            "expirationTime"
        })
        public static class AddProperty
            extends InstProperty
        {

            @XmlElement(name = "EffectiveTime")
            protected EffectMode effectiveTime;
            @XmlElement(name = "ExpirationTime")
            protected String expirationTime;

            /**
             * Obtiene el valor de la propiedad effectiveTime.
             * 
             * @return
             *     possible object is
             *     {@link EffectMode }
             *     
             */
            public EffectMode getEffectiveTime() {
                return effectiveTime;
            }

            /**
             * Define el valor de la propiedad effectiveTime.
             * 
             * @param value
             *     allowed object is
             *     {@link EffectMode }
             *     
             */
            public void setEffectiveTime(EffectMode value) {
                this.effectiveTime = value;
            }

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setExpirationTime(String value) {
                this.expirationTime = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}InstProperty">
         *       &lt;sequence>
         *         &lt;element name="ExpirationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "expirationTime"
        })
        public static class DelProperty
            extends InstProperty
        {

            @XmlElement(name = "ExpirationTime", required = true)
            protected EffectMode expirationTime;

            /**
             * Obtiene el valor de la propiedad expirationTime.
             * 
             * @return
             *     possible object is
             *     {@link EffectMode }
             *     
             */
            public EffectMode getExpirationTime() {
                return expirationTime;
            }

            /**
             * Define el valor de la propiedad expirationTime.
             * 
             * @param value
             *     allowed object is
             *     {@link EffectMode }
             *     
             */
            public void setExpirationTime(EffectMode value) {
                this.expirationTime = value;
            }

        }

    }

}
