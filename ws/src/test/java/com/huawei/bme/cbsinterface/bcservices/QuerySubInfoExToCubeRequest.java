
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QuerySubInfoExToCubeRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QuerySubInfoExToCubeRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccessInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ObjectIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ObjectId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubInfoExToCubeRequest", propOrder = {
    "accessInfo"
})
public class QuerySubInfoExToCubeRequest {

    @XmlElement(name = "AccessInfo", required = true)
    protected QuerySubInfoExToCubeRequest.AccessInfo accessInfo;

    /**
     * Obtiene el valor de la propiedad accessInfo.
     * 
     * @return
     *     possible object is
     *     {@link QuerySubInfoExToCubeRequest.AccessInfo }
     *     
     */
    public QuerySubInfoExToCubeRequest.AccessInfo getAccessInfo() {
        return accessInfo;
    }

    /**
     * Define el valor de la propiedad accessInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link QuerySubInfoExToCubeRequest.AccessInfo }
     *     
     */
    public void setAccessInfo(QuerySubInfoExToCubeRequest.AccessInfo value) {
        this.accessInfo = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ObjectIdType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ObjectId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "objectIdType",
        "objectId"
    })
    public static class AccessInfo {

        @XmlElement(name = "ObjectIdType", required = true)
        protected String objectIdType;
        @XmlElement(name = "ObjectId", required = true)
        protected String objectId;

        /**
         * Obtiene el valor de la propiedad objectIdType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObjectIdType() {
            return objectIdType;
        }

        /**
         * Define el valor de la propiedad objectIdType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObjectIdType(String value) {
            this.objectIdType = value;
        }

        /**
         * Obtiene el valor de la propiedad objectId.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObjectId() {
            return objectId;
        }

        /**
         * Define el valor de la propiedad objectId.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObjectId(String value) {
            this.objectId = value;
        }

    }

}
