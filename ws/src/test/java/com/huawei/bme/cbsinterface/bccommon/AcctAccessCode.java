
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AcctAccessCode complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AcctAccessCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="AccountKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="AccountCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcctAccessCode", propOrder = {
    "primaryIdentity",
    "accountKey",
    "accountCode"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.FeeDeductionRollBackRequest.DeductObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeRedFlagRequest.ChangeObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.QueryPaymentLimitUsageRequest.PayAccount.class,
    com.huawei.bme.cbsinterface.bcservices.QueryRebateRequest.OfferingOwner.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeOfferingPropertyRequest.OfferingInst.OfferingOwner.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleRequest.OfferingInst.OfferingOwner.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.QueryOfferingInstPropertyRequest.OfferingOwner.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.QueryAccumulationUsageRequest.QueryObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ActivateOfferingRequest.OfferingOwner.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationRequest.PayAccount.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeAcctInfoRequest.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeOfferingStatusRequest.OfferingInst.OfferingOwner.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.FeeQuotationRequest.ChargeObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeAcctBillCycleRequest.Account.RootAccount.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeAcctBillCycleRequest.Account.SubAccount.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeAccountOfferingRequest.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoRequest.QueryObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.FeeDeductionRequest.DeductObj.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.AcctDeactivationRequest.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeCustHierachyRequest.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeCustHierachyRequest.NewParentAcct.class,
    com.huawei.bme.cbsinterface.bcservices.QueryOfferingRentCycleResult.OfferingRentCycle.OfferingOwner.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeAcctCreditLimitRequest.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeProductStatusRequest.OfferingInst.OfferingOwner.AcctAccessCode.class,
    com.huawei.bme.cbsinterface.bcservices.ActivateProductRequest.OfferingOwner.AcctAccessCode.class
})
public class AcctAccessCode {

    @XmlElement(name = "PrimaryIdentity")
    protected String primaryIdentity;
    @XmlElement(name = "AccountKey")
    protected String accountKey;
    @XmlElement(name = "AccountCode")
    protected String accountCode;

    /**
     * Obtiene el valor de la propiedad primaryIdentity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryIdentity() {
        return primaryIdentity;
    }

    /**
     * Define el valor de la propiedad primaryIdentity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryIdentity(String value) {
        this.primaryIdentity = value;
    }

    /**
     * Obtiene el valor de la propiedad accountKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountKey() {
        return accountKey;
    }

    /**
     * Define el valor de la propiedad accountKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountKey(String value) {
        this.accountKey = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCode() {
        return accountCode;
    }

    /**
     * Define el valor de la propiedad accountCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCode(String value) {
        this.accountCode = value;
    }

}
