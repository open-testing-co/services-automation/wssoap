
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AccountForBatch complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AccountForBatch">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *         &lt;element name="BillLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DunningFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="LateFeeChargeable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="InitBalance" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/>
 *         &lt;element name="CreditLimit" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FreeBillMedium" type="{http://www.huawei.com/bme/cbsinterface/bccommon}FreeBillMedium" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *         &lt;element name="AcctProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountForBatch", propOrder = {
    "billCycleType",
    "paymentType",
    "currencyID",
    "billLang",
    "dunningFlag",
    "lateFeeChargeable",
    "initBalance",
    "creditLimit",
    "freeBillMedium",
    "acctProperty"
})
public class AccountForBatch {

    @XmlElement(name = "BillCycleType")
    protected String billCycleType;
    @XmlElement(name = "PaymentType")
    protected String paymentType;
    @XmlElement(name = "CurrencyID")
    protected BigInteger currencyID;
    @XmlElement(name = "BillLang")
    protected String billLang;
    @XmlElement(name = "DunningFlag")
    protected String dunningFlag;
    @XmlElement(name = "LateFeeChargeable")
    protected String lateFeeChargeable;
    @XmlElement(name = "InitBalance")
    protected Long initBalance;
    @XmlElement(name = "CreditLimit")
    protected List<AccountForBatch.CreditLimit> creditLimit;
    @XmlElement(name = "FreeBillMedium")
    protected List<FreeBillMedium> freeBillMedium;
    @XmlElement(name = "AcctProperty")
    protected List<SimpleProperty> acctProperty;

    /**
     * Obtiene el valor de la propiedad billCycleType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleType() {
        return billCycleType;
    }

    /**
     * Define el valor de la propiedad billCycleType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleType(String value) {
        this.billCycleType = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Define el valor de la propiedad paymentType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrencyID() {
        return currencyID;
    }

    /**
     * Define el valor de la propiedad currencyID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrencyID(BigInteger value) {
        this.currencyID = value;
    }

    /**
     * Obtiene el valor de la propiedad billLang.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillLang() {
        return billLang;
    }

    /**
     * Define el valor de la propiedad billLang.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillLang(String value) {
        this.billLang = value;
    }

    /**
     * Obtiene el valor de la propiedad dunningFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDunningFlag() {
        return dunningFlag;
    }

    /**
     * Define el valor de la propiedad dunningFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDunningFlag(String value) {
        this.dunningFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad lateFeeChargeable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLateFeeChargeable() {
        return lateFeeChargeable;
    }

    /**
     * Define el valor de la propiedad lateFeeChargeable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLateFeeChargeable(String value) {
        this.lateFeeChargeable = value;
    }

    /**
     * Obtiene el valor de la propiedad initBalance.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInitBalance() {
        return initBalance;
    }

    /**
     * Define el valor de la propiedad initBalance.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInitBalance(Long value) {
        this.initBalance = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditLimit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditLimit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountForBatch.CreditLimit }
     * 
     * 
     */
    public List<AccountForBatch.CreditLimit> getCreditLimit() {
        if (creditLimit == null) {
            creditLimit = new ArrayList<AccountForBatch.CreditLimit>();
        }
        return this.creditLimit;
    }

    /**
     * Gets the value of the freeBillMedium property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the freeBillMedium property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreeBillMedium().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FreeBillMedium }
     * 
     * 
     */
    public List<FreeBillMedium> getFreeBillMedium() {
        if (freeBillMedium == null) {
            freeBillMedium = new ArrayList<FreeBillMedium>();
        }
        return this.freeBillMedium;
    }

    /**
     * Gets the value of the acctProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acctProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcctProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAcctProperty() {
        if (acctProperty == null) {
            acctProperty = new ArrayList<SimpleProperty>();
        }
        return this.acctProperty;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitType",
        "limitValue"
    })
    public static class CreditLimit {

        @XmlElement(name = "LimitType")
        protected String limitType;
        @XmlElement(name = "LimitValue")
        protected long limitValue;

        /**
         * Obtiene el valor de la propiedad limitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitType() {
            return limitType;
        }

        /**
         * Define el valor de la propiedad limitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitType(String value) {
            this.limitType = value;
        }

        /**
         * Obtiene el valor de la propiedad limitValue.
         * 
         */
        public long getLimitValue() {
            return limitValue;
        }

        /**
         * Define el valor de la propiedad limitValue.
         * 
         */
        public void setLimitValue(long value) {
            this.limitValue = value;
        }

    }

}
