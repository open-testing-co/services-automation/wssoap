
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para PaymentLimit complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PaymentLimit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="Limit" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="LimitValueType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="LimitMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LimitRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CBonusFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentLimit", propOrder = {
    "limitCycleType",
    "limit",
    "limitRule",
    "cBonusFlag",
    "currencyID"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.ChangeSubPaymentModeRequest.PaymentModeChange.DFTPayRelation.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeSubOwnershipRequest.NewOwnership.Subscriber.SubDFTAcct.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.ChangePayRelationRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.ChangePayRelationRequest.PaymentRelation.ModPayRelation.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.AddGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.AddGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationResult.PaymentRelationList.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.CreateSubscriberRequest.Subscriber.SubPaymentMode.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.BatchSwitchGroupMemberRequest.PaymentRelation.NewDFTAcct.PaymentLimit.PaymentLimitInfo.class,
    com.huawei.bme.cbsinterface.bcservices.BatchSwitchGroupMemberRequest.PaymentRelation.AddPayRelation.PaymentLimit.PaymentLimitInfo.class
})
public class PaymentLimit {

    @XmlElement(name = "LimitCycleType", required = true)
    protected String limitCycleType;
    @XmlElement(name = "Limit", required = true)
    protected PaymentLimit.Limit limit;
    @XmlElement(name = "LimitRule")
    protected String limitRule;
    @XmlElement(name = "CBonusFlag")
    protected String cBonusFlag;
    @XmlElement(name = "CurrencyID")
    protected BigInteger currencyID;

    /**
     * Obtiene el valor de la propiedad limitCycleType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitCycleType() {
        return limitCycleType;
    }

    /**
     * Define el valor de la propiedad limitCycleType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitCycleType(String value) {
        this.limitCycleType = value;
    }

    /**
     * Obtiene el valor de la propiedad limit.
     * 
     * @return
     *     possible object is
     *     {@link PaymentLimit.Limit }
     *     
     */
    public PaymentLimit.Limit getLimit() {
        return limit;
    }

    /**
     * Define el valor de la propiedad limit.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentLimit.Limit }
     *     
     */
    public void setLimit(PaymentLimit.Limit value) {
        this.limit = value;
    }

    /**
     * Obtiene el valor de la propiedad limitRule.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitRule() {
        return limitRule;
    }

    /**
     * Define el valor de la propiedad limitRule.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitRule(String value) {
        this.limitRule = value;
    }

    /**
     * Obtiene el valor de la propiedad cBonusFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCBonusFlag() {
        return cBonusFlag;
    }

    /**
     * Define el valor de la propiedad cBonusFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCBonusFlag(String value) {
        this.cBonusFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrencyID() {
        return currencyID;
    }

    /**
     * Define el valor de la propiedad currencyID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrencyID(BigInteger value) {
        this.currencyID = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="LimitValueType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="LimitMeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
     *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitType",
        "limitValueType",
        "limitMeasureUnit",
        "limitValue"
    })
    public static class Limit {

        @XmlElement(name = "LimitType", required = true)
        protected String limitType;
        @XmlElement(name = "LimitValueType", required = true)
        protected String limitValueType;
        @XmlElement(name = "LimitMeasureUnit")
        protected BigInteger limitMeasureUnit;
        @XmlElement(name = "LimitValue")
        protected long limitValue;

        /**
         * Obtiene el valor de la propiedad limitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitType() {
            return limitType;
        }

        /**
         * Define el valor de la propiedad limitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitType(String value) {
            this.limitType = value;
        }

        /**
         * Obtiene el valor de la propiedad limitValueType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitValueType() {
            return limitValueType;
        }

        /**
         * Define el valor de la propiedad limitValueType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitValueType(String value) {
            this.limitValueType = value;
        }

        /**
         * Obtiene el valor de la propiedad limitMeasureUnit.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getLimitMeasureUnit() {
            return limitMeasureUnit;
        }

        /**
         * Define el valor de la propiedad limitMeasureUnit.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setLimitMeasureUnit(BigInteger value) {
            this.limitMeasureUnit = value;
        }

        /**
         * Obtiene el valor de la propiedad limitValue.
         * 
         */
        public long getLimitValue() {
            return limitValue;
        }

        /**
         * Define el valor de la propiedad limitValue.
         * 
         */
        public void setLimitValue(long value) {
            this.limitValue = value;
        }

    }

}
