
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CreateSuperGroupRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreateSuperGroupRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SuperGroupAccessCode">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SuperGroupKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SuperGroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SuperGroupName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateSuperGroupRequest", propOrder = {
    "superGroupAccessCode",
    "superGroupName"
})
public class CreateSuperGroupRequest {

    @XmlElement(name = "SuperGroupAccessCode", required = true)
    protected CreateSuperGroupRequest.SuperGroupAccessCode superGroupAccessCode;
    @XmlElement(name = "SuperGroupName")
    protected String superGroupName;

    /**
     * Obtiene el valor de la propiedad superGroupAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link CreateSuperGroupRequest.SuperGroupAccessCode }
     *     
     */
    public CreateSuperGroupRequest.SuperGroupAccessCode getSuperGroupAccessCode() {
        return superGroupAccessCode;
    }

    /**
     * Define el valor de la propiedad superGroupAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateSuperGroupRequest.SuperGroupAccessCode }
     *     
     */
    public void setSuperGroupAccessCode(CreateSuperGroupRequest.SuperGroupAccessCode value) {
        this.superGroupAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad superGroupName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuperGroupName() {
        return superGroupName;
    }

    /**
     * Define el valor de la propiedad superGroupName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuperGroupName(String value) {
        this.superGroupName = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SuperGroupKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SuperGroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "superGroupKey",
        "superGroupCode"
    })
    public static class SuperGroupAccessCode {

        @XmlElement(name = "SuperGroupKey", required = true)
        protected String superGroupKey;
        @XmlElement(name = "SuperGroupCode", required = true)
        protected String superGroupCode;

        /**
         * Obtiene el valor de la propiedad superGroupKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuperGroupKey() {
            return superGroupKey;
        }

        /**
         * Define el valor de la propiedad superGroupKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuperGroupKey(String value) {
            this.superGroupKey = value;
        }

        /**
         * Obtiene el valor de la propiedad superGroupCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuperGroupCode() {
            return superGroupCode;
        }

        /**
         * Define el valor de la propiedad superGroupCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuperGroupCode(String value) {
            this.superGroupCode = value;
        }

    }

}
