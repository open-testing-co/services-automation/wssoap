
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryGroupMemOffNetNumberResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryGroupMemOffNetNumberResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupMemOffNetNumber" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OffNetNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OffNetNumberGroupID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryGroupMemOffNetNumberResult", propOrder = {
    "groupMemOffNetNumber"
})
public class QueryGroupMemOffNetNumberResult {

    @XmlElement(name = "GroupMemOffNetNumber")
    protected List<QueryGroupMemOffNetNumberResult.GroupMemOffNetNumber> groupMemOffNetNumber;

    /**
     * Gets the value of the groupMemOffNetNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupMemOffNetNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupMemOffNetNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryGroupMemOffNetNumberResult.GroupMemOffNetNumber }
     * 
     * 
     */
    public List<QueryGroupMemOffNetNumberResult.GroupMemOffNetNumber> getGroupMemOffNetNumber() {
        if (groupMemOffNetNumber == null) {
            groupMemOffNetNumber = new ArrayList<QueryGroupMemOffNetNumberResult.GroupMemOffNetNumber>();
        }
        return this.groupMemOffNetNumber;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OffNetNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OffNetNumberGroupID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offNetNumber",
        "offNetNumberGroupID",
        "effectiveTime",
        "expirationTime"
    })
    public static class GroupMemOffNetNumber {

        @XmlElement(name = "OffNetNumber", required = true)
        protected String offNetNumber;
        @XmlElement(name = "OffNetNumberGroupID", required = true)
        protected String offNetNumberGroupID;
        @XmlElement(name = "EffectiveTime", required = true)
        protected String effectiveTime;
        @XmlElement(name = "ExpirationTime", required = true)
        protected String expirationTime;

        /**
         * Obtiene el valor de la propiedad offNetNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOffNetNumber() {
            return offNetNumber;
        }

        /**
         * Define el valor de la propiedad offNetNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOffNetNumber(String value) {
            this.offNetNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad offNetNumberGroupID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOffNetNumberGroupID() {
            return offNetNumberGroupID;
        }

        /**
         * Define el valor de la propiedad offNetNumberGroupID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOffNetNumberGroupID(String value) {
            this.offNetNumberGroupID = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Define el valor de la propiedad effectiveTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffectiveTime(String value) {
            this.effectiveTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationTime() {
            return expirationTime;
        }

        /**
         * Define el valor de la propiedad expirationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationTime(String value) {
            this.expirationTime = value;
        }

    }

}
