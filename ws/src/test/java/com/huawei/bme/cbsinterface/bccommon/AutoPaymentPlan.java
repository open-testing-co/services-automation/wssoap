
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AutoPaymentPlan complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AutoPaymentPlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AutoPayType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="PaymentObjectType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="PaymentObjectID" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="PaymentDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/>
 *         &lt;element name="LimitTimes" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutoPaymentPlan", propOrder = {
    "autoPayType",
    "paymentObjectType",
    "paymentObjectID",
    "paymentDate",
    "amount",
    "limitTimes"
})
public class AutoPaymentPlan {

    @XmlElement(name = "AutoPayType", required = true)
    protected String autoPayType;
    @XmlElement(name = "PaymentObjectType", required = true)
    protected String paymentObjectType;
    @XmlElement(name = "PaymentObjectID", required = true)
    protected String paymentObjectID;
    @XmlElement(name = "PaymentDate")
    protected String paymentDate;
    @XmlElement(name = "Amount")
    protected Long amount;
    @XmlElement(name = "LimitTimes")
    protected BigInteger limitTimes;

    /**
     * Obtiene el valor de la propiedad autoPayType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoPayType() {
        return autoPayType;
    }

    /**
     * Define el valor de la propiedad autoPayType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoPayType(String value) {
        this.autoPayType = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentObjectType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentObjectType() {
        return paymentObjectType;
    }

    /**
     * Define el valor de la propiedad paymentObjectType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentObjectType(String value) {
        this.paymentObjectType = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentObjectID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentObjectID() {
        return paymentObjectID;
    }

    /**
     * Define el valor de la propiedad paymentObjectID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentObjectID(String value) {
        this.paymentObjectID = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * Define el valor de la propiedad paymentDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDate(String value) {
        this.paymentDate = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAmount(Long value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad limitTimes.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLimitTimes() {
        return limitTimes;
    }

    /**
     * Define el valor de la propiedad limitTimes.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLimitTimes(BigInteger value) {
        this.limitTimes = value;
    }

}
