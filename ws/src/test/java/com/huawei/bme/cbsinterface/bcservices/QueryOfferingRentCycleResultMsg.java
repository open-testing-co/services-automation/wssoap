
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader" form="unqualified"/>
 *         &lt;element name="QueryOfferingRentCycleResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryOfferingRentCycleResult" minOccurs="0" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "queryOfferingRentCycleResult"
})
@XmlRootElement(name = "QueryOfferingRentCycleResultMsg")
public class QueryOfferingRentCycleResultMsg {

    @XmlElement(name = "ResultHeader", namespace = "", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "QueryOfferingRentCycleResult", namespace = "")
    protected QueryOfferingRentCycleResult queryOfferingRentCycleResult;

    /**
     * Obtiene el valor de la propiedad resultHeader.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Define el valor de la propiedad resultHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad queryOfferingRentCycleResult.
     * 
     * @return
     *     possible object is
     *     {@link QueryOfferingRentCycleResult }
     *     
     */
    public QueryOfferingRentCycleResult getQueryOfferingRentCycleResult() {
        return queryOfferingRentCycleResult;
    }

    /**
     * Define el valor de la propiedad queryOfferingRentCycleResult.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryOfferingRentCycleResult }
     *     
     */
    public void setQueryOfferingRentCycleResult(QueryOfferingRentCycleResult value) {
        this.queryOfferingRentCycleResult = value;
    }

}
