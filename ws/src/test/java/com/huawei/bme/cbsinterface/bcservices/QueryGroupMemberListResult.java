
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;


/**
 * <p>Clase Java para QueryGroupMemberListResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryGroupMemberListResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupMemberList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MemberTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MemberShortNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MemberProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryGroupMemberListResult", propOrder = {
    "groupMemberList",
    "totalNumber",
    "beginRowNum",
    "fetchRowNum"
})
public class QueryGroupMemberListResult {

    @XmlElement(name = "GroupMemberList")
    protected List<QueryGroupMemberListResult.GroupMemberList> groupMemberList;
    @XmlElement(name = "TotalNumber", required = true)
    protected BigInteger totalNumber;
    @XmlElement(name = "BeginRowNum", required = true)
    protected BigInteger beginRowNum;
    @XmlElement(name = "FetchRowNum", required = true)
    protected BigInteger fetchRowNum;

    /**
     * Gets the value of the groupMemberList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupMemberList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupMemberList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryGroupMemberListResult.GroupMemberList }
     * 
     * 
     */
    public List<QueryGroupMemberListResult.GroupMemberList> getGroupMemberList() {
        if (groupMemberList == null) {
            groupMemberList = new ArrayList<QueryGroupMemberListResult.GroupMemberList>();
        }
        return this.groupMemberList;
    }

    /**
     * Obtiene el valor de la propiedad totalNumber.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalNumber() {
        return totalNumber;
    }

    /**
     * Define el valor de la propiedad totalNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalNumber(BigInteger value) {
        this.totalNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad beginRowNum.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Define el valor de la propiedad beginRowNum.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBeginRowNum(BigInteger value) {
        this.beginRowNum = value;
    }

    /**
     * Obtiene el valor de la propiedad fetchRowNum.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Define el valor de la propiedad fetchRowNum.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFetchRowNum(BigInteger value) {
        this.fetchRowNum = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MemberTypeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MemberShortNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MemberProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "primaryIdentity",
        "memberTypeCode",
        "memberShortNo",
        "memberProperty"
    })
    public static class GroupMemberList {

        @XmlElement(name = "PrimaryIdentity", required = true)
        protected String primaryIdentity;
        @XmlElement(name = "MemberTypeCode", required = true)
        protected String memberTypeCode;
        @XmlElement(name = "MemberShortNo")
        protected String memberShortNo;
        @XmlElement(name = "MemberProperty")
        protected List<SimpleProperty> memberProperty;

        /**
         * Obtiene el valor de la propiedad primaryIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryIdentity() {
            return primaryIdentity;
        }

        /**
         * Define el valor de la propiedad primaryIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryIdentity(String value) {
            this.primaryIdentity = value;
        }

        /**
         * Obtiene el valor de la propiedad memberTypeCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMemberTypeCode() {
            return memberTypeCode;
        }

        /**
         * Define el valor de la propiedad memberTypeCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMemberTypeCode(String value) {
            this.memberTypeCode = value;
        }

        /**
         * Obtiene el valor de la propiedad memberShortNo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMemberShortNo() {
            return memberShortNo;
        }

        /**
         * Define el valor de la propiedad memberShortNo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMemberShortNo(String value) {
            this.memberShortNo = value;
        }

        /**
         * Gets the value of the memberProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the memberProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getMemberProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getMemberProperty() {
            if (memberProperty == null) {
                memberProperty = new ArrayList<SimpleProperty>();
            }
            return this.memberProperty;
        }

    }

}
