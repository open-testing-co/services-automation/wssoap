
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SubGroup complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubGroup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubGroupClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubGroupCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="PGroupKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubGroupBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupBasicInfo" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubGroup", propOrder = {
    "subGroupClass",
    "subGroupCode",
    "userCustomerKey",
    "pGroupKey",
    "password",
    "subGroupBasicInfo"
})
@XmlSeeAlso({
    com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoResult.SubGroup.SubGroupInfo.class
})
public class SubGroup {

    @XmlElement(name = "SubGroupClass")
    protected String subGroupClass;
    @XmlElement(name = "SubGroupCode")
    protected String subGroupCode;
    @XmlElement(name = "UserCustomerKey")
    protected String userCustomerKey;
    @XmlElement(name = "PGroupKey")
    protected String pGroupKey;
    @XmlElement(name = "Password")
    protected String password;
    @XmlElement(name = "SubGroupBasicInfo")
    protected SubGroupBasicInfo subGroupBasicInfo;

    /**
     * Obtiene el valor de la propiedad subGroupClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubGroupClass() {
        return subGroupClass;
    }

    /**
     * Define el valor de la propiedad subGroupClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubGroupClass(String value) {
        this.subGroupClass = value;
    }

    /**
     * Obtiene el valor de la propiedad subGroupCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubGroupCode() {
        return subGroupCode;
    }

    /**
     * Define el valor de la propiedad subGroupCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubGroupCode(String value) {
        this.subGroupCode = value;
    }

    /**
     * Obtiene el valor de la propiedad userCustomerKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserCustomerKey() {
        return userCustomerKey;
    }

    /**
     * Define el valor de la propiedad userCustomerKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserCustomerKey(String value) {
        this.userCustomerKey = value;
    }

    /**
     * Obtiene el valor de la propiedad pGroupKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPGroupKey() {
        return pGroupKey;
    }

    /**
     * Define el valor de la propiedad pGroupKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPGroupKey(String value) {
        this.pGroupKey = value;
    }

    /**
     * Obtiene el valor de la propiedad password.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Define el valor de la propiedad password.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Obtiene el valor de la propiedad subGroupBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupBasicInfo }
     *     
     */
    public SubGroupBasicInfo getSubGroupBasicInfo() {
        return subGroupBasicInfo;
    }

    /**
     * Define el valor de la propiedad subGroupBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupBasicInfo }
     *     
     */
    public void setSubGroupBasicInfo(SubGroupBasicInfo value) {
        this.subGroupBasicInfo = value;
    }

}
