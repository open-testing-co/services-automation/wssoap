
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;


/**
 * <p>Clase Java para QueryInstallmentResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryInstallmentResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InstallmentInstID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="TotalCycle" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="ContractID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *         &lt;element name="InatallmentDetail" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CycleSequence" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="InitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="CycleClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CycleDueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RealRepayDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DelayFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryInstallmentResult", propOrder = {
    "installmentInstID",
    "totalAmount",
    "currencyID",
    "totalCycle",
    "contractID",
    "offeringKey",
    "inatallmentDetail"
})
public class QueryInstallmentResult {

    @XmlElement(name = "InstallmentInstID")
    protected long installmentInstID;
    @XmlElement(name = "TotalAmount")
    protected long totalAmount;
    @XmlElement(name = "CurrencyID", required = true)
    protected BigInteger currencyID;
    @XmlElement(name = "TotalCycle", required = true)
    protected BigInteger totalCycle;
    @XmlElement(name = "ContractID", required = true)
    protected String contractID;
    @XmlElement(name = "OfferingKey")
    protected OfferingKey offeringKey;
    @XmlElement(name = "InatallmentDetail", required = true)
    protected List<QueryInstallmentResult.InatallmentDetail> inatallmentDetail;

    /**
     * Obtiene el valor de la propiedad installmentInstID.
     * 
     */
    public long getInstallmentInstID() {
        return installmentInstID;
    }

    /**
     * Define el valor de la propiedad installmentInstID.
     * 
     */
    public void setInstallmentInstID(long value) {
        this.installmentInstID = value;
    }

    /**
     * Obtiene el valor de la propiedad totalAmount.
     * 
     */
    public long getTotalAmount() {
        return totalAmount;
    }

    /**
     * Define el valor de la propiedad totalAmount.
     * 
     */
    public void setTotalAmount(long value) {
        this.totalAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrencyID() {
        return currencyID;
    }

    /**
     * Define el valor de la propiedad currencyID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrencyID(BigInteger value) {
        this.currencyID = value;
    }

    /**
     * Obtiene el valor de la propiedad totalCycle.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalCycle() {
        return totalCycle;
    }

    /**
     * Define el valor de la propiedad totalCycle.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalCycle(BigInteger value) {
        this.totalCycle = value;
    }

    /**
     * Obtiene el valor de la propiedad contractID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractID() {
        return contractID;
    }

    /**
     * Define el valor de la propiedad contractID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractID(String value) {
        this.contractID = value;
    }

    /**
     * Obtiene el valor de la propiedad offeringKey.
     * 
     * @return
     *     possible object is
     *     {@link OfferingKey }
     *     
     */
    public OfferingKey getOfferingKey() {
        return offeringKey;
    }

    /**
     * Define el valor de la propiedad offeringKey.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferingKey }
     *     
     */
    public void setOfferingKey(OfferingKey value) {
        this.offeringKey = value;
    }

    /**
     * Gets the value of the inatallmentDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inatallmentDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInatallmentDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryInstallmentResult.InatallmentDetail }
     * 
     * 
     */
    public List<QueryInstallmentResult.InatallmentDetail> getInatallmentDetail() {
        if (inatallmentDetail == null) {
            inatallmentDetail = new ArrayList<QueryInstallmentResult.InatallmentDetail>();
        }
        return this.inatallmentDetail;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CycleSequence" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="InitialAmount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="CycleClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CycleDueDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RealRepayDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DelayFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cycleSequence",
        "initialAmount",
        "amount",
        "currencyID",
        "cycleClass",
        "status",
        "cycleDueDate",
        "realRepayDate",
        "delayFlag"
    })
    public static class InatallmentDetail {

        @XmlElement(name = "CycleSequence", required = true)
        protected BigInteger cycleSequence;
        @XmlElement(name = "InitialAmount")
        protected long initialAmount;
        @XmlElement(name = "Amount")
        protected long amount;
        @XmlElement(name = "CurrencyID", required = true)
        protected BigInteger currencyID;
        @XmlElement(name = "CycleClass", required = true)
        protected String cycleClass;
        @XmlElement(name = "Status", required = true)
        protected String status;
        @XmlElement(name = "CycleDueDate", required = true)
        protected String cycleDueDate;
        @XmlElement(name = "RealRepayDate", required = true)
        protected String realRepayDate;
        @XmlElement(name = "DelayFlag", required = true)
        protected String delayFlag;

        /**
         * Obtiene el valor de la propiedad cycleSequence.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCycleSequence() {
            return cycleSequence;
        }

        /**
         * Define el valor de la propiedad cycleSequence.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCycleSequence(BigInteger value) {
            this.cycleSequence = value;
        }

        /**
         * Obtiene el valor de la propiedad initialAmount.
         * 
         */
        public long getInitialAmount() {
            return initialAmount;
        }

        /**
         * Define el valor de la propiedad initialAmount.
         * 
         */
        public void setInitialAmount(long value) {
            this.initialAmount = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         */
        public long getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         */
        public void setAmount(long value) {
            this.amount = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Obtiene el valor de la propiedad cycleClass.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCycleClass() {
            return cycleClass;
        }

        /**
         * Define el valor de la propiedad cycleClass.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCycleClass(String value) {
            this.cycleClass = value;
        }

        /**
         * Obtiene el valor de la propiedad status.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Define el valor de la propiedad status.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Obtiene el valor de la propiedad cycleDueDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCycleDueDate() {
            return cycleDueDate;
        }

        /**
         * Define el valor de la propiedad cycleDueDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCycleDueDate(String value) {
            this.cycleDueDate = value;
        }

        /**
         * Obtiene el valor de la propiedad realRepayDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRealRepayDate() {
            return realRepayDate;
        }

        /**
         * Define el valor de la propiedad realRepayDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRealRepayDate(String value) {
            this.realRepayDate = value;
        }

        /**
         * Obtiene el valor de la propiedad delayFlag.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDelayFlag() {
            return delayFlag;
        }

        /**
         * Define el valor de la propiedad delayFlag.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDelayFlag(String value) {
            this.delayFlag = value;
        }

    }

}
