
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader" form="unqualified"/>
 *         &lt;element name="ChangeGroupStatusRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeGroupStatusRequest" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "changeGroupStatusRequest"
})
@XmlRootElement(name = "ChangeGroupStatusRequestMsg")
public class ChangeGroupStatusRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "ChangeGroupStatusRequest", namespace = "", required = true)
    protected ChangeGroupStatusRequest changeGroupStatusRequest;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad changeGroupStatusRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeGroupStatusRequest }
     *     
     */
    public ChangeGroupStatusRequest getChangeGroupStatusRequest() {
        return changeGroupStatusRequest;
    }

    /**
     * Define el valor de la propiedad changeGroupStatusRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeGroupStatusRequest }
     *     
     */
    public void setChangeGroupStatusRequest(ChangeGroupStatusRequest value) {
        this.changeGroupStatusRequest = value;
    }

}
