
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.OfferingOwner;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;


/**
 * <p>Clase Java para SubActivationRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubActivationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
 *         &lt;element name="SubBasicInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SubProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OfferingInst" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OfferingKey">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey">
 *                           &lt;sequence>
 *                             &lt;element name="OfferingOwner" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingOwner" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ActiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubActivationRequest", propOrder = {
    "subAccessCode",
    "subBasicInfo",
    "offeringInst"
})
public class SubActivationRequest {

    @XmlElement(name = "SubAccessCode", required = true)
    protected SubAccessCode subAccessCode;
    @XmlElement(name = "SubBasicInfo")
    protected SubActivationRequest.SubBasicInfo subBasicInfo;
    @XmlElement(name = "OfferingInst")
    protected List<SubActivationRequest.OfferingInst> offeringInst;

    /**
     * Obtiene el valor de la propiedad subAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubAccessCode }
     *     
     */
    public SubAccessCode getSubAccessCode() {
        return subAccessCode;
    }

    /**
     * Define el valor de la propiedad subAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubAccessCode }
     *     
     */
    public void setSubAccessCode(SubAccessCode value) {
        this.subAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad subBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link SubActivationRequest.SubBasicInfo }
     *     
     */
    public SubActivationRequest.SubBasicInfo getSubBasicInfo() {
        return subBasicInfo;
    }

    /**
     * Define el valor de la propiedad subBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SubActivationRequest.SubBasicInfo }
     *     
     */
    public void setSubBasicInfo(SubActivationRequest.SubBasicInfo value) {
        this.subBasicInfo = value;
    }

    /**
     * Gets the value of the offeringInst property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offeringInst property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOfferingInst().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SubActivationRequest.OfferingInst }
     * 
     * 
     */
    public List<SubActivationRequest.OfferingInst> getOfferingInst() {
        if (offeringInst == null) {
            offeringInst = new ArrayList<SubActivationRequest.OfferingInst>();
        }
        return this.offeringInst;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OfferingKey">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey">
     *                 &lt;sequence>
     *                   &lt;element name="OfferingOwner" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingOwner" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ActiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offeringKey",
        "activeTime",
        "activeTimeLimit",
        "trialStartTime",
        "trialEndTime",
        "expirationTime"
    })
    public static class OfferingInst {

        @XmlElement(name = "OfferingKey", required = true)
        protected SubActivationRequest.OfferingInst.OfferingKey offeringKey;
        @XmlElement(name = "ActiveTime")
        protected String activeTime;
        @XmlElement(name = "ActiveTimeLimit")
        protected String activeTimeLimit;
        @XmlElement(name = "TrialStartTime")
        protected String trialStartTime;
        @XmlElement(name = "TrialEndTime")
        protected String trialEndTime;
        @XmlElement(name = "ExpirationTime")
        protected String expirationTime;

        /**
         * Obtiene el valor de la propiedad offeringKey.
         * 
         * @return
         *     possible object is
         *     {@link SubActivationRequest.OfferingInst.OfferingKey }
         *     
         */
        public SubActivationRequest.OfferingInst.OfferingKey getOfferingKey() {
            return offeringKey;
        }

        /**
         * Define el valor de la propiedad offeringKey.
         * 
         * @param value
         *     allowed object is
         *     {@link SubActivationRequest.OfferingInst.OfferingKey }
         *     
         */
        public void setOfferingKey(SubActivationRequest.OfferingInst.OfferingKey value) {
            this.offeringKey = value;
        }

        /**
         * Obtiene el valor de la propiedad activeTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActiveTime() {
            return activeTime;
        }

        /**
         * Define el valor de la propiedad activeTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActiveTime(String value) {
            this.activeTime = value;
        }

        /**
         * Obtiene el valor de la propiedad activeTimeLimit.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActiveTimeLimit() {
            return activeTimeLimit;
        }

        /**
         * Define el valor de la propiedad activeTimeLimit.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActiveTimeLimit(String value) {
            this.activeTimeLimit = value;
        }

        /**
         * Obtiene el valor de la propiedad trialStartTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrialStartTime() {
            return trialStartTime;
        }

        /**
         * Define el valor de la propiedad trialStartTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrialStartTime(String value) {
            this.trialStartTime = value;
        }

        /**
         * Obtiene el valor de la propiedad trialEndTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrialEndTime() {
            return trialEndTime;
        }

        /**
         * Define el valor de la propiedad trialEndTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrialEndTime(String value) {
            this.trialEndTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationTime() {
            return expirationTime;
        }

        /**
         * Define el valor de la propiedad expirationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationTime(String value) {
            this.expirationTime = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey">
         *       &lt;sequence>
         *         &lt;element name="OfferingOwner" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingOwner" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "offeringOwner"
        })
        public static class OfferingKey
            extends com.huawei.bme.cbsinterface.bccommon.OfferingKey
        {

            @XmlElement(name = "OfferingOwner")
            protected OfferingOwner offeringOwner;

            /**
             * Obtiene el valor de la propiedad offeringOwner.
             * 
             * @return
             *     possible object is
             *     {@link OfferingOwner }
             *     
             */
            public OfferingOwner getOfferingOwner() {
                return offeringOwner;
            }

            /**
             * Define el valor de la propiedad offeringOwner.
             * 
             * @param value
             *     allowed object is
             *     {@link OfferingOwner }
             *     
             */
            public void setOfferingOwner(OfferingOwner value) {
                this.offeringOwner = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SubProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "writtenLang",
        "ivrLang",
        "subPassword",
        "subProperty"
    })
    public static class SubBasicInfo {

        @XmlElement(name = "WrittenLang")
        protected String writtenLang;
        @XmlElement(name = "IVRLang")
        protected String ivrLang;
        @XmlElement(name = "SubPassword")
        protected String subPassword;
        @XmlElement(name = "SubProperty")
        protected List<SimpleProperty> subProperty;

        /**
         * Obtiene el valor de la propiedad writtenLang.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWrittenLang() {
            return writtenLang;
        }

        /**
         * Define el valor de la propiedad writtenLang.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWrittenLang(String value) {
            this.writtenLang = value;
        }

        /**
         * Obtiene el valor de la propiedad ivrLang.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIVRLang() {
            return ivrLang;
        }

        /**
         * Define el valor de la propiedad ivrLang.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIVRLang(String value) {
            this.ivrLang = value;
        }

        /**
         * Obtiene el valor de la propiedad subPassword.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubPassword() {
            return subPassword;
        }

        /**
         * Define el valor de la propiedad subPassword.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubPassword(String value) {
            this.subPassword = value;
        }

        /**
         * Gets the value of the subProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the subProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSubProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getSubProperty() {
            if (subProperty == null) {
                subProperty = new ArrayList<SimpleProperty>();
            }
            return this.subProperty;
        }

    }

}
