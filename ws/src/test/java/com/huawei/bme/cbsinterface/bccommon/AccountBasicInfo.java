
package com.huawei.bme.cbsinterface.bccommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AccountBasicInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AccountBasicInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="BillLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DunningFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="LateFeeChargeable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="RedlistFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ContactInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Contact" minOccurs="0" form="qualified"/>
 *         &lt;element name="FreeBillMedium" type="{http://www.huawei.com/bme/cbsinterface/bccommon}FreeBillMedium" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *         &lt;element name="AcctProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountBasicInfo", propOrder = {
    "acctName",
    "billLang",
    "dunningFlag",
    "lateFeeChargeable",
    "redlistFlag",
    "contactInfo",
    "freeBillMedium",
    "acctProperty"
})
public class AccountBasicInfo {

    @XmlElement(name = "AcctName")
    protected String acctName;
    @XmlElement(name = "BillLang")
    protected String billLang;
    @XmlElement(name = "DunningFlag")
    protected String dunningFlag;
    @XmlElement(name = "LateFeeChargeable")
    protected String lateFeeChargeable;
    @XmlElement(name = "RedlistFlag")
    protected String redlistFlag;
    @XmlElement(name = "ContactInfo")
    protected Contact contactInfo;
    @XmlElement(name = "FreeBillMedium")
    protected List<FreeBillMedium> freeBillMedium;
    @XmlElement(name = "AcctProperty")
    protected List<SimpleProperty> acctProperty;

    /**
     * Obtiene el valor de la propiedad acctName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctName() {
        return acctName;
    }

    /**
     * Define el valor de la propiedad acctName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctName(String value) {
        this.acctName = value;
    }

    /**
     * Obtiene el valor de la propiedad billLang.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillLang() {
        return billLang;
    }

    /**
     * Define el valor de la propiedad billLang.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillLang(String value) {
        this.billLang = value;
    }

    /**
     * Obtiene el valor de la propiedad dunningFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDunningFlag() {
        return dunningFlag;
    }

    /**
     * Define el valor de la propiedad dunningFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDunningFlag(String value) {
        this.dunningFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad lateFeeChargeable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLateFeeChargeable() {
        return lateFeeChargeable;
    }

    /**
     * Define el valor de la propiedad lateFeeChargeable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLateFeeChargeable(String value) {
        this.lateFeeChargeable = value;
    }

    /**
     * Obtiene el valor de la propiedad redlistFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedlistFlag() {
        return redlistFlag;
    }

    /**
     * Define el valor de la propiedad redlistFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedlistFlag(String value) {
        this.redlistFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad contactInfo.
     * 
     * @return
     *     possible object is
     *     {@link Contact }
     *     
     */
    public Contact getContactInfo() {
        return contactInfo;
    }

    /**
     * Define el valor de la propiedad contactInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link Contact }
     *     
     */
    public void setContactInfo(Contact value) {
        this.contactInfo = value;
    }

    /**
     * Gets the value of the freeBillMedium property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the freeBillMedium property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFreeBillMedium().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FreeBillMedium }
     * 
     * 
     */
    public List<FreeBillMedium> getFreeBillMedium() {
        if (freeBillMedium == null) {
            freeBillMedium = new ArrayList<FreeBillMedium>();
        }
        return this.freeBillMedium;
    }

    /**
     * Gets the value of the acctProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acctProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcctProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getAcctProperty() {
        if (acctProperty == null) {
            acctProperty = new ArrayList<SimpleProperty>();
        }
        return this.acctProperty;
    }

}
