
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;


/**
 * <p>Clase Java para ChangeMemberShortNoRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeMemberShortNoRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode"/>
 *         &lt;element name="GroupMember" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
 *         &lt;element name="OldMemberShortNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewMemberShortNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeMemberShortNoRequest", propOrder = {
    "subGroupAccessCode",
    "groupMember",
    "oldMemberShortNo",
    "newMemberShortNo"
})
public class ChangeMemberShortNoRequest {

    @XmlElement(name = "SubGroupAccessCode", required = true)
    protected SubGroupAccessCode subGroupAccessCode;
    @XmlElement(name = "GroupMember", required = true)
    protected SubAccessCode groupMember;
    @XmlElement(name = "OldMemberShortNo")
    protected String oldMemberShortNo;
    @XmlElement(name = "NewMemberShortNo", required = true)
    protected String newMemberShortNo;

    /**
     * Obtiene el valor de la propiedad subGroupAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public SubGroupAccessCode getSubGroupAccessCode() {
        return subGroupAccessCode;
    }

    /**
     * Define el valor de la propiedad subGroupAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public void setSubGroupAccessCode(SubGroupAccessCode value) {
        this.subGroupAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad groupMember.
     * 
     * @return
     *     possible object is
     *     {@link SubAccessCode }
     *     
     */
    public SubAccessCode getGroupMember() {
        return groupMember;
    }

    /**
     * Define el valor de la propiedad groupMember.
     * 
     * @param value
     *     allowed object is
     *     {@link SubAccessCode }
     *     
     */
    public void setGroupMember(SubAccessCode value) {
        this.groupMember = value;
    }

    /**
     * Obtiene el valor de la propiedad oldMemberShortNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldMemberShortNo() {
        return oldMemberShortNo;
    }

    /**
     * Define el valor de la propiedad oldMemberShortNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldMemberShortNo(String value) {
        this.oldMemberShortNo = value;
    }

    /**
     * Obtiene el valor de la propiedad newMemberShortNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewMemberShortNo() {
        return newMemberShortNo;
    }

    /**
     * Define el valor de la propiedad newMemberShortNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewMemberShortNo(String value) {
        this.newMemberShortNo = value;
    }

}
