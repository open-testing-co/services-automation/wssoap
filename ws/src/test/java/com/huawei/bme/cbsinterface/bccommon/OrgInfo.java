
package com.huawei.bme.cbsinterface.bccommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para OrgInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OrgInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="IDNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="IDValidity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgShortName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgAddressKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="Industry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubIndustry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgFaxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgWebSite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="OrgProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrgInfo", propOrder = {
    "idType",
    "idNumber",
    "idValidity",
    "orgType",
    "orgName",
    "orgShortName",
    "orgLevel",
    "orgAddressKey",
    "orgSize",
    "industry",
    "subIndustry",
    "orgPhoneNumber",
    "orgFaxNumber",
    "orgEmail",
    "orgWebSite",
    "orgProperty"
})
public class OrgInfo {

    @XmlElement(name = "IDType", required = true)
    protected String idType;
    @XmlElement(name = "IDNumber")
    protected String idNumber;
    @XmlElement(name = "IDValidity")
    protected String idValidity;
    @XmlElement(name = "OrgType")
    protected String orgType;
    @XmlElement(name = "OrgName")
    protected String orgName;
    @XmlElement(name = "OrgShortName")
    protected String orgShortName;
    @XmlElement(name = "OrgLevel")
    protected String orgLevel;
    @XmlElement(name = "OrgAddressKey")
    protected String orgAddressKey;
    @XmlElement(name = "OrgSize")
    protected String orgSize;
    @XmlElement(name = "Industry")
    protected String industry;
    @XmlElement(name = "SubIndustry")
    protected String subIndustry;
    @XmlElement(name = "OrgPhoneNumber")
    protected String orgPhoneNumber;
    @XmlElement(name = "OrgFaxNumber")
    protected String orgFaxNumber;
    @XmlElement(name = "OrgEmail")
    protected String orgEmail;
    @XmlElement(name = "OrgWebSite")
    protected String orgWebSite;
    @XmlElement(name = "OrgProperty")
    protected List<SimpleProperty> orgProperty;

    /**
     * Obtiene el valor de la propiedad idType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDType() {
        return idType;
    }

    /**
     * Define el valor de la propiedad idType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDType(String value) {
        this.idType = value;
    }

    /**
     * Obtiene el valor de la propiedad idNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDNumber() {
        return idNumber;
    }

    /**
     * Define el valor de la propiedad idNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDNumber(String value) {
        this.idNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad idValidity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDValidity() {
        return idValidity;
    }

    /**
     * Define el valor de la propiedad idValidity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDValidity(String value) {
        this.idValidity = value;
    }

    /**
     * Obtiene el valor de la propiedad orgType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgType() {
        return orgType;
    }

    /**
     * Define el valor de la propiedad orgType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgType(String value) {
        this.orgType = value;
    }

    /**
     * Obtiene el valor de la propiedad orgName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * Define el valor de la propiedad orgName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgName(String value) {
        this.orgName = value;
    }

    /**
     * Obtiene el valor de la propiedad orgShortName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgShortName() {
        return orgShortName;
    }

    /**
     * Define el valor de la propiedad orgShortName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgShortName(String value) {
        this.orgShortName = value;
    }

    /**
     * Obtiene el valor de la propiedad orgLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgLevel() {
        return orgLevel;
    }

    /**
     * Define el valor de la propiedad orgLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgLevel(String value) {
        this.orgLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad orgAddressKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgAddressKey() {
        return orgAddressKey;
    }

    /**
     * Define el valor de la propiedad orgAddressKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgAddressKey(String value) {
        this.orgAddressKey = value;
    }

    /**
     * Obtiene el valor de la propiedad orgSize.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgSize() {
        return orgSize;
    }

    /**
     * Define el valor de la propiedad orgSize.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgSize(String value) {
        this.orgSize = value;
    }

    /**
     * Obtiene el valor de la propiedad industry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * Define el valor de la propiedad industry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustry(String value) {
        this.industry = value;
    }

    /**
     * Obtiene el valor de la propiedad subIndustry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubIndustry() {
        return subIndustry;
    }

    /**
     * Define el valor de la propiedad subIndustry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubIndustry(String value) {
        this.subIndustry = value;
    }

    /**
     * Obtiene el valor de la propiedad orgPhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgPhoneNumber() {
        return orgPhoneNumber;
    }

    /**
     * Define el valor de la propiedad orgPhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgPhoneNumber(String value) {
        this.orgPhoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad orgFaxNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgFaxNumber() {
        return orgFaxNumber;
    }

    /**
     * Define el valor de la propiedad orgFaxNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgFaxNumber(String value) {
        this.orgFaxNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad orgEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgEmail() {
        return orgEmail;
    }

    /**
     * Define el valor de la propiedad orgEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgEmail(String value) {
        this.orgEmail = value;
    }

    /**
     * Obtiene el valor de la propiedad orgWebSite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgWebSite() {
        return orgWebSite;
    }

    /**
     * Define el valor de la propiedad orgWebSite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgWebSite(String value) {
        this.orgWebSite = value;
    }

    /**
     * Gets the value of the orgProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orgProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrgProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getOrgProperty() {
        if (orgProperty == null) {
            orgProperty = new ArrayList<SimpleProperty>();
        }
        return this.orgProperty;
    }

}
