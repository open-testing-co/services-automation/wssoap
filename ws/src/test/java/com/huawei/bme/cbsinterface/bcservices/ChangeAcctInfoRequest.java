
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.AccountBasicInfo;
import com.huawei.bme.cbsinterface.bccommon.Address;
import com.huawei.bme.cbsinterface.bccommon.AutoPayChannelInfo;


/**
 * <p>Clase Java para ChangeAcctInfoRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeAcctInfoRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctAccessCode">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
 *                 &lt;sequence>
 *                   &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AcctBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountBasicInfo" minOccurs="0"/>
 *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
 *         &lt;element name="AcctPayMethod" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OldPayMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NewPayMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AddAutoPayChannel" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AutoPayChannelInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="DelAutoPayChannel" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeAcctInfoRequest", propOrder = {
    "acctAccessCode",
    "acctBasicInfo",
    "addressInfo",
    "acctPayMethod"
})
public class ChangeAcctInfoRequest {

    @XmlElement(name = "AcctAccessCode", required = true)
    protected ChangeAcctInfoRequest.AcctAccessCode acctAccessCode;
    @XmlElement(name = "AcctBasicInfo")
    protected AccountBasicInfo acctBasicInfo;
    @XmlElement(name = "AddressInfo")
    protected Address addressInfo;
    @XmlElement(name = "AcctPayMethod")
    protected ChangeAcctInfoRequest.AcctPayMethod acctPayMethod;

    /**
     * Obtiene el valor de la propiedad acctAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctInfoRequest.AcctAccessCode }
     *     
     */
    public ChangeAcctInfoRequest.AcctAccessCode getAcctAccessCode() {
        return acctAccessCode;
    }

    /**
     * Define el valor de la propiedad acctAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctInfoRequest.AcctAccessCode }
     *     
     */
    public void setAcctAccessCode(ChangeAcctInfoRequest.AcctAccessCode value) {
        this.acctAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad acctBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link AccountBasicInfo }
     *     
     */
    public AccountBasicInfo getAcctBasicInfo() {
        return acctBasicInfo;
    }

    /**
     * Define el valor de la propiedad acctBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBasicInfo }
     *     
     */
    public void setAcctBasicInfo(AccountBasicInfo value) {
        this.acctBasicInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad addressInfo.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddressInfo() {
        return addressInfo;
    }

    /**
     * Define el valor de la propiedad addressInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddressInfo(Address value) {
        this.addressInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad acctPayMethod.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctInfoRequest.AcctPayMethod }
     *     
     */
    public ChangeAcctInfoRequest.AcctPayMethod getAcctPayMethod() {
        return acctPayMethod;
    }

    /**
     * Define el valor de la propiedad acctPayMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctInfoRequest.AcctPayMethod }
     *     
     */
    public void setAcctPayMethod(ChangeAcctInfoRequest.AcctPayMethod value) {
        this.acctPayMethod = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AcctAccessCode">
     *       &lt;sequence>
     *         &lt;element name="PayType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "payType"
    })
    public static class AcctAccessCode
        extends com.huawei.bme.cbsinterface.bccommon.AcctAccessCode
    {

        @XmlElement(name = "PayType")
        protected String payType;

        /**
         * Obtiene el valor de la propiedad payType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayType() {
            return payType;
        }

        /**
         * Define el valor de la propiedad payType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayType(String value) {
            this.payType = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OldPayMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NewPayMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AddAutoPayChannel" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AutoPayChannelInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="DelAutoPayChannel" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "oldPayMethod",
        "newPayMethod",
        "addAutoPayChannel",
        "delAutoPayChannel"
    })
    public static class AcctPayMethod {

        @XmlElement(name = "OldPayMethod", required = true)
        protected String oldPayMethod;
        @XmlElement(name = "NewPayMethod", required = true)
        protected String newPayMethod;
        @XmlElement(name = "AddAutoPayChannel")
        protected List<ChangeAcctInfoRequest.AcctPayMethod.AddAutoPayChannel> addAutoPayChannel;
        @XmlElement(name = "DelAutoPayChannel")
        protected List<ChangeAcctInfoRequest.AcctPayMethod.DelAutoPayChannel> delAutoPayChannel;

        /**
         * Obtiene el valor de la propiedad oldPayMethod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOldPayMethod() {
            return oldPayMethod;
        }

        /**
         * Define el valor de la propiedad oldPayMethod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOldPayMethod(String value) {
            this.oldPayMethod = value;
        }

        /**
         * Obtiene el valor de la propiedad newPayMethod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewPayMethod() {
            return newPayMethod;
        }

        /**
         * Define el valor de la propiedad newPayMethod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewPayMethod(String value) {
            this.newPayMethod = value;
        }

        /**
         * Gets the value of the addAutoPayChannel property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addAutoPayChannel property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddAutoPayChannel().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeAcctInfoRequest.AcctPayMethod.AddAutoPayChannel }
         * 
         * 
         */
        public List<ChangeAcctInfoRequest.AcctPayMethod.AddAutoPayChannel> getAddAutoPayChannel() {
            if (addAutoPayChannel == null) {
                addAutoPayChannel = new ArrayList<ChangeAcctInfoRequest.AcctPayMethod.AddAutoPayChannel>();
            }
            return this.addAutoPayChannel;
        }

        /**
         * Gets the value of the delAutoPayChannel property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the delAutoPayChannel property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDelAutoPayChannel().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeAcctInfoRequest.AcctPayMethod.DelAutoPayChannel }
         * 
         * 
         */
        public List<ChangeAcctInfoRequest.AcctPayMethod.DelAutoPayChannel> getDelAutoPayChannel() {
            if (delAutoPayChannel == null) {
                delAutoPayChannel = new ArrayList<ChangeAcctInfoRequest.AcctPayMethod.DelAutoPayChannel>();
            }
            return this.delAutoPayChannel;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AutoPayChannelInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "autoPayChannelKey",
            "autoPayChannelInfo"
        })
        public static class AddAutoPayChannel {

            @XmlElement(name = "AutoPayChannelKey", required = true)
            protected String autoPayChannelKey;
            @XmlElement(name = "AutoPayChannelInfo", required = true)
            protected AutoPayChannelInfo autoPayChannelInfo;

            /**
             * Obtiene el valor de la propiedad autoPayChannelKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAutoPayChannelKey() {
                return autoPayChannelKey;
            }

            /**
             * Define el valor de la propiedad autoPayChannelKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAutoPayChannelKey(String value) {
                this.autoPayChannelKey = value;
            }

            /**
             * Obtiene el valor de la propiedad autoPayChannelInfo.
             * 
             * @return
             *     possible object is
             *     {@link AutoPayChannelInfo }
             *     
             */
            public AutoPayChannelInfo getAutoPayChannelInfo() {
                return autoPayChannelInfo;
            }

            /**
             * Define el valor de la propiedad autoPayChannelInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link AutoPayChannelInfo }
             *     
             */
            public void setAutoPayChannelInfo(AutoPayChannelInfo value) {
                this.autoPayChannelInfo = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "autoPayChannelKey"
        })
        public static class DelAutoPayChannel {

            @XmlElement(name = "AutoPayChannelKey", required = true)
            protected String autoPayChannelKey;

            /**
             * Obtiene el valor de la propiedad autoPayChannelKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAutoPayChannelKey() {
                return autoPayChannelKey;
            }

            /**
             * Define el valor de la propiedad autoPayChannelKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAutoPayChannelKey(String value) {
                this.autoPayChannelKey = value;
            }

        }

    }

}
