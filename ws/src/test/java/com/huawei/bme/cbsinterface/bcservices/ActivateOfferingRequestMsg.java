
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader" form="unqualified"/>
 *         &lt;element name="ActivateOfferingRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ActivateOfferingRequest" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "activateOfferingRequest"
})
@XmlRootElement(name = "ActivateOfferingRequestMsg")
public class ActivateOfferingRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "ActivateOfferingRequest", namespace = "", required = true)
    protected ActivateOfferingRequest activateOfferingRequest;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad activateOfferingRequest.
     * 
     * @return
     *     possible object is
     *     {@link ActivateOfferingRequest }
     *     
     */
    public ActivateOfferingRequest getActivateOfferingRequest() {
        return activateOfferingRequest;
    }

    /**
     * Define el valor de la propiedad activateOfferingRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivateOfferingRequest }
     *     
     */
    public void setActivateOfferingRequest(ActivateOfferingRequest value) {
        this.activateOfferingRequest = value;
    }

}
