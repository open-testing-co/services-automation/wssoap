
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryAccumulationUsageResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryAccumulationUsageResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccmUsageList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AccmType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AccmTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="UnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="MeasureID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="BeginDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryAccumulationUsageResult", propOrder = {
    "accmUsageList"
})
public class QueryAccumulationUsageResult {

    @XmlElement(name = "AccmUsageList")
    protected List<QueryAccumulationUsageResult.AccmUsageList> accmUsageList;

    /**
     * Gets the value of the accmUsageList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accmUsageList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccmUsageList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryAccumulationUsageResult.AccmUsageList }
     * 
     * 
     */
    public List<QueryAccumulationUsageResult.AccmUsageList> getAccmUsageList() {
        if (accmUsageList == null) {
            accmUsageList = new ArrayList<QueryAccumulationUsageResult.AccmUsageList>();
        }
        return this.accmUsageList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AccmType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AccmTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="UnitType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="MeasureID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="BeginDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accmType",
        "accmTypeName",
        "amount",
        "unitType",
        "currencyID",
        "measureID",
        "beginDate",
        "endDate"
    })
    public static class AccmUsageList {

        @XmlElement(name = "AccmType", required = true)
        protected String accmType;
        @XmlElement(name = "AccmTypeName", required = true)
        protected String accmTypeName;
        @XmlElement(name = "Amount")
        protected long amount;
        @XmlElement(name = "UnitType", required = true)
        protected String unitType;
        @XmlElement(name = "CurrencyID")
        protected BigInteger currencyID;
        @XmlElement(name = "MeasureID")
        protected BigInteger measureID;
        @XmlElement(name = "BeginDate", required = true)
        protected String beginDate;
        @XmlElement(name = "EndDate", required = true)
        protected String endDate;

        /**
         * Obtiene el valor de la propiedad accmType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccmType() {
            return accmType;
        }

        /**
         * Define el valor de la propiedad accmType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccmType(String value) {
            this.accmType = value;
        }

        /**
         * Obtiene el valor de la propiedad accmTypeName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccmTypeName() {
            return accmTypeName;
        }

        /**
         * Define el valor de la propiedad accmTypeName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccmTypeName(String value) {
            this.accmTypeName = value;
        }

        /**
         * Obtiene el valor de la propiedad amount.
         * 
         */
        public long getAmount() {
            return amount;
        }

        /**
         * Define el valor de la propiedad amount.
         * 
         */
        public void setAmount(long value) {
            this.amount = value;
        }

        /**
         * Obtiene el valor de la propiedad unitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitType() {
            return unitType;
        }

        /**
         * Define el valor de la propiedad unitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitType(String value) {
            this.unitType = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Obtiene el valor de la propiedad measureID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getMeasureID() {
            return measureID;
        }

        /**
         * Define el valor de la propiedad measureID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setMeasureID(BigInteger value) {
            this.measureID = value;
        }

        /**
         * Obtiene el valor de la propiedad beginDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBeginDate() {
            return beginDate;
        }

        /**
         * Define el valor de la propiedad beginDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBeginDate(String value) {
            this.beginDate = value;
        }

        /**
         * Obtiene el valor de la propiedad endDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEndDate() {
            return endDate;
        }

        /**
         * Define el valor de la propiedad endDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEndDate(String value) {
            this.endDate = value;
        }

    }

}
