
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.AccountBasicInfo;
import com.huawei.bme.cbsinterface.bccommon.Address;
import com.huawei.bme.cbsinterface.bccommon.CustBasicInfo;
import com.huawei.bme.cbsinterface.bccommon.CustInfo;
import com.huawei.bme.cbsinterface.bccommon.IndividualInfo;
import com.huawei.bme.cbsinterface.bccommon.OrgInfo;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;
import com.huawei.bme.cbsinterface.bccommon.SubBasicInfo;


/**
 * <p>Clase Java para SupplementProfileRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SupplementProfileRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
 *         &lt;element name="NewOwnership" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustomerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AccountKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SubBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubBasicInfo" minOccurs="0"/>
 *         &lt;element name="RegisterCustomer" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustBasicInfo" minOccurs="0"/>
 *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UserCustomer" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
 *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Account" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AcctBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountBasicInfo" minOccurs="0"/>
 *                   &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                   &lt;element name="InitBalance" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="CreditLimit" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AcctPayMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="AutoPayChannelInfo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
 *                                     &lt;sequence>
 *                                     &lt;/sequence>
 *                                   &lt;/extension>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddressInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}Address">
 *                 &lt;sequence>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SupplementProfileRequest", propOrder = {
    "subAccessCode",
    "newOwnership",
    "subBasicInfo",
    "registerCustomer",
    "userCustomer",
    "account",
    "addressInfo"
})
public class SupplementProfileRequest {

    @XmlElement(name = "SubAccessCode", required = true)
    protected SubAccessCode subAccessCode;
    @XmlElement(name = "NewOwnership")
    protected SupplementProfileRequest.NewOwnership newOwnership;
    @XmlElement(name = "SubBasicInfo")
    protected SubBasicInfo subBasicInfo;
    @XmlElement(name = "RegisterCustomer")
    protected SupplementProfileRequest.RegisterCustomer registerCustomer;
    @XmlElement(name = "UserCustomer")
    protected SupplementProfileRequest.UserCustomer userCustomer;
    @XmlElement(name = "Account")
    protected List<SupplementProfileRequest.Account> account;
    @XmlElement(name = "AddressInfo")
    protected List<SupplementProfileRequest.AddressInfo> addressInfo;

    /**
     * Obtiene el valor de la propiedad subAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubAccessCode }
     *     
     */
    public SubAccessCode getSubAccessCode() {
        return subAccessCode;
    }

    /**
     * Define el valor de la propiedad subAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubAccessCode }
     *     
     */
    public void setSubAccessCode(SubAccessCode value) {
        this.subAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad newOwnership.
     * 
     * @return
     *     possible object is
     *     {@link SupplementProfileRequest.NewOwnership }
     *     
     */
    public SupplementProfileRequest.NewOwnership getNewOwnership() {
        return newOwnership;
    }

    /**
     * Define el valor de la propiedad newOwnership.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplementProfileRequest.NewOwnership }
     *     
     */
    public void setNewOwnership(SupplementProfileRequest.NewOwnership value) {
        this.newOwnership = value;
    }

    /**
     * Obtiene el valor de la propiedad subBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link SubBasicInfo }
     *     
     */
    public SubBasicInfo getSubBasicInfo() {
        return subBasicInfo;
    }

    /**
     * Define el valor de la propiedad subBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SubBasicInfo }
     *     
     */
    public void setSubBasicInfo(SubBasicInfo value) {
        this.subBasicInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad registerCustomer.
     * 
     * @return
     *     possible object is
     *     {@link SupplementProfileRequest.RegisterCustomer }
     *     
     */
    public SupplementProfileRequest.RegisterCustomer getRegisterCustomer() {
        return registerCustomer;
    }

    /**
     * Define el valor de la propiedad registerCustomer.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplementProfileRequest.RegisterCustomer }
     *     
     */
    public void setRegisterCustomer(SupplementProfileRequest.RegisterCustomer value) {
        this.registerCustomer = value;
    }

    /**
     * Obtiene el valor de la propiedad userCustomer.
     * 
     * @return
     *     possible object is
     *     {@link SupplementProfileRequest.UserCustomer }
     *     
     */
    public SupplementProfileRequest.UserCustomer getUserCustomer() {
        return userCustomer;
    }

    /**
     * Define el valor de la propiedad userCustomer.
     * 
     * @param value
     *     allowed object is
     *     {@link SupplementProfileRequest.UserCustomer }
     *     
     */
    public void setUserCustomer(SupplementProfileRequest.UserCustomer value) {
        this.userCustomer = value;
    }

    /**
     * Gets the value of the account property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the account property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccount().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupplementProfileRequest.Account }
     * 
     * 
     */
    public List<SupplementProfileRequest.Account> getAccount() {
        if (account == null) {
            account = new ArrayList<SupplementProfileRequest.Account>();
        }
        return this.account;
    }

    /**
     * Gets the value of the addressInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addressInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddressInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupplementProfileRequest.AddressInfo }
     * 
     * 
     */
    public List<SupplementProfileRequest.AddressInfo> getAddressInfo() {
        if (addressInfo == null) {
            addressInfo = new ArrayList<SupplementProfileRequest.AddressInfo>();
        }
        return this.addressInfo;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AcctBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountBasicInfo" minOccurs="0"/>
     *         &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *         &lt;element name="InitBalance" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="CreditLimit" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AcctPayMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AutoPayChannelInfo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
     *                           &lt;sequence>
     *                           &lt;/sequence>
     *                         &lt;/extension>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "acctKey",
        "acctBasicInfo",
        "billCycleType",
        "currencyID",
        "initBalance",
        "creditLimit",
        "acctPayMethod",
        "autoPayChannel"
    })
    public static class Account {

        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "AcctBasicInfo")
        protected AccountBasicInfo acctBasicInfo;
        @XmlElement(name = "BillCycleType")
        protected String billCycleType;
        @XmlElement(name = "CurrencyID")
        protected BigInteger currencyID;
        @XmlElement(name = "InitBalance")
        protected Long initBalance;
        @XmlElement(name = "CreditLimit")
        protected List<SupplementProfileRequest.Account.CreditLimit> creditLimit;
        @XmlElement(name = "AcctPayMethod")
        protected String acctPayMethod;
        @XmlElement(name = "AutoPayChannel")
        protected List<SupplementProfileRequest.Account.AutoPayChannel> autoPayChannel;

        /**
         * Obtiene el valor de la propiedad acctKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Define el valor de la propiedad acctKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Obtiene el valor de la propiedad acctBasicInfo.
         * 
         * @return
         *     possible object is
         *     {@link AccountBasicInfo }
         *     
         */
        public AccountBasicInfo getAcctBasicInfo() {
            return acctBasicInfo;
        }

        /**
         * Define el valor de la propiedad acctBasicInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AccountBasicInfo }
         *     
         */
        public void setAcctBasicInfo(AccountBasicInfo value) {
            this.acctBasicInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad billCycleType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBillCycleType() {
            return billCycleType;
        }

        /**
         * Define el valor de la propiedad billCycleType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBillCycleType(String value) {
            this.billCycleType = value;
        }

        /**
         * Obtiene el valor de la propiedad currencyID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getCurrencyID() {
            return currencyID;
        }

        /**
         * Define el valor de la propiedad currencyID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setCurrencyID(BigInteger value) {
            this.currencyID = value;
        }

        /**
         * Obtiene el valor de la propiedad initBalance.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getInitBalance() {
            return initBalance;
        }

        /**
         * Define el valor de la propiedad initBalance.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setInitBalance(Long value) {
            this.initBalance = value;
        }

        /**
         * Gets the value of the creditLimit property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the creditLimit property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCreditLimit().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SupplementProfileRequest.Account.CreditLimit }
         * 
         * 
         */
        public List<SupplementProfileRequest.Account.CreditLimit> getCreditLimit() {
            if (creditLimit == null) {
                creditLimit = new ArrayList<SupplementProfileRequest.Account.CreditLimit>();
            }
            return this.creditLimit;
        }

        /**
         * Obtiene el valor de la propiedad acctPayMethod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctPayMethod() {
            return acctPayMethod;
        }

        /**
         * Define el valor de la propiedad acctPayMethod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctPayMethod(String value) {
            this.acctPayMethod = value;
        }

        /**
         * Gets the value of the autoPayChannel property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the autoPayChannel property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAutoPayChannel().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SupplementProfileRequest.Account.AutoPayChannel }
         * 
         * 
         */
        public List<SupplementProfileRequest.Account.AutoPayChannel> getAutoPayChannel() {
            if (autoPayChannel == null) {
                autoPayChannel = new ArrayList<SupplementProfileRequest.Account.AutoPayChannel>();
            }
            return this.autoPayChannel;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AutoPayChannelInfo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
         *                 &lt;sequence>
         *                 &lt;/sequence>
         *               &lt;/extension>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "autoPayChannelKey",
            "autoPayChannelInfo"
        })
        public static class AutoPayChannel {

            @XmlElement(name = "AutoPayChannelKey", required = true)
            protected String autoPayChannelKey;
            @XmlElement(name = "AutoPayChannelInfo", required = true)
            protected SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo autoPayChannelInfo;

            /**
             * Obtiene el valor de la propiedad autoPayChannelKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAutoPayChannelKey() {
                return autoPayChannelKey;
            }

            /**
             * Define el valor de la propiedad autoPayChannelKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAutoPayChannelKey(String value) {
                this.autoPayChannelKey = value;
            }

            /**
             * Obtiene el valor de la propiedad autoPayChannelInfo.
             * 
             * @return
             *     possible object is
             *     {@link SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo }
             *     
             */
            public SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo getAutoPayChannelInfo() {
                return autoPayChannelInfo;
            }

            /**
             * Define el valor de la propiedad autoPayChannelInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo }
             *     
             */
            public void setAutoPayChannelInfo(SupplementProfileRequest.Account.AutoPayChannel.AutoPayChannelInfo value) {
                this.autoPayChannelInfo = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
             *       &lt;sequence>
             *       &lt;/sequence>
             *     &lt;/extension>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class AutoPayChannelInfo
                extends com.huawei.bme.cbsinterface.bccommon.AutoPayChannelInfo
            {


            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "limitType",
            "limitValue"
        })
        public static class CreditLimit {

            @XmlElement(name = "LimitType")
            protected String limitType;
            @XmlElement(name = "LimitValue")
            protected long limitValue;

            /**
             * Obtiene el valor de la propiedad limitType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLimitType() {
                return limitType;
            }

            /**
             * Define el valor de la propiedad limitType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLimitType(String value) {
                this.limitType = value;
            }

            /**
             * Obtiene el valor de la propiedad limitValue.
             * 
             */
            public long getLimitValue() {
                return limitValue;
            }

            /**
             * Define el valor de la propiedad limitValue.
             * 
             */
            public void setLimitValue(long value) {
                this.limitValue = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}Address">
     *       &lt;sequence>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class AddressInfo
        extends Address
    {


    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustomerKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AccountKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customerKey",
        "accountKey"
    })
    public static class NewOwnership {

        @XmlElement(name = "CustomerKey", required = true)
        protected String customerKey;
        @XmlElement(name = "AccountKey")
        protected String accountKey;

        /**
         * Obtiene el valor de la propiedad customerKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerKey() {
            return customerKey;
        }

        /**
         * Define el valor de la propiedad customerKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerKey(String value) {
            this.customerKey = value;
        }

        /**
         * Obtiene el valor de la propiedad accountKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAccountKey() {
            return accountKey;
        }

        /**
         * Define el valor de la propiedad accountKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAccountKey(String value) {
            this.accountKey = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustBasicInfo" minOccurs="0"/>
     *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custBasicInfo",
        "individualInfo",
        "orgInfo"
    })
    public static class RegisterCustomer {

        @XmlElement(name = "CustBasicInfo")
        protected CustBasicInfo custBasicInfo;
        @XmlElement(name = "IndividualInfo")
        protected IndividualInfo individualInfo;
        @XmlElement(name = "OrgInfo")
        protected OrgInfo orgInfo;

        /**
         * Obtiene el valor de la propiedad custBasicInfo.
         * 
         * @return
         *     possible object is
         *     {@link CustBasicInfo }
         *     
         */
        public CustBasicInfo getCustBasicInfo() {
            return custBasicInfo;
        }

        /**
         * Define el valor de la propiedad custBasicInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link CustBasicInfo }
         *     
         */
        public void setCustBasicInfo(CustBasicInfo value) {
            this.custBasicInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad individualInfo.
         * 
         * @return
         *     possible object is
         *     {@link IndividualInfo }
         *     
         */
        public IndividualInfo getIndividualInfo() {
            return individualInfo;
        }

        /**
         * Define el valor de la propiedad individualInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link IndividualInfo }
         *     
         */
        public void setIndividualInfo(IndividualInfo value) {
            this.individualInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad orgInfo.
         * 
         * @return
         *     possible object is
         *     {@link OrgInfo }
         *     
         */
        public OrgInfo getOrgInfo() {
            return orgInfo;
        }

        /**
         * Define el valor de la propiedad orgInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgInfo }
         *     
         */
        public void setOrgInfo(OrgInfo value) {
            this.orgInfo = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
     *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custKey",
        "custInfo",
        "individualInfo",
        "orgInfo"
    })
    public static class UserCustomer {

        @XmlElement(name = "CustKey", required = true)
        protected String custKey;
        @XmlElement(name = "CustInfo")
        protected CustInfo custInfo;
        @XmlElement(name = "IndividualInfo")
        protected IndividualInfo individualInfo;
        @XmlElement(name = "OrgInfo")
        protected OrgInfo orgInfo;

        /**
         * Obtiene el valor de la propiedad custKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustKey() {
            return custKey;
        }

        /**
         * Define el valor de la propiedad custKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustKey(String value) {
            this.custKey = value;
        }

        /**
         * Obtiene el valor de la propiedad custInfo.
         * 
         * @return
         *     possible object is
         *     {@link CustInfo }
         *     
         */
        public CustInfo getCustInfo() {
            return custInfo;
        }

        /**
         * Define el valor de la propiedad custInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInfo }
         *     
         */
        public void setCustInfo(CustInfo value) {
            this.custInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad individualInfo.
         * 
         * @return
         *     possible object is
         *     {@link IndividualInfo }
         *     
         */
        public IndividualInfo getIndividualInfo() {
            return individualInfo;
        }

        /**
         * Define el valor de la propiedad individualInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link IndividualInfo }
         *     
         */
        public void setIndividualInfo(IndividualInfo value) {
            this.individualInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad orgInfo.
         * 
         * @return
         *     possible object is
         *     {@link OrgInfo }
         *     
         */
        public OrgInfo getOrgInfo() {
            return orgInfo;
        }

        /**
         * Define el valor de la propiedad orgInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgInfo }
         *     
         */
        public void setOrgInfo(OrgInfo value) {
            this.orgInfo = value;
        }

    }

}
