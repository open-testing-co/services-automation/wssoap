
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader" form="unqualified"/>
 *         &lt;element name="QueryOfferingInstPropertyResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryOfferingInstPropertyResult" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "queryOfferingInstPropertyResult"
})
@XmlRootElement(name = "QueryOfferingInstPropertyResultMsg")
public class QueryOfferingInstPropertyResultMsg {

    @XmlElement(name = "ResultHeader", namespace = "", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "QueryOfferingInstPropertyResult", namespace = "", required = true)
    protected QueryOfferingInstPropertyResult queryOfferingInstPropertyResult;

    /**
     * Obtiene el valor de la propiedad resultHeader.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Define el valor de la propiedad resultHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad queryOfferingInstPropertyResult.
     * 
     * @return
     *     possible object is
     *     {@link QueryOfferingInstPropertyResult }
     *     
     */
    public QueryOfferingInstPropertyResult getQueryOfferingInstPropertyResult() {
        return queryOfferingInstPropertyResult;
    }

    /**
     * Define el valor de la propiedad queryOfferingInstPropertyResult.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryOfferingInstPropertyResult }
     *     
     */
    public void setQueryOfferingInstPropertyResult(QueryOfferingInstPropertyResult value) {
        this.queryOfferingInstPropertyResult = value;
    }

}
