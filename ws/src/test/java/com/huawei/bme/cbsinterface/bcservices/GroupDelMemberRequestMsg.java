
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.cbscommon.RequestHeader;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}RequestHeader" form="unqualified"/>
 *         &lt;element name="GroupDelMemberRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}GroupDelMemberRequest" form="unqualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "groupDelMemberRequest"
})
@XmlRootElement(name = "GroupDelMemberRequestMsg")
public class GroupDelMemberRequestMsg {

    @XmlElement(name = "RequestHeader", namespace = "", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "GroupDelMemberRequest", namespace = "", required = true)
    protected GroupDelMemberRequest groupDelMemberRequest;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad groupDelMemberRequest.
     * 
     * @return
     *     possible object is
     *     {@link GroupDelMemberRequest }
     *     
     */
    public GroupDelMemberRequest getGroupDelMemberRequest() {
        return groupDelMemberRequest;
    }

    /**
     * Define el valor de la propiedad groupDelMemberRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDelMemberRequest }
     *     
     */
    public void setGroupDelMemberRequest(GroupDelMemberRequest value) {
        this.groupDelMemberRequest = value;
    }

}
