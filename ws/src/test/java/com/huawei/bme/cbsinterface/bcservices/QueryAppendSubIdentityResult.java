
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryAppendSubIdentityResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryAppendSubIdentityResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubIdentityList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="IMSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryAppendSubIdentityResult", propOrder = {
    "subIdentityList"
})
public class QueryAppendSubIdentityResult {

    @XmlElement(name = "SubIdentityList")
    protected List<QueryAppendSubIdentityResult.SubIdentityList> subIdentityList;

    /**
     * Gets the value of the subIdentityList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subIdentityList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubIdentityList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryAppendSubIdentityResult.SubIdentityList }
     * 
     * 
     */
    public List<QueryAppendSubIdentityResult.SubIdentityList> getSubIdentityList() {
        if (subIdentityList == null) {
            subIdentityList = new ArrayList<QueryAppendSubIdentityResult.SubIdentityList>();
        }
        return this.subIdentityList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="IMSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subIdentity",
        "imsi",
        "subIdentityType"
    })
    public static class SubIdentityList {

        @XmlElement(name = "SubIdentity", required = true)
        protected String subIdentity;
        @XmlElement(name = "IMSI")
        protected String imsi;
        @XmlElement(name = "SubIdentityType")
        protected Integer subIdentityType;

        /**
         * Obtiene el valor de la propiedad subIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubIdentity() {
            return subIdentity;
        }

        /**
         * Define el valor de la propiedad subIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubIdentity(String value) {
            this.subIdentity = value;
        }

        /**
         * Obtiene el valor de la propiedad imsi.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIMSI() {
            return imsi;
        }

        /**
         * Define el valor de la propiedad imsi.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIMSI(String value) {
            this.imsi = value;
        }

        /**
         * Obtiene el valor de la propiedad subIdentityType.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSubIdentityType() {
            return subIdentityType;
        }

        /**
         * Define el valor de la propiedad subIdentityType.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSubIdentityType(Integer value) {
            this.subIdentityType = value;
        }

    }

}
