
package com.huawei.bme.cbsinterface.bccommon;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SubGroupMember complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubGroupMember">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberShortNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubGrpMemberInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupMemberInfo" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubGroupMember", propOrder = {
    "memberShortNo",
    "subGrpMemberInfo"
})
public class SubGroupMember {

    @XmlElement(name = "MemberShortNo")
    protected String memberShortNo;
    @XmlElement(name = "SubGrpMemberInfo")
    protected SubGroupMemberInfo subGrpMemberInfo;

    /**
     * Obtiene el valor de la propiedad memberShortNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberShortNo() {
        return memberShortNo;
    }

    /**
     * Define el valor de la propiedad memberShortNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberShortNo(String value) {
        this.memberShortNo = value;
    }

    /**
     * Obtiene el valor de la propiedad subGrpMemberInfo.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupMemberInfo }
     *     
     */
    public SubGroupMemberInfo getSubGrpMemberInfo() {
        return subGrpMemberInfo;
    }

    /**
     * Define el valor de la propiedad subGrpMemberInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupMemberInfo }
     *     
     */
    public void setSubGrpMemberInfo(SubGroupMemberInfo value) {
        this.subGrpMemberInfo = value;
    }

}
