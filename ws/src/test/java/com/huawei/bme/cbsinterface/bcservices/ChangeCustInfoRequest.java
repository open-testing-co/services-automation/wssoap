
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.Address;
import com.huawei.bme.cbsinterface.bccommon.CustAccessCode;
import com.huawei.bme.cbsinterface.bccommon.CustBasicInfo;
import com.huawei.bme.cbsinterface.bccommon.IndividualInfo;
import com.huawei.bme.cbsinterface.bccommon.OrgInfo;


/**
 * <p>Clase Java para ChangeCustInfoRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeCustInfoRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustAccessCode"/>
 *         &lt;element name="CustInfo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustBasicInfo" minOccurs="0"/>
 *                   &lt;element name="Individual" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                   &lt;element name="Organization" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeCustInfoRequest", propOrder = {
    "custAccessCode",
    "custInfo",
    "addressInfo"
})
public class ChangeCustInfoRequest {

    @XmlElement(name = "CustAccessCode", required = true)
    protected CustAccessCode custAccessCode;
    @XmlElement(name = "CustInfo", required = true)
    protected ChangeCustInfoRequest.CustInfo custInfo;
    @XmlElement(name = "AddressInfo")
    protected Address addressInfo;

    /**
     * Obtiene el valor de la propiedad custAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link CustAccessCode }
     *     
     */
    public CustAccessCode getCustAccessCode() {
        return custAccessCode;
    }

    /**
     * Define el valor de la propiedad custAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link CustAccessCode }
     *     
     */
    public void setCustAccessCode(CustAccessCode value) {
        this.custAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad custInfo.
     * 
     * @return
     *     possible object is
     *     {@link ChangeCustInfoRequest.CustInfo }
     *     
     */
    public ChangeCustInfoRequest.CustInfo getCustInfo() {
        return custInfo;
    }

    /**
     * Define el valor de la propiedad custInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeCustInfoRequest.CustInfo }
     *     
     */
    public void setCustInfo(ChangeCustInfoRequest.CustInfo value) {
        this.custInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad addressInfo.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getAddressInfo() {
        return addressInfo;
    }

    /**
     * Define el valor de la propiedad addressInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setAddressInfo(Address value) {
        this.addressInfo = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustBasicInfo" minOccurs="0"/>
     *         &lt;element name="Individual" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *         &lt;element name="Organization" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custBasicInfo",
        "individual",
        "organization"
    })
    public static class CustInfo {

        @XmlElement(name = "CustBasicInfo")
        protected CustBasicInfo custBasicInfo;
        @XmlElement(name = "Individual")
        protected IndividualInfo individual;
        @XmlElement(name = "Organization")
        protected OrgInfo organization;

        /**
         * Obtiene el valor de la propiedad custBasicInfo.
         * 
         * @return
         *     possible object is
         *     {@link CustBasicInfo }
         *     
         */
        public CustBasicInfo getCustBasicInfo() {
            return custBasicInfo;
        }

        /**
         * Define el valor de la propiedad custBasicInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link CustBasicInfo }
         *     
         */
        public void setCustBasicInfo(CustBasicInfo value) {
            this.custBasicInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad individual.
         * 
         * @return
         *     possible object is
         *     {@link IndividualInfo }
         *     
         */
        public IndividualInfo getIndividual() {
            return individual;
        }

        /**
         * Define el valor de la propiedad individual.
         * 
         * @param value
         *     allowed object is
         *     {@link IndividualInfo }
         *     
         */
        public void setIndividual(IndividualInfo value) {
            this.individual = value;
        }

        /**
         * Obtiene el valor de la propiedad organization.
         * 
         * @return
         *     possible object is
         *     {@link OrgInfo }
         *     
         */
        public OrgInfo getOrganization() {
            return organization;
        }

        /**
         * Define el valor de la propiedad organization.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgInfo }
         *     
         */
        public void setOrganization(OrgInfo value) {
            this.organization = value;
        }

    }

}
