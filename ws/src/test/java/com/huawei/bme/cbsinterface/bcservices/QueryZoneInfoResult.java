
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para QueryZoneInfoResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryZoneInfoResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ZoneList" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ZoneID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="ZoneName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ZoneType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryZoneInfoResult", propOrder = {
    "zoneList"
})
public class QueryZoneInfoResult {

    @XmlElement(name = "ZoneList")
    protected List<QueryZoneInfoResult.ZoneList> zoneList;

    /**
     * Gets the value of the zoneList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zoneList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZoneList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QueryZoneInfoResult.ZoneList }
     * 
     * 
     */
    public List<QueryZoneInfoResult.ZoneList> getZoneList() {
        if (zoneList == null) {
            zoneList = new ArrayList<QueryZoneInfoResult.ZoneList>();
        }
        return this.zoneList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ZoneID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="ZoneName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ZoneType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "zoneID",
        "zoneName",
        "zoneType"
    })
    public static class ZoneList {

        @XmlElement(name = "ZoneID", required = true)
        protected BigInteger zoneID;
        @XmlElement(name = "ZoneName", required = true)
        protected String zoneName;
        @XmlElement(name = "ZoneType", required = true)
        protected String zoneType;

        /**
         * Obtiene el valor de la propiedad zoneID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getZoneID() {
            return zoneID;
        }

        /**
         * Define el valor de la propiedad zoneID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setZoneID(BigInteger value) {
            this.zoneID = value;
        }

        /**
         * Obtiene el valor de la propiedad zoneName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZoneName() {
            return zoneName;
        }

        /**
         * Define el valor de la propiedad zoneName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZoneName(String value) {
            this.zoneName = value;
        }

        /**
         * Obtiene el valor de la propiedad zoneType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getZoneType() {
            return zoneType;
        }

        /**
         * Define el valor de la propiedad zoneType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setZoneType(String value) {
            this.zoneType = value;
        }

    }

}
