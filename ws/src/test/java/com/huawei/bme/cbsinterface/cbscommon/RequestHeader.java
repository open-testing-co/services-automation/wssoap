
package com.huawei.bme.cbsinterface.cbscommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestHeader complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="BusinessCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="MessageSeq" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *         &lt;element name="OwnershipInfo" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}OwnershipInfo" minOccurs="0" form="qualified"/>
 *         &lt;element name="AccessSecurity" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}SecurityInfo" form="qualified"/>
 *         &lt;element name="OperatorInfo" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}OperatorInfo" minOccurs="0" form="qualified"/>
 *         &lt;element name="AccessMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="MsgLanguageCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="TimeFormat" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TimeType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="TimeZoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdditionalProperty" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestHeader", propOrder = {
    "version",
    "businessCode",
    "messageSeq",
    "ownershipInfo",
    "accessSecurity",
    "operatorInfo",
    "accessMode",
    "msgLanguageCode",
    "timeFormat",
    "additionalProperty"
})
public class RequestHeader {

    @XmlElement(name = "Version", required = true)
    protected String version;
    @XmlElement(name = "BusinessCode")
    protected String businessCode;
    @XmlElement(name = "MessageSeq", required = true)
    protected String messageSeq;
    @XmlElement(name = "OwnershipInfo")
    protected OwnershipInfo ownershipInfo;
    @XmlElement(name = "AccessSecurity", required = true)
    protected SecurityInfo accessSecurity;
    @XmlElement(name = "OperatorInfo")
    protected OperatorInfo operatorInfo;
    @XmlElement(name = "AccessMode")
    protected String accessMode;
    @XmlElement(name = "MsgLanguageCode")
    protected String msgLanguageCode;
    @XmlElement(name = "TimeFormat")
    protected RequestHeader.TimeFormat timeFormat;
    @XmlElement(name = "AdditionalProperty")
    protected List<RequestHeader.AdditionalProperty> additionalProperty;

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Obtiene el valor de la propiedad businessCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessCode() {
        return businessCode;
    }

    /**
     * Define el valor de la propiedad businessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessCode(String value) {
        this.businessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad messageSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageSeq() {
        return messageSeq;
    }

    /**
     * Define el valor de la propiedad messageSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageSeq(String value) {
        this.messageSeq = value;
    }

    /**
     * Obtiene el valor de la propiedad ownershipInfo.
     * 
     * @return
     *     possible object is
     *     {@link OwnershipInfo }
     *     
     */
    public OwnershipInfo getOwnershipInfo() {
        return ownershipInfo;
    }

    /**
     * Define el valor de la propiedad ownershipInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OwnershipInfo }
     *     
     */
    public void setOwnershipInfo(OwnershipInfo value) {
        this.ownershipInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad accessSecurity.
     * 
     * @return
     *     possible object is
     *     {@link SecurityInfo }
     *     
     */
    public SecurityInfo getAccessSecurity() {
        return accessSecurity;
    }

    /**
     * Define el valor de la propiedad accessSecurity.
     * 
     * @param value
     *     allowed object is
     *     {@link SecurityInfo }
     *     
     */
    public void setAccessSecurity(SecurityInfo value) {
        this.accessSecurity = value;
    }

    /**
     * Obtiene el valor de la propiedad operatorInfo.
     * 
     * @return
     *     possible object is
     *     {@link OperatorInfo }
     *     
     */
    public OperatorInfo getOperatorInfo() {
        return operatorInfo;
    }

    /**
     * Define el valor de la propiedad operatorInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link OperatorInfo }
     *     
     */
    public void setOperatorInfo(OperatorInfo value) {
        this.operatorInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad accessMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessMode() {
        return accessMode;
    }

    /**
     * Define el valor de la propiedad accessMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessMode(String value) {
        this.accessMode = value;
    }

    /**
     * Obtiene el valor de la propiedad msgLanguageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgLanguageCode() {
        return msgLanguageCode;
    }

    /**
     * Define el valor de la propiedad msgLanguageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgLanguageCode(String value) {
        this.msgLanguageCode = value;
    }

    /**
     * Obtiene el valor de la propiedad timeFormat.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader.TimeFormat }
     *     
     */
    public RequestHeader.TimeFormat getTimeFormat() {
        return timeFormat;
    }

    /**
     * Define el valor de la propiedad timeFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader.TimeFormat }
     *     
     */
    public void setTimeFormat(RequestHeader.TimeFormat value) {
        this.timeFormat = value;
    }

    /**
     * Gets the value of the additionalProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequestHeader.AdditionalProperty }
     * 
     * 
     */
    public List<RequestHeader.AdditionalProperty> getAdditionalProperty() {
        if (additionalProperty == null) {
            additionalProperty = new ArrayList<RequestHeader.AdditionalProperty>();
        }
        return this.additionalProperty;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "value"
    })
    public static class AdditionalProperty {

        @XmlElement(name = "Code", required = true)
        protected String code;
        @XmlElement(name = "Value", required = true)
        protected String value;

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad value.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TimeType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="TimeZoneID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "timeType",
        "timeZoneID"
    })
    public static class TimeFormat {

        @XmlElement(name = "TimeType", required = true)
        protected String timeType;
        @XmlElement(name = "TimeZoneID")
        protected String timeZoneID;

        /**
         * Obtiene el valor de la propiedad timeType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTimeType() {
            return timeType;
        }

        /**
         * Define el valor de la propiedad timeType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTimeType(String value) {
            this.timeType = value;
        }

        /**
         * Obtiene el valor de la propiedad timeZoneID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTimeZoneID() {
            return timeZoneID;
        }

        /**
         * Define el valor de la propiedad timeZoneID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTimeZoneID(String value) {
            this.timeZoneID = value;
        }

    }

}
