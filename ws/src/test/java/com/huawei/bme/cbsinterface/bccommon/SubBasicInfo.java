
package com.huawei.bme.cbsinterface.bccommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SubBasicInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubBasicInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="DunningFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="SubProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubBasicInfo", propOrder = {
    "writtenLang",
    "ivrLang",
    "subLevel",
    "dunningFlag",
    "subProperty"
})
public class SubBasicInfo {

    @XmlElement(name = "WrittenLang")
    protected String writtenLang;
    @XmlElement(name = "IVRLang")
    protected String ivrLang;
    @XmlElement(name = "SubLevel")
    protected String subLevel;
    @XmlElement(name = "DunningFlag")
    protected String dunningFlag;
    @XmlElement(name = "SubProperty")
    protected List<SimpleProperty> subProperty;

    /**
     * Obtiene el valor de la propiedad writtenLang.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWrittenLang() {
        return writtenLang;
    }

    /**
     * Define el valor de la propiedad writtenLang.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWrittenLang(String value) {
        this.writtenLang = value;
    }

    /**
     * Obtiene el valor de la propiedad ivrLang.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIVRLang() {
        return ivrLang;
    }

    /**
     * Define el valor de la propiedad ivrLang.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIVRLang(String value) {
        this.ivrLang = value;
    }

    /**
     * Obtiene el valor de la propiedad subLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubLevel() {
        return subLevel;
    }

    /**
     * Define el valor de la propiedad subLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubLevel(String value) {
        this.subLevel = value;
    }

    /**
     * Obtiene el valor de la propiedad dunningFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDunningFlag() {
        return dunningFlag;
    }

    /**
     * Define el valor de la propiedad dunningFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDunningFlag(String value) {
        this.dunningFlag = value;
    }

    /**
     * Gets the value of the subProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getSubProperty() {
        if (subProperty == null) {
            subProperty = new ArrayList<SimpleProperty>();
        }
        return this.subProperty;
    }

}
