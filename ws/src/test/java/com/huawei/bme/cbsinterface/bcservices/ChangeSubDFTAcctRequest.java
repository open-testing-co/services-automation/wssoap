
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.EffectMode;
import com.huawei.bme.cbsinterface.bccommon.PayRelExtRule;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;
import com.huawei.bme.cbsinterface.bccommon.SubAccessCode;


/**
 * <p>Clase Java para ChangeSubDFTAcctRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeSubDFTAcctRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode"/>
 *         &lt;element name="SubDFTAccount">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OldDFTAcct" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PrePaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PostPaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="DFTAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="NewDFTAcct">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PrePaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PostPaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="DFTAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DFTPayRelation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DelPayRelation" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="AddPayRelation" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                             &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
 *                             &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="PaymentLimit" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="PaymentLimitInfo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
 *                                     &lt;sequence>
 *                                     &lt;/sequence>
 *                                   &lt;/extension>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
 *         &lt;element name="ControlProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeSubDFTAcctRequest", propOrder = {
    "subAccessCode",
    "subDFTAccount",
    "dftPayRelation",
    "effectiveTime",
    "controlProperty"
})
public class ChangeSubDFTAcctRequest {

    @XmlElement(name = "SubAccessCode", required = true)
    protected SubAccessCode subAccessCode;
    @XmlElement(name = "SubDFTAccount", required = true)
    protected ChangeSubDFTAcctRequest.SubDFTAccount subDFTAccount;
    @XmlElement(name = "DFTPayRelation")
    protected ChangeSubDFTAcctRequest.DFTPayRelation dftPayRelation;
    @XmlElement(name = "EffectiveTime", required = true)
    protected EffectMode effectiveTime;
    @XmlElement(name = "ControlProperty")
    protected List<SimpleProperty> controlProperty;

    /**
     * Obtiene el valor de la propiedad subAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubAccessCode }
     *     
     */
    public SubAccessCode getSubAccessCode() {
        return subAccessCode;
    }

    /**
     * Define el valor de la propiedad subAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubAccessCode }
     *     
     */
    public void setSubAccessCode(SubAccessCode value) {
        this.subAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad subDFTAccount.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubDFTAcctRequest.SubDFTAccount }
     *     
     */
    public ChangeSubDFTAcctRequest.SubDFTAccount getSubDFTAccount() {
        return subDFTAccount;
    }

    /**
     * Define el valor de la propiedad subDFTAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubDFTAcctRequest.SubDFTAccount }
     *     
     */
    public void setSubDFTAccount(ChangeSubDFTAcctRequest.SubDFTAccount value) {
        this.subDFTAccount = value;
    }

    /**
     * Obtiene el valor de la propiedad dftPayRelation.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubDFTAcctRequest.DFTPayRelation }
     *     
     */
    public ChangeSubDFTAcctRequest.DFTPayRelation getDFTPayRelation() {
        return dftPayRelation;
    }

    /**
     * Define el valor de la propiedad dftPayRelation.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubDFTAcctRequest.DFTPayRelation }
     *     
     */
    public void setDFTPayRelation(ChangeSubDFTAcctRequest.DFTPayRelation value) {
        this.dftPayRelation = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveTime.
     * 
     * @return
     *     possible object is
     *     {@link EffectMode }
     *     
     */
    public EffectMode getEffectiveTime() {
        return effectiveTime;
    }

    /**
     * Define el valor de la propiedad effectiveTime.
     * 
     * @param value
     *     allowed object is
     *     {@link EffectMode }
     *     
     */
    public void setEffectiveTime(EffectMode value) {
        this.effectiveTime = value;
    }

    /**
     * Gets the value of the controlProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the controlProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getControlProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getControlProperty() {
        if (controlProperty == null) {
            controlProperty = new ArrayList<SimpleProperty>();
        }
        return this.controlProperty;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DelPayRelation" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="AddPayRelation" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                   &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
     *                   &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="PaymentLimit" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PaymentLimitInfo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
     *                           &lt;sequence>
     *                           &lt;/sequence>
     *                         &lt;/extension>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "delPayRelation",
        "addPayRelation",
        "paymentLimit"
    })
    public static class DFTPayRelation {

        @XmlElement(name = "DelPayRelation")
        protected List<ChangeSubDFTAcctRequest.DFTPayRelation.DelPayRelation> delPayRelation;
        @XmlElement(name = "AddPayRelation")
        protected List<ChangeSubDFTAcctRequest.DFTPayRelation.AddPayRelation> addPayRelation;
        @XmlElement(name = "PaymentLimit")
        protected ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit paymentLimit;

        /**
         * Gets the value of the delPayRelation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the delPayRelation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDelPayRelation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeSubDFTAcctRequest.DFTPayRelation.DelPayRelation }
         * 
         * 
         */
        public List<ChangeSubDFTAcctRequest.DFTPayRelation.DelPayRelation> getDelPayRelation() {
            if (delPayRelation == null) {
                delPayRelation = new ArrayList<ChangeSubDFTAcctRequest.DFTPayRelation.DelPayRelation>();
            }
            return this.delPayRelation;
        }

        /**
         * Gets the value of the addPayRelation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the addPayRelation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAddPayRelation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeSubDFTAcctRequest.DFTPayRelation.AddPayRelation }
         * 
         * 
         */
        public List<ChangeSubDFTAcctRequest.DFTPayRelation.AddPayRelation> getAddPayRelation() {
            if (addPayRelation == null) {
                addPayRelation = new ArrayList<ChangeSubDFTAcctRequest.DFTPayRelation.AddPayRelation>();
            }
            return this.addPayRelation;
        }

        /**
         * Obtiene el valor de la propiedad paymentLimit.
         * 
         * @return
         *     possible object is
         *     {@link ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit }
         *     
         */
        public ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit getPaymentLimit() {
            return paymentLimit;
        }

        /**
         * Define el valor de la propiedad paymentLimit.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit }
         *     
         */
        public void setPaymentLimit(ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit value) {
            this.paymentLimit = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *         &lt;element name="PayRelExtRule" type="{http://www.huawei.com/bme/cbsinterface/bccommon}PayRelExtRule" minOccurs="0"/>
         *         &lt;element name="OnlyPayRelFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "payRelationKey",
            "acctKey",
            "priority",
            "payRelExtRule",
            "onlyPayRelFlag",
            "paymentLimitKey"
        })
        public static class AddPayRelation {

            @XmlElement(name = "PayRelationKey")
            protected String payRelationKey;
            @XmlElement(name = "AcctKey", required = true)
            protected String acctKey;
            @XmlElement(name = "Priority")
            protected BigInteger priority;
            @XmlElement(name = "PayRelExtRule")
            protected PayRelExtRule payRelExtRule;
            @XmlElement(name = "OnlyPayRelFlag")
            protected String onlyPayRelFlag;
            @XmlElement(name = "PaymentLimitKey")
            protected String paymentLimitKey;

            /**
             * Obtiene el valor de la propiedad payRelationKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayRelationKey() {
                return payRelationKey;
            }

            /**
             * Define el valor de la propiedad payRelationKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayRelationKey(String value) {
                this.payRelationKey = value;
            }

            /**
             * Obtiene el valor de la propiedad acctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAcctKey() {
                return acctKey;
            }

            /**
             * Define el valor de la propiedad acctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAcctKey(String value) {
                this.acctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad priority.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPriority() {
                return priority;
            }

            /**
             * Define el valor de la propiedad priority.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPriority(BigInteger value) {
                this.priority = value;
            }

            /**
             * Obtiene el valor de la propiedad payRelExtRule.
             * 
             * @return
             *     possible object is
             *     {@link PayRelExtRule }
             *     
             */
            public PayRelExtRule getPayRelExtRule() {
                return payRelExtRule;
            }

            /**
             * Define el valor de la propiedad payRelExtRule.
             * 
             * @param value
             *     allowed object is
             *     {@link PayRelExtRule }
             *     
             */
            public void setPayRelExtRule(PayRelExtRule value) {
                this.payRelExtRule = value;
            }

            /**
             * Obtiene el valor de la propiedad onlyPayRelFlag.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOnlyPayRelFlag() {
                return onlyPayRelFlag;
            }

            /**
             * Define el valor de la propiedad onlyPayRelFlag.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOnlyPayRelFlag(String value) {
                this.onlyPayRelFlag = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentLimitKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPaymentLimitKey() {
                return paymentLimitKey;
            }

            /**
             * Define el valor de la propiedad paymentLimitKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPaymentLimitKey(String value) {
                this.paymentLimitKey = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "payRelationKey"
        })
        public static class DelPayRelation {

            @XmlElement(name = "PayRelationKey")
            protected String payRelationKey;

            /**
             * Obtiene el valor de la propiedad payRelationKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPayRelationKey() {
                return payRelationKey;
            }

            /**
             * Define el valor de la propiedad payRelationKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPayRelationKey(String value) {
                this.payRelationKey = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PaymentLimitKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PaymentLimitInfo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
         *                 &lt;sequence>
         *                 &lt;/sequence>
         *               &lt;/extension>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "paymentLimitKey",
            "paymentLimitInfo"
        })
        public static class PaymentLimit {

            @XmlElement(name = "PaymentLimitKey", required = true)
            protected String paymentLimitKey;
            @XmlElement(name = "PaymentLimitInfo", required = true)
            protected ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo paymentLimitInfo;

            /**
             * Obtiene el valor de la propiedad paymentLimitKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPaymentLimitKey() {
                return paymentLimitKey;
            }

            /**
             * Define el valor de la propiedad paymentLimitKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPaymentLimitKey(String value) {
                this.paymentLimitKey = value;
            }

            /**
             * Obtiene el valor de la propiedad paymentLimitInfo.
             * 
             * @return
             *     possible object is
             *     {@link ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo }
             *     
             */
            public ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo getPaymentLimitInfo() {
                return paymentLimitInfo;
            }

            /**
             * Define el valor de la propiedad paymentLimitInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo }
             *     
             */
            public void setPaymentLimitInfo(ChangeSubDFTAcctRequest.DFTPayRelation.PaymentLimit.PaymentLimitInfo value) {
                this.paymentLimitInfo = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}PaymentLimit">
             *       &lt;sequence>
             *       &lt;/sequence>
             *     &lt;/extension>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class PaymentLimitInfo
                extends com.huawei.bme.cbsinterface.bccommon.PaymentLimit
            {


            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OldDFTAcct" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PrePaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PostPaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="DFTAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="NewDFTAcct">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="PrePaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="PostPaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="DFTAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "oldDFTAcct",
        "newDFTAcct"
    })
    public static class SubDFTAccount {

        @XmlElement(name = "OldDFTAcct")
        protected ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct oldDFTAcct;
        @XmlElement(name = "NewDFTAcct", required = true)
        protected ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct newDFTAcct;

        /**
         * Obtiene el valor de la propiedad oldDFTAcct.
         * 
         * @return
         *     possible object is
         *     {@link ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct }
         *     
         */
        public ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct getOldDFTAcct() {
            return oldDFTAcct;
        }

        /**
         * Define el valor de la propiedad oldDFTAcct.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct }
         *     
         */
        public void setOldDFTAcct(ChangeSubDFTAcctRequest.SubDFTAccount.OldDFTAcct value) {
            this.oldDFTAcct = value;
        }

        /**
         * Obtiene el valor de la propiedad newDFTAcct.
         * 
         * @return
         *     possible object is
         *     {@link ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct }
         *     
         */
        public ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct getNewDFTAcct() {
            return newDFTAcct;
        }

        /**
         * Define el valor de la propiedad newDFTAcct.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct }
         *     
         */
        public void setNewDFTAcct(ChangeSubDFTAcctRequest.SubDFTAccount.NewDFTAcct value) {
            this.newDFTAcct = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PrePaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PostPaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="DFTAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "prePaidAcctKey",
            "postPaidAcctKey",
            "dftAcctKey"
        })
        public static class NewDFTAcct {

            @XmlElement(name = "PrePaidAcctKey")
            protected String prePaidAcctKey;
            @XmlElement(name = "PostPaidAcctKey")
            protected String postPaidAcctKey;
            @XmlElement(name = "DFTAcctKey")
            protected String dftAcctKey;

            /**
             * Obtiene el valor de la propiedad prePaidAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrePaidAcctKey() {
                return prePaidAcctKey;
            }

            /**
             * Define el valor de la propiedad prePaidAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrePaidAcctKey(String value) {
                this.prePaidAcctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad postPaidAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostPaidAcctKey() {
                return postPaidAcctKey;
            }

            /**
             * Define el valor de la propiedad postPaidAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostPaidAcctKey(String value) {
                this.postPaidAcctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad dftAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDFTAcctKey() {
                return dftAcctKey;
            }

            /**
             * Define el valor de la propiedad dftAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDFTAcctKey(String value) {
                this.dftAcctKey = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="PrePaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="PostPaidAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="DFTAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "prePaidAcctKey",
            "postPaidAcctKey",
            "dftAcctKey"
        })
        public static class OldDFTAcct {

            @XmlElement(name = "PrePaidAcctKey")
            protected String prePaidAcctKey;
            @XmlElement(name = "PostPaidAcctKey")
            protected String postPaidAcctKey;
            @XmlElement(name = "DFTAcctKey")
            protected String dftAcctKey;

            /**
             * Obtiene el valor de la propiedad prePaidAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrePaidAcctKey() {
                return prePaidAcctKey;
            }

            /**
             * Define el valor de la propiedad prePaidAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrePaidAcctKey(String value) {
                this.prePaidAcctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad postPaidAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostPaidAcctKey() {
                return postPaidAcctKey;
            }

            /**
             * Define el valor de la propiedad postPaidAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostPaidAcctKey(String value) {
                this.postPaidAcctKey = value;
            }

            /**
             * Obtiene el valor de la propiedad dftAcctKey.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDFTAcctKey() {
                return dftAcctKey;
            }

            /**
             * Define el valor de la propiedad dftAcctKey.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDFTAcctKey(String value) {
                this.dftAcctKey = value;
            }

        }

    }

}
