
package com.huawei.bme.cbsinterface.bccommon;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SubGroupMemberInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubGroupMemberInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="MemberProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubGroupMemberInfo", propOrder = {
    "memberTypeCode",
    "memberProperty"
})
public class SubGroupMemberInfo {

    @XmlElement(name = "MemberTypeCode")
    protected String memberTypeCode;
    @XmlElement(name = "MemberProperty")
    protected List<SimpleProperty> memberProperty;

    /**
     * Obtiene el valor de la propiedad memberTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberTypeCode() {
        return memberTypeCode;
    }

    /**
     * Define el valor de la propiedad memberTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberTypeCode(String value) {
        this.memberTypeCode = value;
    }

    /**
     * Gets the value of the memberProperty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the memberProperty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMemberProperty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SimpleProperty }
     * 
     * 
     */
    public List<SimpleProperty> getMemberProperty() {
        if (memberProperty == null) {
            memberProperty = new ArrayList<SimpleProperty>();
        }
        return this.memberProperty;
    }

}
