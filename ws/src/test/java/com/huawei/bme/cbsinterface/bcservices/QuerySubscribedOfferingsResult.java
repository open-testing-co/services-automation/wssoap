
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;


/**
 * <p>Clase Java para QuerySubscribedOfferingsResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QuerySubscribedOfferingsResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TotalOfferInstNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="BeginRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="FetchRowNum" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="OfferingInstanceInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
 *                   &lt;element name="Primary_Flag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuerySubscribedOfferingsResult", propOrder = {
    "totalOfferInstNum",
    "beginRowNum",
    "fetchRowNum",
    "offeringInstanceInfo"
})
public class QuerySubscribedOfferingsResult {

    @XmlElement(name = "TotalOfferInstNum", required = true)
    protected BigInteger totalOfferInstNum;
    @XmlElement(name = "BeginRowNum", required = true)
    protected BigInteger beginRowNum;
    @XmlElement(name = "FetchRowNum", required = true)
    protected BigInteger fetchRowNum;
    @XmlElement(name = "OfferingInstanceInfo")
    protected List<QuerySubscribedOfferingsResult.OfferingInstanceInfo> offeringInstanceInfo;

    /**
     * Obtiene el valor de la propiedad totalOfferInstNum.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalOfferInstNum() {
        return totalOfferInstNum;
    }

    /**
     * Define el valor de la propiedad totalOfferInstNum.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalOfferInstNum(BigInteger value) {
        this.totalOfferInstNum = value;
    }

    /**
     * Obtiene el valor de la propiedad beginRowNum.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBeginRowNum() {
        return beginRowNum;
    }

    /**
     * Define el valor de la propiedad beginRowNum.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBeginRowNum(BigInteger value) {
        this.beginRowNum = value;
    }

    /**
     * Obtiene el valor de la propiedad fetchRowNum.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFetchRowNum() {
        return fetchRowNum;
    }

    /**
     * Define el valor de la propiedad fetchRowNum.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFetchRowNum(BigInteger value) {
        this.fetchRowNum = value;
    }

    /**
     * Gets the value of the offeringInstanceInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offeringInstanceInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOfferingInstanceInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuerySubscribedOfferingsResult.OfferingInstanceInfo }
     * 
     * 
     */
    public List<QuerySubscribedOfferingsResult.OfferingInstanceInfo> getOfferingInstanceInfo() {
        if (offeringInstanceInfo == null) {
            offeringInstanceInfo = new ArrayList<QuerySubscribedOfferingsResult.OfferingInstanceInfo>();
        }
        return this.offeringInstanceInfo;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey"/>
     *         &lt;element name="Primary_Flag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offeringKey",
        "primaryFlag",
        "status",
        "effectiveTime",
        "expirationTime"
    })
    public static class OfferingInstanceInfo {

        @XmlElement(name = "OfferingKey", required = true)
        protected OfferingKey offeringKey;
        @XmlElement(name = "Primary_Flag", required = true)
        protected String primaryFlag;
        @XmlElement(name = "Status", required = true)
        protected String status;
        @XmlElement(name = "EffectiveTime", required = true)
        protected String effectiveTime;
        @XmlElement(name = "ExpirationTime", required = true)
        protected String expirationTime;

        /**
         * Obtiene el valor de la propiedad offeringKey.
         * 
         * @return
         *     possible object is
         *     {@link OfferingKey }
         *     
         */
        public OfferingKey getOfferingKey() {
            return offeringKey;
        }

        /**
         * Define el valor de la propiedad offeringKey.
         * 
         * @param value
         *     allowed object is
         *     {@link OfferingKey }
         *     
         */
        public void setOfferingKey(OfferingKey value) {
            this.offeringKey = value;
        }

        /**
         * Obtiene el valor de la propiedad primaryFlag.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPrimaryFlag() {
            return primaryFlag;
        }

        /**
         * Define el valor de la propiedad primaryFlag.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPrimaryFlag(String value) {
            this.primaryFlag = value;
        }

        /**
         * Obtiene el valor de la propiedad status.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatus() {
            return status;
        }

        /**
         * Define el valor de la propiedad status.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatus(String value) {
            this.status = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Define el valor de la propiedad effectiveTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEffectiveTime(String value) {
            this.effectiveTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationTime() {
            return expirationTime;
        }

        /**
         * Define el valor de la propiedad expirationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationTime(String value) {
            this.expirationTime = value;
        }

    }

}
