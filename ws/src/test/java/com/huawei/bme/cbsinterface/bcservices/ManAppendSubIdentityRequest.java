
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ManAppendSubIdentityRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ManAppendSubIdentityRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PrimaryIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OperateType" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AppendSubIdentityList" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="RelatedSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="NewSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManAppendSubIdentityRequest", propOrder = {
    "primaryIdentity",
    "operateType",
    "appendSubIdentityList"
})
public class ManAppendSubIdentityRequest {

    @XmlElement(name = "PrimaryIdentity", required = true)
    protected String primaryIdentity;
    @XmlElement(name = "OperateType")
    protected int operateType;
    @XmlElement(name = "AppendSubIdentityList", required = true)
    protected List<ManAppendSubIdentityRequest.AppendSubIdentityList> appendSubIdentityList;

    /**
     * Obtiene el valor de la propiedad primaryIdentity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryIdentity() {
        return primaryIdentity;
    }

    /**
     * Define el valor de la propiedad primaryIdentity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryIdentity(String value) {
        this.primaryIdentity = value;
    }

    /**
     * Obtiene el valor de la propiedad operateType.
     * 
     */
    public int getOperateType() {
        return operateType;
    }

    /**
     * Define el valor de la propiedad operateType.
     * 
     */
    public void setOperateType(int value) {
        this.operateType = value;
    }

    /**
     * Gets the value of the appendSubIdentityList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appendSubIdentityList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppendSubIdentityList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ManAppendSubIdentityRequest.AppendSubIdentityList }
     * 
     * 
     */
    public List<ManAppendSubIdentityRequest.AppendSubIdentityList> getAppendSubIdentityList() {
        if (appendSubIdentityList == null) {
            appendSubIdentityList = new ArrayList<ManAppendSubIdentityRequest.AppendSubIdentityList>();
        }
        return this.appendSubIdentityList;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubIdentityType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="RelatedSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="NewSubIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subIdentity",
        "subIdentityType",
        "relatedSubIdentity",
        "newSubIdentity"
    })
    public static class AppendSubIdentityList {

        @XmlElement(name = "SubIdentity", required = true)
        protected String subIdentity;
        @XmlElement(name = "SubIdentityType")
        protected Integer subIdentityType;
        @XmlElement(name = "RelatedSubIdentity")
        protected String relatedSubIdentity;
        @XmlElement(name = "NewSubIdentity")
        protected String newSubIdentity;

        /**
         * Obtiene el valor de la propiedad subIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubIdentity() {
            return subIdentity;
        }

        /**
         * Define el valor de la propiedad subIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubIdentity(String value) {
            this.subIdentity = value;
        }

        /**
         * Obtiene el valor de la propiedad subIdentityType.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getSubIdentityType() {
            return subIdentityType;
        }

        /**
         * Define el valor de la propiedad subIdentityType.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setSubIdentityType(Integer value) {
            this.subIdentityType = value;
        }

        /**
         * Obtiene el valor de la propiedad relatedSubIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRelatedSubIdentity() {
            return relatedSubIdentity;
        }

        /**
         * Define el valor de la propiedad relatedSubIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRelatedSubIdentity(String value) {
            this.relatedSubIdentity = value;
        }

        /**
         * Obtiene el valor de la propiedad newSubIdentity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewSubIdentity() {
            return newSubIdentity;
        }

        /**
         * Define el valor de la propiedad newSubIdentity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewSubIdentity(String value) {
            this.newSubIdentity = value;
        }

    }

}
