
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.OfferingKey;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;


/**
 * <p>Clase Java para ChangeRscRelationRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ChangeRscRelationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubAccessCode" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode">
 *                 &lt;sequence>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
 *         &lt;element name="RscRelation">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AddRelation" type="{http://www.huawei.com/bme/cbsinterface/bccommon}RscRelation" minOccurs="0"/>
 *                   &lt;element name="DelRelation" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="RelationObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RelationDestIdentify">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *                             &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ModRelation" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="RelationObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="RelationDestIdentify">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="OldRelation" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *                                       &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="NewRelation" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
 *                                       &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="ShareLimit" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                 &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                                                 &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="NewDestIdentify" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="EffTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeRscRelationRequest", propOrder = {
    "subAccessCode",
    "subGroupAccessCode",
    "rscRelation"
})
public class ChangeRscRelationRequest {

    @XmlElement(name = "SubAccessCode")
    protected ChangeRscRelationRequest.SubAccessCode subAccessCode;
    @XmlElement(name = "SubGroupAccessCode")
    protected SubGroupAccessCode subGroupAccessCode;
    @XmlElement(name = "RscRelation", required = true)
    protected ChangeRscRelationRequest.RscRelation rscRelation;

    /**
     * Obtiene el valor de la propiedad subAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link ChangeRscRelationRequest.SubAccessCode }
     *     
     */
    public ChangeRscRelationRequest.SubAccessCode getSubAccessCode() {
        return subAccessCode;
    }

    /**
     * Define el valor de la propiedad subAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeRscRelationRequest.SubAccessCode }
     *     
     */
    public void setSubAccessCode(ChangeRscRelationRequest.SubAccessCode value) {
        this.subAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad subGroupAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public SubGroupAccessCode getSubGroupAccessCode() {
        return subGroupAccessCode;
    }

    /**
     * Define el valor de la propiedad subGroupAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public void setSubGroupAccessCode(SubGroupAccessCode value) {
        this.subGroupAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad rscRelation.
     * 
     * @return
     *     possible object is
     *     {@link ChangeRscRelationRequest.RscRelation }
     *     
     */
    public ChangeRscRelationRequest.RscRelation getRscRelation() {
        return rscRelation;
    }

    /**
     * Define el valor de la propiedad rscRelation.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeRscRelationRequest.RscRelation }
     *     
     */
    public void setRscRelation(ChangeRscRelationRequest.RscRelation value) {
        this.rscRelation = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AddRelation" type="{http://www.huawei.com/bme/cbsinterface/bccommon}RscRelation" minOccurs="0"/>
     *         &lt;element name="DelRelation" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RelationObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RelationDestIdentify">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
     *                   &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ModRelation" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RelationObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="RelationDestIdentify">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="OldRelation" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
     *                             &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="NewRelation" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
     *                             &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ShareLimit" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                       &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                                       &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="NewDestIdentify" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="EffTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "addRelation",
        "delRelation",
        "modRelation"
    })
    public static class RscRelation {

        @XmlElement(name = "AddRelation")
        protected com.huawei.bme.cbsinterface.bccommon.RscRelation addRelation;
        @XmlElement(name = "DelRelation")
        protected List<ChangeRscRelationRequest.RscRelation.DelRelation> delRelation;
        @XmlElement(name = "ModRelation")
        protected ChangeRscRelationRequest.RscRelation.ModRelation modRelation;

        /**
         * Obtiene el valor de la propiedad addRelation.
         * 
         * @return
         *     possible object is
         *     {@link com.huawei.bme.cbsinterface.bccommon.RscRelation }
         *     
         */
        public com.huawei.bme.cbsinterface.bccommon.RscRelation getAddRelation() {
            return addRelation;
        }

        /**
         * Define el valor de la propiedad addRelation.
         * 
         * @param value
         *     allowed object is
         *     {@link com.huawei.bme.cbsinterface.bccommon.RscRelation }
         *     
         */
        public void setAddRelation(com.huawei.bme.cbsinterface.bccommon.RscRelation value) {
            this.addRelation = value;
        }

        /**
         * Gets the value of the delRelation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the delRelation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDelRelation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ChangeRscRelationRequest.RscRelation.DelRelation }
         * 
         * 
         */
        public List<ChangeRscRelationRequest.RscRelation.DelRelation> getDelRelation() {
            if (delRelation == null) {
                delRelation = new ArrayList<ChangeRscRelationRequest.RscRelation.DelRelation>();
            }
            return this.delRelation;
        }

        /**
         * Obtiene el valor de la propiedad modRelation.
         * 
         * @return
         *     possible object is
         *     {@link ChangeRscRelationRequest.RscRelation.ModRelation }
         *     
         */
        public ChangeRscRelationRequest.RscRelation.ModRelation getModRelation() {
            return modRelation;
        }

        /**
         * Define el valor de la propiedad modRelation.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeRscRelationRequest.RscRelation.ModRelation }
         *     
         */
        public void setModRelation(ChangeRscRelationRequest.RscRelation.ModRelation value) {
            this.modRelation = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RelationObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RelationDestIdentify">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
         *         &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="StartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="EndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "relationObjType",
            "relationDestIdentify",
            "offeringKey",
            "shareRule",
            "startTime",
            "endTime"
        })
        public static class DelRelation {

            @XmlElement(name = "RelationObjType", required = true)
            protected String relationObjType;
            @XmlElement(name = "RelationDestIdentify", required = true)
            protected ChangeRscRelationRequest.RscRelation.DelRelation.RelationDestIdentify relationDestIdentify;
            @XmlElement(name = "OfferingKey")
            protected OfferingKey offeringKey;
            @XmlElement(name = "ShareRule")
            protected String shareRule;
            @XmlElement(name = "StartTime")
            protected String startTime;
            @XmlElement(name = "EndTime")
            protected String endTime;

            /**
             * Obtiene el valor de la propiedad relationObjType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationObjType() {
                return relationObjType;
            }

            /**
             * Define el valor de la propiedad relationObjType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationObjType(String value) {
                this.relationObjType = value;
            }

            /**
             * Obtiene el valor de la propiedad relationDestIdentify.
             * 
             * @return
             *     possible object is
             *     {@link ChangeRscRelationRequest.RscRelation.DelRelation.RelationDestIdentify }
             *     
             */
            public ChangeRscRelationRequest.RscRelation.DelRelation.RelationDestIdentify getRelationDestIdentify() {
                return relationDestIdentify;
            }

            /**
             * Define el valor de la propiedad relationDestIdentify.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeRscRelationRequest.RscRelation.DelRelation.RelationDestIdentify }
             *     
             */
            public void setRelationDestIdentify(ChangeRscRelationRequest.RscRelation.DelRelation.RelationDestIdentify value) {
                this.relationDestIdentify = value;
            }

            /**
             * Obtiene el valor de la propiedad offeringKey.
             * 
             * @return
             *     possible object is
             *     {@link OfferingKey }
             *     
             */
            public OfferingKey getOfferingKey() {
                return offeringKey;
            }

            /**
             * Define el valor de la propiedad offeringKey.
             * 
             * @param value
             *     allowed object is
             *     {@link OfferingKey }
             *     
             */
            public void setOfferingKey(OfferingKey value) {
                this.offeringKey = value;
            }

            /**
             * Obtiene el valor de la propiedad shareRule.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getShareRule() {
                return shareRule;
            }

            /**
             * Define el valor de la propiedad shareRule.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setShareRule(String value) {
                this.shareRule = value;
            }

            /**
             * Obtiene el valor de la propiedad startTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStartTime() {
                return startTime;
            }

            /**
             * Define el valor de la propiedad startTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStartTime(String value) {
                this.startTime = value;
            }

            /**
             * Obtiene el valor de la propiedad endTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEndTime() {
                return endTime;
            }

            /**
             * Define el valor de la propiedad endTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEndTime(String value) {
                this.endTime = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "subIdentify"
            })
            public static class RelationDestIdentify {

                @XmlElement(name = "SubIdentify")
                protected com.huawei.bme.cbsinterface.bccommon.SubAccessCode subIdentify;

                /**
                 * Obtiene el valor de la propiedad subIdentify.
                 * 
                 * @return
                 *     possible object is
                 *     {@link com.huawei.bme.cbsinterface.bccommon.SubAccessCode }
                 *     
                 */
                public com.huawei.bme.cbsinterface.bccommon.SubAccessCode getSubIdentify() {
                    return subIdentify;
                }

                /**
                 * Define el valor de la propiedad subIdentify.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link com.huawei.bme.cbsinterface.bccommon.SubAccessCode }
                 *     
                 */
                public void setSubIdentify(com.huawei.bme.cbsinterface.bccommon.SubAccessCode value) {
                    this.subIdentify = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RelationObjType" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="RelationDestIdentify">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="OldRelation" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
         *                   &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="NewRelation" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
         *                   &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ShareLimit" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                             &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *                             &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="NewDestIdentify" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="EffTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "relationObjType",
            "relationDestIdentify",
            "oldRelation",
            "newRelation",
            "newDestIdentify",
            "effTime"
        })
        public static class ModRelation {

            @XmlElement(name = "RelationObjType", required = true)
            protected String relationObjType;
            @XmlElement(name = "RelationDestIdentify", required = true)
            protected ChangeRscRelationRequest.RscRelation.ModRelation.RelationDestIdentify relationDestIdentify;
            @XmlElement(name = "OldRelation")
            protected ChangeRscRelationRequest.RscRelation.ModRelation.OldRelation oldRelation;
            @XmlElement(name = "NewRelation")
            protected ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation newRelation;
            @XmlElement(name = "NewDestIdentify")
            protected ChangeRscRelationRequest.RscRelation.ModRelation.NewDestIdentify newDestIdentify;
            @XmlElement(name = "EffTime")
            protected String effTime;

            /**
             * Obtiene el valor de la propiedad relationObjType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRelationObjType() {
                return relationObjType;
            }

            /**
             * Define el valor de la propiedad relationObjType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRelationObjType(String value) {
                this.relationObjType = value;
            }

            /**
             * Obtiene el valor de la propiedad relationDestIdentify.
             * 
             * @return
             *     possible object is
             *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.RelationDestIdentify }
             *     
             */
            public ChangeRscRelationRequest.RscRelation.ModRelation.RelationDestIdentify getRelationDestIdentify() {
                return relationDestIdentify;
            }

            /**
             * Define el valor de la propiedad relationDestIdentify.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.RelationDestIdentify }
             *     
             */
            public void setRelationDestIdentify(ChangeRscRelationRequest.RscRelation.ModRelation.RelationDestIdentify value) {
                this.relationDestIdentify = value;
            }

            /**
             * Obtiene el valor de la propiedad oldRelation.
             * 
             * @return
             *     possible object is
             *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.OldRelation }
             *     
             */
            public ChangeRscRelationRequest.RscRelation.ModRelation.OldRelation getOldRelation() {
                return oldRelation;
            }

            /**
             * Define el valor de la propiedad oldRelation.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.OldRelation }
             *     
             */
            public void setOldRelation(ChangeRscRelationRequest.RscRelation.ModRelation.OldRelation value) {
                this.oldRelation = value;
            }

            /**
             * Obtiene el valor de la propiedad newRelation.
             * 
             * @return
             *     possible object is
             *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation }
             *     
             */
            public ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation getNewRelation() {
                return newRelation;
            }

            /**
             * Define el valor de la propiedad newRelation.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation }
             *     
             */
            public void setNewRelation(ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation value) {
                this.newRelation = value;
            }

            /**
             * Obtiene el valor de la propiedad newDestIdentify.
             * 
             * @return
             *     possible object is
             *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewDestIdentify }
             *     
             */
            public ChangeRscRelationRequest.RscRelation.ModRelation.NewDestIdentify getNewDestIdentify() {
                return newDestIdentify;
            }

            /**
             * Define el valor de la propiedad newDestIdentify.
             * 
             * @param value
             *     allowed object is
             *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewDestIdentify }
             *     
             */
            public void setNewDestIdentify(ChangeRscRelationRequest.RscRelation.ModRelation.NewDestIdentify value) {
                this.newDestIdentify = value;
            }

            /**
             * Obtiene el valor de la propiedad effTime.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEffTime() {
                return effTime;
            }

            /**
             * Define el valor de la propiedad effTime.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEffTime(String value) {
                this.effTime = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "subIdentify"
            })
            public static class NewDestIdentify {

                @XmlElement(name = "SubIdentify")
                protected com.huawei.bme.cbsinterface.bccommon.SubAccessCode subIdentify;

                /**
                 * Obtiene el valor de la propiedad subIdentify.
                 * 
                 * @return
                 *     possible object is
                 *     {@link com.huawei.bme.cbsinterface.bccommon.SubAccessCode }
                 *     
                 */
                public com.huawei.bme.cbsinterface.bccommon.SubAccessCode getSubIdentify() {
                    return subIdentify;
                }

                /**
                 * Define el valor de la propiedad subIdentify.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link com.huawei.bme.cbsinterface.bccommon.SubAccessCode }
                 *     
                 */
                public void setSubIdentify(com.huawei.bme.cbsinterface.bccommon.SubAccessCode value) {
                    this.subIdentify = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
             *         &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ShareLimit" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
             *                   &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offeringKey",
                "shareRule",
                "shareLimit"
            })
            public static class NewRelation {

                @XmlElement(name = "OfferingKey")
                protected OfferingKey offeringKey;
                @XmlElement(name = "ShareRule")
                protected String shareRule;
                @XmlElement(name = "ShareLimit")
                protected ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation.ShareLimit shareLimit;

                /**
                 * Obtiene el valor de la propiedad offeringKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getOfferingKey() {
                    return offeringKey;
                }

                /**
                 * Define el valor de la propiedad offeringKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setOfferingKey(OfferingKey value) {
                    this.offeringKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad shareRule.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareRule() {
                    return shareRule;
                }

                /**
                 * Define el valor de la propiedad shareRule.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareRule(String value) {
                    this.shareRule = value;
                }

                /**
                 * Obtiene el valor de la propiedad shareLimit.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation.ShareLimit }
                 *     
                 */
                public ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation.ShareLimit getShareLimit() {
                    return shareLimit;
                }

                /**
                 * Define el valor de la propiedad shareLimit.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation.ShareLimit }
                 *     
                 */
                public void setShareLimit(ChangeRscRelationRequest.RscRelation.ModRelation.NewRelation.ShareLimit value) {
                    this.shareLimit = value;
                }


                /**
                 * <p>Clase Java para anonymous complex type.
                 * 
                 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="LimitCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
                 *         &lt;element name="MeasureUnit" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "limitCycleType",
                    "limitValue",
                    "measureUnit"
                })
                public static class ShareLimit {

                    @XmlElement(name = "LimitCycleType")
                    protected String limitCycleType;
                    @XmlElement(name = "LimitValue")
                    protected Long limitValue;
                    @XmlElement(name = "MeasureUnit")
                    protected BigInteger measureUnit;

                    /**
                     * Obtiene el valor de la propiedad limitCycleType.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLimitCycleType() {
                        return limitCycleType;
                    }

                    /**
                     * Define el valor de la propiedad limitCycleType.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLimitCycleType(String value) {
                        this.limitCycleType = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad limitValue.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Long }
                     *     
                     */
                    public Long getLimitValue() {
                        return limitValue;
                    }

                    /**
                     * Define el valor de la propiedad limitValue.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Long }
                     *     
                     */
                    public void setLimitValue(Long value) {
                        this.limitValue = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad measureUnit.
                     * 
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *     
                     */
                    public BigInteger getMeasureUnit() {
                        return measureUnit;
                    }

                    /**
                     * Define el valor de la propiedad measureUnit.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *     
                     */
                    public void setMeasureUnit(BigInteger value) {
                        this.measureUnit = value;
                    }

                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="OfferingKey" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingKey" minOccurs="0"/>
             *         &lt;element name="ShareRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "offeringKey",
                "shareRule"
            })
            public static class OldRelation {

                @XmlElement(name = "OfferingKey")
                protected OfferingKey offeringKey;
                @XmlElement(name = "ShareRule")
                protected String shareRule;

                /**
                 * Obtiene el valor de la propiedad offeringKey.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OfferingKey }
                 *     
                 */
                public OfferingKey getOfferingKey() {
                    return offeringKey;
                }

                /**
                 * Define el valor de la propiedad offeringKey.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OfferingKey }
                 *     
                 */
                public void setOfferingKey(OfferingKey value) {
                    this.offeringKey = value;
                }

                /**
                 * Obtiene el valor de la propiedad shareRule.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShareRule() {
                    return shareRule;
                }

                /**
                 * Define el valor de la propiedad shareRule.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShareRule(String value) {
                    this.shareRule = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="SubIdentify" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "subIdentify"
            })
            public static class RelationDestIdentify {

                @XmlElement(name = "SubIdentify")
                protected com.huawei.bme.cbsinterface.bccommon.SubAccessCode subIdentify;

                /**
                 * Obtiene el valor de la propiedad subIdentify.
                 * 
                 * @return
                 *     possible object is
                 *     {@link com.huawei.bme.cbsinterface.bccommon.SubAccessCode }
                 *     
                 */
                public com.huawei.bme.cbsinterface.bccommon.SubAccessCode getSubIdentify() {
                    return subIdentify;
                }

                /**
                 * Define el valor de la propiedad subIdentify.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link com.huawei.bme.cbsinterface.bccommon.SubAccessCode }
                 *     
                 */
                public void setSubIdentify(com.huawei.bme.cbsinterface.bccommon.SubAccessCode value) {
                    this.subIdentify = value;
                }

            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}SubAccessCode">
     *       &lt;sequence>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class SubAccessCode
        extends com.huawei.bme.cbsinterface.bccommon.SubAccessCode
    {


    }

}
