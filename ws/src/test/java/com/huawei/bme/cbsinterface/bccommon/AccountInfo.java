
package com.huawei.bme.cbsinterface.bccommon;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AccountInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AccountInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="UserCustomerKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="ParentAcctKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="AcctBasicInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountBasicInfo" minOccurs="0" form="qualified"/>
 *         &lt;element name="BillCycleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="AcctType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="PaymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="AcctClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="CurrencyID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *         &lt;element name="InitBalance" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/>
 *         &lt;element name="CreditLimit" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *                   &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AcctPayMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="AutoPayChannel" maxOccurs="unbounded" minOccurs="0" form="qualified">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
 *                   &lt;element name="AutoPayChannelInfo" form="qualified">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
 *                           &lt;sequence>
 *                           &lt;/sequence>
 *                         &lt;/extension>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountInfo", propOrder = {
    "acctCode",
    "userCustomerKey",
    "parentAcctKey",
    "acctBasicInfo",
    "billCycleType",
    "acctType",
    "paymentType",
    "acctClass",
    "currencyID",
    "initBalance",
    "creditLimit",
    "acctPayMethod",
    "autoPayChannel"
})
public class AccountInfo {

    @XmlElement(name = "AcctCode")
    protected String acctCode;
    @XmlElement(name = "UserCustomerKey")
    protected String userCustomerKey;
    @XmlElement(name = "ParentAcctKey")
    protected String parentAcctKey;
    @XmlElement(name = "AcctBasicInfo")
    protected AccountBasicInfo acctBasicInfo;
    @XmlElement(name = "BillCycleType")
    protected String billCycleType;
    @XmlElement(name = "AcctType")
    protected String acctType;
    @XmlElement(name = "PaymentType")
    protected String paymentType;
    @XmlElement(name = "AcctClass")
    protected String acctClass;
    @XmlElement(name = "CurrencyID")
    protected BigInteger currencyID;
    @XmlElement(name = "InitBalance")
    protected Long initBalance;
    @XmlElement(name = "CreditLimit")
    protected List<AccountInfo.CreditLimit> creditLimit;
    @XmlElement(name = "AcctPayMethod")
    protected String acctPayMethod;
    @XmlElement(name = "AutoPayChannel")
    protected List<AccountInfo.AutoPayChannel> autoPayChannel;

    /**
     * Obtiene el valor de la propiedad acctCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctCode() {
        return acctCode;
    }

    /**
     * Define el valor de la propiedad acctCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctCode(String value) {
        this.acctCode = value;
    }

    /**
     * Obtiene el valor de la propiedad userCustomerKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserCustomerKey() {
        return userCustomerKey;
    }

    /**
     * Define el valor de la propiedad userCustomerKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserCustomerKey(String value) {
        this.userCustomerKey = value;
    }

    /**
     * Obtiene el valor de la propiedad parentAcctKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentAcctKey() {
        return parentAcctKey;
    }

    /**
     * Define el valor de la propiedad parentAcctKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentAcctKey(String value) {
        this.parentAcctKey = value;
    }

    /**
     * Obtiene el valor de la propiedad acctBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link AccountBasicInfo }
     *     
     */
    public AccountBasicInfo getAcctBasicInfo() {
        return acctBasicInfo;
    }

    /**
     * Define el valor de la propiedad acctBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBasicInfo }
     *     
     */
    public void setAcctBasicInfo(AccountBasicInfo value) {
        this.acctBasicInfo = value;
    }

    /**
     * Obtiene el valor de la propiedad billCycleType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillCycleType() {
        return billCycleType;
    }

    /**
     * Define el valor de la propiedad billCycleType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillCycleType(String value) {
        this.billCycleType = value;
    }

    /**
     * Obtiene el valor de la propiedad acctType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctType() {
        return acctType;
    }

    /**
     * Define el valor de la propiedad acctType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctType(String value) {
        this.acctType = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Define el valor de la propiedad paymentType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Obtiene el valor de la propiedad acctClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctClass() {
        return acctClass;
    }

    /**
     * Define el valor de la propiedad acctClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctClass(String value) {
        this.acctClass = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyID.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrencyID() {
        return currencyID;
    }

    /**
     * Define el valor de la propiedad currencyID.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrencyID(BigInteger value) {
        this.currencyID = value;
    }

    /**
     * Obtiene el valor de la propiedad initBalance.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getInitBalance() {
        return initBalance;
    }

    /**
     * Define el valor de la propiedad initBalance.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setInitBalance(Long value) {
        this.initBalance = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the creditLimit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreditLimit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountInfo.CreditLimit }
     * 
     * 
     */
    public List<AccountInfo.CreditLimit> getCreditLimit() {
        if (creditLimit == null) {
            creditLimit = new ArrayList<AccountInfo.CreditLimit>();
        }
        return this.creditLimit;
    }

    /**
     * Obtiene el valor de la propiedad acctPayMethod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcctPayMethod() {
        return acctPayMethod;
    }

    /**
     * Define el valor de la propiedad acctPayMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcctPayMethod(String value) {
        this.acctPayMethod = value;
    }

    /**
     * Gets the value of the autoPayChannel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the autoPayChannel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAutoPayChannel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccountInfo.AutoPayChannel }
     * 
     * 
     */
    public List<AccountInfo.AutoPayChannel> getAutoPayChannel() {
        if (autoPayChannel == null) {
            autoPayChannel = new ArrayList<AccountInfo.AutoPayChannel>();
        }
        return this.autoPayChannel;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AutoPayChannelKey" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/>
     *         &lt;element name="AutoPayChannelInfo" form="qualified">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
     *                 &lt;sequence>
     *                 &lt;/sequence>
     *               &lt;/extension>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "autoPayChannelKey",
        "autoPayChannelInfo"
    })
    public static class AutoPayChannel {

        @XmlElement(name = "AutoPayChannelKey", required = true)
        protected String autoPayChannelKey;
        @XmlElement(name = "AutoPayChannelInfo", required = true)
        protected AccountInfo.AutoPayChannel.AutoPayChannelInfo autoPayChannelInfo;

        /**
         * Obtiene el valor de la propiedad autoPayChannelKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAutoPayChannelKey() {
            return autoPayChannelKey;
        }

        /**
         * Define el valor de la propiedad autoPayChannelKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAutoPayChannelKey(String value) {
            this.autoPayChannelKey = value;
        }

        /**
         * Obtiene el valor de la propiedad autoPayChannelInfo.
         * 
         * @return
         *     possible object is
         *     {@link AccountInfo.AutoPayChannel.AutoPayChannelInfo }
         *     
         */
        public AccountInfo.AutoPayChannel.AutoPayChannelInfo getAutoPayChannelInfo() {
            return autoPayChannelInfo;
        }

        /**
         * Define el valor de la propiedad autoPayChannelInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AccountInfo.AutoPayChannel.AutoPayChannelInfo }
         *     
         */
        public void setAutoPayChannelInfo(AccountInfo.AutoPayChannel.AutoPayChannelInfo value) {
            this.autoPayChannelInfo = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}AutoPayChannelInfo">
         *       &lt;sequence>
         *       &lt;/sequence>
         *     &lt;/extension>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "")
        public static class AutoPayChannelInfo
            extends com.huawei.bme.cbsinterface.bccommon.AutoPayChannelInfo
        {


        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LimitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
     *         &lt;element name="LimitValue" type="{http://www.w3.org/2001/XMLSchema}long" form="qualified"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "limitType",
        "limitValue"
    })
    public static class CreditLimit {

        @XmlElement(name = "LimitType")
        protected String limitType;
        @XmlElement(name = "LimitValue")
        protected long limitValue;

        /**
         * Obtiene el valor de la propiedad limitType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLimitType() {
            return limitType;
        }

        /**
         * Define el valor de la propiedad limitType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLimitType(String value) {
            this.limitType = value;
        }

        /**
         * Obtiene el valor de la propiedad limitValue.
         * 
         */
        public long getLimitValue() {
            return limitValue;
        }

        /**
         * Define el valor de la propiedad limitValue.
         * 
         */
        public void setLimitValue(long value) {
            this.limitValue = value;
        }

    }

}
