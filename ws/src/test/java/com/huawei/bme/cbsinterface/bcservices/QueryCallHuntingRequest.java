
package com.huawei.bme.cbsinterface.bcservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SubGroupAccessCode;


/**
 * <p>Clase Java para QueryCallHuntingRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="QueryCallHuntingRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubGroupAccessCode" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroupAccessCode" minOccurs="0"/>
 *         &lt;element name="HuntingNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HuntingMainNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExpireTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryCallHuntingRequest", propOrder = {
    "subGroupAccessCode",
    "huntingNumber",
    "huntingMainNumber",
    "effectiveTime",
    "expireTime"
})
public class QueryCallHuntingRequest {

    @XmlElement(name = "SubGroupAccessCode")
    protected SubGroupAccessCode subGroupAccessCode;
    @XmlElement(name = "HuntingNumber", required = true)
    protected String huntingNumber;
    @XmlElement(name = "HuntingMainNumber", required = true)
    protected String huntingMainNumber;
    @XmlElement(name = "EffectiveTime")
    protected String effectiveTime;
    @XmlElement(name = "ExpireTime")
    protected String expireTime;

    /**
     * Obtiene el valor de la propiedad subGroupAccessCode.
     * 
     * @return
     *     possible object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public SubGroupAccessCode getSubGroupAccessCode() {
        return subGroupAccessCode;
    }

    /**
     * Define el valor de la propiedad subGroupAccessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SubGroupAccessCode }
     *     
     */
    public void setSubGroupAccessCode(SubGroupAccessCode value) {
        this.subGroupAccessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad huntingNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHuntingNumber() {
        return huntingNumber;
    }

    /**
     * Define el valor de la propiedad huntingNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHuntingNumber(String value) {
        this.huntingNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad huntingMainNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHuntingMainNumber() {
        return huntingMainNumber;
    }

    /**
     * Define el valor de la propiedad huntingMainNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHuntingMainNumber(String value) {
        this.huntingMainNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad effectiveTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveTime() {
        return effectiveTime;
    }

    /**
     * Define el valor de la propiedad effectiveTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveTime(String value) {
        this.effectiveTime = value;
    }

    /**
     * Obtiene el valor de la propiedad expireTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpireTime() {
        return expireTime;
    }

    /**
     * Define el valor de la propiedad expireTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpireTime(String value) {
        this.expireTime = value;
    }

}
