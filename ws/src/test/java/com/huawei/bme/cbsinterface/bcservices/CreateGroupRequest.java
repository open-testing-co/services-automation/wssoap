
package com.huawei.bme.cbsinterface.bcservices;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.AccountInfo;
import com.huawei.bme.cbsinterface.bccommon.ActiveMode;
import com.huawei.bme.cbsinterface.bccommon.Address;
import com.huawei.bme.cbsinterface.bccommon.CustInfo;
import com.huawei.bme.cbsinterface.bccommon.EffectMode;
import com.huawei.bme.cbsinterface.bccommon.IndividualInfo;
import com.huawei.bme.cbsinterface.bccommon.OfferingInst;
import com.huawei.bme.cbsinterface.bccommon.OfferingOwner;
import com.huawei.bme.cbsinterface.bccommon.OrgInfo;
import com.huawei.bme.cbsinterface.bccommon.POfferingInst;
import com.huawei.bme.cbsinterface.bccommon.SalesInfo;
import com.huawei.bme.cbsinterface.bccommon.SubGroup;


/**
 * <p>Clase Java para CreateGroupRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreateGroupRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegisterCustomer">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
 *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="OpType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="UserCustomer" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
 *                   &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
 *                   &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Account" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AcctInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Group">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SubGroupKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubGroupInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroup" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PrimaryOffering" type="{http://www.huawei.com/bme/cbsinterface/bccommon}POfferingInst"/>
 *         &lt;element name="SupplementaryOffering" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
 *                 &lt;sequence>
 *                   &lt;element name="OfferingOwner" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingOwner" minOccurs="0"/>
 *                   &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddressInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}Address" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SalesInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SalesInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateGroupRequest", propOrder = {
    "registerCustomer",
    "userCustomer",
    "account",
    "group",
    "primaryOffering",
    "supplementaryOffering",
    "addressInfo",
    "salesInfo"
})
public class CreateGroupRequest {

    @XmlElement(name = "RegisterCustomer", required = true)
    protected CreateGroupRequest.RegisterCustomer registerCustomer;
    @XmlElement(name = "UserCustomer")
    protected CreateGroupRequest.UserCustomer userCustomer;
    @XmlElement(name = "Account")
    protected CreateGroupRequest.Account account;
    @XmlElement(name = "Group", required = true)
    protected CreateGroupRequest.Group group;
    @XmlElement(name = "PrimaryOffering", required = true)
    protected POfferingInst primaryOffering;
    @XmlElement(name = "SupplementaryOffering")
    protected List<CreateGroupRequest.SupplementaryOffering> supplementaryOffering;
    @XmlElement(name = "AddressInfo")
    protected List<Address> addressInfo;
    @XmlElement(name = "SalesInfo")
    protected SalesInfo salesInfo;

    /**
     * Obtiene el valor de la propiedad registerCustomer.
     * 
     * @return
     *     possible object is
     *     {@link CreateGroupRequest.RegisterCustomer }
     *     
     */
    public CreateGroupRequest.RegisterCustomer getRegisterCustomer() {
        return registerCustomer;
    }

    /**
     * Define el valor de la propiedad registerCustomer.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateGroupRequest.RegisterCustomer }
     *     
     */
    public void setRegisterCustomer(CreateGroupRequest.RegisterCustomer value) {
        this.registerCustomer = value;
    }

    /**
     * Obtiene el valor de la propiedad userCustomer.
     * 
     * @return
     *     possible object is
     *     {@link CreateGroupRequest.UserCustomer }
     *     
     */
    public CreateGroupRequest.UserCustomer getUserCustomer() {
        return userCustomer;
    }

    /**
     * Define el valor de la propiedad userCustomer.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateGroupRequest.UserCustomer }
     *     
     */
    public void setUserCustomer(CreateGroupRequest.UserCustomer value) {
        this.userCustomer = value;
    }

    /**
     * Obtiene el valor de la propiedad account.
     * 
     * @return
     *     possible object is
     *     {@link CreateGroupRequest.Account }
     *     
     */
    public CreateGroupRequest.Account getAccount() {
        return account;
    }

    /**
     * Define el valor de la propiedad account.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateGroupRequest.Account }
     *     
     */
    public void setAccount(CreateGroupRequest.Account value) {
        this.account = value;
    }

    /**
     * Obtiene el valor de la propiedad group.
     * 
     * @return
     *     possible object is
     *     {@link CreateGroupRequest.Group }
     *     
     */
    public CreateGroupRequest.Group getGroup() {
        return group;
    }

    /**
     * Define el valor de la propiedad group.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateGroupRequest.Group }
     *     
     */
    public void setGroup(CreateGroupRequest.Group value) {
        this.group = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryOffering.
     * 
     * @return
     *     possible object is
     *     {@link POfferingInst }
     *     
     */
    public POfferingInst getPrimaryOffering() {
        return primaryOffering;
    }

    /**
     * Define el valor de la propiedad primaryOffering.
     * 
     * @param value
     *     allowed object is
     *     {@link POfferingInst }
     *     
     */
    public void setPrimaryOffering(POfferingInst value) {
        this.primaryOffering = value;
    }

    /**
     * Gets the value of the supplementaryOffering property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplementaryOffering property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplementaryOffering().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateGroupRequest.SupplementaryOffering }
     * 
     * 
     */
    public List<CreateGroupRequest.SupplementaryOffering> getSupplementaryOffering() {
        if (supplementaryOffering == null) {
            supplementaryOffering = new ArrayList<CreateGroupRequest.SupplementaryOffering>();
        }
        return this.supplementaryOffering;
    }

    /**
     * Gets the value of the addressInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addressInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddressInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Address }
     * 
     * 
     */
    public List<Address> getAddressInfo() {
        if (addressInfo == null) {
            addressInfo = new ArrayList<Address>();
        }
        return this.addressInfo;
    }

    /**
     * Obtiene el valor de la propiedad salesInfo.
     * 
     * @return
     *     possible object is
     *     {@link SalesInfo }
     *     
     */
    public SalesInfo getSalesInfo() {
        return salesInfo;
    }

    /**
     * Define el valor de la propiedad salesInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link SalesInfo }
     *     
     */
    public void setSalesInfo(SalesInfo value) {
        this.salesInfo = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PayRelationKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AcctKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AcctInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}AccountInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "payRelationKey",
        "acctKey",
        "acctInfo"
    })
    public static class Account {

        @XmlElement(name = "PayRelationKey", required = true)
        protected String payRelationKey;
        @XmlElement(name = "AcctKey", required = true)
        protected String acctKey;
        @XmlElement(name = "AcctInfo")
        protected AccountInfo acctInfo;

        /**
         * Obtiene el valor de la propiedad payRelationKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPayRelationKey() {
            return payRelationKey;
        }

        /**
         * Define el valor de la propiedad payRelationKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPayRelationKey(String value) {
            this.payRelationKey = value;
        }

        /**
         * Obtiene el valor de la propiedad acctKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcctKey() {
            return acctKey;
        }

        /**
         * Define el valor de la propiedad acctKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcctKey(String value) {
            this.acctKey = value;
        }

        /**
         * Obtiene el valor de la propiedad acctInfo.
         * 
         * @return
         *     possible object is
         *     {@link AccountInfo }
         *     
         */
        public AccountInfo getAcctInfo() {
            return acctInfo;
        }

        /**
         * Define el valor de la propiedad acctInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link AccountInfo }
         *     
         */
        public void setAcctInfo(AccountInfo value) {
            this.acctInfo = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SubGroupKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubGroupInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SubGroup" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subGroupKey",
        "subGroupInfo"
    })
    public static class Group {

        @XmlElement(name = "SubGroupKey", required = true)
        protected String subGroupKey;
        @XmlElement(name = "SubGroupInfo")
        protected SubGroup subGroupInfo;

        /**
         * Obtiene el valor de la propiedad subGroupKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubGroupKey() {
            return subGroupKey;
        }

        /**
         * Define el valor de la propiedad subGroupKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubGroupKey(String value) {
            this.subGroupKey = value;
        }

        /**
         * Obtiene el valor de la propiedad subGroupInfo.
         * 
         * @return
         *     possible object is
         *     {@link SubGroup }
         *     
         */
        public SubGroup getSubGroupInfo() {
            return subGroupInfo;
        }

        /**
         * Define el valor de la propiedad subGroupInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link SubGroup }
         *     
         */
        public void setSubGroupInfo(SubGroup value) {
            this.subGroupInfo = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
     *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="OpType" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custKey",
        "custInfo",
        "individualInfo",
        "orgInfo"
    })
    public static class RegisterCustomer {

        @XmlElement(name = "CustKey", required = true)
        protected String custKey;
        @XmlElement(name = "CustInfo")
        protected CustInfo custInfo;
        @XmlElement(name = "IndividualInfo")
        protected IndividualInfo individualInfo;
        @XmlElement(name = "OrgInfo")
        protected OrgInfo orgInfo;
        @XmlAttribute(name = "OpType", required = true)
        protected String opType;

        /**
         * Obtiene el valor de la propiedad custKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustKey() {
            return custKey;
        }

        /**
         * Define el valor de la propiedad custKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustKey(String value) {
            this.custKey = value;
        }

        /**
         * Obtiene el valor de la propiedad custInfo.
         * 
         * @return
         *     possible object is
         *     {@link CustInfo }
         *     
         */
        public CustInfo getCustInfo() {
            return custInfo;
        }

        /**
         * Define el valor de la propiedad custInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInfo }
         *     
         */
        public void setCustInfo(CustInfo value) {
            this.custInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad individualInfo.
         * 
         * @return
         *     possible object is
         *     {@link IndividualInfo }
         *     
         */
        public IndividualInfo getIndividualInfo() {
            return individualInfo;
        }

        /**
         * Define el valor de la propiedad individualInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link IndividualInfo }
         *     
         */
        public void setIndividualInfo(IndividualInfo value) {
            this.individualInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad orgInfo.
         * 
         * @return
         *     possible object is
         *     {@link OrgInfo }
         *     
         */
        public OrgInfo getOrgInfo() {
            return orgInfo;
        }

        /**
         * Define el valor de la propiedad orgInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgInfo }
         *     
         */
        public void setOrgInfo(OrgInfo value) {
            this.orgInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad opType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOpType() {
            return opType;
        }

        /**
         * Define el valor de la propiedad opType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOpType(String value) {
            this.opType = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingInst">
     *       &lt;sequence>
     *         &lt;element name="OfferingOwner" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OfferingOwner" minOccurs="0"/>
     *         &lt;element name="EffectiveTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}EffectMode"/>
     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ActivationTime" type="{http://www.huawei.com/bme/cbsinterface/bccommon}ActiveMode" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offeringOwner",
        "effectiveTime",
        "expirationTime",
        "activationTime"
    })
    public static class SupplementaryOffering
        extends OfferingInst
    {

        @XmlElement(name = "OfferingOwner")
        protected OfferingOwner offeringOwner;
        @XmlElement(name = "EffectiveTime", required = true)
        protected EffectMode effectiveTime;
        @XmlElement(name = "ExpirationTime", required = true)
        protected String expirationTime;
        @XmlElement(name = "ActivationTime")
        protected ActiveMode activationTime;

        /**
         * Obtiene el valor de la propiedad offeringOwner.
         * 
         * @return
         *     possible object is
         *     {@link OfferingOwner }
         *     
         */
        public OfferingOwner getOfferingOwner() {
            return offeringOwner;
        }

        /**
         * Define el valor de la propiedad offeringOwner.
         * 
         * @param value
         *     allowed object is
         *     {@link OfferingOwner }
         *     
         */
        public void setOfferingOwner(OfferingOwner value) {
            this.offeringOwner = value;
        }

        /**
         * Obtiene el valor de la propiedad effectiveTime.
         * 
         * @return
         *     possible object is
         *     {@link EffectMode }
         *     
         */
        public EffectMode getEffectiveTime() {
            return effectiveTime;
        }

        /**
         * Define el valor de la propiedad effectiveTime.
         * 
         * @param value
         *     allowed object is
         *     {@link EffectMode }
         *     
         */
        public void setEffectiveTime(EffectMode value) {
            this.effectiveTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationTime() {
            return expirationTime;
        }

        /**
         * Define el valor de la propiedad expirationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationTime(String value) {
            this.expirationTime = value;
        }

        /**
         * Obtiene el valor de la propiedad activationTime.
         * 
         * @return
         *     possible object is
         *     {@link ActiveMode }
         *     
         */
        public ActiveMode getActivationTime() {
            return activationTime;
        }

        /**
         * Define el valor de la propiedad activationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link ActiveMode }
         *     
         */
        public void setActivationTime(ActiveMode value) {
            this.activationTime = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CustKey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CustInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}CustInfo" minOccurs="0"/>
     *         &lt;element name="IndividualInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}IndividualInfo" minOccurs="0"/>
     *         &lt;element name="OrgInfo" type="{http://www.huawei.com/bme/cbsinterface/bccommon}OrgInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "custKey",
        "custInfo",
        "individualInfo",
        "orgInfo"
    })
    public static class UserCustomer {

        @XmlElement(name = "CustKey", required = true)
        protected String custKey;
        @XmlElement(name = "CustInfo")
        protected CustInfo custInfo;
        @XmlElement(name = "IndividualInfo")
        protected IndividualInfo individualInfo;
        @XmlElement(name = "OrgInfo")
        protected OrgInfo orgInfo;

        /**
         * Obtiene el valor de la propiedad custKey.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustKey() {
            return custKey;
        }

        /**
         * Define el valor de la propiedad custKey.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustKey(String value) {
            this.custKey = value;
        }

        /**
         * Obtiene el valor de la propiedad custInfo.
         * 
         * @return
         *     possible object is
         *     {@link CustInfo }
         *     
         */
        public CustInfo getCustInfo() {
            return custInfo;
        }

        /**
         * Define el valor de la propiedad custInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link CustInfo }
         *     
         */
        public void setCustInfo(CustInfo value) {
            this.custInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad individualInfo.
         * 
         * @return
         *     possible object is
         *     {@link IndividualInfo }
         *     
         */
        public IndividualInfo getIndividualInfo() {
            return individualInfo;
        }

        /**
         * Define el valor de la propiedad individualInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link IndividualInfo }
         *     
         */
        public void setIndividualInfo(IndividualInfo value) {
            this.individualInfo = value;
        }

        /**
         * Obtiene el valor de la propiedad orgInfo.
         * 
         * @return
         *     possible object is
         *     {@link OrgInfo }
         *     
         */
        public OrgInfo getOrgInfo() {
            return orgInfo;
        }

        /**
         * Define el valor de la propiedad orgInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgInfo }
         *     
         */
        public void setOrgInfo(OrgInfo value) {
            this.orgInfo = value;
        }

    }

}
