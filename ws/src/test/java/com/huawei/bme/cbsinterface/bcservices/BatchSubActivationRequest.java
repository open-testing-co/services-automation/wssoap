
package com.huawei.bme.cbsinterface.bcservices;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bccommon.SimpleProperty;


/**
 * <p>Clase Java para BatchSubActivationRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BatchSubActivationRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubBasicInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SubProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OfferingInst" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="OwnerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ActiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatchSubActivationRequest", propOrder = {
    "subBasicInfo",
    "offeringInst",
    "fileName"
})
public class BatchSubActivationRequest {

    @XmlElement(name = "SubBasicInfo")
    protected BatchSubActivationRequest.SubBasicInfo subBasicInfo;
    @XmlElement(name = "OfferingInst")
    protected List<BatchSubActivationRequest.OfferingInst> offeringInst;
    @XmlElement(name = "FileName", required = true)
    protected String fileName;

    /**
     * Obtiene el valor de la propiedad subBasicInfo.
     * 
     * @return
     *     possible object is
     *     {@link BatchSubActivationRequest.SubBasicInfo }
     *     
     */
    public BatchSubActivationRequest.SubBasicInfo getSubBasicInfo() {
        return subBasicInfo;
    }

    /**
     * Define el valor de la propiedad subBasicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchSubActivationRequest.SubBasicInfo }
     *     
     */
    public void setSubBasicInfo(BatchSubActivationRequest.SubBasicInfo value) {
        this.subBasicInfo = value;
    }

    /**
     * Gets the value of the offeringInst property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offeringInst property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOfferingInst().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BatchSubActivationRequest.OfferingInst }
     * 
     * 
     */
    public List<BatchSubActivationRequest.OfferingInst> getOfferingInst() {
        if (offeringInst == null) {
            offeringInst = new ArrayList<BatchSubActivationRequest.OfferingInst>();
        }
        return this.offeringInst;
    }

    /**
     * Obtiene el valor de la propiedad fileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Define el valor de la propiedad fileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="OfferingID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
     *         &lt;element name="OwnerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ActiveTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ActiveTimeLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TrialStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TrialEndTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ExpirationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "offeringID",
        "ownerType",
        "activeTime",
        "activeTimeLimit",
        "trialStartTime",
        "trialEndTime",
        "expirationTime"
    })
    public static class OfferingInst {

        @XmlElement(name = "OfferingID", required = true, nillable = true)
        protected BigInteger offeringID;
        @XmlElement(name = "OwnerType")
        protected String ownerType;
        @XmlElement(name = "ActiveTime")
        protected String activeTime;
        @XmlElement(name = "ActiveTimeLimit")
        protected String activeTimeLimit;
        @XmlElement(name = "TrialStartTime")
        protected String trialStartTime;
        @XmlElement(name = "TrialEndTime")
        protected String trialEndTime;
        @XmlElement(name = "ExpirationTime")
        protected String expirationTime;

        /**
         * Obtiene el valor de la propiedad offeringID.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getOfferingID() {
            return offeringID;
        }

        /**
         * Define el valor de la propiedad offeringID.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setOfferingID(BigInteger value) {
            this.offeringID = value;
        }

        /**
         * Obtiene el valor de la propiedad ownerType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOwnerType() {
            return ownerType;
        }

        /**
         * Define el valor de la propiedad ownerType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOwnerType(String value) {
            this.ownerType = value;
        }

        /**
         * Obtiene el valor de la propiedad activeTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActiveTime() {
            return activeTime;
        }

        /**
         * Define el valor de la propiedad activeTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActiveTime(String value) {
            this.activeTime = value;
        }

        /**
         * Obtiene el valor de la propiedad activeTimeLimit.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActiveTimeLimit() {
            return activeTimeLimit;
        }

        /**
         * Define el valor de la propiedad activeTimeLimit.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActiveTimeLimit(String value) {
            this.activeTimeLimit = value;
        }

        /**
         * Obtiene el valor de la propiedad trialStartTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrialStartTime() {
            return trialStartTime;
        }

        /**
         * Define el valor de la propiedad trialStartTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrialStartTime(String value) {
            this.trialStartTime = value;
        }

        /**
         * Obtiene el valor de la propiedad trialEndTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTrialEndTime() {
            return trialEndTime;
        }

        /**
         * Define el valor de la propiedad trialEndTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTrialEndTime(String value) {
            this.trialEndTime = value;
        }

        /**
         * Obtiene el valor de la propiedad expirationTime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExpirationTime() {
            return expirationTime;
        }

        /**
         * Define el valor de la propiedad expirationTime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExpirationTime(String value) {
            this.expirationTime = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="WrittenLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IVRLang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SubPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SubProperty" type="{http://www.huawei.com/bme/cbsinterface/bccommon}SimpleProperty" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "writtenLang",
        "ivrLang",
        "subPassword",
        "subProperty"
    })
    public static class SubBasicInfo {

        @XmlElement(name = "WrittenLang")
        protected String writtenLang;
        @XmlElement(name = "IVRLang")
        protected String ivrLang;
        @XmlElement(name = "SubPassword")
        protected String subPassword;
        @XmlElement(name = "SubProperty")
        protected List<SimpleProperty> subProperty;

        /**
         * Obtiene el valor de la propiedad writtenLang.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWrittenLang() {
            return writtenLang;
        }

        /**
         * Define el valor de la propiedad writtenLang.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWrittenLang(String value) {
            this.writtenLang = value;
        }

        /**
         * Obtiene el valor de la propiedad ivrLang.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIVRLang() {
            return ivrLang;
        }

        /**
         * Define el valor de la propiedad ivrLang.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIVRLang(String value) {
            this.ivrLang = value;
        }

        /**
         * Obtiene el valor de la propiedad subPassword.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubPassword() {
            return subPassword;
        }

        /**
         * Define el valor de la propiedad subPassword.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubPassword(String value) {
            this.subPassword = value;
        }

        /**
         * Gets the value of the subProperty property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the subProperty property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSubProperty().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SimpleProperty }
         * 
         * 
         */
        public List<SimpleProperty> getSubProperty() {
            if (subProperty == null) {
                subProperty = new ArrayList<SimpleProperty>();
            }
            return this.subProperty;
        }

    }

}
