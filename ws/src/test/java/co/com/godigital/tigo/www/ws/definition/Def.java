package co.com.godigital.tigo.www.ws.definition;

import co.com.godigital.tigo.www.ws.GatewayCBS;
import cucumber.api.java.en.Given;

public class Def {

	GatewayCBS gat = new GatewayCBS();
	
	@Given("^Quiero saber si la cuenta \"([^\"]*)\" existe$")
	public void quiero_saber_si_la_cuenta_existe(String cuenta) {
		gat.consultarCuenta(cuenta);
	}

	@Given("^consulto por el PrimaryIdentity \"([^\"]*)\"$")
	public void consulto_por_el_PrimaryIdentity(String msisdn) {
		gat.consultarPorMsisdn(msisdn);
	}
	
	@Given("^el campo \"([^\"]*)\" tiene el texto \"([^\"]*)\"$")
	public void el_campo_tiene_el_texto(String obj, String text) {
		gat.compararTexto(obj,text);
		
	}
}
