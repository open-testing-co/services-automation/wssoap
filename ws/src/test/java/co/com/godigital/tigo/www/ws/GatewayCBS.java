package co.com.godigital.tigo.www.ws;

import com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoRequest.QueryObj.AcctAccessCode;
import com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoRequest.QueryObj.SubAccessCode;
import com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoResult;
import com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoRequest;
import com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoRequest.QueryObj;

import co.com.godigital.tigo.www.ws.utilities.ActionsUtil;
import co.com.tigo.gatewaycbs.web.ws.GatewayCbsWSException_Exception;
import co.com.tigo.gatewaycbs.web.ws.GatewayCbsWsBcServices;
import co.com.tigo.gatewaycbs.web.ws.GatewayCbsWsBcServicesPortType1;
import co.com.tigo.gatewaycbs.web.ws.QueryCustomerInfoResponse;

public class GatewayCBS {

	GatewayCbsWsBcServicesPortType1 interfaz = new GatewayCbsWsBcServices().getGatewayCbsWsBcServicesPortType1Port();
	
	AcctAccessCode cod = new AcctAccessCode();
	QueryObj obj = new QueryObj();
	QueryCustomerInfoRequest ctm = new QueryCustomerInfoRequest();
	
	public static QueryCustomerInfoResponse.Return response = new QueryCustomerInfoResponse.Return();
	//public static QueryCustomerInfoResult
	
	
	public void consultarCuenta(String cuenta) {
		
		cod.setAccountKey(cuenta);
		obj.setAcctAccessCode(cod);
		ctm.setQueryObj(obj);
		
		try {
			response= interfaz.queryCustomerInfo(ctm);
			System.out.println(response.getResultHeader().getResultDesc());
		} catch (GatewayCbsWSException_Exception e) {
			e.printStackTrace();
		}
		
	}
	
	SubAccessCode Identity= new SubAccessCode();
	
	public void consultarPorMsisdn(String msisdn) {
		Identity.setPrimaryIdentity(msisdn);
		obj.setSubAccessCode(Identity);
		ctm.setQueryObj(obj);

			try {
				response= interfaz.queryCustomerInfo(ctm);
				//System.out.println("aqui"+response.getResultHeader().getResultDesc());
			} catch (GatewayCbsWSException_Exception e) {
				e.printStackTrace();
			}

	}

	public void compararTexto(String obj,String text) {
		ActionsUtil.comparar(obj,text);
		
	}
	
	
	
}
