package co.com.godigital.tigo.www.ws.utilities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoResult.Account;

import co.com.godigital.tigo.www.ws.GatewayCBS;

public class ActionsUtil {

	private ActionsUtil() {
		throw new IllegalStateException("Utility class");
	}
	
	static PropertiesLoader properties = PropertiesLoader.getInstance();

	public static void comparar(String obj,String text) {
		switch (ActionsUtil.textoMinusculasSinEspacios(obj)) {
		case "resultdesc":
			String txt = GatewayCBS.response.getResultHeader().getResultDesc();
			assertEquals("No coinciden los valores a comparar", txt, text);
			break;
			/*
		case "firstname":
			String nm = GatewayCBS.response.getQueryCustomerInfoResult()*/
		default:
			assertTrue("El valor enviado "+ obj +" no se encuentra mapeado", Boolean.FALSE);
			break;
		}
	}
	
	
	public static String textoMinusculasSinEspacios(String texto) {
		String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
		String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
		for (int i = 0; i < original.length(); i++) {
			texto = texto.replace(original.charAt(i), ascii.charAt(i));
		}
		texto = texto.replaceAll("\t|\n| ", "");
		texto = texto.toLowerCase();
		return texto;
	}

	/*
	public static void compareText(String obj,String txt) {
		String valorObtenido = getText(obj, txt);
		txt = txt.replace("\\\"", "\"");
		assertEquals(txt, valorObtenido);
	}*/
	
}
