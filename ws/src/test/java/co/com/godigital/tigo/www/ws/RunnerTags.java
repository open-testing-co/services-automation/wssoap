package co.com.godigital.tigo.www.ws;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

//@tag1 AND @tag2= "@tag1","@tag2"
//@tag1 OR @tag2= "@tag1, @tag2"
//NOT @tag1 = "~@tag1"
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(glue = { "co.com.godigital.tigo.www.ws" }, features = {
		"src/test/resources/features/consultarCuenta.feature"},

		tags = { "" }, monochrome = true, plugin = { "pretty", "html:target/reports/html/",
				"junit:target/reports/junit.xml", "junit:target/reports/test.xml", "junit:target/reports/TEST-all.xml", "json:target/reports/cukes.json" })

public class RunnerTags {
}

