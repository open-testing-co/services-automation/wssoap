
package co.com.tigo.gatewaycbs.web.ws;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "GatewayCbsWsBcServices", targetNamespace = "http://ws.web.gatewaycbs.tigo.com.co/", wsdlLocation = "http://10.65.45.12:7001/gatewaycbs/BcServicesInt?wsdl")
public class GatewayCbsWsBcServices
    extends Service
{

    private final static URL GATEWAYCBSWSBCSERVICES_WSDL_LOCATION;
    private final static WebServiceException GATEWAYCBSWSBCSERVICES_EXCEPTION;
    private final static QName GATEWAYCBSWSBCSERVICES_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "GatewayCbsWsBcServices");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://10.65.45.12:7001/gatewaycbs/BcServicesInt?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        GATEWAYCBSWSBCSERVICES_WSDL_LOCATION = url;
        GATEWAYCBSWSBCSERVICES_EXCEPTION = e;
    }

    public GatewayCbsWsBcServices() {
        super(__getWsdlLocation(), GATEWAYCBSWSBCSERVICES_QNAME);
    }

    public GatewayCbsWsBcServices(WebServiceFeature... features) {
        super(__getWsdlLocation(), GATEWAYCBSWSBCSERVICES_QNAME, features);
    }

    public GatewayCbsWsBcServices(URL wsdlLocation) {
        super(wsdlLocation, GATEWAYCBSWSBCSERVICES_QNAME);
    }

    public GatewayCbsWsBcServices(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, GATEWAYCBSWSBCSERVICES_QNAME, features);
    }

    public GatewayCbsWsBcServices(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public GatewayCbsWsBcServices(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns GatewayCbsWsBcServicesPortType1
     */
    @WebEndpoint(name = "GatewayCbsWsBcServicesPortType1Port")
    public GatewayCbsWsBcServicesPortType1 getGatewayCbsWsBcServicesPortType1Port() {
        return super.getPort(new QName("http://ws.web.gatewaycbs.tigo.com.co/", "GatewayCbsWsBcServicesPortType1Port"), GatewayCbsWsBcServicesPortType1.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns GatewayCbsWsBcServicesPortType1
     */
    @WebEndpoint(name = "GatewayCbsWsBcServicesPortType1Port")
    public GatewayCbsWsBcServicesPortType1 getGatewayCbsWsBcServicesPortType1Port(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.web.gatewaycbs.tigo.com.co/", "GatewayCbsWsBcServicesPortType1Port"), GatewayCbsWsBcServicesPortType1.class, features);
    }

    private static URL __getWsdlLocation() {
        if (GATEWAYCBSWSBCSERVICES_EXCEPTION!= null) {
            throw GATEWAYCBSWSBCSERVICES_EXCEPTION;
        }
        return GATEWAYCBSWSBCSERVICES_WSDL_LOCATION;
    }

}
