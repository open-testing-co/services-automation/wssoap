
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.AcctDeactivationRequest;


/**
 * <p>Clase Java para acctDeactivation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="acctDeactivation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AcctDeactivationRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}AcctDeactivationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "acctDeactivation", propOrder = {
    "acctDeactivationRequest"
})
public class AcctDeactivation {

    @XmlElement(name = "AcctDeactivationRequest")
    protected AcctDeactivationRequest acctDeactivationRequest;

    /**
     * Obtiene el valor de la propiedad acctDeactivationRequest.
     * 
     * @return
     *     possible object is
     *     {@link AcctDeactivationRequest }
     *     
     */
    public AcctDeactivationRequest getAcctDeactivationRequest() {
        return acctDeactivationRequest;
    }

    /**
     * Define el valor de la propiedad acctDeactivationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link AcctDeactivationRequest }
     *     
     */
    public void setAcctDeactivationRequest(AcctDeactivationRequest value) {
        this.acctDeactivationRequest = value;
    }

}
