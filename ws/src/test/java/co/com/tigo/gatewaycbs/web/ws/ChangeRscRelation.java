
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeRscRelationRequest;


/**
 * <p>Clase Java para changeRscRelation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeRscRelation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeRscRelationRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeRscRelationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeRscRelation", propOrder = {
    "changeRscRelationRequest"
})
public class ChangeRscRelation {

    @XmlElement(name = "ChangeRscRelationRequest")
    protected ChangeRscRelationRequest changeRscRelationRequest;

    /**
     * Obtiene el valor de la propiedad changeRscRelationRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeRscRelationRequest }
     *     
     */
    public ChangeRscRelationRequest getChangeRscRelationRequest() {
        return changeRscRelationRequest;
    }

    /**
     * Define el valor de la propiedad changeRscRelationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeRscRelationRequest }
     *     
     */
    public void setChangeRscRelationRequest(ChangeRscRelationRequest value) {
        this.changeRscRelationRequest = value;
    }

}
