
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeCustInfoRequest;


/**
 * <p>Clase Java para changeCustInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeCustInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeCustInfoRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeCustInfoRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeCustInfo", propOrder = {
    "changeCustInfoRequest"
})
public class ChangeCustInfo {

    @XmlElement(name = "ChangeCustInfoRequest")
    protected ChangeCustInfoRequest changeCustInfoRequest;

    /**
     * Obtiene el valor de la propiedad changeCustInfoRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeCustInfoRequest }
     *     
     */
    public ChangeCustInfoRequest getChangeCustInfoRequest() {
        return changeCustInfoRequest;
    }

    /**
     * Define el valor de la propiedad changeCustInfoRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeCustInfoRequest }
     *     
     */
    public void setChangeCustInfoRequest(ChangeCustInfoRequest value) {
        this.changeCustInfoRequest = value;
    }

}
