
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.CreateGroupRequest;


/**
 * <p>Clase Java para createGroup complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="createGroup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateGroupRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}CreateGroupRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createGroup", propOrder = {
    "createGroupRequest"
})
public class CreateGroup {

    @XmlElement(name = "CreateGroupRequest")
    protected CreateGroupRequest createGroupRequest;

    /**
     * Obtiene el valor de la propiedad createGroupRequest.
     * 
     * @return
     *     possible object is
     *     {@link CreateGroupRequest }
     *     
     */
    public CreateGroupRequest getCreateGroupRequest() {
        return createGroupRequest;
    }

    /**
     * Define el valor de la propiedad createGroupRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateGroupRequest }
     *     
     */
    public void setCreateGroupRequest(CreateGroupRequest value) {
        this.createGroupRequest = value;
    }

}
