
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeGroupOfferingRequest;


/**
 * <p>Clase Java para changeGroupOffering complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeGroupOffering">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeGroupOfferingRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeGroupOfferingRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeGroupOffering", propOrder = {
    "changeGroupOfferingRequest"
})
public class ChangeGroupOffering {

    @XmlElement(name = "ChangeGroupOfferingRequest")
    protected ChangeGroupOfferingRequest changeGroupOfferingRequest;

    /**
     * Obtiene el valor de la propiedad changeGroupOfferingRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeGroupOfferingRequest }
     *     
     */
    public ChangeGroupOfferingRequest getChangeGroupOfferingRequest() {
        return changeGroupOfferingRequest;
    }

    /**
     * Define el valor de la propiedad changeGroupOfferingRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeGroupOfferingRequest }
     *     
     */
    public void setChangeGroupOfferingRequest(ChangeGroupOfferingRequest value) {
        this.changeGroupOfferingRequest = value;
    }

}
