
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeAcctInfoRequest;


/**
 * <p>Clase Java para changeAcctInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeAcctInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeAcctInfoRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeAcctInfoRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeAcctInfo", propOrder = {
    "changeAcctInfoRequest"
})
public class ChangeAcctInfo {

    @XmlElement(name = "ChangeAcctInfoRequest")
    protected ChangeAcctInfoRequest changeAcctInfoRequest;

    /**
     * Obtiene el valor de la propiedad changeAcctInfoRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctInfoRequest }
     *     
     */
    public ChangeAcctInfoRequest getChangeAcctInfoRequest() {
        return changeAcctInfoRequest;
    }

    /**
     * Define el valor de la propiedad changeAcctInfoRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctInfoRequest }
     *     
     */
    public void setChangeAcctInfoRequest(ChangeAcctInfoRequest value) {
        this.changeAcctInfoRequest = value;
    }

}
