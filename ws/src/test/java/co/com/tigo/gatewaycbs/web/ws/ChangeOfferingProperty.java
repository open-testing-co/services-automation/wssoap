
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeOfferingPropertyRequest;


/**
 * <p>Clase Java para changeOfferingProperty complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeOfferingProperty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeOfferingPropertyRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeOfferingPropertyRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeOfferingProperty", propOrder = {
    "changeOfferingPropertyRequest"
})
public class ChangeOfferingProperty {

    @XmlElement(name = "ChangeOfferingPropertyRequest")
    protected ChangeOfferingPropertyRequest changeOfferingPropertyRequest;

    /**
     * Obtiene el valor de la propiedad changeOfferingPropertyRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeOfferingPropertyRequest }
     *     
     */
    public ChangeOfferingPropertyRequest getChangeOfferingPropertyRequest() {
        return changeOfferingPropertyRequest;
    }

    /**
     * Define el valor de la propiedad changeOfferingPropertyRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeOfferingPropertyRequest }
     *     
     */
    public void setChangeOfferingPropertyRequest(ChangeOfferingPropertyRequest value) {
        this.changeOfferingPropertyRequest = value;
    }

}
