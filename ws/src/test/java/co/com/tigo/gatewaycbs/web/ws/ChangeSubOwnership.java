
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeSubOwnershipRequest;


/**
 * <p>Clase Java para changeSubOwnership complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeSubOwnership">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeSubOwnershipRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeSubOwnershipRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeSubOwnership", propOrder = {
    "changeSubOwnershipRequest"
})
public class ChangeSubOwnership {

    @XmlElement(name = "ChangeSubOwnershipRequest")
    protected ChangeSubOwnershipRequest changeSubOwnershipRequest;

    /**
     * Obtiene el valor de la propiedad changeSubOwnershipRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubOwnershipRequest }
     *     
     */
    public ChangeSubOwnershipRequest getChangeSubOwnershipRequest() {
        return changeSubOwnershipRequest;
    }

    /**
     * Define el valor de la propiedad changeSubOwnershipRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubOwnershipRequest }
     *     
     */
    public void setChangeSubOwnershipRequest(ChangeSubOwnershipRequest value) {
        this.changeSubOwnershipRequest = value;
    }

}
