
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeGroupOfferingResult;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para changeGroupOfferingResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeGroupOfferingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
 *                   &lt;element name="ChangeGroupOfferingResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeGroupOfferingResult"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeGroupOfferingResponse", propOrder = {
    "_return"
})
public class ChangeGroupOfferingResponse {

    @XmlElement(name = "return")
    protected ChangeGroupOfferingResponse.Return _return;

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link ChangeGroupOfferingResponse.Return }
     *     
     */
    public ChangeGroupOfferingResponse.Return getReturn() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeGroupOfferingResponse.Return }
     *     
     */
    public void setReturn(ChangeGroupOfferingResponse.Return value) {
        this._return = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
     *         &lt;element name="ChangeGroupOfferingResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeGroupOfferingResult"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultHeader",
        "changeGroupOfferingResult"
    })
    public static class Return {

        @XmlElement(name = "ResultHeader", required = true)
        protected ResultHeader resultHeader;
        @XmlElement(name = "ChangeGroupOfferingResult", required = true)
        protected ChangeGroupOfferingResult changeGroupOfferingResult;

        /**
         * Obtiene el valor de la propiedad resultHeader.
         * 
         * @return
         *     possible object is
         *     {@link ResultHeader }
         *     
         */
        public ResultHeader getResultHeader() {
            return resultHeader;
        }

        /**
         * Define el valor de la propiedad resultHeader.
         * 
         * @param value
         *     allowed object is
         *     {@link ResultHeader }
         *     
         */
        public void setResultHeader(ResultHeader value) {
            this.resultHeader = value;
        }

        /**
         * Obtiene el valor de la propiedad changeGroupOfferingResult.
         * 
         * @return
         *     possible object is
         *     {@link ChangeGroupOfferingResult }
         *     
         */
        public ChangeGroupOfferingResult getChangeGroupOfferingResult() {
            return changeGroupOfferingResult;
        }

        /**
         * Define el valor de la propiedad changeGroupOfferingResult.
         * 
         * @param value
         *     allowed object is
         *     {@link ChangeGroupOfferingResult }
         *     
         */
        public void setChangeGroupOfferingResult(ChangeGroupOfferingResult value) {
            this.changeGroupOfferingResult = value;
        }

    }

}
