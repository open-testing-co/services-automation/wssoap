
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeSubDFTAcctRequest;


/**
 * <p>Clase Java para changeSubDFTA complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeSubDFTA">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeSubDFTAcctRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeSubDFTAcctRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeSubDFTA", propOrder = {
    "changeSubDFTAcctRequest"
})
public class ChangeSubDFTA {

    @XmlElement(name = "ChangeSubDFTAcctRequest")
    protected ChangeSubDFTAcctRequest changeSubDFTAcctRequest;

    /**
     * Obtiene el valor de la propiedad changeSubDFTAcctRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubDFTAcctRequest }
     *     
     */
    public ChangeSubDFTAcctRequest getChangeSubDFTAcctRequest() {
        return changeSubDFTAcctRequest;
    }

    /**
     * Define el valor de la propiedad changeSubDFTAcctRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubDFTAcctRequest }
     *     
     */
    public void setChangeSubDFTAcctRequest(ChangeSubDFTAcctRequest value) {
        this.changeSubDFTAcctRequest = value;
    }

}
