
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QueryGroupListBySubscriberResult;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para queryGroupListBySubscriberResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="queryGroupListBySubscriberResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
 *                   &lt;element name="QueryGroupListBySubscriberResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryGroupListBySubscriberResult"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryGroupListBySubscriberResponse", propOrder = {
    "_return"
})
public class QueryGroupListBySubscriberResponse {

    @XmlElement(name = "return")
    protected QueryGroupListBySubscriberResponse.Return _return;

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link QueryGroupListBySubscriberResponse.Return }
     *     
     */
    public QueryGroupListBySubscriberResponse.Return getReturn() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryGroupListBySubscriberResponse.Return }
     *     
     */
    public void setReturn(QueryGroupListBySubscriberResponse.Return value) {
        this._return = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
     *         &lt;element name="QueryGroupListBySubscriberResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryGroupListBySubscriberResult"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultHeader",
        "queryGroupListBySubscriberResult"
    })
    public static class Return {

        @XmlElement(name = "ResultHeader", required = true)
        protected ResultHeader resultHeader;
        @XmlElement(name = "QueryGroupListBySubscriberResult", required = true)
        protected QueryGroupListBySubscriberResult queryGroupListBySubscriberResult;

        /**
         * Obtiene el valor de la propiedad resultHeader.
         * 
         * @return
         *     possible object is
         *     {@link ResultHeader }
         *     
         */
        public ResultHeader getResultHeader() {
            return resultHeader;
        }

        /**
         * Define el valor de la propiedad resultHeader.
         * 
         * @param value
         *     allowed object is
         *     {@link ResultHeader }
         *     
         */
        public void setResultHeader(ResultHeader value) {
            this.resultHeader = value;
        }

        /**
         * Obtiene el valor de la propiedad queryGroupListBySubscriberResult.
         * 
         * @return
         *     possible object is
         *     {@link QueryGroupListBySubscriberResult }
         *     
         */
        public QueryGroupListBySubscriberResult getQueryGroupListBySubscriberResult() {
            return queryGroupListBySubscriberResult;
        }

        /**
         * Define el valor de la propiedad queryGroupListBySubscriberResult.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryGroupListBySubscriberResult }
         *     
         */
        public void setQueryGroupListBySubscriberResult(QueryGroupListBySubscriberResult value) {
            this.queryGroupListBySubscriberResult = value;
        }

    }

}
