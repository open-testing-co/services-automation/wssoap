
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QuerySubscribedOfferingsRequest;


/**
 * <p>Clase Java para querySubscribedOfferings complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="querySubscribedOfferings">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QuerySubscribedOfferingsRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QuerySubscribedOfferingsRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "querySubscribedOfferings", propOrder = {
    "querySubscribedOfferingsRequest"
})
public class QuerySubscribedOfferings {

    @XmlElement(name = "QuerySubscribedOfferingsRequest")
    protected QuerySubscribedOfferingsRequest querySubscribedOfferingsRequest;

    /**
     * Obtiene el valor de la propiedad querySubscribedOfferingsRequest.
     * 
     * @return
     *     possible object is
     *     {@link QuerySubscribedOfferingsRequest }
     *     
     */
    public QuerySubscribedOfferingsRequest getQuerySubscribedOfferingsRequest() {
        return querySubscribedOfferingsRequest;
    }

    /**
     * Define el valor de la propiedad querySubscribedOfferingsRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link QuerySubscribedOfferingsRequest }
     *     
     */
    public void setQuerySubscribedOfferingsRequest(QuerySubscribedOfferingsRequest value) {
        this.querySubscribedOfferingsRequest = value;
    }

}
