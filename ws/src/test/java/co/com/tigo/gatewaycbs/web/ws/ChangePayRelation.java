
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangePayRelationRequest;


/**
 * <p>Clase Java para changePayRelation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changePayRelation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangePayRelationRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangePayRelationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changePayRelation", propOrder = {
    "changePayRelationRequest"
})
public class ChangePayRelation {

    @XmlElement(name = "ChangePayRelationRequest")
    protected ChangePayRelationRequest changePayRelationRequest;

    /**
     * Obtiene el valor de la propiedad changePayRelationRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangePayRelationRequest }
     *     
     */
    public ChangePayRelationRequest getChangePayRelationRequest() {
        return changePayRelationRequest;
    }

    /**
     * Define el valor de la propiedad changePayRelationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangePayRelationRequest }
     *     
     */
    public void setChangePayRelationRequest(ChangePayRelationRequest value) {
        this.changePayRelationRequest = value;
    }

}
