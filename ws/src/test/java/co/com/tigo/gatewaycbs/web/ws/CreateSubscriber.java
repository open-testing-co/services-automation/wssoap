
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.CreateSubscriberRequest;


/**
 * <p>Clase Java para createSubscriber complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="createSubscriber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateSubscriberRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}CreateSubscriberRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createSubscriber", propOrder = {
    "createSubscriberRequest"
})
public class CreateSubscriber {

    @XmlElement(name = "CreateSubscriberRequest")
    protected CreateSubscriberRequest createSubscriberRequest;

    /**
     * Obtiene el valor de la propiedad createSubscriberRequest.
     * 
     * @return
     *     possible object is
     *     {@link CreateSubscriberRequest }
     *     
     */
    public CreateSubscriberRequest getCreateSubscriberRequest() {
        return createSubscriberRequest;
    }

    /**
     * Define el valor de la propiedad createSubscriberRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateSubscriberRequest }
     *     
     */
    public void setCreateSubscriberRequest(CreateSubscriberRequest value) {
        this.createSubscriberRequest = value;
    }

}
