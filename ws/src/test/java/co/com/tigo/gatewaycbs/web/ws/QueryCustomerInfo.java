
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoRequest;


/**
 * <p>Clase Java para queryCustomerInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="queryCustomerInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryCustomerInfoRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryCustomerInfoRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryCustomerInfo", propOrder = {
    "queryCustomerInfoRequest"
})
public class QueryCustomerInfo {

    @XmlElement(name = "QueryCustomerInfoRequest")
    protected QueryCustomerInfoRequest queryCustomerInfoRequest;

    /**
     * Obtiene el valor de la propiedad queryCustomerInfoRequest.
     * 
     * @return
     *     possible object is
     *     {@link QueryCustomerInfoRequest }
     *     
     */
    public QueryCustomerInfoRequest getQueryCustomerInfoRequest() {
        return queryCustomerInfoRequest;
    }

    /**
     * Define el valor de la propiedad queryCustomerInfoRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryCustomerInfoRequest }
     *     
     */
    public void setQueryCustomerInfoRequest(QueryCustomerInfoRequest value) {
        this.queryCustomerInfoRequest = value;
    }

}
