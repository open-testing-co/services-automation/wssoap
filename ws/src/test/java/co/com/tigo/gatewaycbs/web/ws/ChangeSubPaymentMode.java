
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeSubPaymentModeRequest;


/**
 * <p>Clase Java para changeSubPaymentMode complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeSubPaymentMode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeSubPaymentModeRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeSubPaymentModeRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeSubPaymentMode", propOrder = {
    "changeSubPaymentModeRequest"
})
public class ChangeSubPaymentMode {

    @XmlElement(name = "ChangeSubPaymentModeRequest")
    protected ChangeSubPaymentModeRequest changeSubPaymentModeRequest;

    /**
     * Obtiene el valor de la propiedad changeSubPaymentModeRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubPaymentModeRequest }
     *     
     */
    public ChangeSubPaymentModeRequest getChangeSubPaymentModeRequest() {
        return changeSubPaymentModeRequest;
    }

    /**
     * Define el valor de la propiedad changeSubPaymentModeRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubPaymentModeRequest }
     *     
     */
    public void setChangeSubPaymentModeRequest(ChangeSubPaymentModeRequest value) {
        this.changeSubPaymentModeRequest = value;
    }

}
