
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.CreateAccountRequest;


/**
 * <p>Clase Java para createAccount complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="createAccount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateAccountRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}CreateAccountRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createAccount", propOrder = {
    "createAccountRequest"
})
public class CreateAccount {

    @XmlElement(name = "CreateAccountRequest")
    protected CreateAccountRequest createAccountRequest;

    /**
     * Obtiene el valor de la propiedad createAccountRequest.
     * 
     * @return
     *     possible object is
     *     {@link CreateAccountRequest }
     *     
     */
    public CreateAccountRequest getCreateAccountRequest() {
        return createAccountRequest;
    }

    /**
     * Define el valor de la propiedad createAccountRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateAccountRequest }
     *     
     */
    public void setCreateAccountRequest(CreateAccountRequest value) {
        this.createAccountRequest = value;
    }

}
