
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QueryOfferingInstPropertyRequest;


/**
 * <p>Clase Java para queryOfferingInstProperty complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="queryOfferingInstProperty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryOfferingInstPropertyRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryOfferingInstPropertyRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryOfferingInstProperty", propOrder = {
    "queryOfferingInstPropertyRequest"
})
public class QueryOfferingInstProperty {

    @XmlElement(name = "QueryOfferingInstPropertyRequest")
    protected QueryOfferingInstPropertyRequest queryOfferingInstPropertyRequest;

    /**
     * Obtiene el valor de la propiedad queryOfferingInstPropertyRequest.
     * 
     * @return
     *     possible object is
     *     {@link QueryOfferingInstPropertyRequest }
     *     
     */
    public QueryOfferingInstPropertyRequest getQueryOfferingInstPropertyRequest() {
        return queryOfferingInstPropertyRequest;
    }

    /**
     * Define el valor de la propiedad queryOfferingInstPropertyRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryOfferingInstPropertyRequest }
     *     
     */
    public void setQueryOfferingInstPropertyRequest(QueryOfferingInstPropertyRequest value) {
        this.queryOfferingInstPropertyRequest = value;
    }

}
