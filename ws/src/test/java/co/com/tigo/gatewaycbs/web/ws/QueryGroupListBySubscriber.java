
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QueryGroupListBySubscriberRequest;


/**
 * <p>Clase Java para queryGroupListBySubscriber complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="queryGroupListBySubscriber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryGroupListBySubscriberRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryGroupListBySubscriberRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryGroupListBySubscriber", propOrder = {
    "queryGroupListBySubscriberRequest"
})
public class QueryGroupListBySubscriber {

    @XmlElement(name = "QueryGroupListBySubscriberRequest")
    protected QueryGroupListBySubscriberRequest queryGroupListBySubscriberRequest;

    /**
     * Obtiene el valor de la propiedad queryGroupListBySubscriberRequest.
     * 
     * @return
     *     possible object is
     *     {@link QueryGroupListBySubscriberRequest }
     *     
     */
    public QueryGroupListBySubscriberRequest getQueryGroupListBySubscriberRequest() {
        return queryGroupListBySubscriberRequest;
    }

    /**
     * Define el valor de la propiedad queryGroupListBySubscriberRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryGroupListBySubscriberRequest }
     *     
     */
    public void setQueryGroupListBySubscriberRequest(QueryGroupListBySubscriberRequest value) {
        this.queryGroupListBySubscriberRequest = value;
    }

}
