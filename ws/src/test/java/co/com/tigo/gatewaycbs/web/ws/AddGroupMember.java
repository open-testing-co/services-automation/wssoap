
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.AddGroupMemberRequest;


/**
 * <p>Clase Java para addGroupMember complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="addGroupMember">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddGroupMemberRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}AddGroupMemberRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addGroupMember", propOrder = {
    "addGroupMemberRequest"
})
public class AddGroupMember {

    @XmlElement(name = "AddGroupMemberRequest")
    protected AddGroupMemberRequest addGroupMemberRequest;

    /**
     * Obtiene el valor de la propiedad addGroupMemberRequest.
     * 
     * @return
     *     possible object is
     *     {@link AddGroupMemberRequest }
     *     
     */
    public AddGroupMemberRequest getAddGroupMemberRequest() {
        return addGroupMemberRequest;
    }

    /**
     * Define el valor de la propiedad addGroupMemberRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link AddGroupMemberRequest }
     *     
     */
    public void setAddGroupMemberRequest(AddGroupMemberRequest value) {
        this.addGroupMemberRequest = value;
    }

}
