
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeSubInfoRequest;


/**
 * <p>Clase Java para changeSubInfo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeSubInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeSubInfoRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeSubInfoRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeSubInfo", propOrder = {
    "changeSubInfoRequest"
})
public class ChangeSubInfo {

    @XmlElement(name = "ChangeSubInfoRequest")
    protected ChangeSubInfoRequest changeSubInfoRequest;

    /**
     * Obtiene el valor de la propiedad changeSubInfoRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubInfoRequest }
     *     
     */
    public ChangeSubInfoRequest getChangeSubInfoRequest() {
        return changeSubInfoRequest;
    }

    /**
     * Define el valor de la propiedad changeSubInfoRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubInfoRequest }
     *     
     */
    public void setChangeSubInfoRequest(ChangeSubInfoRequest value) {
        this.changeSubInfoRequest = value;
    }

}
