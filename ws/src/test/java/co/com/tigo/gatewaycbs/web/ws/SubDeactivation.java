
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.SubDeactivationRequest;


/**
 * <p>Clase Java para subDeactivation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="subDeactivation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubDeactivationRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}SubDeactivationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subDeactivation", propOrder = {
    "subDeactivationRequest"
})
public class SubDeactivation {

    @XmlElement(name = "SubDeactivationRequest")
    protected SubDeactivationRequest subDeactivationRequest;

    /**
     * Obtiene el valor de la propiedad subDeactivationRequest.
     * 
     * @return
     *     possible object is
     *     {@link SubDeactivationRequest }
     *     
     */
    public SubDeactivationRequest getSubDeactivationRequest() {
        return subDeactivationRequest;
    }

    /**
     * Define el valor de la propiedad subDeactivationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link SubDeactivationRequest }
     *     
     */
    public void setSubDeactivationRequest(SubDeactivationRequest value) {
        this.subDeactivationRequest = value;
    }

}
