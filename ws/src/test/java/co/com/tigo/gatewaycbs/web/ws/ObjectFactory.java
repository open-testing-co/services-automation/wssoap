
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.com.tigo.gatewaycbs.web.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GatewayCbsWSException_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "GatewayCbsWSException");
    private final static QName _CreateSubscriber_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "createSubscriber");
    private final static QName _ChangeAcctInfoResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeAcctInfoResponse");
    private final static QName _SubDeactivation_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "subDeactivation");
    private final static QName _ChangeSubIdentity_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubIdentity");
    private final static QName _ChangeRscRelationResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeRscRelationResponse");
    private final static QName _ChangeSubPaymentMode_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubPaymentMode");
    private final static QName _ChangeOfferingProperty_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeOfferingProperty");
    private final static QName _ChangeGroupOfferingResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeGroupOfferingResponse");
    private final static QName _QuerySubscribedOfferingsResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "querySubscribedOfferingsResponse");
    private final static QName _ChangeAcctBillCycle_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeAcctBillCycle");
    private final static QName _QueryPaymentRelation_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryPaymentRelation");
    private final static QName _ChangeSubInfoResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubInfoResponse");
    private final static QName _ChangeSubOfferingResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubOfferingResponse");
    private final static QName _QueryGroupListBySubscriber_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryGroupListBySubscriber");
    private final static QName _AddGroupMemberResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "addGroupMemberResponse");
    private final static QName _GroupDeactivationResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "groupDeactivationResponse");
    private final static QName _ChangeRscRelation_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeRscRelation");
    private final static QName _ChangePayRelationResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changePayRelationResponse");
    private final static QName _ChangeSubGrpDFTAcct_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubGrpDFTAcct");
    private final static QName _ChangeSubDFTAResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubDFTAResponse");
    private final static QName _CreateSubscriberResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "createSubscriberResponse");
    private final static QName _QueryGroupMemberList_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryGroupMemberList");
    private final static QName _ChangeSubOffering_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubOffering");
    private final static QName _DelGroupMemberResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "delGroupMemberResponse");
    private final static QName _QueryPaymentRelationResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryPaymentRelationResponse");
    private final static QName _DelGroupMember_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "delGroupMember");
    private final static QName _QueryOfferingInstProperty_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryOfferingInstProperty");
    private final static QName _ChangeCustInfoResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeCustInfoResponse");
    private final static QName _SubDeactivationResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "subDeactivationResponse");
    private final static QName _AddGroupMember_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "addGroupMember");
    private final static QName _QueryOfferingInstPropertyResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryOfferingInstPropertyResponse");
    private final static QName _CreateAccount_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "createAccount");
    private final static QName _ChangeOfferingPropertyResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeOfferingPropertyResponse");
    private final static QName _CreateGroup_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "createGroup");
    private final static QName _ChangeSubOwnershipResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubOwnershipResponse");
    private final static QName _ChangePayRelation_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changePayRelation");
    private final static QName _QueryCustomerInfoResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryCustomerInfoResponse");
    private final static QName _QueryGroupMemberListResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryGroupMemberListResponse");
    private final static QName _AcctDeactivation_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "acctDeactivation");
    private final static QName _ChangeSubPaymentModeResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubPaymentModeResponse");
    private final static QName _BatchSubDeactivation_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "batchSubDeactivation");
    private final static QName _ChangeSubIdentityResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubIdentityResponse");
    private final static QName _QuerySubscribedOfferings_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "querySubscribedOfferings");
    private final static QName _QueryCustomerInfo_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryCustomerInfo");
    private final static QName _ChangeSubGrpDFTAcctResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubGrpDFTAcctResponse");
    private final static QName _GroupDeactivation_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "groupDeactivation");
    private final static QName _ChangeSubInfo_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubInfo");
    private final static QName _ChangeCustInfo_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeCustInfo");
    private final static QName _ChangeAcctInfo_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeAcctInfo");
    private final static QName _CreateGroupResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "createGroupResponse");
    private final static QName _BatchSubDeactivationResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "batchSubDeactivationResponse");
    private final static QName _ChangeGroupOffering_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeGroupOffering");
    private final static QName _ChangeSubOwnership_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubOwnership");
    private final static QName _QueryGroupListBySubscriberResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "queryGroupListBySubscriberResponse");
    private final static QName _AcctDeactivationResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "acctDeactivationResponse");
    private final static QName _ChangeSubDFTA_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeSubDFTA");
    private final static QName _ChangeAcctBillCycleResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "changeAcctBillCycleResponse");
    private final static QName _CreateAccountResponse_QNAME = new QName("http://ws.web.gatewaycbs.tigo.com.co/", "createAccountResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.com.tigo.gatewaycbs.web.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateGroupResponse }
     * 
     */
    public CreateGroupResponse createCreateGroupResponse() {
        return new CreateGroupResponse();
    }

    /**
     * Create an instance of {@link BatchSubDeactivationResponse }
     * 
     */
    public BatchSubDeactivationResponse createBatchSubDeactivationResponse() {
        return new BatchSubDeactivationResponse();
    }

    /**
     * Create an instance of {@link QueryGroupListBySubscriberResponse }
     * 
     */
    public QueryGroupListBySubscriberResponse createQueryGroupListBySubscriberResponse() {
        return new QueryGroupListBySubscriberResponse();
    }

    /**
     * Create an instance of {@link AcctDeactivationResponse }
     * 
     */
    public AcctDeactivationResponse createAcctDeactivationResponse() {
        return new AcctDeactivationResponse();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycleResponse }
     * 
     */
    public ChangeAcctBillCycleResponse createChangeAcctBillCycleResponse() {
        return new ChangeAcctBillCycleResponse();
    }

    /**
     * Create an instance of {@link CreateAccountResponse }
     * 
     */
    public CreateAccountResponse createCreateAccountResponse() {
        return new CreateAccountResponse();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipResponse }
     * 
     */
    public ChangeSubOwnershipResponse createChangeSubOwnershipResponse() {
        return new ChangeSubOwnershipResponse();
    }

    /**
     * Create an instance of {@link QueryGroupMemberListResponse }
     * 
     */
    public QueryGroupMemberListResponse createQueryGroupMemberListResponse() {
        return new QueryGroupMemberListResponse();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResponse }
     * 
     */
    public QueryCustomerInfoResponse createQueryCustomerInfoResponse() {
        return new QueryCustomerInfoResponse();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeResponse }
     * 
     */
    public ChangeSubPaymentModeResponse createChangeSubPaymentModeResponse() {
        return new ChangeSubPaymentModeResponse();
    }

    /**
     * Create an instance of {@link ChangeSubIdentityResponse }
     * 
     */
    public ChangeSubIdentityResponse createChangeSubIdentityResponse() {
        return new ChangeSubIdentityResponse();
    }

    /**
     * Create an instance of {@link ChangeSubGrpDFTAcctResponse }
     * 
     */
    public ChangeSubGrpDFTAcctResponse createChangeSubGrpDFTAcctResponse() {
        return new ChangeSubGrpDFTAcctResponse();
    }

    /**
     * Create an instance of {@link AddGroupMemberResponse }
     * 
     */
    public AddGroupMemberResponse createAddGroupMemberResponse() {
        return new AddGroupMemberResponse();
    }

    /**
     * Create an instance of {@link GroupDeactivationResponse }
     * 
     */
    public GroupDeactivationResponse createGroupDeactivationResponse() {
        return new GroupDeactivationResponse();
    }

    /**
     * Create an instance of {@link ChangePayRelationResponse }
     * 
     */
    public ChangePayRelationResponse createChangePayRelationResponse() {
        return new ChangePayRelationResponse();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAResponse }
     * 
     */
    public ChangeSubDFTAResponse createChangeSubDFTAResponse() {
        return new ChangeSubDFTAResponse();
    }

    /**
     * Create an instance of {@link CreateSubscriberResponse }
     * 
     */
    public CreateSubscriberResponse createCreateSubscriberResponse() {
        return new CreateSubscriberResponse();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResponse }
     * 
     */
    public QueryPaymentRelationResponse createQueryPaymentRelationResponse() {
        return new QueryPaymentRelationResponse();
    }

    /**
     * Create an instance of {@link DelGroupMemberResponse }
     * 
     */
    public DelGroupMemberResponse createDelGroupMemberResponse() {
        return new DelGroupMemberResponse();
    }

    /**
     * Create an instance of {@link SubDeactivationResponse }
     * 
     */
    public SubDeactivationResponse createSubDeactivationResponse() {
        return new SubDeactivationResponse();
    }

    /**
     * Create an instance of {@link ChangeCustInfoResponse }
     * 
     */
    public ChangeCustInfoResponse createChangeCustInfoResponse() {
        return new ChangeCustInfoResponse();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyResponse }
     * 
     */
    public QueryOfferingInstPropertyResponse createQueryOfferingInstPropertyResponse() {
        return new QueryOfferingInstPropertyResponse();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyResponse }
     * 
     */
    public ChangeOfferingPropertyResponse createChangeOfferingPropertyResponse() {
        return new ChangeOfferingPropertyResponse();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoResponse }
     * 
     */
    public ChangeAcctInfoResponse createChangeAcctInfoResponse() {
        return new ChangeAcctInfoResponse();
    }

    /**
     * Create an instance of {@link ChangeRscRelationResponse }
     * 
     */
    public ChangeRscRelationResponse createChangeRscRelationResponse() {
        return new ChangeRscRelationResponse();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingResponse }
     * 
     */
    public ChangeGroupOfferingResponse createChangeGroupOfferingResponse() {
        return new ChangeGroupOfferingResponse();
    }

    /**
     * Create an instance of {@link QuerySubscribedOfferingsResponse }
     * 
     */
    public QuerySubscribedOfferingsResponse createQuerySubscribedOfferingsResponse() {
        return new QuerySubscribedOfferingsResponse();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingResponse }
     * 
     */
    public ChangeSubOfferingResponse createChangeSubOfferingResponse() {
        return new ChangeSubOfferingResponse();
    }

    /**
     * Create an instance of {@link ChangeSubInfoResponse }
     * 
     */
    public ChangeSubInfoResponse createChangeSubInfoResponse() {
        return new ChangeSubInfoResponse();
    }

    /**
     * Create an instance of {@link QueryGroupListBySubscriber }
     * 
     */
    public QueryGroupListBySubscriber createQueryGroupListBySubscriber() {
        return new QueryGroupListBySubscriber();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycle }
     * 
     */
    public ChangeAcctBillCycle createChangeAcctBillCycle() {
        return new ChangeAcctBillCycle();
    }

    /**
     * Create an instance of {@link QueryPaymentRelation }
     * 
     */
    public QueryPaymentRelation createQueryPaymentRelation() {
        return new QueryPaymentRelation();
    }

    /**
     * Create an instance of {@link ChangeOfferingProperty }
     * 
     */
    public ChangeOfferingProperty createChangeOfferingProperty() {
        return new ChangeOfferingProperty();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentMode }
     * 
     */
    public ChangeSubPaymentMode createChangeSubPaymentMode() {
        return new ChangeSubPaymentMode();
    }

    /**
     * Create an instance of {@link ChangeSubIdentity }
     * 
     */
    public ChangeSubIdentity createChangeSubIdentity() {
        return new ChangeSubIdentity();
    }

    /**
     * Create an instance of {@link SubDeactivation }
     * 
     */
    public SubDeactivation createSubDeactivation() {
        return new SubDeactivation();
    }

    /**
     * Create an instance of {@link GatewayCbsWSException }
     * 
     */
    public GatewayCbsWSException createGatewayCbsWSException() {
        return new GatewayCbsWSException();
    }

    /**
     * Create an instance of {@link CreateSubscriber }
     * 
     */
    public CreateSubscriber createCreateSubscriber() {
        return new CreateSubscriber();
    }

    /**
     * Create an instance of {@link CreateGroup }
     * 
     */
    public CreateGroup createCreateGroup() {
        return new CreateGroup();
    }

    /**
     * Create an instance of {@link CreateAccount }
     * 
     */
    public CreateAccount createCreateAccount() {
        return new CreateAccount();
    }

    /**
     * Create an instance of {@link AddGroupMember }
     * 
     */
    public AddGroupMember createAddGroupMember() {
        return new AddGroupMember();
    }

    /**
     * Create an instance of {@link DelGroupMember }
     * 
     */
    public DelGroupMember createDelGroupMember() {
        return new DelGroupMember();
    }

    /**
     * Create an instance of {@link QueryOfferingInstProperty }
     * 
     */
    public QueryOfferingInstProperty createQueryOfferingInstProperty() {
        return new QueryOfferingInstProperty();
    }

    /**
     * Create an instance of {@link QueryGroupMemberList }
     * 
     */
    public QueryGroupMemberList createQueryGroupMemberList() {
        return new QueryGroupMemberList();
    }

    /**
     * Create an instance of {@link ChangeSubOffering }
     * 
     */
    public ChangeSubOffering createChangeSubOffering() {
        return new ChangeSubOffering();
    }

    /**
     * Create an instance of {@link ChangeSubGrpDFTAcct }
     * 
     */
    public ChangeSubGrpDFTAcct createChangeSubGrpDFTAcct() {
        return new ChangeSubGrpDFTAcct();
    }

    /**
     * Create an instance of {@link ChangeRscRelation }
     * 
     */
    public ChangeRscRelation createChangeRscRelation() {
        return new ChangeRscRelation();
    }

    /**
     * Create an instance of {@link GroupDeactivation }
     * 
     */
    public GroupDeactivation createGroupDeactivation() {
        return new GroupDeactivation();
    }

    /**
     * Create an instance of {@link QueryCustomerInfo }
     * 
     */
    public QueryCustomerInfo createQueryCustomerInfo() {
        return new QueryCustomerInfo();
    }

    /**
     * Create an instance of {@link QuerySubscribedOfferings }
     * 
     */
    public QuerySubscribedOfferings createQuerySubscribedOfferings() {
        return new QuerySubscribedOfferings();
    }

    /**
     * Create an instance of {@link AcctDeactivation }
     * 
     */
    public AcctDeactivation createAcctDeactivation() {
        return new AcctDeactivation();
    }

    /**
     * Create an instance of {@link BatchSubDeactivation }
     * 
     */
    public BatchSubDeactivation createBatchSubDeactivation() {
        return new BatchSubDeactivation();
    }

    /**
     * Create an instance of {@link ChangePayRelation }
     * 
     */
    public ChangePayRelation createChangePayRelation() {
        return new ChangePayRelation();
    }

    /**
     * Create an instance of {@link ChangeSubDFTA }
     * 
     */
    public ChangeSubDFTA createChangeSubDFTA() {
        return new ChangeSubDFTA();
    }

    /**
     * Create an instance of {@link ChangeSubOwnership }
     * 
     */
    public ChangeSubOwnership createChangeSubOwnership() {
        return new ChangeSubOwnership();
    }

    /**
     * Create an instance of {@link ChangeGroupOffering }
     * 
     */
    public ChangeGroupOffering createChangeGroupOffering() {
        return new ChangeGroupOffering();
    }

    /**
     * Create an instance of {@link ChangeCustInfo }
     * 
     */
    public ChangeCustInfo createChangeCustInfo() {
        return new ChangeCustInfo();
    }

    /**
     * Create an instance of {@link ChangeAcctInfo }
     * 
     */
    public ChangeAcctInfo createChangeAcctInfo() {
        return new ChangeAcctInfo();
    }

    /**
     * Create an instance of {@link ChangeSubInfo }
     * 
     */
    public ChangeSubInfo createChangeSubInfo() {
        return new ChangeSubInfo();
    }

    /**
     * Create an instance of {@link CreateGroupResponse.Return }
     * 
     */
    public CreateGroupResponse.Return createCreateGroupResponseReturn() {
        return new CreateGroupResponse.Return();
    }

    /**
     * Create an instance of {@link BatchSubDeactivationResponse.Return }
     * 
     */
    public BatchSubDeactivationResponse.Return createBatchSubDeactivationResponseReturn() {
        return new BatchSubDeactivationResponse.Return();
    }

    /**
     * Create an instance of {@link QueryGroupListBySubscriberResponse.Return }
     * 
     */
    public QueryGroupListBySubscriberResponse.Return createQueryGroupListBySubscriberResponseReturn() {
        return new QueryGroupListBySubscriberResponse.Return();
    }

    /**
     * Create an instance of {@link AcctDeactivationResponse.Return }
     * 
     */
    public AcctDeactivationResponse.Return createAcctDeactivationResponseReturn() {
        return new AcctDeactivationResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeAcctBillCycleResponse.Return }
     * 
     */
    public ChangeAcctBillCycleResponse.Return createChangeAcctBillCycleResponseReturn() {
        return new ChangeAcctBillCycleResponse.Return();
    }

    /**
     * Create an instance of {@link CreateAccountResponse.Return }
     * 
     */
    public CreateAccountResponse.Return createCreateAccountResponseReturn() {
        return new CreateAccountResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeSubOwnershipResponse.Return }
     * 
     */
    public ChangeSubOwnershipResponse.Return createChangeSubOwnershipResponseReturn() {
        return new ChangeSubOwnershipResponse.Return();
    }

    /**
     * Create an instance of {@link QueryGroupMemberListResponse.Return }
     * 
     */
    public QueryGroupMemberListResponse.Return createQueryGroupMemberListResponseReturn() {
        return new QueryGroupMemberListResponse.Return();
    }

    /**
     * Create an instance of {@link QueryCustomerInfoResponse.Return }
     * 
     */
    public QueryCustomerInfoResponse.Return createQueryCustomerInfoResponseReturn() {
        return new QueryCustomerInfoResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeSubPaymentModeResponse.Return }
     * 
     */
    public ChangeSubPaymentModeResponse.Return createChangeSubPaymentModeResponseReturn() {
        return new ChangeSubPaymentModeResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeSubIdentityResponse.Return }
     * 
     */
    public ChangeSubIdentityResponse.Return createChangeSubIdentityResponseReturn() {
        return new ChangeSubIdentityResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeSubGrpDFTAcctResponse.Return }
     * 
     */
    public ChangeSubGrpDFTAcctResponse.Return createChangeSubGrpDFTAcctResponseReturn() {
        return new ChangeSubGrpDFTAcctResponse.Return();
    }

    /**
     * Create an instance of {@link AddGroupMemberResponse.Return }
     * 
     */
    public AddGroupMemberResponse.Return createAddGroupMemberResponseReturn() {
        return new AddGroupMemberResponse.Return();
    }

    /**
     * Create an instance of {@link GroupDeactivationResponse.Return }
     * 
     */
    public GroupDeactivationResponse.Return createGroupDeactivationResponseReturn() {
        return new GroupDeactivationResponse.Return();
    }

    /**
     * Create an instance of {@link ChangePayRelationResponse.Return }
     * 
     */
    public ChangePayRelationResponse.Return createChangePayRelationResponseReturn() {
        return new ChangePayRelationResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeSubDFTAResponse.Return }
     * 
     */
    public ChangeSubDFTAResponse.Return createChangeSubDFTAResponseReturn() {
        return new ChangeSubDFTAResponse.Return();
    }

    /**
     * Create an instance of {@link CreateSubscriberResponse.Return }
     * 
     */
    public CreateSubscriberResponse.Return createCreateSubscriberResponseReturn() {
        return new CreateSubscriberResponse.Return();
    }

    /**
     * Create an instance of {@link QueryPaymentRelationResponse.Return }
     * 
     */
    public QueryPaymentRelationResponse.Return createQueryPaymentRelationResponseReturn() {
        return new QueryPaymentRelationResponse.Return();
    }

    /**
     * Create an instance of {@link DelGroupMemberResponse.Return }
     * 
     */
    public DelGroupMemberResponse.Return createDelGroupMemberResponseReturn() {
        return new DelGroupMemberResponse.Return();
    }

    /**
     * Create an instance of {@link SubDeactivationResponse.Return }
     * 
     */
    public SubDeactivationResponse.Return createSubDeactivationResponseReturn() {
        return new SubDeactivationResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeCustInfoResponse.Return }
     * 
     */
    public ChangeCustInfoResponse.Return createChangeCustInfoResponseReturn() {
        return new ChangeCustInfoResponse.Return();
    }

    /**
     * Create an instance of {@link QueryOfferingInstPropertyResponse.Return }
     * 
     */
    public QueryOfferingInstPropertyResponse.Return createQueryOfferingInstPropertyResponseReturn() {
        return new QueryOfferingInstPropertyResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeOfferingPropertyResponse.Return }
     * 
     */
    public ChangeOfferingPropertyResponse.Return createChangeOfferingPropertyResponseReturn() {
        return new ChangeOfferingPropertyResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeAcctInfoResponse.Return }
     * 
     */
    public ChangeAcctInfoResponse.Return createChangeAcctInfoResponseReturn() {
        return new ChangeAcctInfoResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeRscRelationResponse.Return }
     * 
     */
    public ChangeRscRelationResponse.Return createChangeRscRelationResponseReturn() {
        return new ChangeRscRelationResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeGroupOfferingResponse.Return }
     * 
     */
    public ChangeGroupOfferingResponse.Return createChangeGroupOfferingResponseReturn() {
        return new ChangeGroupOfferingResponse.Return();
    }

    /**
     * Create an instance of {@link QuerySubscribedOfferingsResponse.Return }
     * 
     */
    public QuerySubscribedOfferingsResponse.Return createQuerySubscribedOfferingsResponseReturn() {
        return new QuerySubscribedOfferingsResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeSubOfferingResponse.Return }
     * 
     */
    public ChangeSubOfferingResponse.Return createChangeSubOfferingResponseReturn() {
        return new ChangeSubOfferingResponse.Return();
    }

    /**
     * Create an instance of {@link ChangeSubInfoResponse.Return }
     * 
     */
    public ChangeSubInfoResponse.Return createChangeSubInfoResponseReturn() {
        return new ChangeSubInfoResponse.Return();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GatewayCbsWSException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "GatewayCbsWSException")
    public JAXBElement<GatewayCbsWSException> createGatewayCbsWSException(GatewayCbsWSException value) {
        return new JAXBElement<GatewayCbsWSException>(_GatewayCbsWSException_QNAME, GatewayCbsWSException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSubscriber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "createSubscriber")
    public JAXBElement<CreateSubscriber> createCreateSubscriber(CreateSubscriber value) {
        return new JAXBElement<CreateSubscriber>(_CreateSubscriber_QNAME, CreateSubscriber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeAcctInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeAcctInfoResponse")
    public JAXBElement<ChangeAcctInfoResponse> createChangeAcctInfoResponse(ChangeAcctInfoResponse value) {
        return new JAXBElement<ChangeAcctInfoResponse>(_ChangeAcctInfoResponse_QNAME, ChangeAcctInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubDeactivation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "subDeactivation")
    public JAXBElement<SubDeactivation> createSubDeactivation(SubDeactivation value) {
        return new JAXBElement<SubDeactivation>(_SubDeactivation_QNAME, SubDeactivation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubIdentity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubIdentity")
    public JAXBElement<ChangeSubIdentity> createChangeSubIdentity(ChangeSubIdentity value) {
        return new JAXBElement<ChangeSubIdentity>(_ChangeSubIdentity_QNAME, ChangeSubIdentity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeRscRelationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeRscRelationResponse")
    public JAXBElement<ChangeRscRelationResponse> createChangeRscRelationResponse(ChangeRscRelationResponse value) {
        return new JAXBElement<ChangeRscRelationResponse>(_ChangeRscRelationResponse_QNAME, ChangeRscRelationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubPaymentMode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubPaymentMode")
    public JAXBElement<ChangeSubPaymentMode> createChangeSubPaymentMode(ChangeSubPaymentMode value) {
        return new JAXBElement<ChangeSubPaymentMode>(_ChangeSubPaymentMode_QNAME, ChangeSubPaymentMode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeOfferingProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeOfferingProperty")
    public JAXBElement<ChangeOfferingProperty> createChangeOfferingProperty(ChangeOfferingProperty value) {
        return new JAXBElement<ChangeOfferingProperty>(_ChangeOfferingProperty_QNAME, ChangeOfferingProperty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeGroupOfferingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeGroupOfferingResponse")
    public JAXBElement<ChangeGroupOfferingResponse> createChangeGroupOfferingResponse(ChangeGroupOfferingResponse value) {
        return new JAXBElement<ChangeGroupOfferingResponse>(_ChangeGroupOfferingResponse_QNAME, ChangeGroupOfferingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuerySubscribedOfferingsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "querySubscribedOfferingsResponse")
    public JAXBElement<QuerySubscribedOfferingsResponse> createQuerySubscribedOfferingsResponse(QuerySubscribedOfferingsResponse value) {
        return new JAXBElement<QuerySubscribedOfferingsResponse>(_QuerySubscribedOfferingsResponse_QNAME, QuerySubscribedOfferingsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeAcctBillCycle }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeAcctBillCycle")
    public JAXBElement<ChangeAcctBillCycle> createChangeAcctBillCycle(ChangeAcctBillCycle value) {
        return new JAXBElement<ChangeAcctBillCycle>(_ChangeAcctBillCycle_QNAME, ChangeAcctBillCycle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryPaymentRelation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryPaymentRelation")
    public JAXBElement<QueryPaymentRelation> createQueryPaymentRelation(QueryPaymentRelation value) {
        return new JAXBElement<QueryPaymentRelation>(_QueryPaymentRelation_QNAME, QueryPaymentRelation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubInfoResponse")
    public JAXBElement<ChangeSubInfoResponse> createChangeSubInfoResponse(ChangeSubInfoResponse value) {
        return new JAXBElement<ChangeSubInfoResponse>(_ChangeSubInfoResponse_QNAME, ChangeSubInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubOfferingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubOfferingResponse")
    public JAXBElement<ChangeSubOfferingResponse> createChangeSubOfferingResponse(ChangeSubOfferingResponse value) {
        return new JAXBElement<ChangeSubOfferingResponse>(_ChangeSubOfferingResponse_QNAME, ChangeSubOfferingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryGroupListBySubscriber }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryGroupListBySubscriber")
    public JAXBElement<QueryGroupListBySubscriber> createQueryGroupListBySubscriber(QueryGroupListBySubscriber value) {
        return new JAXBElement<QueryGroupListBySubscriber>(_QueryGroupListBySubscriber_QNAME, QueryGroupListBySubscriber.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddGroupMemberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "addGroupMemberResponse")
    public JAXBElement<AddGroupMemberResponse> createAddGroupMemberResponse(AddGroupMemberResponse value) {
        return new JAXBElement<AddGroupMemberResponse>(_AddGroupMemberResponse_QNAME, AddGroupMemberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupDeactivationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "groupDeactivationResponse")
    public JAXBElement<GroupDeactivationResponse> createGroupDeactivationResponse(GroupDeactivationResponse value) {
        return new JAXBElement<GroupDeactivationResponse>(_GroupDeactivationResponse_QNAME, GroupDeactivationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeRscRelation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeRscRelation")
    public JAXBElement<ChangeRscRelation> createChangeRscRelation(ChangeRscRelation value) {
        return new JAXBElement<ChangeRscRelation>(_ChangeRscRelation_QNAME, ChangeRscRelation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePayRelationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changePayRelationResponse")
    public JAXBElement<ChangePayRelationResponse> createChangePayRelationResponse(ChangePayRelationResponse value) {
        return new JAXBElement<ChangePayRelationResponse>(_ChangePayRelationResponse_QNAME, ChangePayRelationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubGrpDFTAcct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubGrpDFTAcct")
    public JAXBElement<ChangeSubGrpDFTAcct> createChangeSubGrpDFTAcct(ChangeSubGrpDFTAcct value) {
        return new JAXBElement<ChangeSubGrpDFTAcct>(_ChangeSubGrpDFTAcct_QNAME, ChangeSubGrpDFTAcct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubDFTAResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubDFTAResponse")
    public JAXBElement<ChangeSubDFTAResponse> createChangeSubDFTAResponse(ChangeSubDFTAResponse value) {
        return new JAXBElement<ChangeSubDFTAResponse>(_ChangeSubDFTAResponse_QNAME, ChangeSubDFTAResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "createSubscriberResponse")
    public JAXBElement<CreateSubscriberResponse> createCreateSubscriberResponse(CreateSubscriberResponse value) {
        return new JAXBElement<CreateSubscriberResponse>(_CreateSubscriberResponse_QNAME, CreateSubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryGroupMemberList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryGroupMemberList")
    public JAXBElement<QueryGroupMemberList> createQueryGroupMemberList(QueryGroupMemberList value) {
        return new JAXBElement<QueryGroupMemberList>(_QueryGroupMemberList_QNAME, QueryGroupMemberList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubOffering }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubOffering")
    public JAXBElement<ChangeSubOffering> createChangeSubOffering(ChangeSubOffering value) {
        return new JAXBElement<ChangeSubOffering>(_ChangeSubOffering_QNAME, ChangeSubOffering.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DelGroupMemberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "delGroupMemberResponse")
    public JAXBElement<DelGroupMemberResponse> createDelGroupMemberResponse(DelGroupMemberResponse value) {
        return new JAXBElement<DelGroupMemberResponse>(_DelGroupMemberResponse_QNAME, DelGroupMemberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryPaymentRelationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryPaymentRelationResponse")
    public JAXBElement<QueryPaymentRelationResponse> createQueryPaymentRelationResponse(QueryPaymentRelationResponse value) {
        return new JAXBElement<QueryPaymentRelationResponse>(_QueryPaymentRelationResponse_QNAME, QueryPaymentRelationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DelGroupMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "delGroupMember")
    public JAXBElement<DelGroupMember> createDelGroupMember(DelGroupMember value) {
        return new JAXBElement<DelGroupMember>(_DelGroupMember_QNAME, DelGroupMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryOfferingInstProperty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryOfferingInstProperty")
    public JAXBElement<QueryOfferingInstProperty> createQueryOfferingInstProperty(QueryOfferingInstProperty value) {
        return new JAXBElement<QueryOfferingInstProperty>(_QueryOfferingInstProperty_QNAME, QueryOfferingInstProperty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCustInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeCustInfoResponse")
    public JAXBElement<ChangeCustInfoResponse> createChangeCustInfoResponse(ChangeCustInfoResponse value) {
        return new JAXBElement<ChangeCustInfoResponse>(_ChangeCustInfoResponse_QNAME, ChangeCustInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubDeactivationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "subDeactivationResponse")
    public JAXBElement<SubDeactivationResponse> createSubDeactivationResponse(SubDeactivationResponse value) {
        return new JAXBElement<SubDeactivationResponse>(_SubDeactivationResponse_QNAME, SubDeactivationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddGroupMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "addGroupMember")
    public JAXBElement<AddGroupMember> createAddGroupMember(AddGroupMember value) {
        return new JAXBElement<AddGroupMember>(_AddGroupMember_QNAME, AddGroupMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryOfferingInstPropertyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryOfferingInstPropertyResponse")
    public JAXBElement<QueryOfferingInstPropertyResponse> createQueryOfferingInstPropertyResponse(QueryOfferingInstPropertyResponse value) {
        return new JAXBElement<QueryOfferingInstPropertyResponse>(_QueryOfferingInstPropertyResponse_QNAME, QueryOfferingInstPropertyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAccount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "createAccount")
    public JAXBElement<CreateAccount> createCreateAccount(CreateAccount value) {
        return new JAXBElement<CreateAccount>(_CreateAccount_QNAME, CreateAccount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeOfferingPropertyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeOfferingPropertyResponse")
    public JAXBElement<ChangeOfferingPropertyResponse> createChangeOfferingPropertyResponse(ChangeOfferingPropertyResponse value) {
        return new JAXBElement<ChangeOfferingPropertyResponse>(_ChangeOfferingPropertyResponse_QNAME, ChangeOfferingPropertyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateGroup }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "createGroup")
    public JAXBElement<CreateGroup> createCreateGroup(CreateGroup value) {
        return new JAXBElement<CreateGroup>(_CreateGroup_QNAME, CreateGroup.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubOwnershipResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubOwnershipResponse")
    public JAXBElement<ChangeSubOwnershipResponse> createChangeSubOwnershipResponse(ChangeSubOwnershipResponse value) {
        return new JAXBElement<ChangeSubOwnershipResponse>(_ChangeSubOwnershipResponse_QNAME, ChangeSubOwnershipResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePayRelation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changePayRelation")
    public JAXBElement<ChangePayRelation> createChangePayRelation(ChangePayRelation value) {
        return new JAXBElement<ChangePayRelation>(_ChangePayRelation_QNAME, ChangePayRelation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryCustomerInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryCustomerInfoResponse")
    public JAXBElement<QueryCustomerInfoResponse> createQueryCustomerInfoResponse(QueryCustomerInfoResponse value) {
        return new JAXBElement<QueryCustomerInfoResponse>(_QueryCustomerInfoResponse_QNAME, QueryCustomerInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryGroupMemberListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryGroupMemberListResponse")
    public JAXBElement<QueryGroupMemberListResponse> createQueryGroupMemberListResponse(QueryGroupMemberListResponse value) {
        return new JAXBElement<QueryGroupMemberListResponse>(_QueryGroupMemberListResponse_QNAME, QueryGroupMemberListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcctDeactivation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "acctDeactivation")
    public JAXBElement<AcctDeactivation> createAcctDeactivation(AcctDeactivation value) {
        return new JAXBElement<AcctDeactivation>(_AcctDeactivation_QNAME, AcctDeactivation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubPaymentModeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubPaymentModeResponse")
    public JAXBElement<ChangeSubPaymentModeResponse> createChangeSubPaymentModeResponse(ChangeSubPaymentModeResponse value) {
        return new JAXBElement<ChangeSubPaymentModeResponse>(_ChangeSubPaymentModeResponse_QNAME, ChangeSubPaymentModeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BatchSubDeactivation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "batchSubDeactivation")
    public JAXBElement<BatchSubDeactivation> createBatchSubDeactivation(BatchSubDeactivation value) {
        return new JAXBElement<BatchSubDeactivation>(_BatchSubDeactivation_QNAME, BatchSubDeactivation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubIdentityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubIdentityResponse")
    public JAXBElement<ChangeSubIdentityResponse> createChangeSubIdentityResponse(ChangeSubIdentityResponse value) {
        return new JAXBElement<ChangeSubIdentityResponse>(_ChangeSubIdentityResponse_QNAME, ChangeSubIdentityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuerySubscribedOfferings }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "querySubscribedOfferings")
    public JAXBElement<QuerySubscribedOfferings> createQuerySubscribedOfferings(QuerySubscribedOfferings value) {
        return new JAXBElement<QuerySubscribedOfferings>(_QuerySubscribedOfferings_QNAME, QuerySubscribedOfferings.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryCustomerInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryCustomerInfo")
    public JAXBElement<QueryCustomerInfo> createQueryCustomerInfo(QueryCustomerInfo value) {
        return new JAXBElement<QueryCustomerInfo>(_QueryCustomerInfo_QNAME, QueryCustomerInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubGrpDFTAcctResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubGrpDFTAcctResponse")
    public JAXBElement<ChangeSubGrpDFTAcctResponse> createChangeSubGrpDFTAcctResponse(ChangeSubGrpDFTAcctResponse value) {
        return new JAXBElement<ChangeSubGrpDFTAcctResponse>(_ChangeSubGrpDFTAcctResponse_QNAME, ChangeSubGrpDFTAcctResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupDeactivation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "groupDeactivation")
    public JAXBElement<GroupDeactivation> createGroupDeactivation(GroupDeactivation value) {
        return new JAXBElement<GroupDeactivation>(_GroupDeactivation_QNAME, GroupDeactivation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubInfo")
    public JAXBElement<ChangeSubInfo> createChangeSubInfo(ChangeSubInfo value) {
        return new JAXBElement<ChangeSubInfo>(_ChangeSubInfo_QNAME, ChangeSubInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeCustInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeCustInfo")
    public JAXBElement<ChangeCustInfo> createChangeCustInfo(ChangeCustInfo value) {
        return new JAXBElement<ChangeCustInfo>(_ChangeCustInfo_QNAME, ChangeCustInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeAcctInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeAcctInfo")
    public JAXBElement<ChangeAcctInfo> createChangeAcctInfo(ChangeAcctInfo value) {
        return new JAXBElement<ChangeAcctInfo>(_ChangeAcctInfo_QNAME, ChangeAcctInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "createGroupResponse")
    public JAXBElement<CreateGroupResponse> createCreateGroupResponse(CreateGroupResponse value) {
        return new JAXBElement<CreateGroupResponse>(_CreateGroupResponse_QNAME, CreateGroupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BatchSubDeactivationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "batchSubDeactivationResponse")
    public JAXBElement<BatchSubDeactivationResponse> createBatchSubDeactivationResponse(BatchSubDeactivationResponse value) {
        return new JAXBElement<BatchSubDeactivationResponse>(_BatchSubDeactivationResponse_QNAME, BatchSubDeactivationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeGroupOffering }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeGroupOffering")
    public JAXBElement<ChangeGroupOffering> createChangeGroupOffering(ChangeGroupOffering value) {
        return new JAXBElement<ChangeGroupOffering>(_ChangeGroupOffering_QNAME, ChangeGroupOffering.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubOwnership }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubOwnership")
    public JAXBElement<ChangeSubOwnership> createChangeSubOwnership(ChangeSubOwnership value) {
        return new JAXBElement<ChangeSubOwnership>(_ChangeSubOwnership_QNAME, ChangeSubOwnership.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryGroupListBySubscriberResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "queryGroupListBySubscriberResponse")
    public JAXBElement<QueryGroupListBySubscriberResponse> createQueryGroupListBySubscriberResponse(QueryGroupListBySubscriberResponse value) {
        return new JAXBElement<QueryGroupListBySubscriberResponse>(_QueryGroupListBySubscriberResponse_QNAME, QueryGroupListBySubscriberResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcctDeactivationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "acctDeactivationResponse")
    public JAXBElement<AcctDeactivationResponse> createAcctDeactivationResponse(AcctDeactivationResponse value) {
        return new JAXBElement<AcctDeactivationResponse>(_AcctDeactivationResponse_QNAME, AcctDeactivationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeSubDFTA }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeSubDFTA")
    public JAXBElement<ChangeSubDFTA> createChangeSubDFTA(ChangeSubDFTA value) {
        return new JAXBElement<ChangeSubDFTA>(_ChangeSubDFTA_QNAME, ChangeSubDFTA.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeAcctBillCycleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "changeAcctBillCycleResponse")
    public JAXBElement<ChangeAcctBillCycleResponse> createChangeAcctBillCycleResponse(ChangeAcctBillCycleResponse value) {
        return new JAXBElement<ChangeAcctBillCycleResponse>(_ChangeAcctBillCycleResponse_QNAME, ChangeAcctBillCycleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAccountResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.web.gatewaycbs.tigo.com.co/", name = "createAccountResponse")
    public JAXBElement<CreateAccountResponse> createCreateAccountResponse(CreateAccountResponse value) {
        return new JAXBElement<CreateAccountResponse>(_CreateAccountResponse_QNAME, CreateAccountResponse.class, null, value);
    }

}
