
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.GroupDeactivationRequest;


/**
 * <p>Clase Java para groupDeactivation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="groupDeactivation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupDeactivationRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}GroupDeactivationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "groupDeactivation", propOrder = {
    "groupDeactivationRequest"
})
public class GroupDeactivation {

    @XmlElement(name = "GroupDeactivationRequest")
    protected GroupDeactivationRequest groupDeactivationRequest;

    /**
     * Obtiene el valor de la propiedad groupDeactivationRequest.
     * 
     * @return
     *     possible object is
     *     {@link GroupDeactivationRequest }
     *     
     */
    public GroupDeactivationRequest getGroupDeactivationRequest() {
        return groupDeactivationRequest;
    }

    /**
     * Define el valor de la propiedad groupDeactivationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupDeactivationRequest }
     *     
     */
    public void setGroupDeactivationRequest(GroupDeactivationRequest value) {
        this.groupDeactivationRequest = value;
    }

}
