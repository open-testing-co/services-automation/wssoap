
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QueryCustomerInfoResult;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para queryCustomerInfoResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="queryCustomerInfoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
 *                   &lt;element name="QueryCustomerInfoResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryCustomerInfoResult"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryCustomerInfoResponse", propOrder = {
    "_return"
})
public class QueryCustomerInfoResponse {

    @XmlElement(name = "return")
    protected QueryCustomerInfoResponse.Return _return;

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link QueryCustomerInfoResponse.Return }
     *     
     */
    public QueryCustomerInfoResponse.Return getReturn() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryCustomerInfoResponse.Return }
     *     
     */
    public void setReturn(QueryCustomerInfoResponse.Return value) {
        this._return = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
     *         &lt;element name="QueryCustomerInfoResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryCustomerInfoResult"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultHeader",
        "queryCustomerInfoResult"
    })
    public static class Return {

        @XmlElement(name = "ResultHeader", required = true)
        protected ResultHeader resultHeader;
        @XmlElement(name = "QueryCustomerInfoResult", required = true)
        protected QueryCustomerInfoResult queryCustomerInfoResult;

        /**
         * Obtiene el valor de la propiedad resultHeader.
         * 
         * @return
         *     possible object is
         *     {@link ResultHeader }
         *     
         */
        public ResultHeader getResultHeader() {
            return resultHeader;
        }

        /**
         * Define el valor de la propiedad resultHeader.
         * 
         * @param value
         *     allowed object is
         *     {@link ResultHeader }
         *     
         */
        public void setResultHeader(ResultHeader value) {
            this.resultHeader = value;
        }

        /**
         * Obtiene el valor de la propiedad queryCustomerInfoResult.
         * 
         * @return
         *     possible object is
         *     {@link QueryCustomerInfoResult }
         *     
         */
        public QueryCustomerInfoResult getQueryCustomerInfoResult() {
            return queryCustomerInfoResult;
        }

        /**
         * Define el valor de la propiedad queryCustomerInfoResult.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryCustomerInfoResult }
         *     
         */
        public void setQueryCustomerInfoResult(QueryCustomerInfoResult value) {
            this.queryCustomerInfoResult = value;
        }

    }

}
