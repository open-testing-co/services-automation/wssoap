
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QueryGroupMemberListRequest;


/**
 * <p>Clase Java para queryGroupMemberList complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="queryGroupMemberList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryGroupMemberListRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryGroupMemberListRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryGroupMemberList", propOrder = {
    "queryGroupMemberListRequest"
})
public class QueryGroupMemberList {

    @XmlElement(name = "QueryGroupMemberListRequest")
    protected QueryGroupMemberListRequest queryGroupMemberListRequest;

    /**
     * Obtiene el valor de la propiedad queryGroupMemberListRequest.
     * 
     * @return
     *     possible object is
     *     {@link QueryGroupMemberListRequest }
     *     
     */
    public QueryGroupMemberListRequest getQueryGroupMemberListRequest() {
        return queryGroupMemberListRequest;
    }

    /**
     * Define el valor de la propiedad queryGroupMemberListRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryGroupMemberListRequest }
     *     
     */
    public void setQueryGroupMemberListRequest(QueryGroupMemberListRequest value) {
        this.queryGroupMemberListRequest = value;
    }

}
