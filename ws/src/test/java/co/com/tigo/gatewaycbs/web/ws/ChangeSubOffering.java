
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeSubOfferingRequest;


/**
 * <p>Clase Java para changeSubOffering complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeSubOffering">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeSubOfferingRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeSubOfferingRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeSubOffering", propOrder = {
    "changeSubOfferingRequest"
})
public class ChangeSubOffering {

    @XmlElement(name = "ChangeSubOfferingRequest")
    protected ChangeSubOfferingRequest changeSubOfferingRequest;

    /**
     * Obtiene el valor de la propiedad changeSubOfferingRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubOfferingRequest }
     *     
     */
    public ChangeSubOfferingRequest getChangeSubOfferingRequest() {
        return changeSubOfferingRequest;
    }

    /**
     * Define el valor de la propiedad changeSubOfferingRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubOfferingRequest }
     *     
     */
    public void setChangeSubOfferingRequest(ChangeSubOfferingRequest value) {
        this.changeSubOfferingRequest = value;
    }

}
