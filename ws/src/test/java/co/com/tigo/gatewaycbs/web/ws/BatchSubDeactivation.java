
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.BatchSubDeactivationRequest;


/**
 * <p>Clase Java para batchSubDeactivation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="batchSubDeactivation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BatchSubDeactivationRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}BatchSubDeactivationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "batchSubDeactivation", propOrder = {
    "batchSubDeactivationRequest"
})
public class BatchSubDeactivation {

    @XmlElement(name = "BatchSubDeactivationRequest")
    protected BatchSubDeactivationRequest batchSubDeactivationRequest;

    /**
     * Obtiene el valor de la propiedad batchSubDeactivationRequest.
     * 
     * @return
     *     possible object is
     *     {@link BatchSubDeactivationRequest }
     *     
     */
    public BatchSubDeactivationRequest getBatchSubDeactivationRequest() {
        return batchSubDeactivationRequest;
    }

    /**
     * Define el valor de la propiedad batchSubDeactivationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchSubDeactivationRequest }
     *     
     */
    public void setBatchSubDeactivationRequest(BatchSubDeactivationRequest value) {
        this.batchSubDeactivationRequest = value;
    }

}
