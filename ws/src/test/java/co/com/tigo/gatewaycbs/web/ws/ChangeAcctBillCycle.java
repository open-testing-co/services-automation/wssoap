
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeAcctBillCycleRequest;


/**
 * <p>Clase Java para changeAcctBillCycle complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeAcctBillCycle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeAcctBillCycleRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeAcctBillCycleRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeAcctBillCycle", propOrder = {
    "changeAcctBillCycleRequest"
})
public class ChangeAcctBillCycle {

    @XmlElement(name = "ChangeAcctBillCycleRequest")
    protected ChangeAcctBillCycleRequest changeAcctBillCycleRequest;

    /**
     * Obtiene el valor de la propiedad changeAcctBillCycleRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeAcctBillCycleRequest }
     *     
     */
    public ChangeAcctBillCycleRequest getChangeAcctBillCycleRequest() {
        return changeAcctBillCycleRequest;
    }

    /**
     * Define el valor de la propiedad changeAcctBillCycleRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeAcctBillCycleRequest }
     *     
     */
    public void setChangeAcctBillCycleRequest(ChangeAcctBillCycleRequest value) {
        this.changeAcctBillCycleRequest = value;
    }

}
