
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QueryGroupMemberListResult;
import com.huawei.bme.cbsinterface.cbscommon.ResultHeader;


/**
 * <p>Clase Java para queryGroupMemberListResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="queryGroupMemberListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
 *                   &lt;element name="QueryGroupMemberListResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryGroupMemberListResult"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryGroupMemberListResponse", propOrder = {
    "_return"
})
public class QueryGroupMemberListResponse {

    @XmlElement(name = "return")
    protected QueryGroupMemberListResponse.Return _return;

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link QueryGroupMemberListResponse.Return }
     *     
     */
    public QueryGroupMemberListResponse.Return getReturn() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryGroupMemberListResponse.Return }
     *     
     */
    public void setReturn(QueryGroupMemberListResponse.Return value) {
        this._return = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ResultHeader" type="{http://www.huawei.com/bme/cbsinterface/cbscommon}ResultHeader"/>
     *         &lt;element name="QueryGroupMemberListResult" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryGroupMemberListResult"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "resultHeader",
        "queryGroupMemberListResult"
    })
    public static class Return {

        @XmlElement(name = "ResultHeader", required = true)
        protected ResultHeader resultHeader;
        @XmlElement(name = "QueryGroupMemberListResult", required = true)
        protected QueryGroupMemberListResult queryGroupMemberListResult;

        /**
         * Obtiene el valor de la propiedad resultHeader.
         * 
         * @return
         *     possible object is
         *     {@link ResultHeader }
         *     
         */
        public ResultHeader getResultHeader() {
            return resultHeader;
        }

        /**
         * Define el valor de la propiedad resultHeader.
         * 
         * @param value
         *     allowed object is
         *     {@link ResultHeader }
         *     
         */
        public void setResultHeader(ResultHeader value) {
            this.resultHeader = value;
        }

        /**
         * Obtiene el valor de la propiedad queryGroupMemberListResult.
         * 
         * @return
         *     possible object is
         *     {@link QueryGroupMemberListResult }
         *     
         */
        public QueryGroupMemberListResult getQueryGroupMemberListResult() {
            return queryGroupMemberListResult;
        }

        /**
         * Define el valor de la propiedad queryGroupMemberListResult.
         * 
         * @param value
         *     allowed object is
         *     {@link QueryGroupMemberListResult }
         *     
         */
        public void setQueryGroupMemberListResult(QueryGroupMemberListResult value) {
            this.queryGroupMemberListResult = value;
        }

    }

}
