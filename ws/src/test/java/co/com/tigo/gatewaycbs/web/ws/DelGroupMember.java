
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.DelGroupMemberRequest;


/**
 * <p>Clase Java para delGroupMember complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="delGroupMember">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DelGroupMemberRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}DelGroupMemberRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "delGroupMember", propOrder = {
    "delGroupMemberRequest"
})
public class DelGroupMember {

    @XmlElement(name = "DelGroupMemberRequest")
    protected DelGroupMemberRequest delGroupMemberRequest;

    /**
     * Obtiene el valor de la propiedad delGroupMemberRequest.
     * 
     * @return
     *     possible object is
     *     {@link DelGroupMemberRequest }
     *     
     */
    public DelGroupMemberRequest getDelGroupMemberRequest() {
        return delGroupMemberRequest;
    }

    /**
     * Define el valor de la propiedad delGroupMemberRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link DelGroupMemberRequest }
     *     
     */
    public void setDelGroupMemberRequest(DelGroupMemberRequest value) {
        this.delGroupMemberRequest = value;
    }

}
