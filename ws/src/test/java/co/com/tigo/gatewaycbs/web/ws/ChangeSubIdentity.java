
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.ChangeSubIdentityRequest;


/**
 * <p>Clase Java para changeSubIdentity complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="changeSubIdentity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeSubIdentityRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}ChangeSubIdentityRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeSubIdentity", propOrder = {
    "changeSubIdentityRequest"
})
public class ChangeSubIdentity {

    @XmlElement(name = "ChangeSubIdentityRequest")
    protected ChangeSubIdentityRequest changeSubIdentityRequest;

    /**
     * Obtiene el valor de la propiedad changeSubIdentityRequest.
     * 
     * @return
     *     possible object is
     *     {@link ChangeSubIdentityRequest }
     *     
     */
    public ChangeSubIdentityRequest getChangeSubIdentityRequest() {
        return changeSubIdentityRequest;
    }

    /**
     * Define el valor de la propiedad changeSubIdentityRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeSubIdentityRequest }
     *     
     */
    public void setChangeSubIdentityRequest(ChangeSubIdentityRequest value) {
        this.changeSubIdentityRequest = value;
    }

}
