
package co.com.tigo.gatewaycbs.web.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.bme.cbsinterface.bcservices.QueryPaymentRelationRequest;


/**
 * <p>Clase Java para queryPaymentRelation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="queryPaymentRelation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryPaymentRelationRequest" type="{http://www.huawei.com/bme/cbsinterface/bcservices}QueryPaymentRelationRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "queryPaymentRelation", propOrder = {
    "queryPaymentRelationRequest"
})
public class QueryPaymentRelation {

    @XmlElement(name = "QueryPaymentRelationRequest")
    protected QueryPaymentRelationRequest queryPaymentRelationRequest;

    /**
     * Obtiene el valor de la propiedad queryPaymentRelationRequest.
     * 
     * @return
     *     possible object is
     *     {@link QueryPaymentRelationRequest }
     *     
     */
    public QueryPaymentRelationRequest getQueryPaymentRelationRequest() {
        return queryPaymentRelationRequest;
    }

    /**
     * Define el valor de la propiedad queryPaymentRelationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryPaymentRelationRequest }
     *     
     */
    public void setQueryPaymentRelationRequest(QueryPaymentRelationRequest value) {
        this.queryPaymentRelationRequest = value;
    }

}
